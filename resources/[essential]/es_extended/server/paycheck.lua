function TableHasValue(table, value)
  for k, v in pairs(table) do
    if v == value then
      return true
    end
  end
  return false
end

ESX.StartPayCheck = function()

  function payCheck()

    local xPlayers = ESX.GetPlayers()


    for i=1, #xPlayers, 1 do

      local xPlayer = ESX.GetPlayerFromId(xPlayers[i])

      if TableHasValue(Config.NeedInServiceToBePayed, xPlayer.job.name) then
        ESX.TriggerServerCallback('esx_service:isInService', 0, xPlayer.source, function(result)
          if xPlayer.job.grade_salary > 0 and result then
            xPlayer.addMoney(xPlayer.job.grade_salary)
            TriggerClientEvent('esx:showNotification', xPlayer.source, _U('rec_salary') .. '~g~$' .. xPlayer.job.grade_salary)
          end
        end, xPlayer.job.name)
      else
        if xPlayer.job.grade_salary > 0 then
          xPlayer.addMoney(xPlayer.job.grade_salary)

          if xPlayer.job.grade_name == 'interim' then
            TriggerClientEvent('esx:showNotification', xPlayer.source, _U('rec_help') .. '~g~$' .. xPlayer.job.grade_salary)
          else
            TriggerClientEvent('esx:showNotification', xPlayer.source, _U('rec_salary') .. '~g~$' .. xPlayer.job.grade_salary)
          end
        end
      end
    end
    SetTimeout(Config.PaycheckInterval, payCheck)
  end
  SetTimeout(Config.PaycheckInterval, payCheck)
end
