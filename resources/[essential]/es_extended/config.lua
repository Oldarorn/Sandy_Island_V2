Config                          = {}
Config.MaxPlayers               = 20
Config.Accounts                 = { 'bank', 'black_money' }
Config.AccountLabels            = { bank = 'Banque', black_money = 'Argent Sale' } -- French
-- Config.AccountLabels            = { bank = 'Bank', black_money = 'Dirty Money' } -- English
Config.PaycheckInterval         = 24 * 60000
Config.ShowDotAbovePlayer       = false
Config.DisableWantedLevel       = true
Config.RemoveInventoryItemDelay = 5000
Config.EnableWeaponPickup       = true
Config.Locale                   = 'fr'
Config.EnableDebug              = false
Config.NeedInServiceToBePayed   = {
	'police',
	'ambulance',
	'mecano',
	'sheriff',
	'taxi',
	'faimdus',
	'brinks',
	'brasseur',
	'theater'
}
