Config = {}

Config.WeedPlants = {
	{x = 2215.85, y = 5575.36, z = 53.69},
	{x = 2216.24, y = 5577.55, z = 53.78},
	{x = 2218.19, y = 5575.20, z = 53.70},
	{x = 2221.00, y = 5575.00, z = 53.71},
	{x = 2222.70, y = 5574.80, z = 53.73},
	{x = 2227.30, y = 5574.60, z = 53.79},
	{x = 2230.70, y = 5574.40, z = 53.90},
	{x = 2232.43, y = 5576.30, z = 53.97},
	{x = 2230.22, y = 5576.55, z = 53.93},
	{x = 2227.74, y = 5576.66, z = 53.86},
	{x = 2225.44, y = 5576.90, z = 53.85},
	{x = 2223.05, y = 5577.12, z = 53.83},
	{x = 2220.60, y = 5577.06, z = 53.83},
	{x = 2218.60, y = 5577.26, z = 53.85},
	{x = 2219.00, y = 5579.42, z = 53.89},
	{x = 2223.80, y = 5579.30, z = 53.91},
	{x = 2225.40, y = 5578.94, z = 53.88},
	{x = 2230.15, y = 5579.00, z = 53.97},
	{x = 2233.90, y = 5578.70, z = 54.11},
}
Config.TreatmentOutsidePos = {x = -1157.1418, y = -1569.722, z = 4.028}
Config.TreatmentInsidePos = {x = 1066.402, y = -3183.468, z = -39.363}
Config.TreatmentPos = {x = 1034.065, y = -3203.7287, z = -37.778}

Config.TimeToGrow = 250
Config.TimeToTransform = 250

Config.DefaultPrice = 250
Config.MinimumPolice = 0

Config.AllowedZoneToSell = { "Palomino Avenue", "Vitus Street", "Cortes Street", "Conquistador Street", "Vespucci Boulevard", "Sandcastle Way", "Red Desert Avenue", "Magellan Avenue", "Bay City Avenue"}