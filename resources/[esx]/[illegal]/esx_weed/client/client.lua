local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


ESX = nil
local PlayerData = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}

local CurrentState = {}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end

	for k,v in pairs(Config.WeedPlants) do
		CurrentState[k] = 0
	end

	ESX.TriggerServerCallback("esx_weed:getPlantsState", function(result)
		CurrentState = result
	end)

	Citizen.Wait(1000)
	PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent("esx:setJob")
AddEventHandler("esx:setJob", function(job)
	PlayerData.job = job
end)

RegisterNetEvent("esx_weed:plantPlant")
AddEventHandler("esx_weed:plantPlant", function(zone)
	CurrentState[zone] = 1
end)

RegisterNetEvent("esx_weed:plantHarvested")
AddEventHandler("esx_weed:plantHarvested", function(zone)
	CurrentState[zone] = 0
	if CurrentAction == "wait" or CurrentAction == "harvest" then
		CurrentAction = "plant"
		CurrentActionMsg = "Appuyez sur ~INPUT_CONTEXT~ pour planter un pied"
	end
end)

AddEventHandler('esx_weed:hasEnteredMarker', function(zone)

	if zone == "EnterTreatment" then
		CurrentAction = "EnterTreatment"
		CurrentActionMsg = "Appuyez sur ~INPUT_CONTEXT~ pour entrer dans le laboratoire"
	elseif zone == "ExitTreatment" then
		CurrentAction = "ExitTreatment"
		CurrentActionMsg = "Appuyez sur ~INPUT_CONTEXT~ pour sortir du laboratoire"
	elseif zone == "Transform" then
		CurrentAction = "Transform"
		CurrentActionMsg = "Appuyez sur ~INPUT_CONTEXT~ pour traiter la weed"
	else
		if CurrentState[zone] == 0 then
			CurrentAction     = 'plant'
			CurrentActionMsg  = "Appuyez sur ~INPUT_CONTEXT~ pour planter un pied"
		elseif CurrentState[zone] >= 1 and CurrentState[zone] <= 99 then
			CurrentAction = 'wait'
			CurrentActionMsg = "Le pied est en train de pousser ("..CurrentState[zone].."%)"
	 	elseif CurrentState[zone] == 100 then
	 		CurrentAction = "harvest"
			CurrentActionMsg = "Appuyez sur ~INPUT_CONTEXT~ pour recolter (100%)"
	 	end
		CurrentActionData = {zone = zone}
	end
end)

AddEventHandler('esx_weed:hasExitedMarker', function(zone)

	if zone == "Transform" then
		TriggerServerEvent("esx_weed:stopTransform")
	end

	CurrentAction = nil
	CurrentActionData = {}
	ESX.UI.Menu.CloseAll()
end)

function TableHasValue(table, value)
	for k,v in pairs(table) do
		if v == value then
			return true
		end
	end
	return false
end

Citizen.CreateThread(function()
	local BlockedNPC = {}
	while true do

    local isInMarker = false
		local currentZone = nil
		local playerPed = GetPlayerPed(-1)
		local pCoords = GetEntityCoords(playerPed)

		for k,v in pairs(Config.WeedPlants) do
			if GetDistanceBetweenCoords(pCoords, v.x, v.y, v.z, true) <= 0.6 then
				isInMarker = true
				currentZone = k
				LastZone = k
			end
		end
		if GetDistanceBetweenCoords(pCoords, Config.TreatmentOutsidePos.x, Config.TreatmentOutsidePos.y, Config.TreatmentOutsidePos.z, true) <= 1.0 then
			isInMarker = true
			currentZone = "EnterTreatment"
			LastZone = "EnterTreatment"
		end
		if GetDistanceBetweenCoords(pCoords, Config.TreatmentInsidePos.x, Config.TreatmentInsidePos.y, Config.TreatmentInsidePos.z, true) <= 1.0 then
			isInMarker = true
			currentZone = "ExitTreatment"
			LastZone = "ExitTreatment"
		end
		if GetDistanceBetweenCoords(pCoords, Config.TreatmentPos.x, Config.TreatmentPos.y, Config.TreatmentPos.z, true) <= 1.5 then
			isInMarker = true
			currentZone = "Transform"
			LastZone = "Transform"
		end
		if isInMarker and not HasAlreadyEnteredMarker then
			HasAlreadyEnteredMarker = true
			TriggerEvent('esx_weed:hasEnteredMarker', currentZone)
		end
		if not isInMarker and HasAlreadyEnteredMarker then
			HasAlreadyEnteredMarker = false
			TriggerEvent('esx_weed:hasExitedMarker', LastZone)
		end
		if CurrentAction ~= nil then

	      SetTextComponentFormat('STRING')
	      AddTextComponentString(CurrentActionMsg)
	      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

	      if IsControlJustReleased(0, 38) then

	        if CurrentAction == 'plant' then
	          CurrentState[CurrentActionData.zone] = 1
	          TriggerServerEvent("esx_weed:plantWeed", CurrentActionData.zone)
	        end

	        if CurrentAction == "harvest" then
	        	CurrentState[CurrentActionData.zone] = 0
	        	TriggerServerEvent("esx_weed:harvestWeed", CurrentActionData.zone)
	        end

	        if CurrentAction == "EnterTreatment" then
	        	DoScreenFadeOut(750)
	        	Citizen.Wait(1000)
	        	ESX.Game.Teleport(GetPlayerPed(-1), Config.TreatmentInsidePos)
	        	Citizen.Wait(250)
	        	DoScreenFadeIn(750)
	        end

	        if CurrentAction == "ExitTreatment" then
	        	DoScreenFadeOut(750)
	        	Citizen.Wait(1000)
	        	ESX.Game.Teleport(GetPlayerPed(-1), Config.TreatmentOutsidePos)
	        	Citizen.Wait(250)
	        	DoScreenFadeIn(750)
	        end

	        if CurrentAction == "Transform" then
				TriggerServerEvent("esx_weed:transform")
			end

	        CurrentAction = nile
	      end
	    end
	    if PlayerData.job ~= nil and PlayerData.job.name ~= "police" and PlayerData.job.name ~= "sheriff" then
		    if IsControlJustReleased(0,  Keys['U']) then
		    	if not IsPedInAnyVehicle(GetPlayerPed(-1), false) then
			      local playerPed = GetPlayerPed(-1)
					  local coords    = GetEntityCoords(playerPed)
  					if TableHasValue(Config.AllowedZoneToSell, GetStreetNameFromHashKey(Citizen.InvokeNative(0x2EB41072B4C1E4C0, coords.x, coords.y, coords.z, Citizen.PointerValueInt(), Citizen.PointerValueInt()))) then

  						local closestPed, closestDistance = ESX.Game.GetClosestPed({
  							x = coords.x,
  							y = coords.y,
  							z = coords.z
  						}, {playerPed})

  						if closestPed ~= -1 and closestDistance <= 5.0 then
  							local random = math.random(0,100)
  							if not TableHasValue(BlockedNPC, closestPed) then
  								local playerData = ESX.GetPlayerData()

  								for i = 1, #playerData.inventory do
  									if playerData.inventory[i].name == "weed_pooch" then
  										if playerData.inventory[i].count > 0 then
  											TaskStandStill(closestPed,  -1)
  											TaskLookAtEntity(closestPed,  playerPed,  -1,  2048,  3)

  											ESX.SetTimeout(2000, function()
  												StartWalking(closestPed)
  											end)
  											if random <= 10 then
  												ESX.ShowNotification("~r~Cette personne n'est pas intéréssée")
  											else
  												TriggerServerEvent("esx_weed:buyWeed")
  											end
  												table.insert(BlockedNPC, closestPed)
  												Citizen.Wait(250)
  										else
  											ESX.ShowNotification("Vous n'avez rien a vendre")
  										end
  									end
  								end
  							else
  								ESX.ShowNotification("Vous avez négocier avec ce client")
  							end
  							if random <= 5 then
  								TaskStartScenarioInPlace(closestPed, 'WORLD_HUMAN_STAND_MOBILE', 0, true)
  								TriggerServerEvent("esx_weed:warnPolice")
  							end
  						else
  							ESX.ShowNotification("Aucun client a proximité")
  						end
  					else
  						ESX.ShowNotification("Ce quartier n'est pas intéréssant")
  					end
  				end
	      	end
      	end
		Citizen.Wait(1)
	end
end)

function StartWalking(ped)

	Citizen.CreateThread(function()

		RequestAnimDict('move_m@generic_variations@walk')

		while not HasAnimDictLoaded('move_m@generic_variations@walk') do
			Citizen.Wait(0)
		end
		TaskPlayAnim(ped,  'move_m@generic_variations@walk',  'walk_a',  1.0,  -1.0,  -1,  0,  1,  false,  false,  false)
	end)
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(Config.TimeToGrow)
		for k,v in pairs(CurrentState) do
			if v > 0 and v < 100 then
				CurrentState[k] = v + 1
				if CurrentState[k] < 100 and CurrentAction == "wait" then
					CurrentActionMsg = "Le pied est en train de pousser ("..CurrentState[k].."%)"
				elseif CurrentState[k] == 100 and CurrentAction == "wait" then
					CurrentAction = "harvest"
					CurrentActionMsg = "Appuyez sur ~INPUT_CONTEXT~ pour recolter (100%)"
				end
			end
		end
	end
end)
