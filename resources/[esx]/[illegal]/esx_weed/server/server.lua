ESX = nil
local PlantsStates = {}
local PlayersTransforming = {}
local CopsConnected = 0

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

for k,v in pairs(Config.WeedPlants) do
	PlantsStates[k] = 0
end

ESX.RegisterServerCallback("esx_weed:getPlantsState", function(source, cb)
	cb(PlantsStates)
end)

AddEventHandler("esx_service:playerStartService", function(name)
	if name == "police" or name == "sheriff" then
		CopsConnected = CopsConnected + 1
	end
end)

AddEventHandler("esx_service:playerStopService", function(name)
	if name == "police" or name == "sheriff" then
		CopsConnected = CopsConnected - 1
	end
	if CopsConnected < 0 then
		CopsConnected = 0
	end
end)

-- AddEventHandler('esx:playerLoaded', function(source)
-- 	local xPlayer = ESX.GetPlayerFromId(source)

-- 	if xPlayer.job.name == 'police' or xPlayer.job.name == 'sheriff' then
-- 		CopsConnected = CopsConnected + 1
-- 	end
-- end)

-- AddEventHandler('esx:playerDropped', function(source)
-- 	local xPlayer = ESX.GetPlayerFromId(source)

-- 	if xPlayer.job.name == 'police' or xPlayer.job.name == 'sheriff' then
-- 		CopsConnected = CopsConnected - 1
-- 	end
-- end)

-- AddEventHandler('esx:setJob', function(source, job, lastJob)
-- 	local xPlayer = ESX.GetPlayerFromId(source)

-- 	if (job.name == 'police' and lastJob.name ~= 'police') or (job.name == 'sheriff' and lastJob.name ~= 'sheriff') then
-- 		CopsConnected = CopsConnected + 1
-- 	end

-- 	if (job.name ~= 'police' and lastJob.name == 'police') or (job.name ~= 'sheriff' and lastJob.name == 'sheriff') then
-- 		CopsConnected = CopsConnected - 1
-- 	end
-- end)

function GrowPlant(index)
	SetTimeout(Config.TimeToGrow, function()
		PlantsStates[index] = PlantsStates[index] + 1
		if PlantsStates[index] < 100 then
			GrowPlant(index)
		end
	end)
end

RegisterNetEvent("esx_weed:plantWeed")
AddEventHandler("esx_weed:plantWeed", function(zone)
	PlantsStates[zone] = 1
	TriggerClientEvent("esx_weed:plantPlant", -1, zone)
	GrowPlant(zone)
end)

function Transform(player)
	SetTimeout(Config.TimeToTransform, function()
		if PlayersTransforming[player] == true then
			local xPlayer = ESX.GetPlayerFromId(player)
			if xPlayer.getInventoryItem('weed').count >= 1 then
				xPlayer.removeInventoryItem("weed", 1)
				xPlayer.addInventoryItem("weed_pooch", 1)
				Transform(player)
			end
		end
	end)
end

RegisterNetEvent("esx_weed:transform")
AddEventHandler("esx_weed:transform", function()
	local _source = source
	if PlayersTransforming[_source] == nil or PlayersTransforming[_source] == false then
		PlayersTransforming[_source] = true
		Transform(_source)
	end
end)

RegisterNetEvent("esx_weed:stopTransform")
AddEventHandler("esx_weed:stopTransform", function()
	local _source = source
	PlayersTransforming[_source] = nil
end)

RegisterNetEvent("esx_weed:harvestWeed")
AddEventHandler("esx_weed:harvestWeed", function(zone)
	if PlantsStates[zone] == 100 then
		local _source = source
		local xPlayer = ESX.GetPlayerFromId(_source)
		xPlayer.addInventoryItem("weed", 5)
		TriggerClientEvent("esx_weed:plantHarvested", -1, zone)
		PlantsStates[zone] = 0
	else
		TriggerClientEvent("esx:showNotification", source, "~r~C'est pas bien de tricher !")
	end
end)

RegisterNetEvent("esx_weed:buyWeed")
AddEventHandler("esx_weed:buyWeed", function()
	if CopsConnected >= Config.MinimumPolice then
		local _source = source
		local xPlayer = ESX.GetPlayerFromId(_source)
		local weed_pooch = xPlayer.getInventoryItem("weed_pooch")

		if weed_pooch.count > 0 then
			local nbr = math.random(1,5)
			if nbr > weed_pooch.count then nbr = weed_pooch.count end;
			xPlayer.removeInventoryItem("weed_pooch", nbr)
			local pay
			if CopsConnected < 4 then
				pay = Config.DefaultPrice * nbr * CopsConnected
			else
				pay = Config.DefaultPrice * nbr * 4
			end
			xPlayer.addAccountMoney('black_money', pay)
			TriggerClientEvent("esx:showNotification", _source, "Vous venez de vendre ~r~"..nbr.." pochon")
		end
	else
		TriggerClientEvent("esx:showNotification", source, "Présence Policière trop ~r~Faible")
	end
end)

RegisterNetEvent("esx_weed:warnPolice")
AddEventHandler("esx_weed:warnPolice", function()
	ESX.TriggerServerCallback("esx_service:getPlayersInService", 0, 0, function(players)
		for k, v in pairs(players) do
			TriggerClientEvent("esx:showNotification", k, "~r~Appel Anonyme:~w~ Une personne à éssayée de me vendre du canabis")	
		end
	end, "police")
end)