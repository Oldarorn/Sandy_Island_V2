ESX          = nil
local IsDead = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end
end)

AddEventHandler('esx_basicneeds:resetStatus', function()
	TriggerEvent('esx_status:set', 'hunger', 1000000/2)
	TriggerEvent('esx_status:set', 'thirst', 1000000/2)
	TriggerEvent('esx_status:set', 'needs', 0)

end)

AddEventHandler('playerSpawned', function()

	if IsDead then
		TriggerEvent('esx_basicneeds:resetStatus')
	end

	IsDead = false
end)

AddEventHandler('esx_status:loaded', function(status)

	TriggerEvent('esx_status:registerStatus', 'hunger', 1000000, 'fas fa-utensils yellow',
		function(status)
			return true
		end,
		function(status)
			if GetEntityHealth(GetPlayerPed(-1)) > 0 then
				status.remove(150)
			end
		end
	)

	TriggerEvent('esx_status:registerStatus', 'thirst', 1000000, 'fab fa-gulp blue',
		function(status)
			return true
		end,
		function(status)
			if GetEntityHealth(GetPlayerPed(-1)) > 0 then
				status.remove(200)
			end
		end
	)

	TriggerEvent('esx_status:registerStatus', 'needs', 0, 'fas fa-poo brown',
		function(status)
			return true
		end,
		function(status)
			if GetEntityHealth(GetPlayerPed(-1)) > 0 then
				status.add(200)
			end
		end
	)

	Citizen.CreateThread(function()
		while true do
			Citizen.Wait(1000)

			local playerPed  = GetPlayerPed(-1)
			local prevHealth = GetEntityHealth(playerPed)
			local health     = prevHealth

			TriggerEvent('esx_status:getStatus', 'hunger', function(status)

				if status.val <= 0 then
					SetEntityHealth(playerPed, 0)
				end
			end)

			TriggerEvent('esx_status:getStatus', 'thirst', function(status)

				if status.val <= 0 then
					SetEntityHealth(playerPed, 0)
				end
			end)

			TriggerEvent('esx_status:getStatus', 'needs', function(status)

				if status.val >= 1000000 then
					SetEntityHealth(playerPed, 0)
				end
			end)

			if health ~= prevHealth then
				SetEntityHealth(playerPed,  health)
			end
		end
	end)

	Citizen.CreateThread(function()
		while true do
			Citizen.Wait(1)

			local playerPed = GetPlayerPed(-1)

			if IsEntityDead(playerPed) and not IsDead then
				IsDead = true
			end
		end
	end)
end)

RegisterNetEvent('esx_basicneeds:onEat')
AddEventHandler('esx_basicneeds:onEat', function()

	Citizen.CreateThread(function()

	  local playerPed = GetPlayerPed(-1)
	  local coords    = GetEntityCoords(playerPed)
	  local boneIndex = GetPedBoneIndex(playerPed, 18905)

	  RequestAnimDict('mp_player_inteat@burger')

	  while not HasAnimDictLoaded('mp_player_inteat@burger') do
	  	Citizen.Wait(0)
	  end

		ESX.Game.SpawnObject('prop_cs_burger_01', {
			x = coords.x,
			y = coords.y,
			z = coords.z + 2
		}, function(object)

			Citizen.CreateThread(function()

			  AttachEntityToEntity(object, playerPed, boneIndex, 0.12, 0, 0.02, 90.0, 90.0, 410.0, true, true, false, true, 1, true)
			  Citizen.Wait(1000)
			  TaskPlayAnim(playerPed, 'mp_player_inteat@burger', 'mp_player_int_eat_burger', 1.0, -1.0, 2000, 0, 1, true, true, true)
			  Citizen.Wait(2000)
			  DeleteObject(object)
			end)
		end)
	end)
end)

RegisterNetEvent('esx_basicneeds:onDrink')
AddEventHandler('esx_basicneeds:onDrink', function()

	Citizen.CreateThread(function()

	  local playerPed = GetPlayerPed(-1)
	  local coords    = GetEntityCoords(playerPed)
	  local boneIndex = GetPedBoneIndex(playerPed, 57005)

	  RequestAnimDict('amb@world_human_drinking@coffee@male@idle_a')

	  while not HasAnimDictLoaded('amb@world_human_drinking@coffee@male@idle_a') do
	  	Citizen.Wait(0)
	  end

		ESX.Game.SpawnObject('prop_ld_flow_bottle', {
			x = coords.x,
			y = coords.y,
			z = coords.z + 2
			}, function(object)

			Citizen.CreateThread(function()

				AttachEntityToEntity(object, playerPed, boneIndex, 0.12, 0, -0.02, 90.0, 90.0, 210.0, true, true, false, true, 1, true)
				Citizen.Wait(1000)
				TaskPlayAnim(playerPed, 'amb@world_human_drinking@coffee@male@idle_a', 'idle_a', 1.0, -1.0, 2000, 0, 1, true, true, true)
				Citizen.Wait(5000)
				DeleteObject(object)
			end)
		end)
	end)
end)

RegisterNetEvent('esx_basicneeds:onPee')
AddEventHandler('esx_basicneeds:onPee', function()
	Citizen.CreateThread(function()
		local playerPed = GetPlayerPed(-1)
		local hashSkin = GetHashKey("mp_m_freemode_01")

		if IsPedInAnyVehicle(GetPlayerPed(-1), true) == false then
			if(GetEntityModel(GetPlayerPed(-1)) ~= hashSkin) then
				Citizen.CreateThread(function()
					RequestAnimDict('missfbi3ig_0')
					local pedid = PlayerPedId()
					TaskPlayAnim(pedid, 'missfbi3ig_0', 'shit_loop_trev', 8.0, 8, -1, 0, 0, 0, 0, 0)
					ClearPedTasks(ped)
					end)
			else
				Citizen.CreateThread(function()
					RequestAnimDict('misscarsteal2peeing')
					local pedid = PlayerPedId()
					TaskPlayAnim(pedid, 'misscarsteal2peeing', 'peeing_intro', 8.0, -8, -1, 0, 0, 0, 0, 0)
					Citizen.Wait(GetAnimDuration('misscarsteal2peeing', 'peeing_intro'))
					TaskPlayAnim(pedid, 'misscarsteal2peeing', 'peeing_loop', 8.0, -8, -1, 0, 0, 0, 0, 0)
					Citizen.Wait(GetAnimDuration('misscarsteal2peeing', 'peeing_loop'))
					TaskPlayAnim(pedid, 'misscarsteal2peeing', 'peeing_outro', 8.0, -8, -1, 0, 0, 0, 0, 0)
					ClearPedTasks(ped)
				end)
			end
		end
	end)
end)
