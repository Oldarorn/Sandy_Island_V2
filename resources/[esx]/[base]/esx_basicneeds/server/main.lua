ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

--[[ NOURRITURES ]]--
-- sandwich
ESX.RegisterUsableItem('sandwich', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('sandwich', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 45000)
	TriggerClientEvent('esx_basicneeds:onEat', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_bread'))
end)

-- Bâtonnet de poisson
ESX.RegisterUsableItem('fish_bat', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('fish_bat', 1)

  TriggerClientEvent('esx_status:add', source, 'hunger', 200000)
  TriggerClientEvent('esx_basicneeds:onEat', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_fish_bat'))
end)

-- Pain
ESX.RegisterUsableItem('bread', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('bread', 1)

  TriggerClientEvent('esx_status:add', source, 'hunger', 150000)
  TriggerClientEvent('esx_basicneeds:onEat', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_bread'))
end)

-- Fruit
ESX.RegisterUsableItem('fruit', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('fruit', 1)

  TriggerClientEvent('esx_status:add', source, 'hunger', 10000)
  TriggerClientEvent('esx_basicneeds:onEat', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_fruit'))
end)

-- Bonbon
ESX.RegisterUsableItem('candy', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('candy', 1)

  TriggerClientEvent('esx_status:add', source, 'hunger', 10000)
  TriggerClientEvent('esx_basicneeds:onEat', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_candy'))
end)

-- Burger
ESX.RegisterUsableItem('hamburger', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('hamburger', 1)

  TriggerClientEvent('esx_status:add', source, 'hunger', 25000)
  TriggerClientEvent('esx_basicneeds:onEat', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_hamburger'))
end)

-- Hot dog
ESX.RegisterUsableItem('hotdog', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('hotdog', 1)

  TriggerClientEvent('esx_status:add', source, 'hunger', 25000)
  TriggerClientEvent('esx_basicneeds:onEat', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_hotdog'))
end)

--[[ BOISSONS ]]--
-- eau
ESX.RegisterUsableItem('water', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('water', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 40000)
	TriggerClientEvent('esx_basicneeds:onDrink', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_water'))

	Citizen.Wait(1000*60*2)
	TriggerClientEvent('esx_status:add', source, 'needs', 50000)
end)

--Limonade
ESX.RegisterUsableItem('limonade', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('limonade', 1)

  TriggerClientEvent('esx_status:add', source, 'thirst', 200000)
  TriggerClientEvent('esx_basicneeds:onDrink', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_limonade'))

  Citizen.Wait(1000*60*2)
  TriggerClientEvent('esx_status:add', source, 'needs', 50000)
end)

-- Coca
ESX.RegisterUsableItem('coca', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('coca', 1)

  TriggerClientEvent('esx_status:add', source, 'thirst', 15000)
  TriggerClientEvent('esx_basicneeds:onDrink', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_coca'))

  Citizen.Wait(1000*60*2)
  TriggerClientEvent('esx_status:add', source, 'needs', 50000)
end)

-- Sprunk
ESX.RegisterUsableItem('sprunk', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('sprunk', 1)

  TriggerClientEvent('esx_status:add', source, 'thirst', 20000)
  TriggerClientEvent('esx_basicneeds:onDrink', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_sprunk'))

  Citizen.Wait(1000*60*2)
  TriggerClientEvent('esx_status:add', source, 'needs', 50000)
end)

--Café
ESX.RegisterUsableItem('cofee', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('cofee', 1)

  TriggerClientEvent('esx_status:add', source, 'thirst', 10000)
  TriggerClientEvent('esx_basicneeds:onDrink', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_cofee'))

  Citizen.Wait(1000*60*2)
  TriggerClientEvent('esx_status:add', source, 'needs', 50000)
end)

--[[ ALCOOL ]]--
-- Vin
ESX.RegisterUsableItem('wine', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('wine', 1)

  TriggerClientEvent('esx_status:add', source, 'thirst', 225000)
  TriggerClientEvent('esx_basicneeds:onDrink', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_wine'))

  Citizen.Wait(1000*60*2)
  TriggerClientEvent('esx_status:add', source, 'needs', 50000)
end)

-- Vodka
ESX.RegisterUsableItem('vodka', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('vodka', 1)

  TriggerClientEvent('esx_status:add', source, 'thirst', 50000)
  TriggerClientEvent('esx_status:add', source, 'drunk', 270000)
  TriggerClientEvent('esx_optionalneeds:onDrunk', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_vodka'))

  Citizen.Wait(1000*60*2)
  TriggerClientEvent('esx_status:add', source, 'needs', 50000)
end)

-- Bière
ESX.RegisterUsableItem('beer', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('beer', 1)

  TriggerClientEvent('esx_status:add', source, 'thirst', 50000)
  TriggerClientEvent('esx_status:add', source, 'drunk', 200000)
  TriggerClientEvent('esx_optionalneeds:onDrunk', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_beer'))

  Citizen.Wait(1000*60*2)
  TriggerClientEvent('esx_status:add', source, 'needs', 50000)
end)

-- Cocktail
ESX.RegisterUsableItem('cocktail', function(source)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('cocktail', 1)

  TriggerClientEvent('esx_status:add', source, 'thirst', 50000)
  TriggerClientEvent('esx_status:add', source, 'drunk', 230000)
  TriggerClientEvent('esx_optionalneeds:onDrunk', source)
  TriggerClientEvent('esx:showNotification', source, _U('used_cocktail'))

  Citizen.Wait(1000*60*2)
  TriggerClientEvent('esx_status:add', source, 'needs', 50000)
end)

-- [[ Others ]] --
AddEventHandler( 'chatMessage', function( s, n, msg )
  msg = string.lower(msg)
  if (msg == "/pee") then
    CancelEvent()
    TriggerClientEvent('esx_status:set', s, 'needs', 0)
    TriggerClientEvent('esx_basicneeds:onPee', s)
  end
end)
