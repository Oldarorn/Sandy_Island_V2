Config                = {}
Config.DrawDistance   = 15
Config.DrawIllegalDistance   = 3
Config.Size           = { x = 1.5, y = 1.5, z = 1.5 }
Config.Color          = { r = 0, g = 128, b = 255 }
Config.ColorIllegal   = { r = 255, g = 69, b = 0}
Config.Type           = 27
Config.Locale         = 'fr'
Config.EnableLicense  = false -- only turn this on if you are using esx_license
Config.LicensePrice   = 5000

Config.Zones = {

    GunShop = {
        legal = 0,
        Items = {
            {label = "Pistolet 9MM", name = "WEAPON_PISTOL", price = 85000},
            {label = "Couteau", name = "WEAPON_KNIFE", price = 25000},
            {label = "Batte de Baseball", name = "WEAPON_BAT", price = 8500},
            {label = "Club de Golf", name = "WEAPON_GOLFCLUB", price = 10000},
            {label = "Fusées Éclairantes", name = "WEAPON_FLARE", price = 1000},
            {label = "Pistolet de Détresse", name = "WEAPON_FLAREGUN", price = 7500},
            {label = "Tazer", name = "WEAPON_STUNGUN", price = 25000},
            {label = "Pétoire", name = "WEAPON_SNSPISTOL", price = 50000},
            {label = "Bouteille Cassée", name = "WEAPON_BOTTLE", price = 1500},
        },
        UsableItems = {
            {label = "Chargeur", name = "clip", price = 1000},
        },
        Pos   = {
            { x = -662.180,   y = -934.961,   z = 20.829 },
            { x = 810.25,     y = -2157.60,   z = 28.62 },
            { x = 1693.44,    y = 3760.16,    z = 33.71 },
            { x = -330.24,    y = 6083.88,    z = 30.45 },
            { x = 252.63,     y = -50.00,     z = 68.94 },
            { x = 22.09,      y = -1107.28,   z = 28.80 },
            { x = 2567.69,    y = 294.38,     z = 107.73 },
            { x = -1117.58,   y = 2698.61,    z = 17.55 },
            { x = 842.44,     y = -1033.42,   z = 27.19 },
            { x = -1306.239,   y = -394.018,  z = 35.695 },

        }
    },

    BlackWeashop = {
        legal = 1,
        Items = {
            {label = "Gaz BZ", name = "WEAPON_BZGAS", price = 4500},
            {label = "Revolver Lourd", name = "WEAPON_REVOLVER", price = 75000},
            {label = "Pistolet Lourd", name = "WEAPON_HEAVYPISTOL", price = 125000},
            {label = "Pistolet Perforant", name = "WEAPON_APPISTOL", price = 195000},
            {label = "Pistolet De Combat", name = "WEAPON_COMBATPISTOL", price = 195000},
            {label = "Pistolet Vintage", name = "WEAPON_VINTAGEPISTOL", price = 195000},
            {label = "Couteau à Cran d'arrêt", name = "WEAPON_SWITCHBLADE", price = 25000},
            {label = "Poing Américain", name = "WEAPON_KNUCKLE", price = 12000},
            {label = "UZI", name = "WEAPON_MICROSMG", price = 300000},
            {label = "Cocktail Molotov", name = "WEAPON_MOLOTOV", price = 4500},
        },
        Pos   = {
            {x = 111.888221, y = -1978.47717285, z = 19.6708900}
        }
    },

}
