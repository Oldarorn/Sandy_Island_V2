local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX                           = nil
local GUI                     = {}
GUI.Time                      = 0
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasPayed                = false
local LastSkin 				  			= nil

local Accessories = {
	Helmet = {h1 = 0, h2 = 0, OnHead = false},
	Glasses = {g1 = 0, g2 = 0, OnHead = false}
}
	

Citizen.CreateThread(function()

	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end

end)

function OpenShopMenu(category)
	HasPayed = false
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'base',
		{
			title = "Tenues",
			align = 'left',
			elements = {
				{label = "Acheter une Tenue", value = 'buy_suit'},
				{label = "Acheter un Chapeau/Casque", value = 'buy_helmet'},
				{label = "Acheter des Lunettes", value = 'buy_glasses'},
				{label = "S'habiller", value = 'suit_up'},
			}
		},
		function(data, menu)

			TriggerEvent('skinchanger:getSkin', function(skin)
			    LastSkin = skin
			end)

			if data.current.value == 'buy_suit' then
				menu.close()
				-- Get List Suits
				ESX.TriggerServerCallback('esx_clotheshop:getSuits', function(suits)
					local elements = {}
					for k, v in pairs(suits) do
						table.insert(elements, {label = v.name.. " ("..v.price.."$)", value = v.id, loadaout = v.loadaout, price = v.price})
					end
					TriggerEvent('skinchanger:getSkin', function(skin)

						local playerPed = GetPlayerPed(-1)

		        	local clothesSkin = json.decode(elements[1].loadaout)
		        	TriggerEvent('skinchanger:loadClothes', clothesSkin)
			    		ClearPedBloodDamage(playerPed)
			    		ResetPedVisibleDamage(playerPed)
			    		ClearPedLastWeaponDamage(playerPed)
			    	end)
					ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'buy_suits_list',
						{
							title = "Acheter une Tenue",
							align = 'left',
							elements = elements
						},
						function(data2, menu2)
							menu2.close()
							ESX.UI.Menu.Open(
								'default', GetCurrentResourceName(), 'shop_confirm',
								{
									title = _U('valid_this_purchase'),
									align = 'left',
									elements = {
										{label = _U('yes'), value = 'yes'},
										{label = _U('no'), value = 'no'},
									}
								},
								function(data3, menu3)
									menu3.close()
									if data3.current.value == 'yes' then
										ESX.TriggerServerCallback('esx_clotheshop:buySuit', function(cb)
											if cb == true or cb == "already_bought" then
												TriggerEvent('skinchanger:getSkin', function(skin)
													TriggerServerEvent('esx_skin:save', skin)
													LastSkin = skin
												end)
												HasPayed = true
												if cb == true then
													ESX.ShowNotification("Vous venez d'acheter la tenue ~g~"..data2.current.label)
													ESX.ShowNotification("Cette tenue est enregistrée dans votre garde robe")
												else
													ESX.ShowNotification("Vous Possédiez déjà ~g~"..data2.current.label)
												end
											elseif cb == "not_enough_money" then
												TriggerEvent('skinchanger:loadSkin', LastSkin)
												ESX.ShowNotification("~r~Vous n'avez pas assez d'argent sur vous pour acheter cette tenue")
											end
										end, data2.current.value, data2.current.price)
									else
										TriggerEvent('skinchanger:loadSkin', LastSkin)
									end
								end)
						end,
						function(data2, menu2)
							menu2.close()
							TriggerEvent('skinchanger:loadSkin', LastSkin)
						end,
						function(data2, menu2)
							TriggerEvent('skinchanger:getSkin', function(skin)

								local playerPed = GetPlayerPed(-1)

				        		local clothesSkin = json.decode(data2.current.loadaout)
				        		TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
					    		ClearPedBloodDamage(playerPed)
					    		ResetPedVisibleDamage(playerPed)
					    		ClearPedLastWeaponDamage(playerPed)
					    	end)
						end
					)
				end,  LastSkin['sex'], category)
			elseif data.current.value == "suit_up" then
				menu.close()
				-- Get List Suits
				ESX.TriggerServerCallback('esx_clotheshop:getOwnedSuits', function(suits)
					local elements = {}
					for k, v in pairs(suits) do
						table.insert(elements, {label = v.name, value = v.id, loadaout = v.loadaout})
					end
						if #elements > 0 then
						TriggerEvent('skinchanger:getSkin', function(skin)

							local playerPed = GetPlayerPed(-1)

			        		local clothesSkin = json.decode(elements[1].loadaout)
			        		TriggerEvent('skinchanger:loadClothes', clothesSkin)
				    		ClearPedBloodDamage(playerPed)
				    		ResetPedVisibleDamage(playerPed)
				    		ClearPedLastWeaponDamage(playerPed)
				    	end)
				    end
					ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'owned_suits_list',
						{
							title = "S'habiller",
							align = 'left',
							elements = elements
						},
						function(data2, menu2)
							menu2.close()
							TriggerEvent('skinchanger:getSkin', function(skin)
								TriggerServerEvent('esx_skin:save', skin)
								LastSkin = skin
							end)
							HasPayed = true
						end,
						function(data2, menu2)
							menu2.close()
							TriggerEvent('skinchanger:loadSkin', LastSkin)
						end,
						function(data2, menu2)
							TriggerEvent('skinchanger:getSkin', function(skin)

								local playerPed = GetPlayerPed(-1)

				        		local clothesSkin = json.decode(data2.current.loadaout)
				        		TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
					    		ClearPedBloodDamage(playerPed)
					    		ResetPedVisibleDamage(playerPed)
					    		ClearPedLastWeaponDamage(playerPed)
					    	end)
						end
					)
				end)
			elseif data.current.value == "buy_glasses" then
				local elements = {}
				for i = 1, GetNumberOfPedPropDrawableVariations(GetPlayerPed(-1), 	1) - 1 do
					table.insert(elements, {label = "Lunettes N°"..i, value = i})
				end
				ESX.UI.Menu.Open(
					'default', GetCurrentResourceName(), 'buy_glasses_list',
					{
						title = "Acheter des Lunettes",
						align = 'left',
						elements = elements
					},
					function(data2, menu2)
						menu2.close()
						local options = {}
						for y = 1, GetNumberOfPedPropTextureVariations(GetPlayerPed(-1), 	1, data2.current.value - 1) do
							table.insert(options, {label = "Couleur "..y, value = y})
						end
						ESX.UI.Menu.Open(
							'default', GetCurrentResourceName(), 'select_glasses_color_list',
							{
								title = "Couleur des Lunettes",
								align = 'left',
								elements = options
							},
							function(data, menu)
								menu.close()
								SetGlasses(data2.current.value, data.current.value)
								HasPayed = true
							end,
							function(data, menu)
								menu.close()
								TriggerEvent('skinchanger:loadSkin', LastSkin)
							end,
							function(data, menu)
								TriggerEvent('skinchanger:change', 'glasses_2', data.current.value)
							end
						)
					end,
					function(data2, menu2)
						menu2.close()
						TriggerEvent('skinchanger:loadSkin', LastSkin)
					end,
					function(data2, menu2)
						TriggerEvent('skinchanger:change', 'glasses_1', data2.current.value)
						TriggerEvent('skinchanger:change', 'glasses_2', 0)
					end
				)
			elseif data.current.value == "buy_helmet" then
				local elements = {}
				for i = 1, GetNumberOfPedPropDrawableVariations(GetPlayerPed(-1), 	0) - 1 do
					table.insert(elements, {label = "Chapeau/Casque N°"..i, value = i})
				end
				ESX.UI.Menu.Open(
					'default', GetCurrentResourceName(), 'buy_helmet_list',
					{
						title = "Acheter un Chapeau / Casque",
						align = 'left',
						elements = elements
					},
					function(data2, menu2)
						menu2.close()
						local options = {}
						for y = 1, GetNumberOfPedPropTextureVariations(GetPlayerPed(-1), 	0, data2.current.value - 1) do
							table.insert(options, {label = "Couleur "..y, value = y})
						end
						ESX.UI.Menu.Open(
							'default', GetCurrentResourceName(), 'select_helmet_color_list',
							{
								title = "Couleur du Chapeau / Casque",
								align = 'left',
								elements = options
							},
							function(data, menu)
								menu.close()
								SetHelmet(data2.current.value, data.current.value)
								HasPayed = true
							end,
							function(data, menu)
								menu.close()
								TriggerEvent('skinchanger:loadSkin', LastSkin)
							end,
							function(data, menu)
								TriggerEvent('skinchanger:change', 'helmet_2', data.current.value)
							end
						)
					end,
					function(data2, menu2)
						menu2.close()
						TriggerEvent('skinchanger:loadSkin', LastSkin)
					end,
					function(data2, menu2)
						TriggerEvent('skinchanger:change', 'helmet_1', data2.current.value)
						TriggerEvent('skinchanger:change', 'helmet_2', 0)
					end
				)
			end
		end,
		function(data, menu)
			menu.close()
		end
	)
end

AddEventHandler('esx_clotheshop:hasEnteredMarker', function(zone, category)
	CurrentAction     = 'shop_menu'
	CurrentActionMsg  = _U('press_menu')
	CurrentActionData = {category = category}
end)

AddEventHandler('esx_clotheshop:hasExitedMarker', function(zone)
	
	ESX.UI.Menu.CloseAll()
	CurrentAction = nil

	if not HasPayed then
		TriggerEvent('skinchanger:loadSkin', LastSkin)
	end

end)

-- Create Blips
Citizen.CreateThread(function()

	for i=1, #Config.Shops, 1 do

		local blip = AddBlipForCoord(Config.Shops[i].Pos.x, Config.Shops[i].Pos.y, Config.Shops[i].Pos.z)

		SetBlipSprite (blip, 73)
		SetBlipDisplay(blip, 4)
		SetBlipScale  (blip, 1.0)
		SetBlipColour (blip, 47)
		SetBlipAsShortRange(blip, true)

		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString(_U('clothes'))
		EndTextCommandSetBlipName(blip)
	end

end)

-- Display markers
Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do
			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
			end
		end

	end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil
		local shopCategory = 1

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
				shopCategory = v.Category
			end
		end

		if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
			HasAlreadyEnteredMarker = true
			LastZone                = currentZone
			TriggerEvent('esx_clotheshop:hasEnteredMarker', currentZone, shopCategory)
		end

		if not isInMarker and HasAlreadyEnteredMarker then
			HasAlreadyEnteredMarker = false
			TriggerEvent('esx_clotheshop:hasExitedMarker', LastZone)
		end

	end
end)

-- Key controls
Citizen.CreateThread(function()
	while true do

		Citizen.Wait(1)

		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E']) and (GetGameTimer() - GUI.Time) > 300 then

				if CurrentAction == 'shop_menu' then
					OpenShopMenu(CurrentActionData.category)
				end

				--CurrentAction = nil
				GUI.Time      = GetGameTimer()

			end

		end

		if IsControlPressed(0, Keys['LEFTALT']) and IsControlJustReleased(0, Keys['H']) then
			if Accessories.Helmet.h1 == 0 then
				ESX.ShowNotification("Vous n'avez pas de couvre chef")
			elseif not Accessories.Helmet.OnHead then
				TriggerEvent('skinchanger:change', 'helmet_1', Accessories.Helmet.h1)
				TriggerEvent('skinchanger:change', 'helmet_2', Accessories.Helmet.h2)
				Accessories.Helmet.OnHead = true
			else
				TriggerEvent('skinchanger:change', 'helmet_1', -1)
				Accessories.Helmet.OnHead = false
			end
		elseif IsControlPressed(0, Keys['LEFTALT']) and IsControlJustReleased(0, Keys['G']) then
			if Accessories.Glasses.g1 == 0 then
				ESX.ShowNotification("Vous n'avez pas de lunettes")
			elseif not Accessories.Glasses.OnHead then
				TriggerEvent('skinchanger:change', 'glasses_1', Accessories.Glasses.g1)
				TriggerEvent('skinchanger:change', 'glasses_2', Accessories.Glasses.g2)
				Accessories.Glasses.OnHead = true
			else
				TriggerEvent('skinchanger:change', 'glasses_1', 0)
				Accessories.Glasses.OnHead = false
			end
		end

	end
end)

function SetHelmet(h1, h2)
	Accessories.Helmet.h1 = h1
	Accessories.Helmet.h2 = h2
	TriggerServerEvent('esx_clotheshop:setUserHelmet', h1, h2)
end

function SetGlasses(g1, g2)
	Accessories.Glasses.g1 = g1
	Accessories.Glasses.g2 = g2
	TriggerServerEvent('esx_clotheshop:setUserGlasses', g1, g2)
end

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	ESX.TriggerServerCallback('esx_clotheshop:getAccessories', function(accessories)
		Accessories = accessories
	end)
end)