ESX = nil
local Suits = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_clotheshop:pay')
AddEventHandler('esx_clotheshop:pay', function()

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeMoney(Config.Price)

	TriggerClientEvent('esx:showNotification', source, _U('you_paid') .. Config.Price)

end)

RegisterServerEvent('esx_clotheshop:saveOutfit')
AddEventHandler('esx_clotheshop:saveOutfit', function(label, skin)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
	TriggerEvent('esx_datastore:getDataStore', 'property', xPlayer.identifier, function(store)

		local dressing = store.get('dressing')

		if dressing == nil then
			dressing = {}
		end

		table.insert(dressing, {
			label = label,
			skin  = skin
		})

		store.set('dressing', dressing)

	end)

end)

function GetSuits()
	MySQL.Async.fetchAll('SELECT * FROM suits WHERE sex = @sex AND (category = @category or category = 0)', {
			['@sex'] = sex,
			['@category'] = category
		},
		function(results)
			Suits = results
		end
	)
end

GetSuits()

ESX.RegisterServerCallback('esx_clotheshop:getSuits', function(source, cb, sex, category)
	cb(Suits)
end)

function GetSuitById(id)
	local result = MySQL.Sync.fetchAll('SELECT * FROM suits WHERE id = @id', {
		['@id'] = id
	})
	return result[1]
end

function GetSuitsList(owner)
	local result = MySQL.Sync.fetchAll('SELECT * FROM owned_suits WHERE owner = @owner', {
		['@owner'] = owner,
	})
	return result[1]
end

ESX.RegisterServerCallback('esx_clotheshop:getOwnedSuits', function(source, cb)
	local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
	local owned_suits = MySQL.Sync.fetchAll('SELECT * FROM owned_suits WHERE owner = @owner', {
		['@owner'] = xPlayer.identifier,
	})
	local suits = {}
	for match in (owned_suits[1].suit_ids..","):gmatch("(.-)"..",") do
        local suit = MySQL.Sync.fetchAll('SELECT * FROM suits WHERE id = @id', {
			['@id'] = tonumber(match)
		})
        if suit ~= nil then
        	table.insert(suits, suit[1])
        end
    end
    if suits == {} then
    	cb(nil)
    else
		cb(suits)
	end
end)

ESX.RegisterServerCallback('esx_clotheshop:deleteOwnedSuit', function(source, cb, id)
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
	MySQL.Async.fetchAll('SELECT * FROM owned_suits WHERE owner = @owner', {
			['@owner'] = xPlayer.identifier,
		},
		function(results)
			local owned_suits = ""
			local deleted = false
			for match in (results[1].suit_ids..","):gmatch("(.-)"..",") do
		        if tonumber(match) ~= id then
		        	owned_suits = owned_suits..match..","
		        else
		        	deleted = true
		        end
		    end
		    if deleted then
				cb(true)
			else
				cb(false)
			end
		end
	)
end)

ESX.RegisterServerCallback('esx_clotheshop:buySuit', function(source, cb, id, price)
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

    MySQL.Async.fetchAll('SELECT * FROM owned_suits WHERE owner = @owner', {
			['@owner'] = xPlayer.identifier
		},
		function(results)
			if #results == 0 then
				MySQL.Async.execute(
					'INSERT INTO owned_suits (owner, suit_ids) VALUES (@owner, @suit_ids)',
					{
						['@owner'] = xPlayer.identifier,
						['@suit_ids'] = ""
					}, function(rowsChanged)
					end
				)
			end
			local owned_suits = {}
			for match in (results[1].suit_ids..","):gmatch("(.-)"..",") do
				if tonumber(match) == id then
		        	table.insert(owned_suits, match)
		        end
		    end
			if #owned_suits == 0 then
				if xPlayer.get('money') >= price then
			    	xPlayer.removeMoney(price)
			    	MySQL.Async.execute(
						'UPDATE owned_suits SET suit_ids = @suit_ids WHERE owner = @owner',
						{
							['@owner'] = xPlayer.identifier,
							['@suit_ids'] = results[1].suit_ids..id..","
						}, function(rowsChanged)
						end
					)
			    	cb(true)
			    else
			    	cb('not_enough_money')
			    end
			else
				cb('already_bought')
			end
		end
	)
end)

ESX.RegisterServerCallback('esx_clotheshop:checkMoney', function(source, cb)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.get('money') >= Config.Price then
		cb(true)
	else
		cb(false)
	end

end)

ESX.RegisterServerCallback('esx_clotheshop:checkPropertyDataStore', function(source, cb)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
	local foundStore = false

	TriggerEvent('esx_datastore:getDataStore', 'property', xPlayer.identifier, function(store)
		foundStore = true
	end)

	cb(foundStore)

end)

ESX.RegisterServerCallback('esx_clotheshop:getAccessories', function(source, cb)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	MySQL.Async.fetchAll('SELECT * FROM user_accessories WHERE owner = @owner', {
			['@owner'] = xPlayer.identifier
		},
		function(results)
			if #results == 0 then
				MySQL.Async.execute(
					'INSERT INTO user_accessories (owner, helmet_1, helmet_2, glasses_1, glasses_2) VALUES (@owner, 0, 0, 0, 0)',
					{
						['@owner'] = xPlayer.identifier,
					}, function(rowsChanged)
						cb({Helmet = {h1 = 0, h2 = 0}, Glasses = {g1 = 0, g2 = 0}})
					end
				)
			else
				local Accessories = {
					Helmet = {
						h1 = results[1].helmet_1,
						h2 = results[1].helmet_2,
						OnHead = false
					},
					Glasses = {
						g1 = results[1].glasses_1,
						g2 = results[1].glasses_2,
						OnHead = false
					}
				}
				cb(Accessories)
			end
		end
	)
end)

RegisterNetEvent('esx_clotheshop:setUserHelmet')
AddEventHandler('esx_clotheshop:setUserHelmet', function(h1, h2)
	local xPlayer = ESX.GetPlayerFromId(source)
	MySQL.Async.execute(
		'UPDATE user_accessories SET `helmet_1` = @helmet_1, `helmet_2` = @helmet_2 WHERE `owner` = @owner',
		{
			['@helmet_1'] = h1,
			['@helmet_2'] = h2,
			['@owner'] = xPlayer.identifier,
		}, function(rowsChanged)
		end
	)
end)

RegisterNetEvent('esx_clotheshop:setUserGlasses')
AddEventHandler('esx_clotheshop:setUserGlasses', function(g1, g2)
	local xPlayer = ESX.GetPlayerFromId(source)
	MySQL.Async.execute(
		'UPDATE user_accessories SET `glasses_1` = @glasses_1, `glasses_2` = @glasses_2 WHERE `owner` = @owner',
		{
			['@glasses_1'] = g1,
			['@glasses_2'] = g2,
			['@owner'] = xPlayer.identifier,
		}, function(rowsChanged)
		end
	)
end)

ESX.RegisterServerCallback('esx_clotheshop:getHelmet', function(source, cb)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	MySQL.Async.execute(
		'SELECT helmet_1, helmet_2 FROM user_accessories WHERE owner = @owner',
		{
			['@owner'] = xPlayer.identifier,
		}, function(result)
			cb(result[1].helmet_1, result[1].helmet_2)
		end
	)
end)
