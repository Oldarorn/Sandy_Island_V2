Config = {}
Config.Locale = 'fr'

Config.Price = 250

Config.DrawDistance = 100.0
Config.MarkerSize   = {x = 5.0, y = 5.0, z = 1.0}
Config.MarkerSize2   = {x = 3.0, y = 3.0, z = 1.0}
Config.MarkerColor  = {r = 102, g = 102, b = 204}
Config.MarkerType   = 27

Config.Zones = {}

Config.Shops = {
  {
    Pos = {x=72.254,    y=-1399.102, z=28.376},
    Category = 1
  },
  {
    Pos = {x=-703.776,  y=-152.258,  z=36.415},
    Category = 2
  },
  {
    Pos = {x=-167.863,  y=-298.969,  z=38.733},
    Category = 3
  },
  {
    Pos = {x=428.694,   y=-800.106,  z=28.491},
    Category = 4
  },
  {
    Pos = {x=-829.413,  y=-1073.710, z=10.328},
    Category = 5
  },
  {
    Pos = {x=-1447.797, y=-242.461,  z=48.820},
    Category = 6
  },
  {
    Pos = {x=11.632,    y=6514.224,  z=30.877},
    Category = 7
  },
  {
    Pos = {x=123.646,   y=-219.440,  z=53.557},
    Category = 8
  },
  {
    Pos = {x=1696.291,  y=4829.312,  z=41.063},
    Category = 9
  },
  {
    Pos = {x=618.093,   y=2759.629,  z=41.088},
    Category = 10
  },
  {
    Pos = {x=1190.550,  y=2713.441,  z=37.222},
    Category = 11
  },
  {
    Pos = {x=-1193.429, y=-772.262,  z=16.324},
    Category = 12
  },
  {
    Pos = {x=-3172.496, y=1048.133,  z=19.863},
    Category = 13
  },
  {
    Pos = {x=-1108.441, y=2708.923,  z=18.107},
    Category = 14
  },
  {
    Pos = {x=-1037.771, y=-2737.482, z=19.46},
    Category = -1
  }
}

for i=1, #Config.Shops, 1 do

	Config.Zones['Shop_' .. i] = {
	 	Pos   = Config.Shops[i].Pos,
	 	Size  = Config.MarkerSize2,
	 	Color = Config.MarkerColor,
	 	Type  = Config.MarkerType,
    Category = Config.Shops[i].Category
  }

end
