ESX                = nil
local InService    = {}
local MaxInService = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function GetInServiceCount(name)

	local count = 0

	for k,v in pairs(InService[name]) do
		if v == true then
			count = count + 1
		end
	end

	return count

end

AddEventHandler('esx_service:activateService', function(name, max)
	InService[name]    = {}
	MaxInService[name] = max
end)

RegisterServerEvent('esx_service:disableService')
AddEventHandler('esx_service:disableService', function(name)
	if InService[name] ~= nil and InService[name][source] ~= nil then
		InService[name][source] = nil
		TriggerEvent("esx_service:playerStopService", name, source)
	end
end)

ESX.RegisterServerCallback('esx_service:isInService', function(source, cb, name)
	if InService[name] ~= nil and InService[name][source] ~= nil and InService[name][source] == true then
		cb(true)
	else
		cb(false)
	end
end)

ESX.RegisterServerCallback('esx_service:getPlayersInService', function(source, cb, name)
	cb(InService[name])
end)

ESX.RegisterServerCallback('esx_service:getNumberInService', function(source, cb, name)
	cb(GetInServiceCount(name))
end)

ESX.RegisterServerCallback('esx_service:enableService', function(source, cb, name)

	local inServiceCount = GetInServiceCount(name)

	if inServiceCount >= MaxInService[name] then
		cb(false, MaxInService[name], inServiceCount)
	else
		InService[name][source] = true
		cb(true, MaxInService[name], inServiceCount)
		TriggerEvent("esx_service:playerStartService", name, source)
	end

end)

RegisterNetEvent("esx:setJob")
AddEventHandler("esx:setJob", function(job, old)
	if InService[old] ~= nil and InService[old][source] == true then
		InService[old][source] = nil
		TriggerEvent("esx_service:playerStopService", old, source)
	end
end)

AddEventHandler('playerDropped', function()

	local _source = source
		
	for k,v in pairs(InService) do
		if v[_source] == true then
			v[_source] = nil
			TriggerEvent("esx_service:playerStopService", k, _source)
		end
	end

end)
