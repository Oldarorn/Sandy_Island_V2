ESX = nil
Radars = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function IsVehicleOwned(plate)
	MySQL.Sync.fetchAll("SELECT `vehicle`, `owner` from `owned_vehicles` WHERE `vehicle` LIKE '%@plate%'", {["@plate"] = plate})
	if #vehicles > 0 then
		return vehicles[1].owner
	end
	return nil
end

function IsOwnerConnected(identifier)
	local xPlayers = ESX.GetPlayers()
	for i = 1, #xPlayers do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
		if xPlayer.identifier == identifier then
			return xPlayers[i]
		end
	end
	return nil
end

RegisterServerEvent("esx_radars:sendflashedtopolice")
AddEventHandler("esx_radars:sendflashedtopolice", function(plate, speed, limit, street, model, isRobbed)
	local _source = source
	local xPlayers = ESX.GetPlayers()
	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
		if (TableHasValue(Config.NotifiedJobs, xPlayer.getJob().name) and xPlayers[i] ~= _source) then
			TriggerClientEvent('esx:showNotification', xPlayers[i], "Vehicule de modele ~g~"..model.." ~w~Immatriculée : ~g~"..plate)
			TriggerClientEvent('esx:showNotification', xPlayers[i], "~w~flashé à ~r~"..speed.." ~w~km/h au lieu de ~g~"..limit.."~w~ km/h à ~g~"..street)
			if isRobbed then
				TriggerClientEvent('esx:showNotification', xPlayers[i], "~r~Véhicule signalé comme volé !")
			end
		end
	end
	print("[RADAR] => Vehicule "..model.." Conduit par ".. GetPlayerName(_source) .." Flasher à "..speed.." | Rue : "..street)
end)

ESX.RegisterServerCallback("esx_radars:payfine", function(source, cb, plate, speed, limit)
	local _source = source
	local finetype = speed - limit
	local price = 0
	local isRobbed = false
	if finetype > 0 and finetype <= 10 then
		price = Config.Fines.OneToTen
	elseif finetype > 10 and finetype <= 15 then
		price = Config.Fines.TenToFifteen
	elseif finetype > 15 and finetype <= 30 then
		price = Config.Fines.FifteenToThirty
	elseif finetype > 30 and finetype <= 50 then
		price = Config.Fines.ThirtyToFifty
	elseif finetype > 50 then
		price = Config.Fines.MoreThanFifty
	end
	MySQL.Async.fetchAll("SELECT `vehicle`, `owner` from `owned_vehicles` WHERE `vehicle` LIKE '%"..plate.."%'", {},
	function(vehicle)
		local vehicleOwner
		if #vehicle > 0 then
			vehicleOwner = vehicle[1].owner
		end
		if vehicleOwner ~= nil then
			if vehicleOwner == ESX.GetPlayerFromId(_source).identifier then
				local xPlayer = ESX.GetPlayerFromId(_source)
				xPlayer.removeAccountMoney('bank', price)
				TriggerClientEvent('esx:showNotification', _source, "Vous venez de payer une amende de ~r~$"..price)
			else
				local owner = IsOwnerConnected(vehicleOwner)
				if owner ~= nil then
					local xPlayer = ESX.GetPlayerFromId(owner)
					xPlayer.removeAccountMoney('bank', price)
					TriggerClientEvent('esx:showNotification', owner, "Vous venez de payer une amende de ~r~$"..price)
				else
					local xPlayer = ESX.GetPlayerFromId(_source)
					xPlayer.removeAccountMoney('bank', price)
					TriggerClientEvent('esx:showNotification', _source, "Vous venez de payer une amende de ~r~$"..price)
				end
			end
			TriggerEvent('esx_addonaccount:getSharedAccount', 'society_police', function(account)
	      		account.addMoney(math.floor(price*0.3))
		    end)
		    TriggerEvent('esx_addonaccount:getSharedAccount', 'society_sheriff', function(account)
		      	account.addMoney(math.floor(price*0.3))
		    end)
		else
			isRobbed = true
		end
	    cb(isRobbed)
	end)
end)

RegisterServerEvent("esx_radars:sendFlashToPassengers")
AddEventHandler("esx_radars:sendFlashToPassengers", function(speed, passengers)
	local _source = source
	for k, v in pairs(passengers) do
		if (v ~= -1) then
			TriggerClientEvent("esx_radars:playerIsInFlashedVehicle", v, source, speed)
		end
	end
end)