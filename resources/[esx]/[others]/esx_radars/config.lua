Config 			= {}
Config.Marge = 4
Config.NotifiedJobs = {"police", "sheriff"}
Config.Fines = {
	OneToTen = 1000,
	TenToFifteen = 1500,
	FifteenToThirty = 3000,
	ThirtyToFifty = 4000,
	MoreThanFifty = 6500
}
-- https://wiki.fivem.net/wiki/Vehicles (UPPERCASE !)
Config.AllowedVehicles = {
	"POLICE",
	"POLICE2",
	"POLICE3",
	"718",
	"FBI",
	"FBI2",
	"RIOT",
	"RIOT2",
	"POLICEB",
	"SHERIFF",
	"SHERIFF2",
	"SHERIFF3",
	"PODOMINATOR",
	"AMBULANCE",
	"LGUARD"
}
Config.AllowedImmatriculations = {
	
}
Config.Debug = false

Config.Radars = {
	{
		pos = {x = 2410.195, y = 2908.239, z = 40.3},
		size = 50.0,
		limit = 140
	},
	{
		pos = {x = -2162.101, y = -350.339, z = 13.18},
		size = 30.0,
		limit = 90
	},
	{
		pos = {x = -121.47, y = -711.65, z = 35.013},
		size = 25.0,
		limit = 90
	},
	{
		pos = {x = 215.408, y = -700.441, z = 35.81},
		size = 27.0,
		limit = 90
	},
}