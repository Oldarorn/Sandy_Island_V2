/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : essentialmode

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-02-02 17:34:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for radars
-- ----------------------------
DROP TABLE IF EXISTS `radars`;
CREATE TABLE `radars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `posx` float(65,10) DEFAULT NULL,
  `posy` float(65,10) DEFAULT NULL,
  `posz` float(65,10) DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  `size` double DEFAULT NULL,
  `activated` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of radars
-- ----------------------------
INSERT INTO `radars` VALUES ('1', 'Autoroute de l\'aeroport', '-454.7422790527', '-1609.9610595703', '37.1275138855', '120', '22', '1');
INSERT INTO `radars` VALUES ('2', 'Autoroute de l\'est', '2015.2735595703', '2584.6066894531', '52.3166007996', '140', '33', '1');
INSERT INTO `radars` VALUES ('3', 'Autoroute Paleto', '1438.8979492188', '6466.1425781250', '18.8400001526', '120', '18', '1');
INSERT INTO `radars` VALUES ('4', 'Autoroute de l\'ouest', '-2932.9716796875', '86.4774017334', '11.5600004196', '120', '18', '1');
INSERT INTO `radars` VALUES ('5', 'Banque', '118.2489013672', '226.3363952637', '105.6600036621', '90', '20', '1');
INSERT INTO `radars` VALUES ('6', 'Radar Travaux', '-164.1647033691', '-840.0684814453', '28.5599994659', '90', '15', '1');
INSERT INTO `radars` VALUES ('7', 'Depanneurs', '-283.8189086914', '-175.1486968994', '37.8899993896', '90', '27', '1');
