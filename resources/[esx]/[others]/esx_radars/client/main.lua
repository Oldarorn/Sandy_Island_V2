ESX              = nil
local PlayerData = {}
Radars = Config.Radars

Citizen.CreateThread(function()
	while ESX == nil do
		Citizen.Wait(1)
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
		local player = GetPlayerPed(-1)
		if IsPedInAnyVehicle(player, true) and not IsPedInAnyBoat(player) and not IsPedInAnyHeli(player) and not IsPedInAnyPlane(player) then
			local coords = GetEntityCoords(player, true)
			for k,radar in pairs(Config.Radars) do
				if (GetDistanceBetweenCoords(coords, radar.pos.x, radar.pos.y, radar.pos.z, true) <= radar.size) then
					checkSpeed(radar.limit)
				end
			end
		end
	end
end)
if (Config.Debug) then
	Citizen.CreateThread(function()
		while true do
			Citizen.Wait(0)
			for k,v in pairs(Config.Radars) do
				local color = {
					r = 0,
					g = 108,
					b = 255
				}
				DrawMarker(1, v.pos.x, v.pos.y, v.pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.size, v.size, v.size, color.r, color.g, color.b, 100, false, true, 2, false, false, false, false)
			end
		end
	end)
end

function checkSpeed(limit)
	local pP = GetPlayerPed(-1)
	local speed = GetEntitySpeed(pP)
	local playerPos = GetEntityCoords(pP, true)
	local vehicle = GetVehiclePedIsIn(pP, false)
	local isRobbed = false
	local driver = GetPedInVehicleSeat(vehicle, -1)
	local plate = GetVehicleNumberPlateText(vehicle)
	local mphspeed = math.floor(speed * 3.6 + 0.5)
	local marge = math.floor(limit * (Config.Marge / 100))
	local street = Citizen.InvokeNative( 0x2EB41072B4C1E4C0, playerPos.x, playerPos.y, playerPos.z, Citizen.PointerValueInt(), Citizen.PointerValueInt() )
	if mphspeed > limit + marge and driver == pP then
		TriggerEvent('esx:showNotification', "Vous venez d'être flashé à ~r~"..mphspeed.." ~w~km/h, vitesse limitée a ~r~"..limit.." ~w~km/h")
		local numPas = GetVehicleMaxNumberOfPassengers(vehicle) - 1
		local pedsincar = {}
		for i = -1, numPas do
	      	if not IsVehicleSeatFree(vehicle, i) then
		        local pasInSeat = GetPedInVehicleSeat(vehicle, i)
		        table.insert(pedsincar, GetPlayerServerId(NetworkGetPlayerIndexFromPed(pasInSeat)))
	      	end
	    end
	    TriggerServerEvent("esx_radars:sendFlashToPassengers", mphspeed, pedsincar)
		if ((TableHasValue(Config.AllowedVehicles, GetDisplayNameFromVehicleModel(ESX.Game.GetVehicleProperties(vehicle).model)) and IsVehicleSirenOn(vehicle)) or TableHasValue(Config.AllowedImmatriculations, ESX.Game.GetVehicleProperties(vehicle).plate)) then
			Citizen.Wait(300)
			TriggerEvent('esx:showNotification', "~g~Vehicule autorisé à dépassé les limitations")
		else
			ESX.TriggerServerCallback("esx_radars:payfine", function(isrobbed)
				isRobbed = isrobbed
				local displayName
				local vehicleName = GetDisplayNameFromVehicleModel(ESX.Game.GetVehicleProperties(vehicle).model)
				if (GetLabelText(vehicleName) == "NULL") then
					displayName = vehicleName
				else
					displayName = GetLabelText(vehicleName)
				end
				TriggerServerEvent("esx_radars:sendflashedtopolice", plate, mphspeed, limit, GetStreetNameFromHashKey(street), displayName, isRobbed)
			end, plate, mphspeed, limit)
			--TriggerServerEvent("esx_radars:payfine", plate, mphspeed, limit)
		end
		Citizen.Wait(3000)
	end
end

RegisterNetEvent('esx_radars:playerIsInFlashedVehicle')
AddEventHandler('esx_radars:playerIsInFlashedVehicle', function(driver, speed)
	local player = GetPlayerPed(-1)
	StartScreenEffect("DeathFailNeutralIn", 150, false)
	Citizen.Wait(175)
	StartScreenEffect("DeathFailNeutralIn", 150, false)
	Citizen.Wait(500)
	if (GetPlayerServerId(NetworkGetPlayerIndexFromPed(player)) ~= driver) then
		TriggerEvent('esx:showNotification', "Le conducteur viens d'être flasher à ~r~"..speed.." ~w~km/h")
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
end)