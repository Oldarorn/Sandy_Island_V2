RegisterNetEvent("esx_alerts:policeWarrant")
AddEventHandler("esx_alerts:policeWarrant", function(message)
	SendNUIMessage({display = true, alerttype = "police", text = message})
	Citizen.Wait(15000)
	SendNUIMessage({display = false, alerttype = "police"})
end)

RegisterNetEvent("esx_alerts:sheriffWarrant")
AddEventHandler("esx_alerts:sheriffWarrant", function(message)
	SendNUIMessage({display = true, alerttype = "sheriff", text = message})
	Citizen.Wait(15000)
	SendNUIMessage({display = false, alerttype = "sheriff"})
end)