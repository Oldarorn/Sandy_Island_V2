ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function GetTime()
	return os.date("%I:%M:%S")
end

AddEventHandler("playerDropped", function(reason)
	local _source = source
	local message = "[Logs] - ".. GetTime()

	if (reason == "Quit: Disconnected." or reason == "Disconnected.") then
		message = message .. GetPlayerName(_source) .. " s'est volontairement deconnecte"
	elseif (reason == "Quit: Exiting." or reason == "Exiting") then
		message = message .. GetPlayerName(_source) .. " a volontairement quitter le jeu"
	elseif (reason == "Timed out after 15 seconds." or reason == "Timed out after 10 seconds.") then
		message = message .. GetPlayerName(_source) .. " a Timed Out"
	else
		message = message .. GetPlayerName(_source) .. " a été déconnecte, raison inconnue : " .. reason
	end
	print(message)
end)

AddEventHandler("esx_logs:playerConnected", function(name)
	print(GetTime() .. " - " .. name .." Connected")
end)

AddEventHandler("baseevents:onPlayerKilled", function(killer, killerInfos)
	local _source = source
	local KilledName = GetPlayerName(_source)
	local KillerName = GetPlayerName(killer)

	print("[Kill] => " .. KilledName .. " has been killed by " .. KillerName)
end)
