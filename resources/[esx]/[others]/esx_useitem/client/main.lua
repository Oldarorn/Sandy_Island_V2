local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}

local GUI                     = {}
GUI.Time                      = 0
ESX                           = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

-----------------------------------------------------------------------------------------------
----------------------------------------------- GPS -------------------------------------------
-----------------------------------------------------------------------------------------------
RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer

  TriggerEvent('esx_useitem:removeGPS')

  for i=1, #PlayerData.inventory, 1 do
    if PlayerData.inventory[i].name == 'gps' then
      if PlayerData.inventory[i].count > 0 then
        if (IsPedSittingInAnyVehicle(GetPlayerPed(-1))) then
          TriggerEvent('esx_useitem:addGPS')
        end
      end
    end
  end
end)

RegisterNetEvent('esx_useitem:addGPS')
AddEventHandler('esx_useitem:addGPS', function()
  DisplayRadar(true)
end)

RegisterNetEvent('esx_useitem:removeGPS')
AddEventHandler('esx_useitem:removeGPS', function()
  DisplayRadar(false)
end)

-----------------------------------------------------------------------------------------------
--------------------------------------- Tenus Hasmat ------------------------------------------
-----------------------------------------------------------------------------------------------
-- Jaune
RegisterNetEvent('esx_objects:settenuehaz3')
AddEventHandler('esx_objects:settenuehaz3', function()
  if UseTenu then

    TriggerEvent('skinchanger:getSkin', function(skin)

        if skin.sex == 0 then
            local clothesSkin = {
                ['tshirt_1']  = 62, ['tshirt_2']  = 2,
                ['torso_1']   = 67, ['torso_2']   = 2,
                ['decals_1']  = 0,  ['decals_2']  = 0,
                ['mask_1']    = 46, ['mask_2']  = 0,
                ['arms']    = 38,
                ['pants_1']   = 40, ['pants_2']   = 2,
                ['shoes_1']   = 25, ['shoes_2']   = 0,
                ['helmet_1']  = -1, ['helmet_2']  = 0,
                ['bags_1']    = 44, ['bags_2']  = 0,
                ['bproof_1']  = 0,  ['bproof_2']  = 0
            }
            TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
        else
            local clothesSkin = {
                ['tshirt_1']  = 43, ['tshirt_2']  = 2,
                ['torso_1']   = 61, ['torso_2']   = 2,
                ['decals_1']  = 0,  ['decals_2']  = 0,
                ['mask_1']    = 46, ['mask_2']  = 0,
                ['arms']    = 101,
                ['pants_1']   = 40, ['pants_2']   = 2,
                ['shoes_1']   = 25, ['shoes_2']   = 0,
                ['helmet_1']  = -1, ['helmet_2']  = 0,
                ['bags_1']    = 44, ['bags_2']  = 0,
                ['bproof_1']  = 0,  ['bproof_2']  = 0
            }
            TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
          end
          local playerPed = GetPlayerPed(-1)
        ClearPedBloodDamage(playerPed)
        ResetPedVisibleDamage(playerPed)
        ClearPedLastWeaponDamage(playerPed)
      end)
  else

    TriggerEvent('skinchanger:getSkin', function(skin)

      ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, hasSkin)

        if hasSkin then

          TriggerEvent('skinchanger:loadSkin', skin)
          TriggerEvent('esx:restoreLoadout')
        end
      end)
    end)
  end
  UseTenu  = not UseTenu
  GUI.Time = GetGameTimer()
end)

--Tenue Jailer
RegisterNetEvent('esx_objects:settenuejailer')
AddEventHandler('esx_objects:settenuejailer', function()
  if UseTenu then

    TriggerEvent('skinchanger:getSkin', function(skin)

      if skin.sex == 0 then

        local clothesSkin = {
          ['tshirt_1'] = 15, ['tshirt_2'] = 0,
          ['torso_1'] = 146, ['torso_2'] = 0,
          ['decals_1'] = 0, ['decals_2'] = 0,
          ['arms'] = 0,
          ['pants_1'] = 3, ['pants_2'] = 7,
          ['shoes_1'] = 12, ['shoes_2'] = 12,
          ['chain_1'] = 0, ['chain_2'] = 0
        }
        TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
      else

        local clothesSkin = {
          ['tshirt_1'] = 3, ['tshirt_2'] = 0,
          ['torso_1'] = 38, ['torso_2'] = 3,
          ['decals_1'] = 0, ['decals_2'] = 0,
          ['arms'] = 2,
          ['pants_1'] = 3, ['pants_2'] = 15,
          ['shoes_1'] = 66, ['shoes_2'] = 5,
          ['chain_1'] = 0, ['chain_2'] = 0
        }
        TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
      end

			local playerPed = GetPlayerPed(-1)
			ClearPedBloodDamage(playerPed)
			ResetPedVisibleDamage(playerPed)
			ClearPedLastWeaponDamage(playerPed)
			ResetPedMovementClipset(playerPed, 0)
		end)
  else

    TriggerEvent('skinchanger:getSkin', function(skin)

      ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, hasSkin)

        if hasSkin then

          TriggerEvent('skinchanger:loadSkin', skin)
          TriggerEvent('esx:restoreLoadout')
        end
      end)
    end)
  end
  UseTenu  = not UseTenu
  GUI.Time = GetGameTimer()
end)

-- EMS Intervention
RegisterNetEvent('esx_objects:setTenueEms')
AddEventHandler('esx_objects:setTenueEms', function()
  if UseTenu then

    TriggerEvent('skinchanger:getSkin', function(skin)

      if skin.sex == 0 then
        local clothesSkin = {
          ['tshirt_1']  = 15, ['tshirt_2']  = 0,
          ['torso_1']   = 249, ['torso_2']   = 1,
          ['decals_1']  = 57,  ['decals_2']  = 0,
          ['arms']      = 79,
          ['pants_1']   = 96, ['pants_2']   = 1,
          ['shoes_1']   = 54, ['shoes_2']   = 0,
          ['helmet_1']  = 121, ['helmet_2']  = 0,
          ['chain_1']    = 126, ['chain_2']    = 0
        }
        TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
      else
        local clothesSkin = {
          ['tshirt_1']  = 8, ['tshirt_2']  = 0,
          ['torso_1']   = 257, ['torso_2']   = 1,
          ['decals_1']  = 65,  ['decals_2']  = 0,
          ['arms']      = 104,
          ['pants_1']   = 99, ['pants_2']   = 1,
          ['shoes_1']   = 3, ['shoes_2']   = 5,
          ['helmet_1']  = 121, ['helmet_2']  = 1,
          ['chain_1']    = 96, ['chain_2']    = 0
        }
        TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
      end
      local playerPed = GetPlayerPed(-1)
      ClearPedBloodDamage(playerPed)
      ResetPedVisibleDamage(playerPed)
      ClearPedLastWeaponDamage(playerPed)
    end)
  else

    TriggerEvent('skinchanger:getSkin', function(skin)

      ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, hasSkin)

        if hasSkin then

          TriggerEvent('skinchanger:loadSkin', skin)
          TriggerEvent('esx:restoreLoadout')
        end
      end)
    end)
  end
  UseTenu  = not UseTenu
  GUI.Time = GetGameTimer()
end)

------------------------------------------------------------------------------------
------------------------------------ Use ROSE ------------------------------------
------------------------------------------------------------------------------------
RegisterNetEvent('esx_useitem:rose')
AddEventHandler('esx_useitem:rose', function()

  local playerPed = GetPlayerPed(-1)
  local coords    = GetEntityCoords(playerPed)

  Citizen.CreateThread(function()

    local playerPed = GetPlayerPed(-1)
    local coords    = GetEntityCoords(playerPed)
    local boneIndex = GetPedBoneIndex(playerPed, 57005)

    RequestAnimDict('amb@code_human_wander_drinking@beer@male@base')

    while not HasAnimDictLoaded('amb@code_human_wander_drinking@beer@male@base') do
      Citizen.Wait(0)
    end

    ESX.Game.SpawnObject('p_single_rose_s', {
      x = coords.x,
      y = coords.y,
      z = coords.z + 2
    }, function(object)

      Citizen.CreateThread(function()

        AttachEntityToEntity(object, playerPed, boneIndex, 0.10, 0, -0.001, 80.0, 150.0, 200.0, true, true, false, true, 1, true)
        TaskPlayAnim(playerPed, "amb@code_human_wander_drinking@beer@male@base", "static", 3.5, -8, -1, 49, 0, 0, 0, 0)
        Citizen.Wait(30000)
        DeleteObject(object)
        ClearPedSecondaryTask(playerPed)
      end)
    end)
  end)
end)

------------------------------------------------------------------------------------
----------------------------------- Use Chargeur -----------------------------------
------------------------------------------------------------------------------------
RegisterNetEvent('esx_useitem:clip')
AddEventHandler('esx_useitem:clip', function()
  ped = GetPlayerPed(-1)
    if IsPedArmed(ped, 4) then
      hash = GetSelectedPedWeapon(ped)
      if hash ~= nil then
        TriggerServerEvent('esx_useitem:clipUsed')
        AddAmmoToPed(GetPlayerPed(-1), hash, GetWeaponClipSize(hash))
      end
    end
end)

------------------------------------------------------------------------------------
----------------------------------- Bullet Proof -----------------------------------
------------------------------------------------------------------------------------
function AddArmor()
  SetPedArmour(GetPlayerPed(-1), 100)
end

RegisterNetEvent('esx_useitem:bulletproof')
AddEventHandler('esx_useitem:bulletproof', function()
  AddArmor()
  TriggerEvent('skinchanger:getSkin', function(skin)
    if skin.sex == 0 then
      TriggerEvent('skinchanger:loadClothes', skin, {['bproof_1'] = 11,  ['bproof_2'] = 1})
    else
      TriggerEvent('skinchanger:loadClothes', skin, {['bproof_1'] = 13,  ['bproof_2'] = 1})
    end
  end)
end)

RegisterNetEvent('esx_useitem:police_bulletproof')
AddEventHandler('esx_useitem:police_bulletproof', function()
  AddArmor()
  TriggerEvent('skinchanger:getSkin', function(skin)
    if skin.sex == 0 then
      TriggerEvent('skinchanger:loadClothes', skin, {['bproof_1'] = 11,  ['bproof_2'] = 1})
    else
      TriggerEvent('skinchanger:loadClothes', skin, {['bproof_1'] = 13,  ['bproof_2'] = 1})
    end
  end)
end)

RegisterNetEvent('esx_useitem:sheriff_bulletproof')
AddEventHandler('esx_useitem:sheriff_bulletproof', function()
  AddArmor()
  TriggerEvent('skinchanger:getSkin', function(skin)
    if skin.sex == 0 then
      TriggerEvent('skinchanger:loadClothes', skin, {['bproof_1'] = 20,  ['bproof_2'] = 7})
    else
      TriggerEvent('skinchanger:loadClothes', skin, {['bproof_1'] = 20,  ['bproof_2'] = 7})
    end
  end)
end)


function ShowHospitalListingMenu(data)

  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'Hospital Listing',
    {
      title    = "Tenue",
      elements = {
        {label = "Vétements Personnel", value = 'citizen_wear'},
        {label = "Vétements hopital",   value = 'hospi_wear'},
      },
    },
    function(data, menu)
      menu.close()

      if data.current.value == 'citizen_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jailSkin)
          TriggerEvent('skinchanger:loadSkin', skin)
        end)
      elseif data.current.value == 'hospi_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jailSkin)
          if skin.sex == 0 then
            SetPedComponentVariation(GetPlayerPed(-1), 3, 15, 0, 0)--Gants
            SetPedComponentVariation(GetPlayerPed(-1), 4, 61, 4, 0)--Jean
            SetPedComponentVariation(GetPlayerPed(-1), 6, 34, 0, 0)--Chaussure
            SetPedComponentVariation(GetPlayerPed(-1), 11, 15, 0, 0)--Veste
            SetPedComponentVariation(GetPlayerPed(-1), 8, 15, 0, 0)--Tshirt
          elseif skin.sex == 1 then
            SetPedComponentVariation(GetPlayerPed(-1), 3, 4, 0, 0)--Gants
            SetPedComponentVariation(GetPlayerPed(-1), 4, 57, 0, 0)--Jean
            SetPedComponentVariation(GetPlayerPed(-1), 6, 35, 0, 0)--Chaussure
            SetPedComponentVariation(GetPlayerPed(-1), 11, 105, 0, 0)--Veste
            SetPedComponentVariation(GetPlayerPed(-1), 8, 8, 0, 0)--Tshirt
          else
            TriggerEvent('skinchanger:loadClothes', skin, jailSkin.skin_female)
          end
        end)
      end
      CurrentAction     = 'hospi_suits'
      CurrentActionMsg  = "open_menu"
      CurrentActionData = {}
    end,
    function(data, menu)
      menu.close()
    end
  )
end

AddEventHandler('esx_useitem:hasEnteredMarker', function(zone)

  ESX.UI.Menu.CloseAll()

  for i=1, #Config.Hospital, 1 do
    if zone == 'Hospital' .. i then
      CurrentAction     = 'hospi_suits'
      CurrentActionMsg  = 'BlaBla'
      CurrentActionData = {}
    end
  end
end)

AddEventHandler('esx_useitem:hasExitedMarker', function(zone)
  CurrentAction = nil
  ESX.UI.Menu.CloseAll()
end)

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    local coords = GetEntityCoords(GetPlayerPed(-1))

    for k,v in pairs(Config.Zones) do
      if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
        DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
      end
    end
  end
end)

-- Activate menu when player is inside marker
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    local coords      = GetEntityCoords(GetPlayerPed(-1))
    local isInMarker  = false
    local currentZone = nil

    for k,v in pairs(Config.Zones) do
      if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x / 2) then
        isInMarker  = true
        currentZone = k
      end
    end

    if isInMarker and not HasAlreadyEnteredMarker then
      HasAlreadyEnteredMarker = true
      LastZone                = currentZone
      TriggerEvent('esx_useitem:hasEnteredMarker', currentZone)
    end

    if not isInMarker and HasAlreadyEnteredMarker then
      HasAlreadyEnteredMarker = false
      TriggerEvent('esx_useitem:hasExitedMarker', LastZone)
    end
  end
end)

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if CurrentAction ~= nil then
      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)
      if IsControlJustReleased(0, 38) then

        if CurrentAction == 'hospi_suits' then
          ShowHospitalListingMenu()
        end
        CurrentAction = nil
      end
    end
  end
end)
