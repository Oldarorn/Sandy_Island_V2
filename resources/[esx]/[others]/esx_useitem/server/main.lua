ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

------------------------------------
---------------- GPS ---------------
------------------------------------
AddEventHandler('esx:onAddInventoryItem', function(source, item, count)
	if item.name == 'gps' then
		TriggerClientEvent('esx_useitem:addGPS', source)
	end
end)

AddEventHandler('esx:onRemoveInventoryItem', function(source, item, count)
	if item.name == 'gps' and item.count < 1 then
		TriggerClientEvent('esx_useitem:removeGPS', source)
	end
end)

---------------------------
--------- Tenues ----------
---------------------------
--HAZMAT BREAKINGBAD--
ESX.RegisterUsableItem('hazmat3', function(source)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
    
  TriggerClientEvent('esx_objects:settenuehaz3', _source)
end)

-- EMS --
ESX.RegisterUsableItem('ems_suits', function(source)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
    
  TriggerClientEvent('esx_objects:setTenueEms', _source)
end)

------------------------------------
---------- Utiliser ROSE -----------
------------------------------------
ESX.RegisterUsableItem('rose', function(source)

	TriggerClientEvent('esx_useitem:rose', source)
end)

------------------------------------
---------- Bullet Proof ------------
------------------------------------
ESX.RegisterUsableItem('bulletproof', function(source)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  xPlayer.removeInventoryItem('bulletproof', 1)
  TriggerClientEvent('esx_useitems:bulletproof', _source)
end)

ESX.RegisterUsableItem('police_bulletproof', function(source)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  xPlayer.removeInventoryItem('police_bulletproof', 1)
  TriggerClientEvent('esx_useitems:police_bulletproof', _source)
end)

ESX.RegisterUsableItem('sheriff_bulletproof', function(source)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  xPlayer.removeInventoryItem('sheriff_bulletproof', 1)
  TriggerClientEvent('esx_useitems:sheriff_bulletproof', _source)
end)

------------------------------------
---------- Use Chargeur ------------
------------------------------------
ESX.RegisterUsableItem('clip', function(source)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  TriggerClientEvent('esx_useitems:clip', _source)
end)

RegisterNetEvent('esx_useitems:clipUsed')
AddEventHandler('esx_useitems:clipUsed', function()
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  xPlayer.removeInventoryItem('clip')
end)