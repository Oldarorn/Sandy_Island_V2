Config              = {}
Config.DrawDistance = 100.0
Config.Locale       = 'fr'
Config.Zones        = {}

Config.Hospital = {
	{x=249.335, y=-1358.8557, x=23.537},
	{x=260.194, y=-1350.128, x=23.537},
}

for i=1, #Config.Hospital, 1 do
  Config.Zones['Hospital' .. i] = {
    Pos   = Config.Hospital[i],
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Type  = 27,
    Color = {r = 153, g = 51, b = 255},
  }
end

