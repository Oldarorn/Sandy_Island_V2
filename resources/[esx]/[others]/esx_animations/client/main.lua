local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX               = nil
local handsup     = false
local keyPressed  = false
local once        = true
local oldval      = false
local oldvalped   = false
local crouched    = false
local proned      = false
crouchKey         = 36
proneKey          = 164
local isDead      = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function startAttitude(lib, anim)
 	Citizen.CreateThread(function()

    local playerPed = GetPlayerPed(-1)
    RequestAnimSet(anim)

    while not HasAnimSetLoaded(anim) do
      Citizen.Wait(0)
    end
    SetPedMovementClipset(playerPed, anim, true)
	end)
end

function startAnim(lib, anim)

	Citizen.CreateThread(function()

	  RequestAnimDict(lib)

	  while not HasAnimDictLoaded( lib) do
	    Citizen.Wait(0)
	  end

	  TaskPlayAnim(GetPlayerPed(-1), lib ,anim ,8.0, -8.0, -1, 0, 0, false, false, false )
	end)
end

function startScenario(anim)
  TaskStartScenarioInPlace(GetPlayerPed(-1), anim, 0, false)
end

function OpenAnimationsMenu()

	local elements = {}

  table.insert(elements, {label = "Faire pipi", value = "pee"})

	for i=1, #Config.Animations, 1 do
		table.insert(elements, {label = Config.Animations[i].label, value = Config.Animations[i].name})
	end

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'animations',
		{
			title    = 'Animations',
			elements = elements
		},
		function(data, menu)
      if data.current.value == "pee" then
        TriggerEvent('esx_status:set', 'needs', 0)
        TriggerEvent('esx_basicneeds:onPee', s)
      else
			  OpenAnimationsSubMenu(data.current.value)
		  end
    end,
		function(data, menu)
			menu.close()
		end
	)
end

function OpenAnimationsSubMenu(menu)

	local title    = nil
	local elements = {}

	for i=1, #Config.Animations, 1 do

		if Config.Animations[i].name == menu then

			title = Config.Animations[i].label

			for j=1, # Config.Animations[i].items, 1 do
				table.insert(elements, {label = Config.Animations[i].items[j].label, type = Config.Animations[i].items[j].type, value = Config.Animations[i].items[j].data})
			end
			break
		end
	end

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'animations_sub',
		{
			title    = title,
			elements = elements
		},
		function(data, menu)

			local type = data.current.type
			local lib  = data.current.value.lib
			local anim = data.current.value.anim

			if type == 'scenario' then
				startScenario(anim)
			else
				if type == 'attitude' then
					startAttitude(lib, anim)
				else
					startAnim(lib, anim)
				end
			end
		end,
		function(data, menu)
			menu.close()
		end
	)
end

function SetProned()
  ped = PlayerPedId()
  ClearPedTasksImmediately(ped)
  TaskPlayAnimAdvanced(ped, "move_crawl", "onfront_fwd", GetEntityCoords(ped), 0.0, 0.0, GetEntityHeading(ped), 1.0, 1.0, 1.0, 46, 1.0, 0, 0)
end

function ProneMovement()
  if proned then
    ped = PlayerPedId()

    if IsControlJustPressed(0, 32) and not movefwd then
      movefwd = true
      TaskPlayAnimAdvanced(ped, "move_crawl", "onfront_fwd", GetEntityCoords(ped), 1.0, 0.0, GetEntityHeading(ped), 1.0, 1.0, 1.0, 47, 1.0, 0, 0)
    elseif IsControlJustReleased(0, 32) and movefwd then
      TaskPlayAnimAdvanced(ped, "move_crawl", "onfront_fwd", GetEntityCoords(ped), 1.0, 0.0, GetEntityHeading(ped), 1.0, 1.0, 1.0, 46, 1.0, 0, 0)
      movefwd = false
    end

    if IsControlJustPressed(0, 33) and not movebwd then
      movebwd = true
      TaskPlayAnimAdvanced(ped, "move_crawl", "onfront_bwd", GetEntityCoords(ped), 1.0, 0.0, GetEntityHeading(ped), 1.0, 1.0, 1.0, 47, 1.0, 0, 0)
    elseif IsControlJustReleased(0, 33) and movebwd then
      TaskPlayAnimAdvanced(ped, "move_crawl", "onfront_bwd", GetEntityCoords(ped), 1.0, 0.0, GetEntityHeading(ped), 1.0, 1.0, 1.0, 46, 1.0, 0, 0)
      movebwd = false
    end

    if IsControlPressed(0, 34) then
      SetEntityHeading(ped, GetEntityHeading(ped)+2.0 )
    elseif IsControlPressed(0, 35) then
      SetEntityHeading(ped, GetEntityHeading(ped)-2.0 )
    end
  end
end

-- Key Controls
Citizen.CreateThread(function()
  while true do
    local ped  = GetPlayerPed(-1)
    local dict = "missminuteman_1ig_2"

    RequestAnimDict(dict)
    while not HasAnimDictLoaded(dict) do
      Citizen.Wait(100)
    end
    Citizen.Wait(1)

	  if IsControlJustReleased(1, Keys["F3"]) and not isDead then
	  	OpenAnimationsMenu()
	  end

	  if IsControlJustReleased(1, Keys["X"]) and not isDead then
	  	ClearPedTasks(GetPlayerPed(-1))
      ResetPedMovementClipset(GetPlayerPed(-1), 0.0)
	  end

    if IsControlJustPressed(1, Keys["X"]) and not isDead then
      if not handsup then
        TaskPlayAnim(GetPlayerPed(-1), dict, "handsup_enter", 8.0, 8.0, -1, 50, 0, false, false, false)
        handsup = true
      else
        handsup = false
        ClearPedTasks(GetPlayerPed(-1))
      end
    end

    if ( DoesEntityExist( ped ) and not IsEntityDead( ped ) ) then
      ProneMovement()
      DisableControlAction( 0, proneKey, true )
      DisableControlAction( 0, crouchKey, true )
      if ( not IsPauseMenuActive() ) then
        if ( IsDisabledControlJustPressed( 0, crouchKey ) and not proned ) then
          RequestAnimSet( "move_ped_crouched" )
          RequestAnimSet("MOVE_M@TOUGH_GUY@")

          while ( not HasAnimSetLoaded( "move_ped_crouched" ) ) do
            Citizen.Wait( 100 )
          end
          while ( not HasAnimSetLoaded( "MOVE_M@TOUGH_GUY@" ) ) do
            Citizen.Wait( 100 )
          end
          if ( crouched and not proned ) then
            ResetPedMovementClipset( ped )
            ResetPedStrafeClipset(ped)
            SetPedMovementClipset( ped,"MOVE_M@TOUGH_GUY@", 0.5)
            crouched = false
          elseif ( not crouched and not proned ) then
            SetPedMovementClipset( ped, "move_ped_crouched", 0.55 )
            SetPedStrafeClipset(ped, "move_ped_crouched_strafing")
            crouched = true
          end
        elseif ( IsDisabledControlJustPressed(0, proneKey) and not crouched and not IsPedInAnyVehicle(ped, true) and not IsPedFalling(ped) and not IsPedDiving(ped) and not IsPedInCover(ped, false) and not IsPedInParachuteFreeFall(ped) and (GetPedParachuteState(ped) == 0 or GetPedParachuteState(ped) == -1) ) then
          if proned then
            ClearPedTasksImmediately(ped)
            proned = false
          elseif not proned then
            RequestAnimSet( "move_crawl" )
            while ( not HasAnimSetLoaded( "move_crawl" ) ) do
              Citizen.Wait( 100 )
            end
            ClearPedTasksImmediately(ped)
            proned = true
            if IsPedSprinting(ped) or IsPedRunning(ped) or GetEntitySpeed(ped) > 5 then
              TaskPlayAnim(ped, "move_jump", "dive_start_run", 8.0, 1.0, 1, 0, 0.0, 0, 0, 0)
              Citizen.Wait(1000)
            end
            SetProned()
          end
        end
      end
    else
      proned = false
      crouched = false
    end

    if once then
      once = false
    end
  end
end)

AddEventHandler('esx:onPlayerDeath', function()
  isDead = true
end)

AddEventHandler('playerSpawned', function(spawn)
 isDead = false
end)
