local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end

	while true do
		Citizen.Wait(5)
		if IsControlPressed(0, Keys['F10']) then
			OpenSearchMenu()
		end
	end
end)

function OpenSearchMenu()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'search',
		{
			title = "Fouille",
			align = 'top-left',
			elements = {
				{label = "Fouiller", value = "search"}
			}
		},
		function(data, menu)
			if data.current.value == "search" then
				local player, distance = ESX.Game.GetClosestPlayer()
				if distance ~= -1 and distance <= 2.0 then
					ESX.TriggerServerCallback('esx_search:getOtherPlayerData', function(data)

				    local elements = {}

				    local blackMoney = 0

				    table.insert(elements, {
				    	label = "Prendre le cash: $"..data.money,
				    	value = 'money',
				    	itemType = 'item_money',
				    	amount = data.money
				    })

				    for i=1, #data.accounts, 1 do
				      if data.accounts[i].name == 'black_money' then
				        blackMoney = data.accounts[i].money
				      end
				    end

				    table.insert(elements, {
				      label          = "Prendre l'argent sale : $" .. blackMoney,
				      value          = 'black_money',
				      itemType       = 'item_account',
				      amount         = blackMoney
				    })

				    table.insert(elements, {label = '--- Armes ---', value = nil})

				    for i=1, #data.weapons, 1 do
				      table.insert(elements, {
				        label          = "Prendre " .. ESX.GetWeaponLabel(data.weapons[i].name),
				        value          = data.weapons[i].name,
				        itemType       = 'item_weapon',
				        amount         = data.ammo,
				      })
				    end

				    table.insert(elements, {label = '--- Inventaire ---', value = nil})

				    for i=1, #data.inventory, 1 do
				      if data.inventory[i].count > 0 then
				        table.insert(elements, {
				          label          = "Prendre " .. data.inventory[i].count .. ' ' .. data.inventory[i].label,
				          value          = data.inventory[i].name,
				          itemType       = 'item_standard',
				          amount         = data.inventory[i].count,
				        })
				      end
				    end


				    ESX.UI.Menu.Open(
				      'default', GetCurrentResourceName(), 'body_search',
				      {
				        title    = "Fouille",
				        align    = 'top-left',
				        elements = elements,
				      },
				      function(data, menu)

				        local itemType = data.current.itemType
				        local itemName = data.current.value
				        local amount   = data.current.amount

				        if data.current.value ~= nil then

				          TriggerServerEvent('esx_search:confiscatePlayerItem', GetPlayerServerId(player), itemType, itemName, amount)

				          OpenSearchMenu(player)

				        end

				      end,
				      function(data, menu)
				        menu.close()
				      end
				    )

				  end, GetPlayerServerId(player))
				else
					ESX.ShowNotification("Aucun joueur a proximité")
				end
			end
		end,
	function(data, menu)
		menu.close()
	end)
end