ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback("esx_drivelicenses:getLicenses", function(source, cb, player)
	local xPlayer = ESX.GetPlayerFromId(player)
	MySQL.Async.FetchAll("SELECT * FROM user_drive_licenses WHERE owner = @owner", {
		['@owner'] = xPlayer.identifier
	}, function(result)
		cb(result)
	end)
end)

RegisterNetEvent("esx_drivelicenses:addLicense")
AddEventHandler("esx_drivelicenses:addLicense", function(type)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	MySQL.Async.Execute("INSERT INTO user_drive_licenses (owner, type, points) VALUES (@owner, @type, @points)", {
		["@owner"] = xPlayer.identifier,
		["@type"] = type,
		["@points"] = 12
	})
end)

RegisterNetEvent("esx_drivelicenses:removeLicense")
AddEventHandler("esx_drivelicenses:removeLicense", function(type, player)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(player)
	MySQL.Async.Execute("DELETE FROM user_drive_licenses WHERE owner = @owner AND type = @type", {
		["@owner"] = xPlayer.identifier,
		["@type"] = type,
	})
	TriggerClientEvent("esx:showNotification", player, "Votre permis a été ~r~supprimer")
end)

ESX.RegisterServerCallback("esx_drivelicenses:removePoints", function(source, cb, type, number, player)
	local xPlayer = ESX.GetPlayerFromId(player)
	MySQL.Sync.FetchAll("SELECT points FROM user_drive_licenses WHERE owner = @owner AND type = @type", {
		["@owner"] = xPlayer.identifier,
		["@type"] = type
	}, function(result)
		if result ~= nil and result[1].points - points > 0 then
			MySQL.Async.Execute("UPDATE user_drive_licenses SET points = points - @points WHERE type = @type AND owner = @owner", {
				["@owner"] = xPlayer.identifier,
				["@type"] = type,
				["@points"] = number
			})
			TriggerClientEvent("esx:showNotification", player, "Vous venez de perdre ~r~"..number.." points")
			cb(true)
		elseif result ~= nil and result[1].points - points <= 0 then
			MySQL.Async.Execute("DELETE FROM user_drive_licenses WHERE owner = @owner and type = @type", {
				["@owner"] = xPlayer.identifier,
				["@type"] = type,
			})
			TriggerClientEvent("esx:showNotification", player, "Votre permis a été ~r~supprimer~w~ par manque de point")
			cb("removed")
		else
			cb(false)
		end
	end)
end)