ESX = nil
Hiden = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end
end)

RegisterNetEvent('esx_hidehud:hideHUD')
AddEventHandler('esx_hidehud:hideHUD', function()
	ESX.UI.HUD.SetDisplay(0.0)
	Hiden = true
end)

RegisterNetEvent('esx_hidehud:showHUD')
AddEventHandler('esx_hidehud:showHUD', function()
	ESX.UI.HUD.SetDisplay(1.0)
	Hiden = false
end)

Citizen.CreateThread(function()
	while true do
		if Hiden then
			HideHudComponentThisFrame(2)
			HideHudComponentThisFrame(3)
			HideHudComponentThisFrame(4)
			HideHudComponentThisFrame(5)
			HideHudComponentThisFrame(6)
			HideHudComponentThisFrame(7)
			HideHudComponentThisFrame(8)
			HideHudComponentThisFrame(9)
			HideHudComponentThisFrame(13)
			HideHudComponentThisFrame(14)
			HideHudComponentThisFrame(19)
			HideHudAndRadarThisFrame()
		end
		Citizen.Wait(1)	
	end
end)