ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

TriggerEvent('es:addGroupCommand', 'hidehud', 'user', function(source, args, user)
	TriggerClientEvent('esx_hidehud:hideHUD', source)
	TriggerClientEvent('esx_status:setDisplay', source, 0.0)
end, {help = "HIDE HUD"})

TriggerEvent('es:addGroupCommand', 'showhud', 'user', function(source, args, user)
	TriggerClientEvent('esx_hidehud:showHUD', source)
	TriggerClientEvent('esx_status:setDisplay', source, 1.0)
end, {help = "SHOW HUD"})