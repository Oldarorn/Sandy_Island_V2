Citizen.CreateThread(function()
	local ped = GetPlayerPed(-1)
    while true do
        local SelectedWeapon = GetSelectedPedWeapon(ped)
        if SelectedWeapon == 100416529 or SelectedWeapon == 205991906 or SelectedWeapon == 3342088282 or SelectedWeapon == 177293209 then
        	HideHudComponentThisFrame(14)
        end
        Citizen.Wait(1)
    end
end)

-----------------------------------------------------------------
------------------ PNJ DROP WEAPON DESACTIVE --------------------
-----------------------------------------------------------------
local pedindex = {}

function SetWeaponDrops() -- This function will set the closest entity to you as the variable entity.
  local handle, ped = FindFirstPed()
  local finished = false -- FindNextPed will turn the first variable to false when it fails to find another ped in the index
  repeat

  if not IsEntityDead(ped) then
    pedindex[ped] = {}
  end

  finished, ped = FindNextPed(handle) -- first param returns true while entities are found
  until not finished
  EndFindPed(handle)

  for peds,_ in pairs(pedindex) do
    if peds ~= nil then -- set all peds to not drop weapons on death.
      SetPedDropsWeaponsWhenDead(peds, false)
    end
  end
end

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(100)
    SetWeaponDrops()
  end
end)

-----------------------------------------------------------------
--------------------------- NO SCOPE ----------------------------
-----------------------------------------------------------------
function ManageReticle()
  local ped = GetPlayerPed(-1)

  if (DoesEntityExist(ped) and not IsEntityDead(ped)) then
    local _, hash = GetCurrentPedWeapon( ped, true )

    if (hash ~= 3342088282 and hash ~=205991906 and hash~=100416529) then
      HideHudComponentThisFrame( 14 )
    end
  end
end

Citizen.CreateThread(function()
  while true do
    ManageReticle()
    Citizen.Wait(0)
  end
end)

-----------------------------------------------------------------
----------------------- No Weapon Reward ------------------------
-----------------------------------------------------------------
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
    DisablePlayerVehicleRewards(PlayerId())
  end
end)


local vehWeapons = {
  "WEAPON_PUMPSHOTGUN",
  "WEAPON_SNIPERRIFLE",
  "WEAPON_CARBINERIFLE",
}

local hasBeenInPoliceVehicle = false
local alreadyHaveWeapon = {}

Citizen.CreateThread(function()

  while true do
    Citizen.Wait(150)

    if(IsPedInAnyPoliceVehicle(GetPlayerPed(-1))) then
      if(not hasBeenInPoliceVehicle) then
        hasBeenInPoliceVehicle = true
      end
    else
      if(hasBeenInPoliceVehicle) then
        for i,k in pairs(vehWeapons) do
          if(not alreadyHaveWeapon[i]) then
            TriggerServerEvent("esx_various:VehWeapCheck",k)
          end
        end
        hasBeenInPoliceVehicle = false
      end
    end
  end
end)

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(300)

    if(not IsPedInAnyVehicle(GetPlayerPed(-1))) then
      if IsPedGettingIntoAVehicle(GetPlayerPed(-1)) then
        for i= 1, #vehWeapons do
          local ch = GetHashKey(vehWeapons[i])
          if(HasPedGotWeapon(GetPlayerPed(-1), ch, false)==1) then
            alreadyHaveWeapon[i] = true
          else
            alreadyHaveWeapon[i] = false
          end
        end
      end
    end
  end
end)

RegisterNetEvent("esx_various:VehWeapDel")
AddEventHandler("esx_various:VehWeapDel", function(weapon)
  RemoveWeaponFromPed(GetPlayerPed(-1), weapon)
end)

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)
    DisableControlAction(0, 140,  true) -- Melee Attack Alternate -- Touche R
    DisableControlAction(0, 263,  true) -- Input Melee Attack
    DisableControlAction(0, 264,  true) -- Input Melee Attack 2
  end
end)

-----------------------------------------------------------------
-------------------- DESACTIVE PNJ POLICE -----------------------
-----------------------------------------------------------------
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
    local myCoords = GetEntityCoords(GetPlayerPed(-1))
    ClearAreaOfCops(myCoords.x, myCoords.y, myCoords.z, 100.0, 0)
  end
end)

-----------------------------------------------------------------
------------------------- FIX TRAFFIC ---------------------------
-----------------------------------------------------------------
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)
    -- 1.
    SetVehicleDensityMultiplierThisFrame(0.6)
    SetPedDensityMultiplierThisFrame(0.6)
    --SetRandomVehicleDensityMultiplierThisFrame(1.0)
    --SetParkedVehicleDensityMultiplierThisFrame(1.0)
    --SetScenarioPedDensityMultiplierThisFrame(2.0, 2.0)

    --local playerPed = GetPlayerPed(-1)
    --local pos = GetEntityCoords(playerPed)
    --RemoveVehiclesFromGeneratorsInArea(pos['x'] - 900.0, pos['y'] - 900.0, pos['z'] - 900.0, pos['x'] + 900.0, pos['y'] + 900.0, pos['z'] + 900.0);

    -- 2.
    --SetGarbageTrucks(0)
    --SetRandomBoats(0)
    --SetRandomBus(0)
  end
end)

Citizen.CreateThread(function()

  for i=1, #Config.Map, 1 do

    local blip = AddBlipForCoord(Config.Map[i].x, Config.Map[i].y, Config.Map[i].z)
    SetBlipSprite (blip, Config.Map[i].id)
    SetBlipDisplay(blip, 4)
    SetBlipColour (blip, Config.Map[i].color)
    SetBlipAsShortRange(blip, true)

    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(Config.Map[i].name)
    EndTextCommandSetBlipName(blip)
  end
end)
