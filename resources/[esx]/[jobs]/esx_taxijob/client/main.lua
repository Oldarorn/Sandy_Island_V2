local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData                = {}
local GUI                       = {}
local HasAlreadyEnteredMarker   = false
local LastZone                  = nil
local CurrentAction             = nil
local CurrentActionMsg          = ''
local CurrentActionData         = {}
local OnJob                     = false
local CurrentCustomer           = nil
local CurrentCustomerBlip       = nil
local DestinationBlip           = nil

local IsNearCustomer            = false
local CustomerIsEnteringVehicle = false
local CustomerEnteredVehicle    = false
local TargetCoords              = nil
local IsDead                    = false
local InService                 = false

ESX                             = nil
GUI.Time                        = 0

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end

	Citizen.Wait(5000)
	PlayerData = ESX.GetPlayerData()
end)

function DrawSub(msg, time)
  ClearPrints()
  SetTextEntry_2("STRING")
  AddTextComponentString(msg)
  DrawSubtitleTimed(time, 1)
end

function ShowLoadingPromt(msg, time, type)
  Citizen.CreateThread(function()
    Citizen.Wait(1)
    N_0xaba17d7ce615adbf("STRING")
    AddTextComponentString(msg)
    N_0xbd12f8228410d9b4(type)
    Citizen.Wait(time)
    N_0x10d373323e5b9c0d()
  end)
end

function GetRandomWalkingNPC()

  local search = {}
  local peds   = ESX.Game.GetPeds()

  for i=1, #peds, 1 do
    if IsPedHuman(peds[i]) and IsPedWalking(peds[i]) and not IsPedAPlayer(peds[i]) then
      table.insert(search, peds[i])
    end
  end

  if #search > 0 then
    return search[GetRandomIntInRange(1, #search)]
  end

  for i=1, 250, 1 do

    local ped = GetRandomPedAtCoord(0.0,  0.0,  0.0,  math.huge + 0.0,  math.huge + 0.0,  math.huge + 0.0,  26)

    if DoesEntityExist(ped) and IsPedHuman(ped) and IsPedWalking(ped) and not IsPedAPlayer(ped) then
      table.insert(search, ped)
    end
  end

  if #search > 0 then
    return search[GetRandomIntInRange(1, #search)]
  end
end

function ClearCurrentMission()

  if DoesBlipExist(CurrentCustomerBlip) then
    RemoveBlip(CurrentCustomerBlip)
  end

  if DoesBlipExist(DestinationBlip) then
    RemoveBlip(DestinationBlip)
  end

  CurrentCustomer           = nil
  CurrentCustomerBlip       = nil
  DestinationBlip           = nil
  IsNearCustomer            = false
  CustomerIsEnteringVehicle = false
  CustomerEnteredVehicle    = false
  TargetCoords              = nil
end

function StartTaxiJob()

  ShowLoadingPromt(_U('taking_service') .. 'Taxi/Uber', 5000, 3)
  ClearCurrentMission()

  OnJob = true
end

function StopTaxiJob()

  local playerPed = GetPlayerPed(-1)

  if IsPedInAnyVehicle(playerPed, false) and CurrentCustomer ~= nil then
    local vehicle = GetVehiclePedIsIn(playerPed,  false)
    TaskLeaveVehicle(CurrentCustomer,  vehicle,  0)

    if CustomerEnteredVehicle then
      TaskGoStraightToCoord(CurrentCustomer,  TargetCoords.x,  TargetCoords.y,  TargetCoords.z,  1.0,  -1,  0.0,  0.0)
    end
  end

  ClearCurrentMission()
  OnJob = false
  DrawSub(_U('mission_complete'), 5000)
end

function OpenTaxiActionsMenu()

  local elements = {
    {label = _U('spawn_veh'), value = 'spawn_vehicle'}
  }

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'taxi_actions',
    {
      title    = 'Taxi',
      align    = 'top-left',
      elements = elements
    },
    function(data, menu)

      if data.current.value == 'spawn_vehicle' then

        if Config.EnableSocietyOwnedVehicles then

          local elements = {}

          ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

            for i=1, #vehicles, 1 do
              table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
            end

            ESX.UI.Menu.Open(
              'default', GetCurrentResourceName(), 'vehicle_spawner',
              {
                title    = _U('spawn_veh'),
                align    = 'top-left',
                elements = elements
              },
              function(data, menu)

                menu.close()

                local vehicleProps = data.current.value

                ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
                  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                  local playerPed = GetPlayerPed(-1)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
                end)

                TriggerServerEvent('esx_society:removeVehicleFromGarage', 'taxi', vehicleProps)
              end,
              function(data, menu)
                menu.close()
              end
            )
          end, 'taxi')
        else

          menu.close()

          if Config.MaxInService == -1 then

            local playerPed = GetPlayerPed(-1)
            local coords    = Config.Zones.VehicleSpawnPoint.Pos

            ESX.Game.SpawnVehicle('taxi', coords, 225.0, function(vehicle)
              TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
            end)
          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                local playerPed = GetPlayerPed(-1)
                local coords    = Config.Zones.VehicleSpawnPoint.Pos

                ESX.Game.SpawnVehicle('taxi', coords, 225.0, function(vehicle)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                end)
              else
                ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)
              end
            end, 'taxi')
          end
        end
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'taxi_actions_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}
    end
  )
end

function OpenStockMenu()

  local elements = {
    {label = _U('get_object'), value = 'get_stock'},
    {label = _U('put_object'), value = 'put_stock'}
  }

  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'stock',
    {
      title    = 'Coffre',
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      if data.current.value == 'put_stock' then
        OpenPutStocksMenu()
      end

      if data.current.value == 'get_stock' then
        OpenGetStocksMenu()
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_stock'
      CurrentActionMsg  = _U('open_stock')
      CurrentActionData = {}
    end
  )
end

function OpenCloakroomMenu()

  local elements = {
    {label = "Prise de Service", value = 'take_service'},
    {label = "Fin de Service",   value = 'end_service'},
  }

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = _U('cloakroom'),
      align    = 'top-left',
      elements = elements,
    },

      function(data, menu)
        if data.current.value == "take_service" then
          ESX.TriggerServerCallback('esx_service:enableService', function(result)
            if result then
              ESX.ShowNotification('~g~Prise de Service')
              InService = true
            end
          end, 'taxi')
        end

        if data.current.value == "end_service" then
          TriggerServerEvent('esx_service:disableService', 'taxi')
          ESX.ShowNotification("~r~Fin de Service")
          InService = false
        end

      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour prendre votre service'
      CurrentActionData = {}
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour prendre votre service'
      CurrentActionData = {}
    end
  )
end

function OpenMobileTaxiActionsMenu()

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'mobile_taxi_actions',
    {
      title    = 'Taxi',
      align    = 'top-left',
      elements = {
        {label = "Appels",      value = 'calls'},
        {label = _U('billing'), value = 'billing'},
        {label = "GPS",         value = 'gps'}
      }
    },
    function(data, menu)

      if data.current.value == 'calls' then
        ESX.TriggerServerCallback('esx_taxijob:getCallsList', function(calls)
          local options = {}

          if calls ~= nil then
            for k, v in pairs(calls) do
              table.insert(options, {label = v.text, value = k})
            end
          else
            table.insert(options, {label = "Aucun Appel en Attente"})
          end
            ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'taxi_calls',
            {
              title    = 'Taxi - Appels',
              align    = 'top-left',
              elements = options
            }, function(data2, menu2)
              local callid = data2.current.value
              local options2 = {
                {label = "Prendre l'appel",     value="take_call"},
                {label = "Position de l'appel", value="pos_call"},
                {label = "Appel Résolu",        value="end_call"},
                {label = "Refuser l'appel",     value="refused_call"}
              }
              ESX.UI.Menu.Open(
              'default', GetCurrentResourceName(), 'taxi_call',
              {
                title    = 'Actions',
                align    = 'top-left',
                elements = options2
              }, function(data3, menu3)
              if data3.current.value == "take_call" and SelectedCall ~= data2.current.value then
                SelectedCall = data2.current.value
                TriggerServerEvent('esx_taxijob:takeCall', data2.current.value)
                local x = calls[data2.current.value].position.x
                local y = calls[data2.current.value].position.y
                SetNewWaypoint(x, y)
              end
              if data3.current.value == "pos_call" then
                local x = calls[data2.current.value].position.x
                local y = calls[data2.current.value].position.y
                SetNewWaypoint(x, y)
              end
              if data3.current.value == "end_call" then
                TriggerServerEvent('esx_taxijob:endCall', callid)
                ESX.ShowNotification("~g~Appel Résolu")
              end
              if data3.current.value == "refused_call" then
                TriggerServerEvent("esx_taxijob:refuseCall", callid)
              end
            end,
            function(data3, menu3)
              menu3.close()
            end)
          end,
          function(data2, menu2)
            menu2.close()
          end)
        end)
      end

      if data.current.value == 'billing' then

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'billing',
          {
            title = _U('invoice_amount')
          },
          function(data, menu)

            local amount = tonumber(data.value)

            if amount == nil then
              ESX.ShowNotification(_U('amount_invalid'))
            else

              menu.close()

              local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

              if closestPlayer == -1 or closestDistance > 3.0 then
                ESX.ShowNotification(_U('no_players_near'))
              else
                TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_taxi', 'Taxi', amount)
              end
            end
          end,
          function(data, menu)
            menu.close()
          end
        )
      elseif data.current.value == 'gps' then

        local blips = {
          {title="Récolte",       x = -3153.825, y = 1053.940, z = 20.839 },
          {title="Fabrication",   x = 750.953, y = 1273.881, z = 360.296 },
          {title="Revente",       x = 822.274, y = -3194.265, z = 5.994 }
        }

        for _, info in pairs(blips) do
          info.blip = AddBlipForCoord(info.x, info.y, info.z)
          SetBlipSprite(info.blip, 1)
          SetBlipDisplay(info.blip, 4)
          SetBlipScale(info.blip, 0.8)
          SetBlipColour(info.blip, 27)
          SetBlipAsShortRange(info.blip, true)

          BeginTextCommandSetBlipName("STRING")
          AddTextComponentString(info.title)
          EndTextCommandSetBlipName(info.blip)
        end
        ESX.ShowNotification("Position ajouté à votre gps !")
      end
    end,
    function(data, menu)
      menu.close()
    end
  )
end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('esx_taxijob:getStockItems', function(items)
    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = 'Coffre',
        align    = 'top-left',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              TriggerServerEvent('esx_taxijob:getStockItem', itemName, count)

              Citizen.Wait(1000)
              OpenGetStocksMenu()
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

function OpenPutStocksMenu()

  ESX.TriggerServerCallback('esx_taxijob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        align    = 'top-left',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              TriggerServerEvent('esx_taxijob:putStockItems', itemName, count)

              Citizen.Wait(1000)
              OpenPutStocksMenu()
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

AddEventHandler('esx_taxijob:hasEnteredMarker', function(zone)

  if zone == 'Cloakrooms' then
    CurrentAction     = 'menu_cloakroom'
    CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour prendre votre service'
    CurrentActionData = {}
  end

  if InService then
    if zone == 'TaxiActions' then
      CurrentAction     = 'taxi_actions_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}
    end

    if zone == 'BossActions' then
      CurrentAction     = 'menu_boss_actions'
      CurrentActionMsg  = _U('open_bossmenu')
      CurrentActionData = {}
    end

    if zone == 'Stocks' then
      CurrentAction     = 'menu_stock'
      CurrentActionMsg  = _U('open_stock')
      CurrentActionData = {}
    end

    if zone == 'Craft' then
      CurrentAction     = 'craft_menu'
      CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour fabriquer des GPS'
      CurrentActionData = {}
    end

    if zone == 'Resell' then
      CurrentAction     = 'resell'
      CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour vendre des GPS'
      CurrentActionData = {}
    end

    if zone == 'Garage' then
      CurrentAction     = 'harvest_menu'
      CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour récupérer des composants électoniques'
      CurrentActionData = {}
    end

    if zone == 'VehicleDeleter' then

      local playerPed = GetPlayerPed(-1)
      local vehicle = GetVehiclePedIsIn(playerPed, false)

      if IsPedInAnyVehicle(playerPed,  false) then
        CurrentAction     = 'delete_vehicle'
        CurrentActionMsg  = _U('store_veh')
        CurrentActionData = { vehicle = vehicle }
      end
    end
  end
end)

AddEventHandler('esx_taxijob:hasExitedMarker', function(zone)
  ESX.UI.Menu.CloseAll()
  CurrentAction = nil

  TriggerServerEvent('esx_taxijob:stopHarvest')
  TriggerServerEvent('esx_taxijob:stopCraft')
  TriggerServerEvent('esx_taxijob:stopSell')
end)

-- Create Blips
Citizen.CreateThread(function()

  local blip = AddBlipForCoord(Config.Zones.TaxiActions.Pos.x, Config.Zones.TaxiActions.Pos.y, Config.Zones.TaxiActions.Pos.z)

  SetBlipSprite (blip, 198)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 1.0)
  SetBlipColour (blip, 5)
  SetBlipAsShortRange(blip, true)

  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString("Taxi")
  EndTextCommandSetBlipName(blip)
end)

-- Display markers
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if PlayerData.job ~= nil and PlayerData.job.name == 'taxi' then

      local coords = GetEntityCoords(GetPlayerPed(-1))

      for k,v in pairs(Config.Zones) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end

      for k,v in pairs(Config.TeleportZones) do
        if(v.Marker ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Marker, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end
    end
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if PlayerData.job ~= nil and PlayerData.job.name == 'taxi' then

      local coords      = GetEntityCoords(GetPlayerPed(-1))
      local isInMarker  = false
      local currentZone = nil

      for k,v in pairs(Config.Zones) do
        if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
          isInMarker  = true
          currentZone = k
        end
      end

      if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
        HasAlreadyEnteredMarker = true
        LastZone                = currentZone
        TriggerEvent('esx_taxijob:hasEnteredMarker', currentZone)
      end

      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('esx_taxijob:hasExitedMarker', LastZone)
      end

      if IsControlJustReleased(0, Keys["E"]) and isInPublicMarker then
        TriggerEvent('esx_taxijob:teleportMarkers', position)
      end

      if isInPublicMarker then
        hintToDisplay = zone.Hint
        hintIsShowed = true
      else
        if not isInMarker then
          hintToDisplay = "no hint to display"
          hintIsShowed = false
        end
      end

      for k,v in pairs(Config.TeleportZones) do
        if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
          isInPublicMarker = true
          position = v.Teleport
          zone = v
          break
        else
          isInPublicMarker  = false
        end
      end
    end
  end
end)

-- Taxi Job
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    local playerPed = GetPlayerPed(-1)

    if OnJob then

      if CurrentCustomer == nil then

        DrawSub(_U('drive_search_pass'), 5000)

        if IsPedInAnyVehicle(playerPed,  false) and GetEntitySpeed(playerPed) > 0 then

          local waitUntil = GetGameTimer() + GetRandomIntInRange(30000,  45000)

          while OnJob and waitUntil > GetGameTimer() do
            Citizen.Wait(1)
          end

          if OnJob and IsPedInAnyVehicle(playerPed,  false) and GetEntitySpeed(playerPed) > 0 then

            CurrentCustomer = GetRandomWalkingNPC()

            if CurrentCustomer ~= nil then

              CurrentCustomerBlip = AddBlipForEntity(CurrentCustomer)

              SetBlipAsFriendly(CurrentCustomerBlip, 1)
              SetBlipColour(CurrentCustomerBlip, 2)
              SetBlipCategory(CurrentCustomerBlip, 3)
              SetBlipRoute(CurrentCustomerBlip,  true)

              SetEntityAsMissionEntity(CurrentCustomer,  true, false)
              ClearPedTasksImmediately(CurrentCustomer)
              SetBlockingOfNonTemporaryEvents(CurrentCustomer, 1)

              local standTime = GetRandomIntInRange(60000,  180000)
              TaskStandStill(CurrentCustomer, standTime)
              ESX.ShowNotification(_U('customer_found'))
            end
          end
        end
      else

        if IsPedFatallyInjured(CurrentCustomer) then

          ESX.ShowNotification(_U('client_unconcious'))

          if DoesBlipExist(CurrentCustomerBlip) then
            RemoveBlip(CurrentCustomerBlip)
          end

          if DoesBlipExist(DestinationBlip) then
            RemoveBlip(DestinationBlip)
          end

          SetEntityAsMissionEntity(CurrentCustomer,  false, true)

          CurrentCustomer           = nil
          CurrentCustomerBlip       = nil
          DestinationBlip           = nil
          IsNearCustomer            = false
          CustomerIsEnteringVehicle = false
          CustomerEnteredVehicle    = false
          TargetCoords              = nil
        end

        if IsPedInAnyVehicle(playerPed,  false) then

          local vehicle          = GetVehiclePedIsIn(playerPed,  false)
          local playerCoords     = GetEntityCoords(playerPed)
          local customerCoords   = GetEntityCoords(CurrentCustomer)
          local customerDistance = GetDistanceBetweenCoords(playerCoords.x,  playerCoords.y,  playerCoords.z,  customerCoords.x,  customerCoords.y,  customerCoords.z)

          if IsPedSittingInVehicle(CurrentCustomer,  vehicle) then

            if CustomerEnteredVehicle then

              local targetDistance = GetDistanceBetweenCoords(playerCoords.x,  playerCoords.y,  playerCoords.z,  TargetCoords.x,  TargetCoords.y,  TargetCoords.z)

              if targetDistance <= 10.0 then

                TaskLeaveVehicle(CurrentCustomer,  vehicle,  0)

                ESX.ShowNotification(_U('arrive_dest'))

                TaskGoStraightToCoord(CurrentCustomer,  TargetCoords.x,  TargetCoords.y,  TargetCoords.z,  1.0,  -1,  0.0,  0.0)
                SetEntityAsMissionEntity(CurrentCustomer,  false, true)

                TriggerServerEvent('esx_taxijob:success')

                RemoveBlip(DestinationBlip)

                local scope = function(customer)
                  ESX.SetTimeout(60000, function()
                    DeletePed(customer)
                  end)
                end

                scope(CurrentCustomer)

                CurrentCustomer           = nil
                CurrentCustomerBlip       = nil
                DestinationBlip           = nil
                IsNearCustomer            = false
                CustomerIsEnteringVehicle = false
                CustomerEnteredVehicle    = false
                TargetCoords              = nil
              end

              if TargetCoords ~= nil then
                DrawMarker(1, TargetCoords.x, TargetCoords.y, TargetCoords.z - 1.0, 0, 0, 0, 0, 0, 0, 4.0, 4.0, 2.0, 178, 236, 93, 155, 0, 0, 2, 0, 0, 0, 0)
              end
            else

              RemoveBlip(CurrentCustomerBlip)
              CurrentCustomerBlip = nil
              TargetCoords = Config.JobLocations[GetRandomIntInRange(1,  #Config.JobLocations)]

              local street = table.pack(GetStreetNameAtCoord(TargetCoords.x, TargetCoords.y, TargetCoords.z))
              local msg    = nil

              if street[2] ~= 0 and street[2] ~= nil then
                msg = string.format(_U('take_me_to_near', GetStreetNameFromHashKey(street[1]),GetStreetNameFromHashKey(street[2])))
              else
                msg = string.format(_U('take_me_to', GetStreetNameFromHashKey(street[1])))
              end

              ESX.ShowNotification(msg)
              DestinationBlip = AddBlipForCoord(TargetCoords.x, TargetCoords.y, TargetCoords.z)

              BeginTextCommandSetBlipName("STRING")
              AddTextComponentString("Destination")
              EndTextCommandSetBlipName(blip)

              SetBlipRoute(DestinationBlip,  true)
              CustomerEnteredVehicle = true
            end
          else

            DrawMarker(1, customerCoords.x, customerCoords.y, customerCoords.z - 1.0, 0, 0, 0, 0, 0, 0, 4.0, 4.0, 2.0, 178, 236, 93, 155, 0, 0, 2, 0, 0, 0, 0)

            if not CustomerEnteredVehicle then

              if customerDistance <= 30.0 then

                if not IsNearCustomer then
                  ESX.ShowNotification(_U('close_to_client'))
                  IsNearCustomer = true
                end
              end

              if customerDistance <= 100.0 then

                if not CustomerIsEnteringVehicle then
                  ClearPedTasksImmediately(CurrentCustomer)
                  local seat = 0

                  for i=4, 0, 1 do
                    if IsVehicleSeatFree(vehicle,  seat) then
                      seat = i
                      break
                    end
                  end

                  TaskEnterVehicle(CurrentCustomer,  vehicle,  -1,  seat,  2.0,  1)
                  CustomerIsEnteringVehicle = true
                end
              end
            end
          end
        else
          DrawSub(_U('return_to_veh'), 5000)
        end
      end
    end
  end
end)

-- Key Controls
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlPressed(0, Keys['E']) and PlayerData.job ~= nil and PlayerData.job.name == 'taxi' and (GetGameTimer() - GUI.Time) > 300 then

        if CurrentAction == 'menu_cloakroom' then
          OpenCloakroomMenu()
        end

        if InService then

          if CurrentAction == 'taxi_actions_menu' then
            OpenTaxiActionsMenu()
          end

          if CurrentAction == 'craft_menu' then
            TriggerServerEvent('esx_taxijob:startCraft')
          end

          if CurrentAction == 'resell' then
            TriggerServerEvent('esx_taxijob:startSell')
          end

          if CurrentAction == 'harvest_menu' then
            TriggerServerEvent('esx_taxijob:startHarvest')
          end

          if CurrentAction == 'menu_stock' then
            OpenStockMenu()
          end

          if CurrentAction == 'menu_boss_actions' then

            ESX.UI.Menu.CloseAll()

            TriggerEvent('esx_society:openBossMenu', 'taxi', function(data, menu)

              menu.close()
              CurrentAction     = 'menu_boss_actions'
              CurrentActionMsg  = _U('open_bossmenu')
              CurrentActionData = {}
            end)
          end

          if CurrentAction == 'delete_vehicle' then

            local playerPed = GetPlayerPed(-1)

            if Config.EnableSocietyOwnedVehicles then
              local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
              TriggerServerEvent('esx_society:putVehicleInGarage', 'taxi', vehicleProps)
            else
              if GetEntityModel(CurrentActionData.vehicle) == GetHashKey('taxi') then
                if Config.MaxInService ~= -1 then
                  TriggerServerEvent('esx_service:disableService', 'taxi')
                end
              end
            end
            ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
          end
        end
        CurrentAction = nil
        GUI.Time      = GetGameTimer()
      end
    end

    if IsControlPressed(0, Keys['F6']) and InService and not IsDead and Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.name == 'taxi' and (GetGameTimer() - GUI.Time) > 150 then
      OpenMobileTaxiActionsMenu()
      GUI.Time = GetGameTimer()
    end
  end
end)

AddEventHandler('esx:onPlayerDeath', function()
	IsDead = true
end)

AddEventHandler('playerSpawned', function(spawn)
	IsDead = false
end)

RegisterNetEvent('esx_taxijob:callService')
AddEventHandler('esx_taxijob:callService', function(datas)
  local pos = GetEntityCoords(GetPlayerPed(-1), false)

  TriggerServerEvent('esx_taxijob:newCall', datas.type, {x=pos.x, y=pos.y})

  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)

RegisterNetEvent('esx_taxijob:noTaxis')
AddEventHandler('esx_taxijob:noTaxis', function()
  ESX.ShowNotification("~r~Aucun Taxi en Service Actuellement")
end)

RegisterNetEvent('esx_taxijob:callTaked')
AddEventHandler('esx_taxijob:callTaked', function()
  ESX.ShowNotification("~g~Votre appel viens d'être accepter, un véhicule est en route")
end)

RegisterNetEvent('esx_taxijob:endTaked')
AddEventHandler('esx_taxijob:endTaked', function()
  ESX.ShowNotification("~g~Appel Terminer")
end)

RegisterNetEvent('esx_taxijob:refuseTaked')
AddEventHandler('esx_taxijob:refuseTaked', function()
  ESX.ShowNotification("~r~Votre Appel à été refusé")
end)

RegisterNetEvent('esx_taxijob:cancelCall')
AddEventHandler('esx_taxijob:cancelCall', function()
  ESX.TriggerServerCallback("esx_taxijob:cancelCall", function(result)
    if result then
      ESX.ShowNotification("~g~Appel Annulé")
    end
  end)
end)

-- TELEPORTERS
AddEventHandler('esx_taxijob:teleportMarkers', function(position)
  SetEntityCoords(GetPlayerPed(-1), position.x, position.y, position.z)
end)

-- Show top left hint
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
    if hintIsShowed == true then
      SetTextComponentFormat("STRING")
      AddTextComponentString(hintToDisplay)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)
    end
  end
end)
