ESX = nil
local CallsList    = {}
PlayersHarvesting  = {}
PlayersCrafting    = {}
PlayersSelling     = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'taxi', Config.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'taxi', _U('taxi_client'), true, true)
TriggerEvent('esx_society:registerSociety', 'taxi', 'Taxi', 'society_taxi', 'society_taxi', 'society_taxi', {type = 'public'})

-------------- Récupération
local function Harvest(source)

  SetTimeout(4000, function()

    if PlayersHarvesting[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local electroniqueQuantity = xPlayer.getInventoryItem('electronique').count

      if electroniqueQuantity >= 20 then
        TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
      else
        xPlayer.addInventoryItem('electronique', 1)
        Harvest(source)
      end
    end
  end)
end

RegisterServerEvent('esx_taxijob:startHarvest')
AddEventHandler('esx_taxijob:startHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('recovery_gas_can'))
  Harvest(source)
end)

RegisterServerEvent('esx_taxijob:stopHarvest')
AddEventHandler('esx_taxijob:stopHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = false
end)

------------ Craft Lockpick -------------------
local function Craft(source)

  SetTimeout(4000, function()

    if PlayersCrafting[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local electroniqueQuantity = xPlayer.getInventoryItem('electronique').count

      if  electroniqueQuantity.limit ~= -1 and electroniqueQuantity.count >= electroniqueQuantity.limit then
        TriggerClientEvent('esx:showNotification', source, _U('not_enough_gas_can'))
      else
        xPlayer.removeInventoryItem('electronique', 1)
        xPlayer.addInventoryItem('gps', 1)
        Craft(source)
      end
    end
  end)
end

RegisterServerEvent('esx_taxijob:startCraft')
AddEventHandler('esx_taxijob:startCraft', function()
  local _source = source
  PlayersCrafting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('assembling_blowtorch'))
  Craft(_source)
end)

RegisterServerEvent('esx_taxijob:stopCraft')
AddEventHandler('esx_taxijob:stopCraft', function()
  local _source = source
  PlayersCrafting[_source] = false
end)

------------ Sell Lockpick -------------------
function Sell(source)

  SetTimeout(5000, function()

    math.randomseed(os.time())
    local _source          = source
    local xPlayer          = ESX.GetPlayerFromId(_source)
    local total            = math.random(50)
    local playerMoney      = math.floor(total / 100 * 30)
    local societyMoney     = math.floor(total / 100 * 70)

    if xPlayer.job.grade >= 3 then
      total = total * 2
    end

    TriggerEvent('esx_addonaccount:getSharedAccount', 'society_taxi', function(account)
      societyAccount = account
    end)

    if societyAccount ~= nil then

      if PlayersSelling[source] == true then

        local gpsQuantity = xPlayer.getInventoryItem('gps').count

        if gpsQuantity == 0 then
          TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas de GPS à vendre !')
        else
          xPlayer.removeInventoryItem('gps', 1)
          xPlayer.addMoney(playerMoney)
          societyAccount.addMoney(societyMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_earned') .. playerMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. societyMoney)
          Sell(source)
        end
      end
    end
  end)
end

RegisterServerEvent('esx_taxijob:startSell')
AddEventHandler('esx_taxijob:startSell', function()

  local _source = source
  PlayersSelling[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))
  Sell(_source)
end)

RegisterServerEvent('esx_taxijob:stopSell')
AddEventHandler('esx_taxijob:stopSell', function()

  local _source = source
  PlayersSelling[_source] = false
end)

RegisterServerEvent('esx_taxijob:success')
AddEventHandler('esx_taxijob:success', function()

  math.randomseed(os.time())

  local xPlayer        = ESX.GetPlayerFromId(source)
  local total          = math.random(Config.NPCJobEarnings.min, Config.NPCJobEarnings.max);
  local societyAccount = nil

  if xPlayer.job.grade >= 3 then
    total = total * 2
  end

  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_taxi', function(account)
    societyAccount = account
  end)

  if societyAccount ~= nil then

    local playerMoney  = math.floor(total / 100 * 30)
    local societyMoney = math.floor(total / 100 * 70)

    xPlayer.addMoney(playerMoney)
    societyAccount.addMoney(societyMoney)

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_earned') .. playerMoney)
    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. societyMoney)

  else

    xPlayer.addMoney(total)
    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_earned') .. total)

  end

end)

RegisterServerEvent('esx_taxijob:getStockItem')
AddEventHandler('esx_taxijob:getStockItem', function(itemName, count)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_taxi', function(inventory)
		local item = inventory.getItem(itemName)
		local sourceItem = xPlayer.getInventoryItem(itemName)

		-- is there enough in the society?
		if count > 0 and item.count >= count then

			-- can the player carry the said amount of x item?
			if sourceItem.limit ~= -1 and (sourceItem.count + count) > sourceItem.limit then
				TriggerClientEvent('esx:showNotification', xPlayer.source, _U('player_cannot_hold'))
			else
				inventory.removeItem(itemName, count)
				xPlayer.addInventoryItem(itemName, count)
				TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)
			end
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end
	end)
end)

ESX.RegisterServerCallback('esx_taxijob:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_taxi', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_taxijob:putStockItems')
AddEventHandler('esx_taxijob:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_taxi', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= 0 then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_taxijob:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })

end)

function SendNotificationToPlayersInService(message)
  local xPlayers = ESX.GetPlayers()
  local countCops = 0
  for k, v in pairs(xPlayers) do
    ESX.TriggerServerCallback('esx_service:isInService', 0, v, function(inService)
      if inService then
        TriggerClientEvent('esx:showNotification', v, message)
        countCops = countCops + 1
      end
    end, 'taxi')
  end
  if countCops <= 0 then
    return false
  end
  return true
end

ESX.RegisterServerCallback('esx_taxijob:getCallsList', function(source, cb)
  local _source = source
  cb(CallsList)
end)

RegisterNetEvent('esx_taxijob:newCall')
AddEventHandler('esx_taxijob:newCall', function(type, pos, message)
  local _source = source
  if type == "1" then
    message = "1 Personne"
  elseif type == "2" then
    message = "2 Personnes"
  elseif type == "3" then
    message = "3 Personnes"
  elseif type == "more" then
    message = "4 Personnes ou plus"
  end
  CallsList[_source] = {
    type = type,
    position = pos,
    text = message,
    nbr = 0
  }
  local notif = "Un nouvel appel viens d'arriver :"
  if not SendNotificationToPlayersInService(notif) then
    TriggerClientEvent('esx_taxijob:noTaxis', _source)
  else
    SendNotificationToPlayersInService("~r~"..message)
  end
end)

RegisterNetEvent('esx_taxijob:takeCall')
AddEventHandler('esx_taxijob:takeCall', function(id)
  CallsList[id].nbr = CallsList[id].nbr + 1
  TriggerClientEvent('esx_taxijob:callTaked', id)
end)

RegisterNetEvent('esx_taxijob:endCall')
AddEventHandler('esx_taxijob:endCall', function(id)
  TriggerClientEvent('esx_taxijob:endCall', id)
  CallsList[id] = nil
end)

RegisterNetEvent('esx_taxijob:refuseCall')
AddEventHandler('esx_taxijob:refuseCall', function(id)
  TriggerClientEvent('esx_taxijob:refuseCall', id)
  CallsList[id] = nil
end)

ESX.RegisterServerCallback("esx_taxijob:cancelCall", function(source, cb)
  local _source = source
  if CallsList[_source] ~= nil then
    CallsList[_source] = nil
    SendNotificationToPlayersInService("Appel Annulé")
    cb(true)
  else
    cb(false)
  end
end)
