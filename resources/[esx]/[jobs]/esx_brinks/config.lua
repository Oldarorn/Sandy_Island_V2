Config               = {}
Config.DrawDistance  = 100.0
Config.nameJob       = "brinks"
Config.nameJobLabel  = "brinks"
Config.platePrefix   = "BRINKS"
Config.Locale        = 'fr'

Config.Zones = {

  Cloakroom = {
    Pos        = {x = 4.580, y = -656.477, z = 32.450},
    Size       = {x = 1.5, y = 1.5, z = 0.6},
    Color      = {r = 11, g = 203, b = 159},
    Type       = 27,
  },

  VehicleSpawner = {
  	Pos   = {x = -6.943, y = -654.230, z = 32.405},
  	Size  = {x = 1.5, y = 1.5, z = 0.6},
  	Color = {r = 11, g = 203, b = 159},
  	Type  = 27,
  },

  VehicleSpawnPoint = {
  	Pos   = {x = -3.238, y = -671.238, z = 31.338},
  	Size  = {x = 3.0, y = 3.0, z = 1.0},
  	Type  = -1,
  	Heading = 182,
  },

  VehicleDeleter = {
  	Pos   = {x = -19.23, y = -672.34, z = 31.35},
  	Size  = {x = 3.0, y = 3.0, z = 0.9},
  	Color = {r = 255, g = 0, b = 0},
  	Type  = 27,
  },

  BossActions = {
    Pos   = { x = 11.533, y = -6661.530, z = 32.448 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 27,
  },

  Harvest0 = {
    Pos   = { x = -13.131, y = 6500.871, z = 30.496 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 27,
  },

  Harvest1 = { -- PVC
    Pos   = { x = -322.336, y = -1530.216, z = 26.545 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 27,
  },

  Craft = {
    Pos   = { x = 817.487, y = -2122.076, z = 28.327 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 27,
  },

  Resell = {
    Pos   = { x = 791.554, y = -2977.018, z = 5.023 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 27,
  },

  Stocks = {
    Pos   = { x = 1172.834, y = 2635.740, z = 36.788 },
    Size  = { x = 1.3, y = 1.3, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },

  Vault = {
    Pos   = { x = 3.131, y = -659.397, z = 32.450 },
    Size  = { x = 1.3, y = 1.3, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },
}
