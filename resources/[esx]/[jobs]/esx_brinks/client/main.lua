local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX                           = nil
local PlayerData              = {}
local GUI                     = {}
GUI.Time                      = 0
local HasAlreadyEnteredMarker = false
local LastZone                = nil

local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local Blips                   = {}
local Done                    = false

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(1)
  end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

-- Vestiare
function OpenCloakroomMenu()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = _U('cloakroom'),
      align    = 'top-left',
      elements = {
        {label = _U('clothes_civil'), value = 'citizen_wear'},
        {label = _U('clothes'),   value = 'work_wear'},
      },
    },
    function(data, menu)
      menu.close()

      if data.current.value == 'citizen_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
          TriggerEvent('skinchanger:loadSkin', skin)
          TriggerServerEvent('esx_service:disableService', 'brinks')
          ESX.ShowNotification("~r~Fin de Service")
          InService = false
        end)
      end

      if data.current.value == 'work_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

          if skin.sex == 0 then
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
          else
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
          end
          ESX.TriggerServerCallback('esx_service:enableService', function(result)
            if result then
              ESX.ShowNotification('~g~Prise de Service')
              InService = true
            end
          end, 'brinks')
        end)
      end
      CurrentAction     = 'cloackroom_menu'
      CurrentActionMsg  = _U('open_menu')
      CurrentActionData = {}
    end,
    function(data, menu)
      menu.close()
    end
  )
end

-- Prise du véhicule
function VehicleMenu()

  local elements = {
    {label = Config.Vehicles.Truck.Label, value = Config.Vehicles.Truck}
  }

  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
  'default', GetCurrentResourceName(), 'spawn_vehicle',
  {
    title = _U('Vehicle_Menu_Title'),
    elements = elements
  },
  function(data, menu)
    local elements = {}

    ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

      for i=1, #vehicles, 1 do
        table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'vehicle_spawner',
        {
        title    = _U('service_vehicle'),
        align    = 'top-left',
        elements = elements,
        },
        function(data, menu)

          menu.close()

          local vehicleProps = data.current.value

          ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
            ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
            local playerPed = GetPlayerPed(-1)
            TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
            end)

          TriggerServerEvent('esx_society:removeVehicleFromGarage', 'brinks', vehicleProps)
          end,
          function(data, menu)
            menu.close()
          end
          )
      end, 'brinks')
    else

      local elements = {
        {label = _U('flat_bed'), value = 'stockade'},
        {label = _U('tow_truck'), value = 'utillitruck2'}
      }

      ESX.UI.Menu.CloseAll()
      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'spawn_vehicle',
        {
          title    = _U('service_vehicle'),
          align    = 'top-left',
          elements = elements
        },
        function(data, menu)
          for i=1, #elements, 1 do
            if Config.MaxInService == -1 then
              ESX.Game.SpawnVehicle(data.current.value, Config.Zones.VehicleSpawnPoint.Pos, 90.0, function(vehicle)
                local playerPed = GetPlayerPed(-1)
                TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
              end)
              break
            else
              ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)
                if canTakeService then
                  ESX.Game.SpawnVehicle(data.current.value, Config.Zones.VehicleSpawnPoint.Pos, 90.0, function(vehicle)
                    local playerPed = GetPlayerPed(-1)
                    TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                  end)
                else
                  ESX.ShowNotification(_U('service_full') .. inServiceCount .. '/' .. maxInService)
                end
              end, 'brinks')
              break
            end
          end
            menu.close()
          end,
          function(data, menu)
            menu.close()
            OpenVehicleSpawnerMenu()
          end
          )
        end
      )
    end
  end,
  function(data, menu)
    menu.close()
    CurrentAction = 'vehiclespawn_menu'
    CurrentActionMsg = ''
    CurrentActionData = {}
  end
  )
end

function OpenMobileBrinksActionsMenu()
  ESX.UI.Menu.CloseAll() 

  ESX.UI.Menu.Open(
  'default', GetCurrentResourceName(), 'mobile_ambulance_actions',
  {
    title    = _U('ambulance'),
    align    = 'top-left',
    elements = {
      {label = "Appels",       value = 'calls'},
      {label = _U('billing'),  value = 'billing'},
    }
  }, function(data, menu)

      if data.current.value == "calls" then
        ESX.TriggerServerCallback('esx_brinks:getCalls', function(calls)
          local options = {}
          for k,v in pairs(calls) do
            table.insert(options, {label = v.text, value = k})
          end
          ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'brinks_calls',
            {
              title = "Appels",
              align = 'top-left',
              elements = options
            },
            function(data, menu)
              local callid = data.current.value
              ESX.UI.Menu.Open(
                'default', GetCurrentResourceName(), 'ambulance_call_details',
                {
                  title = "Appels - Actions",
                  align = "top-left",
                  elements = {
                    {label = "Prendre L'appel", value = "take_call"},
                    {label = "Position de L'appel", value = "pos_call"},
                    {label = "Terminer L'appel", value = "end_call"}
                  }
                },
                function(data2, menu2)
                  if data2.current.value == "take_call" and SelectedCall ~= callid then
                    SelectedCall = callid
                    TriggerServerEvent('esx_brinks:takeCall', callid)
                    local x = calls[callid].position.x
                    local y = calls[callid].position.y
                    SetNewWaypoint(x, y)
                  end
                  if data2.current.value == "pos_call" then
                    local x = calls[callid].position.x
                    local y = calls[callid].position.y
                    SetNewWaypoint(x, y)
                  end
                  if data2.current.value == "end_call" then
                    TriggerServerEvent('esx_brinks:endCall', callid)
                    SelectedCall = nil
                    ESX.ShowNotification("~g~Appel Résolu")
                  end
                  menu2.close()
                end)
            end,
            function(data, menu)
              menu.close()
            end)
        end)
      elseif data.current.value == 'billing' then

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'billing',
          {
          title = "Entrez le montant"
          },
          function(data, menu)

            local amount = tonumber(data.value)

            if amount == nil then
              ESX.ShowNotification("Montant Invalide")
            else

              menu.close()
              local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

              if closestPlayer == -1 or closestDistance > 3.0 then
                ESX.ShowNotification("Aucun Joueur a Proximité")
              else
                TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_ambulance', 'Ambulance', amount)
              end
            end
          end,
          function(data, menu)
            menu.close()
          end
        )
      end
    end,
    function(data, menu)
      menu.close()
    end
  )
end

-- Quand le joueur entre dans la zone
AddEventHandler('esx_brinks:hasEnteredMarker', function(zone)

  if zone == 'Cloakroom' then
    CurrentAction = 'cloakroom_menu'
    CurrentActionMsg = _U('open_cloackroom')
    CurrentActionData = {}
  end

  if InService then

    if zone == 'VehicleSpawner' then
      CurrentAction = 'vehiclespawn_menu'
      CurrentActionMsg = ''
      CurrentActionData = {}
    end

    if zone == 'VehicleDeleter' then
      local playerPed = GetPlayerPed(-1)
      local vehicle = GetVehiclePedIsIn(playerPed, false)

      if IsPedInAnyVehicle(playerPed, false) then
        CurrentAction = 'delete_vehicle'
        CurrentActionMsg = _U('store_veh')
        CurrentActionData = {}
      end
    end
    
    if zone == 'Vente' then
      CurrentAction = 'vente'
      CurrentActionMsg = ''
      CurrentActionData = {}
    end

    if zone == 'BossActions' then
      CurrentAction     = 'menu_boss_actions'
      CurrentActionMsg  = _U('open_bossmenu')
      CurrentActionData = {}
    end

    if zone == 'Stocks' then
      CurrentAction     = 'menu_stock'
      CurrentActionMsg  = _U('open_stock')
      CurrentActionData = {}
    end
  end
end)

-- Quand le joueur sort de la zone
AddEventHandler('esx_brinks:hasExitedMarker', function(zone)

  TriggerServerEvent('esx_brinks:stopHarvest')
  TriggerServerEvent('esx_brinks:stopHarvest2')
  TriggerServerEvent('esx_brinks:stopCraft')
  TriggerServerEvent('esx_brinks:stopSelling')

  CurrentAction = nil
  ESX.UI.Menu.CloseAll()
end)

-- Create Blips
Citizen.CreateThread(function()
  local blip = AddBlipForCoord(Config.Zones.MecanoActions.Pos.x, Config.Zones.MecanoActions.Pos.y, Config.Zones.MecanoActions.Pos.z)
  SetBlipSprite (blip, 67)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 1.0)
  SetBlipColour (blip, 52)
  SetBlipAsShortRange(blip, true)
  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString("Convoyeur de fonds")
  EndTextCommandSetBlipName(blip)
end)

-- Activation du marker au sol
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if PlayerData.job ~= nil then
      local coords = GetEntityCoords(GetPlayerPed(-1))

      if PlayerData.job.name == Config.nameJob then

        for k, v in pairs(Config.Zones) do
          if v ~= Config.Zones.Cloakroom then
            if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
              DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
            end
          end
        end

        local Cloakroom = Config.Zones.Cloakroom
        if(Cloakroom.Type ~= -1 and GetDistanceBetweenCoords(coords, Cloakroom.Pos.x, Cloakroom.Pos.y, Cloakroom.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(Cloakroom.Type, Cloakroom.Pos.x, Cloakroom.Pos.y, Cloakroom.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Cloakroom.Size.x, Cloakroom.Size.y, Cloakroom.Size.z, Cloakroom.Color.r, Cloakroom.Color.g, Cloakroom.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end
    end
  end
end)

-- Detection de l'entrer/sortie de la zone du joueur
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if PlayerData.job ~= nil then
      local coords = GetEntityCoords(GetPlayerPed(-1))
      local isInMarker = false
      local currentZone = nil

      if PlayerData.job.name == Config.nameJob then
        if InService then
          for k, v in pairs(Config.Zones) do
            if v ~= Config.Zones.Cloakroom then
              if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) <= v.Size.x) then
                isInMarker = true
                currentZone = k
              end
            end
          end
        end

        local Cloakroom = Config.Zones.Cloakroom
        if(GetDistanceBetweenCoords(coords, Cloakroom.Pos.x, Cloakroom.Pos.y, Cloakroom.Pos.z, true) <= Cloakroom.Size.x) then
          isInMarker  = true
          currentZone = "Cloakroom"
        end
      end

      if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
        HasAlreadyEnteredMarker = true
        LastZone = currentZone
        TriggerEvent('esx_brinks:hasEnteredMarker', currentZone)
      end

      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('esx_brinks:hasExitedMarker', LastZone)
      end
    end
  end
end)

-- Action après la demande d'accés
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, - 1)

      if (IsControlJustReleased(1, Keys["E"]) and PlayerData.job ~= nil and PlayerData.job.name == 'brinks' then

        local playerPed = GetPlayerPed(-1)

        if PlayerData.job.name == Config.nameJob then

          if CurrentAction == 'menu_cloakroom' then
            OpenCloakroomMenu()
          end

          if InService then

            if CurrentAction == 'vehiclespawn_menu' then
              VehicleMenu()
            end

            if CurrentAction == 'menu_stock' then
              OpenStockMenu()
            end

            if CurrentAction == 'craft_menu' then
              TriggerServerEvent('esx_brinks:startCraft')
            end

            if CurrentAction == 'harvest_menu' then
              TriggerServerEvent('esx_brinks:startHarvest')
            end

            if CurrentAction == 'vente' then
              TriggerServerEvent('esx_brinks:startVente')
            end

            if CurrentAction == 'delete_vehicle' then

              local playerPed   = GetPlayerPed(-1)
              local vehicle     = GetVehiclePedIsIn(playerPed, false)
              local hash        = GetEntityModel(vehicle)
              local plate       = GetVehicleNumberPlateText(vehicle)
              local plate       = string.gsub(plate, " ", "")
              local platePrefix = Config.platePrefix

              if string.find (plate, platePrefix) then
                local truck = Config.Vehicles.Truck

                if hash == GetHashKey(truck.Hash) then
                  if GetVehicleEngineHealth(vehicle) <= 500 or GetVehicleBodyHealth(vehicle) <= 500 then
                    ESX.ShowNotification(_U('vehicle_broken'))
                  else
                    TriggerServerEvent('esx_vehiclelock:vehjobSup', plate, 'no')
                    DeleteVehicle(vehicle)
                  end
                end
              else
                ESX.ShowNotification(_U('bad_vehicle'))
              end
            end
          end
          CurrentAction = nil
        end
      end

      if InService and IsControlJustReleased(0, Keys['F6']) and not IsDead and PlayerData.job ~= nil and PlayerData.job.name == 'brinks' then
        OpenMobileBrinksActionsMenu()
      end
    end
  end
end)

AddEventHandler('esx:onPlayerDeath', function()
  IsDead = true
end)

AddEventHandler('playerSpawned', function(spawn)
  IsDead = false
end)

RegisterNetEvent('esx_brinks:callBrinks')
AddEventHandler('esx_brinks:callBrinks', function(datas)
  local pos = GetEntityCoords(GetPlayerPed(-1), false)

  TriggerServerEvent('esx_brinks:newCall', datas.type, {x=pos.x, y=pos.y})
  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)

RegisterNetEvent('esx_brinks:noBrinks')
AddEventHandler('esx_brinks:noBrinks', function()
  ESX.ShowNotification("~r~Aucun Convoyeurs en Service Actuellement")
end)

RegisterNetEvent('esx_brinks:callTaked')
AddEventHandler('esx_brinks:callTaked', function()
  ESX.ShowNotification("~g~Votre appel viens d'être accepter, un convoyeur est en route")
end)

RegisterNetEvent('esx_brinks:endTaked')
AddEventHandler('esx_brinks:endTaked', function()
  ESX.ShowNotification("~g~Appel Terminer")
end)

RegisterNetEvent('esx_brinks:refuseTaked')
AddEventHandler('esx_brinks:refuseTaked', function()
  ESX.ShowNotification("~r~Votre Appel à été refusé")
end)

RegisterNetEvent('esx_brinks:cancelCall')
AddEventHandler('esx_brinks:cancelCall', function()
  ESX.TriggerServerCallback("esx_brinks:cancelCall", function(result)
    if result then
      ESX.ShowNotification("~g~Appel Annulé")
    end
  end)
end)