INSERT INTO `addon_account` (name, label, shared) VALUES
  ('society_brinks', 'Brinks', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  ('society_brinks', 'Brinks', 1)
;

INSERT INTO `jobs` (`name`, `label`) VALUES
  ('brinks', 'Brinks')
;

INSERT INTO `job_grades` (`job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
  ('brinks', 0, 'interim', 'Intérimaire', 1500, '{}', '{}'),
  ('brinks', 1, 'operator', 'Opérateur', 1500, '{}', '{}'),
  ('brinks', 2, 'conveyor', 'Convoyeur', 1800, '{}', '{}'),
  ('brinks', 3, 'chief', 'Chef de mouvement', 2000, '{}', '{}'),
  ('brinks', 4, 'director', 'Directeur', 2200, '{}', '{}')
;


INSERT INTO `items` (name, label) VALUES
  ('battery','Batterie'),
  ('pvc','PVC'),
  ('tazer', 'Tazer')
;
