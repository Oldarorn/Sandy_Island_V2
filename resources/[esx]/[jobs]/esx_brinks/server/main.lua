ESX                = nil
PlayersHarvesting  = {}
PlayersHarvesting2 = {}
PlayersCrafting    = {}
PlayersSelling     = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'brinks', Config.MaxInService)
end

TriggerEvent('esx_society:registerSociety', 'brinks', 'Brinks', 'society_brinks', 'society_brinks', 'society_brinks', {type = 'private'})

-- [ Run Tazer ] --
---- Récupération 1
local function Harvest(source)

  SetTimeout(4000, function()

    if PlayersHarvesting[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local batteryQuantity = xPlayer.getInventoryItem('battery').count

      if batteryQuantity >= 5 then
        TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
      else
        xPlayer.addInventoryItem('battery', 1)
        Harvest(source)
      end
    end
  end)
end

RegisterServerEvent('esx_brinks:startHarvest')
AddEventHandler('esx_brinks:startHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('recovery_gas_can'))
  Harvest(source)
end)

RegisterServerEvent('esx_brinks:stopHarvest')
AddEventHandler('esx_brinks:stopHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = false
end)

---- Récupération 2
local function Harvest2(source)

  SetTimeout(4000, function()

    if PlayersHarvesting2[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local pvcQuantity = xPlayer.getInventoryItem('pvc').count

      if pvcQuantity >= 5 then
        TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
      else
        xPlayer.addInventoryItem('pvc', 1)
        Harvest2(source)
      end
    end
  end)
end

RegisterServerEvent('esx_brinks:startHarvest2')
AddEventHandler('esx_brinks:startHarvest2', function()
  local _source = source
  PlayersHarvesting2[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('recovery_pvc'))
  Harvest2(source)
end)

RegisterServerEvent('esx_brinks:stopHarvest2')
AddEventHandler('esx_brinks:stopHarvest2', function()
  local _source = source
  PlayersHarvesting2[_source] = false
end)

---- Craft
local function Craft(source)
  SetTimeout(4000, function()

    if PlayersCrafting[source] == true then

      local xPlayer         = ESX.GetPlayerFromId(source)
      local batteryQuantity = xPlayer.getInventoryItem('battery').count
      local pvcQuantity     = xPlayer.getInventoryItem('pvc').count

      if batteryQuantity.limit ~= -1 and batteryQuantity.count >= batteryQuantity.limit and batteryQuantity.limit ~= -1 then
        TriggerClientEvent('esx:showNotification', source, _U('not_enough_gas_can'))
      elseif pvcQuantity.limit ~= -1 and pvcQuantity.count >= pvcQuantity.limit and pvcQuantity.limit ~= -1 then
        TriggerClientEvent('esx:showNotification', source, "Vous n'avez pas assez de PVC")
      else
        xPlayer.removeInventoryItem('battery', 1)
        xPlayer.removeInventoryItem('pvc', 1)
        xPlayer.addInventoryItem('tazer', 1)
        Craft(source)
      end
    end
  end)
end

RegisterServerEvent('esx_brinks:startCraft')
AddEventHandler('esx_brinks:startCraft', function()
  local _source = source
  PlayersCrafting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('assembling_blowtorch'))
  Craft(_source)
end)

RegisterServerEvent('esx_brinks:stopCraft')
AddEventHandler('esx_brinks:stopCraft', function()
  local _source = source
  PlayersCrafting[_source] = false
end)

---- Vente
function Sell(source)

  SetTimeout(5000, function()

    math.randomseed(os.time())
    local _source          = source
    local xPlayer          = ESX.GetPlayerFromId(_source)
    local total            = math.random(50)
    local playerMoney      = math.floor(total / 100 * 30)
    local societyMoney     = math.floor(total / 100 * 70)

    if xPlayer.job.grade >= 3 then
      total = total * 2
    end

    TriggerEvent('esx_addonaccount:getSharedAccount', 'society_brinks', function(account)
      societyAccount = account
    end)

    if societyAccount ~= nil then

      if PlayersSelling[source] == true then

        local lockpickQuantity = xPlayer.getInventoryItem('tazer').count

        if endromedQuantity == 0 then
          TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas de Tazer à vendre !')
        else
          xPlayer.removeInventoryItem('tazer', 1)
          xPlayer.addMoney(playerMoney)
          societyAccount.addMoney(societyMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_earned') .. playerMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. societyMoney)
          Sell(source)
        end
      end
    end
  end)
end

RegisterServerEvent('esx_brinks:startSell')
AddEventHandler('esx_brinks:startSell', function()

  local _source = source
  PlayersSelling[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))
  Sell(_source)
end)

RegisterServerEvent('esx_brinks:stopSell')
AddEventHandler('esx_brinks:stopSell', function()

  local _source = source
  PlayersSelling[_source] = false
end)

function SendNotificationToPlayersInService(message)
  local xPlayers  = ESX.GetPlayers()
  local countCops = 0

  for k, v in pairs(xPlayers) do
    ESX.TriggerServerCallback('esx_service:isInService', 0, v, function(inService)
      if inService then
        TriggerClientEvent('esx:showNotification', v, message)
        countCops = countCops + 1
      end
    end, 'brinks')
  end

  if countCops <= 0 then
    return false
  end
  return true
end

ESX.RegisterServerCallback('esx_brinks:getCallsList', function(source, cb)
  local _source = source
  cb(CallsList)
end)

RegisterNetEvent('esx_brinks:newCall')
AddEventHandler('esx_brinks:newCall', function(type, pos, message)
  local _source = source
  if type == "Prise de contact" then
    message = "Une personne souhaite prendre contact"
  end
  CallsList[_source] = {
    type = type,
    position = pos,
    text = message,
    nbr = 0
  }
  local notif = "Un nouvel appel viens d'arriver :"
  if not SendNotificationToPlayersInService(notif) then
    TriggerClientEvent('esx_brinks:noBrinks', _source)
  else
    SendNotificationToPlayersInService("~r~"..message)
  end
end)

RegisterNetEvent('esx_brinks:takeCall')
AddEventHandler('esx_brinks:takeCall', function(id)
  CallsList[id].nbr = CallsList[id].nbr + 1
  TriggerClientEvent('esx_brinks:callTaked', id)
end)

RegisterNetEvent('esx_brinks:endCall')
AddEventHandler('esx_brinks:endCall', function(id)
  TriggerClientEvent('esx_brinks:endCall', id)
  CallsList[id] = nil
end)

RegisterNetEvent('esx_brinks:refuseCall')
AddEventHandler('esx_brinks:refuseCall', function(id)
  TriggerClientEvent('esx_brinks:refuseCall', id)
  CallsList[id] = nil
end)

ESX.RegisterServerCallback("esx_brinks:cancelCall", function(source, cb)
  local _source = source
  if CallsList[_source] ~= nil then
    CallsList[_source] = nil
    SendNotificationToPlayersInService("Appel Annulé")
    cb(true)
  else
    cb(false)
  end
end)
