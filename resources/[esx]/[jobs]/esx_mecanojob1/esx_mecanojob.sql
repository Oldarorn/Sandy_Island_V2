USE `essentialmode`;

INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_mecano1', 'Mécano du nord', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_mecano1', 'Mécano du nord', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('mecano1', 'Mécano du nord')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('mecano1',0,'recrue','Recrue',12,'{}','{}'),
	('mecano1',1,'novice','Novice',24,'{}','{}'),
	('mecano1',2,'experimente','Experimente',36,'{}','{}'),
	('mecano1',3,'chief','Chef d\'équipe',48,'{}','{}'),
	('mecano1',4,'boss','Patron',0,'{}','{}')
;

INSERT INTO `items` (name, label) VALUES
	('iron', 'Fer'),
	('lockpick', 'Kits de crochetage'),
	('electrod', 'Eléctrode'),
  ('defibrillator', 'Défibrillateur'),
  ('electronique', 'Électronique')
;
