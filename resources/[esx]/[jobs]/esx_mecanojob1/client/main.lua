local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local GUI                     = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local OnJob                   = false
local TargetCoords            = nil
local CurrentlyTowedVehicle   = nil
local Blips                   = {}

local NPCOnJob                = false
local NPCTargetTowable        = nil
local NPCTargetTowableZone    = nil
local NPCHasSpawnedTowable    = false
local NPCLastCancel           = GetGameTimer() - 5 * 60000
local NPCHasBeenNextToTowable = false
local NPCTargetDeleterZone    = false
local IsDead                  = false
local InService               = false

ESX                           = nil
GUI.Time                      = 0

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end

	Citizen.Wait(5000)
	PlayerData = ESX.GetPlayerData()
end)

function SelectRandomTowable()

  local index = GetRandomIntInRange(1,  #Config.Towables)

  for k,v in pairs(Config.Zones) do
    if v.Pos.x == Config.Towables[index].x and v.Pos.y == Config.Towables[index].y and v.Pos.z == Config.Towables[index].z then
      return k
    end
  end
end

function StartNPCJob()

  NPCOnJob = true

  NPCTargetTowableZone = SelectRandomTowable()
  local zone       = Config.Zones[NPCTargetTowableZone]

  Blips['NPCTargetTowableZone'] = AddBlipForCoord(zone.Pos.x,  zone.Pos.y,  zone.Pos.z)
  SetBlipRoute(Blips['NPCTargetTowableZone'], true)
  ESX.ShowNotification(_U('drive_to_indicated'))
end

function StopNPCJob(cancel)

  if Blips['NPCTargetTowableZone'] ~= nil then
    RemoveBlip(Blips['NPCTargetTowableZone'])
    Blips['NPCTargetTowableZone'] = nil
  end

  if Blips['NPCDelivery'] ~= nil then
    RemoveBlip(Blips['NPCDelivery'])
    Blips['NPCDelivery'] = nil
  end

  Config.Zones.VehicleDelivery.Type = -1

  NPCOnJob                = false
  NPCTargetTowable        = nil
  NPCTargetTowableZone    = nil
  NPCHasSpawnedTowable    = false
  NPCHasBeenNextToTowable = false

  if cancel then
    ESX.ShowNotification(_U('mission_canceled'))
  else
    TriggerServerEvent('esx_mecanojob1:onNPCJobCompleted')
  end
end

function OpenVehicleSpawnerMenu()

  local elements = {
    {label = _U('vehicle_list'), value = 'vehicle_list'},
  }

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'mecano1_actions',
    {
      title    = _U('mechanic'),
      align    = 'top-left',
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'vehicle_list' then

        if Config.EnableSocietyOwnedVehicles then

            local elements = {}

            ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

              for i=1, #vehicles, 1 do
                table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
              end

              ESX.UI.Menu.Open(
                'default', GetCurrentResourceName(), 'vehicle_spawner',
                {
                  title    = _U('service_vehicle'),
                  align    = 'top-left',
                  elements = elements,
                },
                function(data, menu)

                  menu.close()

                  local vehicleProps = data.current.value

                  ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
                    ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                    local playerPed = GetPlayerPed(-1)
                    TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
                  end)

                  TriggerServerEvent('esx_society:removeVehicleFromGarage', 'mecano1', vehicleProps)
                end,
                function(data, menu)
                  menu.close()
                end
              )
            end, 'mecano1')
          else

            local elements = {
              {label = _U('flat_bed'), value = 'flatbed'},
              {label = _U('tow_truck'), value = 'towtruck2'}
            }

            if Config.EnablePlayerManagement and PlayerData.job ~= nil and
              (PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'chef' or PlayerData.job.grade_name == 'experimente') then
              table.insert(elements, {label = 'SlamVan', value = 'slamvan3'})
            end

            ESX.UI.Menu.CloseAll()

            ESX.UI.Menu.Open(
              'default', GetCurrentResourceName(), 'spawn_vehicle',
              {
                title    = _U('service_vehicle'),
                align    = 'top-left',
                elements = elements
              },
              function(data, menu)
                for i=1, #elements, 1 do
                  if Config.MaxInService == -1 then
                    ESX.Game.SpawnVehicle(data.current.value, Config.Zones.VehicleSpawnPoint.Pos, 90.0, function(vehicle)
                      local playerPed = GetPlayerPed(-1)
                      TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
                    end)
                    break
                  else
                    ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)
                      if canTakeService then
                        ESX.Game.SpawnVehicle(data.current.value, Config.Zones.VehicleSpawnPoint.Pos, 90.0, function(vehicle)
                          local playerPed = GetPlayerPed(-1)
                          TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                        end)
                      else
                        ESX.ShowNotification(_U('service_full') .. inServiceCount .. '/' .. maxInService)
                      end
                    end, 'mecano1')
                    break
                  end
                end
                menu.close()
              end,
              function(data, menu)
                menu.close()
                OpenVehicleSpawnerMenu()
              end
            )
          end
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'mecano1_actions_menu'
      CurrentActionMsg  = _U('open_actions')
      CurrentActionData = {}
    end
  )
end

function OpenStockMenu()

  local elements = {
    {label = _U('get_object'), value = 'get_stock'},
    {label = _U('put_object'), value = 'put_stock'}
  }

  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'vault',
    {
      title    = 'Coffre',
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      if data.current.value == 'put_stock' then
        OpenPutStocksMenu()
      end

      if data.current.value == 'get_stock' then
        OpenGetStocksMenu()
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_stock'
      CurrentActionMsg  = _U('open_stock')
      CurrentActionData = {}
    end
  )
end

function OpenCloakroomMenu()

  local elements = {
    {label = _U('citizen_wear'), value = 'cloakroom2'},
    {label = _U('emploie_outfit'), value = 'cloakroom'},
  }

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = _U('cloakroom'),
      align    = 'top-left',
      elements = elements,
    },

      function(data, menu)
      --menu.close()

        if data.current.value == 'cloakroom' then
        menu.close()
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

          if skin.sex == 0 then
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
          else
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
          end

          ESX.TriggerServerCallback('esx_service:enableService', function(result)
            if result then
              ESX.ShowNotification('~g~Prise de Service')
              InService = true
              TriggerEvent("esx_mecanojob1:mecanoStartService")
            end
          end, 'mecano1')
        end)
      end

      if data.current.value == 'cloakroom2' then
        menu.close()
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

          TriggerEvent('skinchanger:loadSkin', skin)
          TriggerServerEvent('esx_service:disableService', 'mecano1')
          ESX.ShowNotification("~r~Fin de Service")
          InService = false
          TriggerEvent("esx_mecanojob1:mecanoStopService")
        end)
      end
      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour vous changer'
      CurrentActionData = {}
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour vous changer'
      CurrentActionData = {}
    end
  )
end

function OpenMobileMecano1ActionsMenu()

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'mobile_mecano1_actions',
    {
      title    = _U('mechanic'),
      align    = 'top-left',
      elements = {
        {label = "Appels",            value = 'calls'},
        {label = "GPS",               value = 'gps'},
        {label = _U('billing'),       value = 'billing'},
        {label = _U('hijack'),        value = 'hijack_vehicle'},
        {label = _U('repair'),        value = 'fix_vehicle'},
        {label = _U('clean'),         value = 'clean_vehicle'},
        {label = _U('imp_veh'),       value = 'del_vehicle'},
        {label = _U('flat_bed'),      value = 'dep_vehicle'},
        {label = "Changer la Plaque", value = 'change_plate'}
      }
    },
    function(data, menu)

      if data.current.value == 'calls' then
        ESX.TriggerServerCallback('esx_mecanojob1:getCallsList', function(calls)
          local options = {}

          if calls ~= nil then
            for k, v in pairs(calls) do
              table.insert(options, {label = v.text, value = k})
            end
          else
            table.insert(options, {label = "Aucun Appel en Attente"})
          end
            ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'taxi_calls',
            {
              title    = 'Mécano - Appels',
              align    = 'top-left',
              elements = options
            }, function(data2, menu2)
              local callid = data2.current.value
              local options2 = {
                {label = "Prendre l'appel",     value="take_call"},
                {label = "Position de l'appel", value="pos_call"},
                {label = "Appel Résolu",        value="end_call"},
                {label = "Refuser l'appel",     value="refused_call"}
              }
              ESX.UI.Menu.Open(
              'default', GetCurrentResourceName(), 'taxi_call',
              {
                title    = 'Actions',
                align    = 'top-left',
                elements = options2
              }, function(data3, menu3)
                if data3.current.value == "take_call" and SelectedCall ~= data2.current.value then
                  SelectedCall = data2.current.value
                  TriggerServerEvent('esx_mecanojob1:takeCall', data2.current.value)
                  local x = calls[data2.current.value].position.x
                  local y = calls[data2.current.value].position.y
                  SetNewWaypoint(x, y)
                end
                if data3.current.value == "pos_call" then
                  local x = calls[data2.current.value].position.x
                  local y = calls[data2.current.value].position.y
                  SetNewWaypoint(x, y)
                end
                if data3.current.value == "end_call" then
                  TriggerServerEvent('esx_mecanojob1:endCall', callid)
                  ESX.ShowNotification("~g~Appel Résolu")
                end
                if data3.current.value == "refused_call" then
                  TriggerServerEvent("esx_mecanojob1:refuseCall", callid)
                end
              end,
              function(data3, menu3)
                menu3.close()
              end
            )
          end,
          function(data2, menu2)
            menu2.close()
          end)
        end)
      elseif data.current.value == 'gps' then

        local blips = {
          {title="Récolte",       x = 2358.042, y = 3138.705, z = 47.208 },
          {title="Fabrication",   x = -246.878, y = 6068.477, z = 31.344 },
          {title="Revente",       x = 162.477, y = -1809.861, z = 81.735 }
        }

        for _, info in pairs(blips) do
          info.blip = AddBlipForCoord(info.x, info.y, info.z)
          SetBlipSprite(info.blip, 1)
          SetBlipDisplay(info.blip, 4)
          SetBlipScale(info.blip, 0.8)
          SetBlipColour(info.blip, 27)
          SetBlipAsShortRange(info.blip, true)

          BeginTextCommandSetBlipName("STRING")
          AddTextComponentString(info.title)
          EndTextCommandSetBlipName(info.blip)
        end
        ESX.ShowNotification("Position ajouté à votre gps !")
      elseif data.current.value == 'billing' then
        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'billing',
          {
            title = _U('invoice_amount')
          },
          function(data, menu)
            local amount = tonumber(data.value)
            if amount == nil or amount < 0 then
              ESX.ShowNotification(_U('amount_invalid'))
            else
              menu.close()
              local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
              if closestPlayer == -1 or closestDistance > 3.0 then
                ESX.ShowNotification(_U('no_players_nearby'))
              else
                TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_mecano1', _U('mechanic'), amount)
              end
            end
          end,
        function(data, menu)
          menu.close()
        end
        )
      elseif data.current.value == 'hijack_vehicle' then

        local playerPed = GetPlayerPed(-1)
        local coords    = GetEntityCoords(playerPed)

        if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then

          local vehicle = nil

          if IsPedInAnyVehicle(playerPed, false) then
            vehicle = GetVehiclePedIsIn(playerPed, false)
          else
            vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
          end

          if DoesEntityExist(vehicle) then
            TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_WELDING", 0, true)
            Citizen.CreateThread(function()
              Citizen.Wait(10000)
              SetVehicleDoorsLocked(vehicle, 1)
              SetVehicleDoorsLockedForAllPlayers(vehicle, false)
              ClearPedTasksImmediately(playerPed)
              ESX.ShowNotification(_U('vehicle_unlocked'))
            end)
          end
        end
      elseif data.current.value == 'fix_vehicle' then

        local playerPed = GetPlayerPed(-1)
        local coords    = GetEntityCoords(playerPed)

        if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then

          local vehicle = nil

          if IsPedInAnyVehicle(playerPed, false) then
            vehicle = GetVehiclePedIsIn(playerPed, false)
          else
            vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
          end

          if DoesEntityExist(vehicle) then
            TaskStartScenarioInPlace(playerPed, "PROP_HUMAN_BUM_BIN", 0, true)
            Citizen.CreateThread(function()
              Citizen.Wait(20000)
              SetVehicleFixed(vehicle)
              SetVehicleDeformationFixed(vehicle)
              SetVehicleUndriveable(vehicle, false)
              SetVehicleEngineOn(vehicle,  true,  true)
              ClearPedTasksImmediately(playerPed)
              ESX.ShowNotification(_U('vehicle_repaired'))
            end)
          end
        end
      elseif data.current.value == 'clean_vehicle' then

        local playerPed = GetPlayerPed(-1)
        local coords    = GetEntityCoords(playerPed)

        if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then

          local vehicle = nil

          if IsPedInAnyVehicle(playerPed, false) then
            vehicle = GetVehiclePedIsIn(playerPed, false)
          else
            vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
          end

          if DoesEntityExist(vehicle) then
            TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_MAID_CLEAN", 0, true)
            Citizen.CreateThread(function()
              Citizen.Wait(10000)
              SetVehicleDirtLevel(vehicle, 0)
              ClearPedTasksImmediately(playerPed)
              ESX.ShowNotification(_U('vehicle_cleaned'))
            end)
          end
        end
      elseif data.current.value == 'del_vehicle' then

        local ped = GetPlayerPed( -1 )

        if ( DoesEntityExist( ped ) and not IsEntityDead( ped ) ) then
          local pos = GetEntityCoords( ped )

          if ( IsPedSittingInAnyVehicle( ped ) ) then
            local vehicle = GetVehiclePedIsIn( ped, false )

            if ( GetPedInVehicleSeat( vehicle, -1 ) == ped ) then
              ESX.ShowNotification(_U('vehicle_impounded'))
              SetEntityAsMissionEntity( vehicle, true, true )
              deleteCar( vehicle )
            else
              ESX.ShowNotification(_U('must_seat_driver'))
            end
          else
            local playerPos = GetEntityCoords( ped, 1 )
            local inFrontOfPlayer = GetOffsetFromEntityInWorldCoords( ped, 0.0, distanceToCheck, 0.0 )
            local vehicle = GetVehicleInDirection( playerPos, inFrontOfPlayer )

            if ( DoesEntityExist( vehicle ) ) then
              ESX.ShowNotification(_U('vehicle_impounded'))
              SetEntityAsMissionEntity( vehicle, true, true )
              deleteCar( vehicle )
            else
              ESX.ShowNotification(_U('must_near'))
            end
          end
        end
      elseif data.current.value == 'dep_vehicle' then

        local playerped = GetPlayerPed(-1)
        local vehicle = GetVehiclePedIsIn(playerped, true)

        local towmodel = GetHashKey('flatbed')
        local isVehicleTow = IsVehicleModel(vehicle, towmodel)

        if isVehicleTow then

          local coordA = GetEntityCoords(playerped, 1)
          local coordB = GetOffsetFromEntityInWorldCoords(playerped, 0.0, 5.0, 0.0)
          local targetVehicle = getVehicleInDirection(coordA, coordB)

          if CurrentlyTowedVehicle == nil then
            if targetVehicle ~= 0 then
              if not IsPedInAnyVehicle(playerped, true) then
                if vehicle ~= targetVehicle then
                  AttachEntityToEntity(targetVehicle, vehicle, 20, -0.5, -5.0, 1.0, 0.0, 0.0, 0.0, false, false, false, false, 20, true)
                  CurrentlyTowedVehicle = targetVehicle
                  ESX.ShowNotification(_U('vehicle_success_attached'))

                  if NPCOnJob then

                    if NPCTargetTowable == targetVehicle then
                      ESX.ShowNotification(_U('please_drop_off'))

                      Config.Zones.VehicleDelivery.Type = 1

                      if Blips['NPCTargetTowableZone'] ~= nil then
                        RemoveBlip(Blips['NPCTargetTowableZone'])
                        Blips['NPCTargetTowableZone'] = nil
                      end

                      Blips['NPCDelivery'] = AddBlipForCoord(Config.Zones.VehicleDelivery.Pos.x,  Config.Zones.VehicleDelivery.Pos.y,  Config.Zones.VehicleDelivery.Pos.z)
                      SetBlipRoute(Blips['NPCDelivery'], true)
                    end
                  end
                else
                  ESX.ShowNotification(_U('cant_attach_own_tt'))
                end
              end
            else
              ESX.ShowNotification(_U('no_veh_att'))
            end
          else

            AttachEntityToEntity(CurrentlyTowedVehicle, vehicle, 20, -0.5, -12.0, 1.0, 0.0, 0.0, 0.0, false, false, false, false, 20, true)
            DetachEntity(CurrentlyTowedVehicle, true, true)

            if NPCOnJob then

              if NPCTargetDeleterZone then

                if CurrentlyTowedVehicle == NPCTargetTowable then
                  ESX.Game.DeleteVehicle(NPCTargetTowable)
                  TriggerServerEvent('esx_mecanojob1:onNPCJobMissionCompleted')
                  StopNPCJob()
                  NPCTargetDeleterZone = false
                else
                  ESX.ShowNotification(_U('not_right_veh'))
                end
              else
                ESX.ShowNotification(_U('not_right_place'))
              end
            end

            CurrentlyTowedVehicle = nil
            ESX.ShowNotification(_U('veh_det_succ'))
          end
        else
          ESX.ShowNotification(_U('imp_flatbed'))
        end
      elseif data.current.value == 'change_plate' then
        local playerPed = GetPlayerPed(-1)
        local coords    = GetEntityCoords(playerPed)
        local plate = ''

        if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then

          local vehicle = nil

          if IsPedInAnyVehicle(playerPed, false) then
            vehicle = GetVehiclePedIsIn(playerPed, false)
          else
            vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
          end

          if DoesEntityExist(vehicle) then
            DisplayOnscreenKeyboard(1, "FMMC_MPM_NA", "", GetVehicleNumberPlateText(vehicle), "", "", "", 8)
            while (UpdateOnscreenKeyboard() == 0) do
              DisableAllControlActions(0);
              Citizen.Wait(1);
            end
            if (GetOnscreenKeyboardResult()) then
              plate = GetOnscreenKeyboardResult()
            end
            local alreadyExist = false
            ESX.TriggerServerCallback('esx_vehicleshop:isVehicleOwned', function(result)
              if result then
                alreadyExist = true
              end
            end, plate)
            if not alreadyExist then
              TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_HAMMERING", 0, true)
              Citizen.CreateThread(function()
                local lastPlate = GetVehicleNumberPlateText(vehicle)
                Citizen.Wait(5000)
                SetVehicleNumberPlateText(vehicle, plate)
                ClearPedTasksImmediately(playerPed)
                ESX.ShowNotification(_U('vehicle_cleaned'))
                TriggerServerEvent('esx_mecanojob1:plateChanged', lastPlate, plate)
              end)
            else
              ESX.ShowNotification('~r~Un Véhicule possède déjà cette immatriculation')
            end
          end
        end
      end
    end,
  function(data, menu)
    menu.close()
  end
  )
end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('esx_mecanojob1:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('mechanic_stock'),
        align    = 'top-left',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('esx_mecanojob1:getStockItem', itemName, count)
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('esx_mecanojob1:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        align    = 'top-left',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('esx_mecanojob1:putStockItems', itemName, count)
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

RegisterNetEvent('esx_mecanojob1:onHijack')
AddEventHandler('esx_mecanojob1:onHijack', function()
  local playerPed = GetPlayerPed(-1)
  local coords    = GetEntityCoords(playerPed)

  if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then

    local vehicle = nil

    if IsPedInAnyVehicle(playerPed, false) then
      vehicle = GetVehiclePedIsIn(playerPed, false)
    else
      vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
    end

    local crochete = math.random(100)
    local alarm    = math.random(100)

    if DoesEntityExist(vehicle) then
      if alarm <= 33 then
        SetVehicleAlarm(vehicle, true)
        StartVehicleAlarm(vehicle)
      end
      TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_WELDING", 0, true)
      Citizen.CreateThread(function()
        Citizen.Wait(10000)
        if crochete <= 66 then
          SetVehicleDoorsLocked(vehicle, 1)
          SetVehicleDoorsLockedForAllPlayers(vehicle, false)
          ClearPedTasksImmediately(playerPed)
          ESX.ShowNotification(_U('veh_unlocked'))
        else
          ESX.ShowNotification(_U('hijack_failed'))
          ClearPedTasksImmediately(playerPed)
        end
      end)
    end
  end
end)

RegisterNetEvent('esx_mecanojob1:onCarokit')
AddEventHandler('esx_mecanojob1:onCarokit', function()
  local playerPed = GetPlayerPed(-1)
  local coords    = GetEntityCoords(playerPed)

  if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then

    local vehicle = nil

    if IsPedInAnyVehicle(playerPed, false) then
      vehicle = GetVehiclePedIsIn(playerPed, false)
    else
      vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
    end

    if DoesEntityExist(vehicle) then
      TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_HAMMERING", 0, true)
      Citizen.CreateThread(function()
        Citizen.Wait(10000)
        SetVehicleFixed(vehicle)
        SetVehicleDeformationFixed(vehicle)
        ClearPedTasksImmediately(playerPed)
        ESX.ShowNotification(_U('body_repaired'))
      end)
    end
  end
end)

RegisterNetEvent('esx_mecanojob1:onFixkit')
AddEventHandler('esx_mecanojob1:onFixkit', function()
  local playerPed = GetPlayerPed(-1)
  local coords    = GetEntityCoords(playerPed)

  if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then

    local vehicle = nil

    if IsPedInAnyVehicle(playerPed, false) then
      vehicle = GetVehiclePedIsIn(playerPed, false)
    else
      vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)
    end

    if DoesEntityExist(vehicle) then
      TaskStartScenarioInPlace(playerPed, "PROP_HUMAN_BUM_BIN", 0, true)
      Citizen.CreateThread(function()
        Citizen.Wait(20000)
        SetVehicleFixed(vehicle)
        SetVehicleDeformationFixed(vehicle)
        SetVehicleUndriveable(vehicle, false)
        ClearPedTasksImmediately(playerPed)
        ESX.ShowNotification(_U('veh_repaired'))
      end)
    end
  end
end)

function setEntityHeadingFromEntity ( vehicle, playerPed )
    local heading = GetEntityHeading(vehicle)
    SetEntityHeading( playerPed, heading )
end

function getVehicleInDirection(coordFrom, coordTo)
  local rayHandle = CastRayPointToPoint(coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 10, GetPlayerPed(-1), 0)
  local a, b, c, d, vehicle = GetRaycastResult(rayHandle)
  return vehicle
end

function deleteCar( entity )
  Citizen.InvokeNative( 0xEA386986E786A54F, Citizen.PointerValueIntInitialized( entity ) )
end

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

AddEventHandler('esx_mecanojob1:hasEnteredMarker', function(zone)

  if zone == 'Cloakrooms' then
    CurrentAction     = 'menu_cloakroom'
    CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour vous changer'
    CurrentActionData = {}
  end

  --if InService then

    if zone == NPCJobTargetTowable then
    end

    if zone =='VehicleDelivery' then
      NPCTargetDeleterZone = true
    end

    if zone == 'BossActions' then
      CurrentAction     = 'menu_boss_actions'
      CurrentActionMsg  = _U('open_bossmenu')
      CurrentActionData = {}
    end

    if zone == 'Stocks' then
      CurrentAction     = 'menu_stock'
      CurrentActionMsg  = _U('open_stock')
      CurrentActionData = {}
    end

    if zone == 'Mecano1Actions' then
      CurrentAction     = 'mecano1_actions_menu'
      CurrentActionMsg  = _U('open_actions')
      CurrentActionData = {}
    end

    if zone == 'VehicleDeleter' then

      local playerPed = GetPlayerPed(-1)
      if IsPedInAnyVehicle(playerPed,  false) then

        local vehicle = GetVehiclePedIsIn(playerPed,  false)
        CurrentAction     = 'delete_vehicle'
        CurrentActionMsg  = _U('veh_stored')
        CurrentActionData = {vehicle = vehicle}
      end
    end

    if zone == 'Garage' then
      CurrentAction     = 'mecano1_harvest_menu'
      CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour récupérer du fer'
      CurrentActionData = {}
    end

    if zone == 'Craft' then
      CurrentAction     = 'mecano1_craft_menu'
      CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour fabriquer des lockpick'
      CurrentActionData = {}
    end

    if zone == 'Resell' then
      CurrentAction     = 'resell'
      CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour vendre des lockpick'
      CurrentActionData = {}
    end
  --end
end)

AddEventHandler('esx_mecanojob1:hasExitedMarker', function(zone)

  if zone =='VehicleDelivery' then
    NPCTargetDeleterZone = false
  end

  TriggerServerEvent('esx_mecanojob1:stopHarvest')
  TriggerServerEvent('esx_mecanojob1:stopCraft')
  TriggerServerEvent('esx_mecanojob1:stopSell')
  CurrentAction = nil
  ESX.UI.Menu.CloseAll()
end)

-- Pop NPC mission vehicle when inside area
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if NPCTargetTowableZone ~= nil and not NPCHasSpawnedTowable then

      local coords = GetEntityCoords(GetPlayerPed(-1))
      local zone   = Config.Zones[NPCTargetTowableZone]

      if GetDistanceBetweenCoords(coords, zone.Pos.x, zone.Pos.y, zone.Pos.z, true) < Config.NPCSpawnDistance then

        local model = Config.Vehicles[GetRandomIntInRange(1,  #Config.Vehicles)]

        ESX.Game.SpawnVehicle(model, zone.Pos, 0, function(vehicle)
          NPCTargetTowable = vehicle
        end)
        NPCHasSpawnedTowable = true
      end
    end

    if NPCTargetTowableZone ~= nil and NPCHasSpawnedTowable and not NPCHasBeenNextToTowable then

      local coords = GetEntityCoords(GetPlayerPed(-1))
      local zone   = Config.Zones[NPCTargetTowableZone]

      if(GetDistanceBetweenCoords(coords, zone.Pos.x, zone.Pos.y, zone.Pos.z, true) < Config.NPCNextToDistance) then
        ESX.ShowNotification(_U('please_tow'))
        NPCHasBeenNextToTowable = true
      end
    end
  end
end)

-- Create Blips
Citizen.CreateThread(function()
  local blip = AddBlipForCoord(Config.Zones.Mecano1Actions.Pos.x, Config.Zones.Mecano1Actions.Pos.y, Config.Zones.Mecano1Actions.Pos.z)
  SetBlipSprite (blip, 446)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 1.0)
  SetBlipColour (blip, 5)
  SetBlipAsShortRange(blip, true)
  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString(_U('mechanic'))
  EndTextCommandSetBlipName(blip)
end)

-- Display markers
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if PlayerData.job ~= nil and PlayerData.job.name == 'mecano1' then

      local coords = GetEntityCoords(GetPlayerPed(-1))

      for k,v in pairs(Config.Zones) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end
    end
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if PlayerData.job ~= nil and PlayerData.job.name == 'mecano1' then
      local coords      = GetEntityCoords(GetPlayerPed(-1))
      local isInMarker  = false
      local currentZone = nil
      for k,v in pairs(Config.Zones) do
        if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
          isInMarker  = true
          currentZone = k
        end
      end
      if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
        HasAlreadyEnteredMarker = true
        LastZone                = currentZone
        TriggerEvent('esx_mecanojob1:hasEnteredMarker', currentZone)
      end
      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('esx_mecanojob1:hasExitedMarker', LastZone)
      end
    end
  end
end)

-- Key Controls
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlJustReleased(0, 38) and PlayerData.job ~= nil and PlayerData.job.name == 'mecano1' then

        if CurrentAction == 'menu_cloakroom' then
          OpenCloakroomMenu()
        end

       --if InService then

          if CurrentAction == 'menu_stock' then
            OpenStockMenu()
          end

          if CurrentAction == 'mecano1_actions_menu' then
            OpenVehicleSpawnerMenu()
          end

          if CurrentAction == 'mecano1_craft_menu' then
            TriggerServerEvent('esx_mecanojob1:startCraft')
          end

          if CurrentAction == 'resell' then
            TriggerServerEvent('esx_mecanojob1:startSell')
          end

          if CurrentAction == 'mecano1_harvest_menu' then
            TriggerServerEvent('esx_mecanojob1:startHarvest')
          end

          if CurrentAction == 'delete_vehicle' then

            if Config.EnableSocietyOwnedVehicles then

              local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
              TriggerServerEvent('esx_society:putVehicleInGarage', 'mecano1', vehicleProps)
            else

              if
                GetEntityModel(vehicle) == GetHashKey('flatbed')   or
                GetEntityModel(vehicle) == GetHashKey('towtruck2') or
                GetEntityModel(vehicle) == GetHashKey('slamvan3')
                then
                TriggerServerEvent('esx_service:disableService', 'mecano1')
              end
            end
            ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
          end

          if CurrentAction == 'menu_boss_actions' then

            ESX.UI.Menu.CloseAll()

            TriggerEvent('esx_society:openBossMenu', 'mecano1', function(data, menu)

              menu.close()
              CurrentAction     = 'menu_boss_actions'
              CurrentActionMsg  = _U('open_bossmenu')
              CurrentActionData = {}
            end)
          end

          if CurrentAction == 'remove_entity' then
            DeleteEntity(CurrentActionData.entity)
          end
        --end
        CurrentAction = nil
      end
    end

    if IsControlJustReleased(0, Keys['F6'])  and not IsDead and PlayerData.job ~= nil and PlayerData.job.name == 'mecano1' then
      OpenMobileMecano1ActionsMenu()
    end
  end
end)

AddEventHandler('esx:onPlayerDeath', function()
	IsDead = true
end)

AddEventHandler('playerSpawned', function(spawn)
	IsDead = false
end)

RegisterNetEvent('esx_mecanojob1:setmax')
AddEventHandler('esx_mecanojob1:setmax', function()
  local playerPed = GetPlayerPed(-1)
  local vehicle    = GetVehiclePedIsIn(playerPed, false)

  if DoesEntityExist(vehicle) then
    local props = {
      modEngine       = GetVehicleModVariation(vehicle, 11),
      modBrakes       = GetVehicleModVariation(vehicle, 12),
      modTransmission = GetVehicleModVariation(vehicle, 13),
      modSuspension   = GetVehicleModVariation(vehicle, 15),
      modTurbo        = true,
    }
    ESX.Game.SetVehicleProperties(vehicle, props)
    ESX.ShowNotification("Vehicle Maxé")
  end
end)

RegisterNetEvent('esx_mecanojob1:repair')
AddEventHandler('esx_mecanojob1:repair', function()
  local playerPed = GetPlayerPed(-1)
  local vehicle    = GetVehiclePedIsIn(playerPed, false)

  if DoesEntityExist(vehicle) then
    SetVehicleFixed(vehicle)
    SetVehicleDeformationFixed(vehicle)
    SetVehicleUndriveable(vehicle, false)
    SetVehicleEngineOn(vehicle,  true,  true)
    ESX.ShowNotification(_U('vehicle_repaired'))
  end
end)

RegisterNetEvent('esx_mecanojob1:callMecano1')
AddEventHandler('esx_mecanojob1:callMecano1', function(datas)
  local pos = GetEntityCoords(GetPlayerPed(-1), false)

  TriggerServerEvent('esx_mecanojob1:newCall', datas.type, {x=pos.x, y=pos.y})
  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)

RegisterNetEvent('esx_mecanojob1:noMeca')
AddEventHandler('esx_mecanojob1:noMeca', function()
  ESX.ShowNotification("~r~Aucun Mécanicien en Service Actuellement")
end)

RegisterNetEvent('esx_mecanojob1:callTaked')
AddEventHandler('esx_mecanojob1:callTaked', function()
  ESX.ShowNotification("~g~Votre appel viens d'être accepter, un employé est en route")
end)

RegisterNetEvent('esx_mecanojob1:endTaked')
AddEventHandler('esx_mecanojob1:endTaked', function()
  ESX.ShowNotification("~g~Appel Terminer")
end)

RegisterNetEvent('esx_mecanojob1:refuseTaked')
AddEventHandler('esx_mecanojob1:refuseTaked', function()
  ESX.ShowNotification("~r~Votre Appel à été refusé")
end)

RegisterNetEvent('esx_mecanojob1:cancelCall')
AddEventHandler('esx_mecanojob1:cancelCall', function()
  ESX.TriggerServerCallback("esx_mecanojob1:cancelCall", function(result)
    if result then
      ESX.ShowNotification("~g~Appel Annulé")
    end
  end)
end)
