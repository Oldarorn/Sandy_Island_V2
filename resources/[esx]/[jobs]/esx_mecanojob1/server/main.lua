ESX                = nil
PlayersHarvesting  = {}
PlayersCrafting    = {}
PlayersSelling     = {}
local CallsList    = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'mecano1', Config.MaxInService)
end

TriggerEvent('esx_society:registerSociety', 'mecano1', 'Mecano1', 'society_mecano1', 'society_mecano1', 'society_mecano1', {type = 'private'})

-------------- Récupération
local function Harvest(source)

  SetTimeout(4000, function()

    if PlayersHarvesting[source] == true then

      local xPlayer      = ESX.GetPlayerFromId(source)
      local ironQuantity = xPlayer.getInventoryItem('iron').count

      if ironQuantity >= 5 then
        TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
      else
        xPlayer.addInventoryItem('iron', 1)
        Harvest(source)
      end
    end
  end)
end

RegisterServerEvent('esx_mecanojob1:startHarvest')
AddEventHandler('esx_mecanojob1:startHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('recovery_gas_can'))
  Harvest(source)
end)

RegisterServerEvent('esx_mecanojob1:stopHarvest')
AddEventHandler('esx_mecanojob1:stopHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = false
end)

------------ Craft Lockpick
local function Craft(source)

  SetTimeout(4000, function()

    if PlayersCrafting[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local ironQuantity = xPlayer.getInventoryItem('iron').count

      if ironQuantity <= 0 then
        TriggerClientEvent('esx:showNotification', source, _U('not_enough_gas_can'))
      else
        xPlayer.removeInventoryItem('iron', 1)
        xPlayer.addInventoryItem('lockpick', 1)

        Craft(source)
      end
    end
  end)
end

RegisterServerEvent('esx_mecanojob1:startCraft')
AddEventHandler('esx_mecanojob1:startCraft', function()
  local _source = source
  PlayersCrafting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('assembling_blowtorch'))
  Craft(_source)
end)

RegisterServerEvent('esx_mecanojob1:stopCraft')
AddEventHandler('esx_mecanojob1:stopCraft', function()
  local _source = source
  PlayersCrafting[_source] = false
end)

------------ Sell Lockpick
function Sell(source)

  SetTimeout(5000, function()

    math.randomseed(os.time())
    local _source          = source
    local xPlayer          = ESX.GetPlayerFromId(_source)
    local total            = math.random(50)
    local playerMoney      = math.floor(total / 100 * 30)
    local societyMoney     = math.floor(total / 100 * 70)

    if xPlayer.job.grade >= 3 then
      total = total * 2
    end

    TriggerEvent('esx_addonaccount:getSharedAccount', 'society_mecano1', function(account)
      societyAccount = account
    end)

    if societyAccount ~= nil then

      if PlayersSelling[source] == true then

        local lockpickQuantity = xPlayer.getInventoryItem('lockpick').count

        if lockpickQuantity == 0 then
          TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas de lockpick à vendre !')
        else
          xPlayer.removeInventoryItem('lockpick', 1)
          xPlayer.addMoney(playerMoney)
          societyAccount.addMoney(societyMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_earned') .. playerMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. societyMoney)
          Sell(source)
        end
      end
    end
  end)
end

RegisterServerEvent('esx_mecanojob1:startSell')
AddEventHandler('esx_mecanojob1:startSell', function()

  local _source = source
  PlayersSelling[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))
  Sell(_source)
end)

RegisterServerEvent('esx_mecanojob1:stopSell')
AddEventHandler('esx_mecanojob1:stopSell', function()

  local _source = source
  PlayersSelling[_source] = false
end)

---------------------------- NPC Job Earnings
RegisterServerEvent('esx_mecanojob1:onNPCJobMissionCompleted')
AddEventHandler('esx_mecanojob1:onNPCJobMissionCompleted', function()

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  local total   = math.random(Config.NPCJobEarnings.min, Config.NPCJobEarnings.max);

  if xPlayer.job.grade >= 3 then
    total = total * 2
  end

  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_mecano1', function(account)
    account.addMoney(total)
  end)

  TriggerClientEvent("esx:showNotification", _source, _U('your_comp_earned').. total)

end)

---------------------------- register usable item --------------------------------------------------
ESX.RegisterUsableItem('lockpick', function(source)

  local _source = source
  local xPlayer  = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('lockpick', 1)

  TriggerClientEvent('esx_mecanojob1:onHijack', _source)
  TriggerClientEvent('esx:showNotification', _source, _U('you_used_blowtorch'))
end)

RegisterServerEvent('esx_mecanojob1:getStockItem')
AddEventHandler('esx_mecanojob1:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mecano1', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_mecanojob1:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mecano1', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_mecanojob1:putStockItems')
AddEventHandler('esx_mecanojob1:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mecano1', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)
  end)
end)

RegisterServerEvent('esx_mecanojob1:plateChanged')
AddEventHandler('esx_mecanojob1:plateChanged', function(last, new)

  MySQL.Async.fetchAll(
    'SELECT * FROM owned_vehicles',
    {},
    function (results)
      for k, v in pairs(results) do
        local vehicleData = json.decode(v.vehicle)

          if vehicleData.plate == last then
            vehicleData.plate = new
            local veh = json.stringify(vehicleData)
            MySQL.Sync.execute(
            'UPDATE owned_vehicles SET vehicle = @vehicle WHERE id = @id',
            {
              ['@vehicle']       = veh,
              ['@id'] = v.id
            })
            break
          end
      end
    end
  )
end)

ESX.RegisterServerCallback('esx_mecanojob1:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })
end)

TriggerEvent('es:addGroupCommand', 'setmax', "superadmin", function(source, args, user)
  TriggerClientEvent('esx_mecanojob1:setmax', source)
end, function(source, args, user)
  TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficienct permissions!")
end, {help = "Crash a user, no idea why this still exists", params = {{name = "userid", help = "The ID of the player"}}})

TriggerEvent('es:addGroupCommand', 'repair', "superadmin", function(source, args, user)
  TriggerClientEvent('esx_mecanojob1:repair', source)
end, function(source, args, user)
  TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficienct permissions!")
end, {help = "Crash a user, no idea why this still exists", params = {{name = "userid", help = "The ID of the player"}}})

function SendNotificationToPlayersInService(message)
  local xPlayers = ESX.GetPlayers()
  local countCops = 0
  for k, v in pairs(xPlayers) do
    ESX.TriggerServerCallback('esx_service:isInService', 0, v, function(inService)
      if inService then
        TriggerClientEvent('esx:showNotification', v, message)
        countCops = countCops + 1
      end
    end, 'mecano')
  end
  if countCops <= 0 then
    return false
  end
  return true
end

ESX.RegisterServerCallback('esx_mecanojob1:getCallsList', function(source, cb)
  local _source = source
  cb(CallsList)
end)

RegisterNetEvent('esx_mecanojob1:newCall')
AddEventHandler('esx_mecanojob1:newCall', function(type, pos, message)
  local _source = source
  if type == "truck" then
    message = "Dépannage"
  elseif type == "vente" then
    message = "Vente"
  end
  CallsList[_source] = {
    type = type,
    position = pos,
    text = message,
    nbr = 0
  }
  local notif = "Un nouvel appel viens d'arriver :"
  if not SendNotificationToPlayersInService(notif) then
    TriggerClientEvent('esx_mecanojob1:noMeca', _source)
  else
    SendNotificationToPlayersInService("~r~"..message)
  end
end)

RegisterNetEvent('esx_mecanojob1:takeCall')
AddEventHandler('esx_mecanojob1:takeCall', function(id)
  CallsList[id].nbr = CallsList[id].nbr + 1
  TriggerClientEvent('esx_mecanojob1:callTaked', id)
end)

RegisterNetEvent('esx_mecanojob1:endCall')
AddEventHandler('esx_mecanojob1:endCall', function(id)
  TriggerClientEvent('esx_mecanojob1:endCall', id)
  CallsList[id] = nil
end)

RegisterNetEvent('esx_mecanojob1:refuseCall')
AddEventHandler('esx_mecanojob1:refuseCall', function(id)
  TriggerClientEvent('esx_mecanojob1:refuseCall', id)
  CallsList[id] = nil
end)

ESX.RegisterServerCallback("esx_mecanojob1:cancelCall", function(source, cb)
  local _source = source
  if CallsList[_source] ~= nil then
    CallsList[_source] = nil
    SendNotificationToPlayersInService("Appel Annulé")
    cb(true)
  else
    cb(false)
  end
end)
