Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerColor                = { r = 120, g = 120, b = 240 }
Config.EnablePlayerManagement     = true -- If set to true, you need esx_addonaccount, esx_billing and esx_society
Config.EnablePvCommand            = false
Config.EnableOwnedVehicles        = true
Config.EnableSocietyOwnedVehicles = true
Config.ResellPercentage           = 70
Config.Locale                     = 'fr'

Config.Zones = {

  ShopEntering = {
    Pos   = { x = -29.821, y = -1104.415, z =  25.422 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Type  = 1,
  },

  ShopInside = {
    Pos     = { x = -48.093, y = -1078.855, z = 25.422 },
    Size    = { x = 1.5, y = 1.5, z = 1.0 },
    Heading = 73.60,
    Type    = -1,
  },

  ShopOutside = {
    Pos     = { x = -48.093, y = -1078.855, z = 25.771 },
    Pos     = { x = -47.093, y = -1077.855, z = 25.771 },
    Pos     = { x = -46.093, y = -1076.855, z = 25.771 },
    Pos     = { x = -48.668, y = -1079.944, z = 25.771 },
    Size    = { x = 2.5, y = 2.5, z = 2.0 },
    Heading = 73.60,
    Type    = -1,
  },

  BossActions = {
    Pos   = { x = -32.065, y = -1114.277, z = 25.422 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Type  = -1,
  },

  GiveBackVehicle = {
    Pos   = { x = -18.227, y = -1078.558, z = 25.675 },
    Size  = { x = 3.0, y = 3.0, z = 1.0 },
    Type  = (Config.EnablePlayerManagement and 1 or -1),
  },

  ResellVehicle = {
    Pos   = { x = -10.263, y = -1096.835, z = 25.683 },
    Size  = { x = 3.0, y = 3.0, z = 1.0 },
    Type  = 1,
  },
}
