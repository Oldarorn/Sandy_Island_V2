local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local GUI                     = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local PlayerData              = {}
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local IsInShopMenu            = false
local Categories              = {}
local Vehicles                = {}
local LastVehicles            = {}
local CurrentVehicleData      = nil

ESX                           = nil
GUI.Time                      = 0

Citizen.CreateThread(function ()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(1)
  end

  ESX.TriggerServerCallback('esx_vehicleshop:getCategories', function (categories)
    Categories = categories
  end)

  ESX.TriggerServerCallback('esx_vehicleshop:getVehicles', function (vehicles)
    Vehicles = vehicles
  end)
end)

function Message()
  Citizen.CreateThread(function()
    while messagenotfinish do
        Citizen.Wait(1)

      DisplayOnscreenKeyboard(1, "FMMC_MPM_NA", "", "", "", "", "", 30)
        while (UpdateOnscreenKeyboard() == 0) do
            DisableAllControlActions(0);
           Citizen.Wait(1)
        end
        if (GetOnscreenKeyboardResult()) then
            local result = GetOnscreenKeyboardResult()
            messagenotfinish = false
           TriggerServerEvent('esx_vehicleshop:annonce',result)
            
        end


    end
  end)
  
end

function DeleteShopInsideVehicles ()
  while #LastVehicles > 0 do
    local vehicle = LastVehicles[1]
    ESX.Game.DeleteVehicle(vehicle)
    table.remove(LastVehicles, 1)
  end
end

function OpenShopMenu ()
  IsInShopMenu = true

  ESX.UI.Menu.CloseAll()

  local playerPed = GetPlayerPed(-1)

  FreezeEntityPosition(playerPed, true)
  SetEntityVisible(playerPed, false)
  SetEntityCoords(playerPed, Config.Zones.ShopInside.Pos.x, Config.Zones.ShopInside.Pos.y, Config.Zones.ShopInside.Pos.z)

  local vehiclesByCategory = {}
  local elements           = {}
  local firstVehicleData   = nil

  for i=1, #Categories, 1 do
    vehiclesByCategory[Categories[i].name] = {}
  end

  for i=1, #Vehicles, 1 do
    table.insert(vehiclesByCategory[Vehicles[i].category], Vehicles[i])
  end

  for i=1, #Categories, 1 do
    local category         = Categories[i]
    local categoryVehicles = vehiclesByCategory[category.name]
    local options          = {}

    for j=1, #categoryVehicles, 1 do
      local vehicle = categoryVehicles[j]

      if i == 1 and j == 1 then
        firstVehicleData = vehicle
      end

      table.insert(options, vehicle.name .. ' $' .. vehicle.price)
    end

    table.insert(elements, {
      name    = category.name,
      label   = category.label,
      value   = 0,
      type    = 'slider',
      max     = #Categories[i],
      options = options
    })
  end

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'vehicle_shop',
    {
      title    = _U('car_dealer'),
      align    = 'left',
      elements = elements,
    },
    function (data, menu)
      local vehicleData = vehiclesByCategory[data.current.name][data.current.value + 1]

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'shop_confirm',
        {
          title = _U('buy') .. ' ' .. vehicleData.name .. ' ' .. _U('for') .. ' ' .. vehicleData.price .. ' ?',
          align = 'left',
          elements = {
            {label = _U('yes'), value = 'yes'},
            {label = _U('no'), value = 'no'},
          },
        },
        function (data2, menu2)
          if data2.current.value == 'yes' then
            if Config.EnablePlayerManagement then
              ESX.TriggerServerCallback('esx_vehicleshop:buyVehicleSociety', function (hasEnoughMoney)
                if hasEnoughMoney then
                  IsInShopMenu = false

                  DeleteShopInsideVehicles()

                  local playerPed = GetPlayerPed(-1)

                  CurrentAction     = 'shop_menu'
                  CurrentActionMsg  = _U('shop_menu')
                  CurrentActionData = {}

                  FreezeEntityPosition(playerPed, false)
                  SetEntityVisible(playerPed, true)
                  SetEntityCoords(playerPed, Config.Zones.BossActions.Pos.x, Config.Zones.BossActions.Pos.y, Config.Zones.BossActions.Pos.z)

                  menu2.close()
                  menu.close()

                  ESX.ShowNotification(_U('vehicle_purchased'))
                else
                  ESX.ShowNotification(_U('broke_company'))
                end

              end, 'cardealer', vehicleData.model)
            else
              local playerData = ESX.GetPlayerData()

              if Config.EnableSocietyOwnedVehicles and playerData.job.grade_name == 'boss' then
                ESX.UI.Menu.Open(
                  'default', GetCurrentResourceName(), 'shop_confirm_buy_type',
                  {
                    title = 'Type d\'achat',
                    align = 'left',
                    elements = {
                      {label = 'Personnel', value = 'personnal'},
                      {label = 'Societé',   value = 'society'},
                    },
                  },
                  function (data3, menu3)

                    if data3.current.value == 'personnal' then
                      ESX.TriggerServerCallback('esx_vehicleshop:buyVehicle', function (hasEnoughMoney)
                        if hasEnoughMoney then
                          IsInShopMenu = false

                          menu3.close()
                          menu2.close()
                          menu.close()

                          DeleteShopInsideVehicles()

                          ESX.Game.SpawnVehicle(vehicleData.model, {
                            x = Config.Zones.ShopOutside.Pos.x,
                            y = Config.Zones.ShopOutside.Pos.y,
                            z = Config.Zones.ShopOutside.Pos.z
                          }, Config.Zones.ShopOutside.Heading, function (vehicle)

                            TaskWarpPedIntoVehicle(playerPed, vehicle, -1)

                            local vehicleProps = ESX.Game.GetVehicleProperties(vehicle)

                            if Config.EnableOwnedVehicles then
                              TriggerServerEvent('esx_vehicleshop:setVehicleOwned', vehicleProps)
                            end

                            ESX.ShowNotification(_U('vehicle_purchased'))
                          end)

                          FreezeEntityPosition(playerPed, false)
                          SetEntityVisible(playerPed, true)
                        else
                          ESX.ShowNotification(_U('not_enough_money'))
                        end
                      end, vehicleData.model)
                    end

                    if data3.current.value == 'society' then
                      ESX.TriggerServerCallback('esx_vehicleshop:buyVehicleSociety', function (hasEnoughMoney)
                        if hasEnoughMoney then

                          IsInShopMenu = false

                          menu3.close()
                          menu2.close()
                          menu.close()

                          DeleteShopInsideVehicles()

                          ESX.Game.SpawnVehicle(vehicleData.model, {
                            x = Config.Zones.ShopOutside.Pos.x,
                            y = Config.Zones.ShopOutside.Pos.y,
                            z = Config.Zones.ShopOutside.Pos.z
                          }, Config.Zones.ShopOutside.Heading, function (vehicle)

                            TaskWarpPedIntoVehicle(playerPed, vehicle, -1)

                            local vehicleProps = ESX.Game.GetVehicleProperties(vehicle)

                            TriggerServerEvent('esx_vehicleshop:setVehicleOwnedSociety', playerData.job.name, vehicleProps)

                            ESX.ShowNotification(_U('vehicle_purchased'))

                          end)

                          FreezeEntityPosition(playerPed, false)
                          SetEntityVisible(playerPed, true)
                        else
                          ESX.ShowNotification(_U('broke_company'))
                        end
                      end, playerData.job.name, vehicleData.model)
                    end
                  end,
                  function (data3, menu3)
                    menu3.close()
                  end
                )
              else
                ESX.TriggerServerCallback('esx_vehicleshop:buyVehicle', function (hasEnoughMoney)
                  if hasEnoughMoney then

                    IsInShopMenu = false

                    menu2.close()
                    menu.close()

                    DeleteShopInsideVehicles()

                    ESX.Game.SpawnVehicle(vehicleData.model, {
                      x = Config.Zones.ShopOutside.Pos.x,
                      y = Config.Zones.ShopOutside.Pos.y,
                      z = Config.Zones.ShopOutside.Pos.z
                    }, Config.Zones.ShopOutside.Heading, function (vehicle)

                      TaskWarpPedIntoVehicle(playerPed, vehicle, -1)

                      local vehicleProps = ESX.Game.GetVehicleProperties(vehicle)

                      if Config.EnableOwnedVehicles then
                        TriggerServerEvent('esx_vehicleshop:setVehicleOwned', vehicleProps)
                      end

                      ESX.ShowNotification(_U('vehicle_purchased'))
                    end)

                    FreezeEntityPosition(playerPed, false)
                    SetEntityVisible(playerPed, true)
                  else
                    ESX.ShowNotification(_U('not_enough_money'))
                  end
                end, vehicleData.model)
              end
            end
          end

          if data2.current.value == 'no' then

          end

        end,
        function (data2, menu2)
          menu2.close()
        end
      )

    end,
    function (data, menu)

      menu.close()

      DeleteShopInsideVehicles()

      local playerPed = GetPlayerPed(-1)

      CurrentAction     = 'shop_menu'
      CurrentActionMsg  = _U('shop_menu')
      CurrentActionData = {}

      FreezeEntityPosition(playerPed, false)
      SetEntityVisible(playerPed, true)

      if Config.EnablePlayerManagement then
        SetEntityCoords(playerPed, Config.Zones.BossActions.Pos.x, Config.Zones.BossActions.Pos.y, Config.Zones.BossActions.Pos.z)
      else
        SetEntityCoords(playerPed, Config.Zones.ShopEntering.Pos.x, Config.Zones.ShopEntering.Pos.y, Config.Zones.ShopEntering.Pos.z)
      end

      IsInShopMenu = false

    end,
    function (data, menu)
      local vehicleData = vehiclesByCategory[data.current.name][data.current.value + 1]
      local playerPed   = GetPlayerPed(-1)

      DeleteShopInsideVehicles()

      ESX.Game.SpawnLocalVehicle(vehicleData.model, {
        x = Config.Zones.ShopInside.Pos.x,
        y = Config.Zones.ShopInside.Pos.y,
        z = Config.Zones.ShopInside.Pos.z
      }, Config.Zones.ShopInside.Heading, function(vehicle)
        table.insert(LastVehicles, vehicle)
        TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
        FreezeEntityPosition(vehicle, true)
      end)
    end
  )

  DeleteShopInsideVehicles()

  ESX.Game.SpawnLocalVehicle(firstVehicleData.model, {
    x = Config.Zones.ShopInside.Pos.x,
    y = Config.Zones.ShopInside.Pos.y,
    z = Config.Zones.ShopInside.Pos.z
  }, Config.Zones.ShopInside.Heading, function (vehicle)
    table.insert(LastVehicles, vehicle)
    TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
    FreezeEntityPosition(vehicle, true)
  end)

end

function OpenResellerMenu ()

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'reseller',
    {
      title    = _U('car_dealer'),
      align    = 'left',
      elements = {
        {label = _U('pop_vehicle'),                    value = 'pop_vehicle'},
        {label = _U('depop_vehicle'),                  value = 'depop_vehicle'},
        {label = _U('create_bill'),                    value = 'create_bill'},
        {label = _U('get_rented_vehicles'),            value = 'get_rented_vehicles'},
        {label = _U('set_vehicle_owner_sell'),         value = 'set_vehicle_owner_sell'},
        {label = _U('set_vehicle_owner_rent'),         value = 'set_vehicle_owner_rent'},
        {label = _U('set_vehicle_owner_sell_society'), value = 'set_vehicle_owner_sell_society'},
        {label = 'Passer une annonce',                 value = 'announce'},
        {label = _U('deposit_stock'),                  value = 'put_stock'},
        {label = _U('take_stock'),                     value = 'get_stock'},
      }
    },
    function (data, menu)
      if data.current.value == 'put_stock' then
        OpenPutStocksMenu()
      end

      if data.current.value == 'get_stock' then
        OpenGetStocksMenu()
      end

      if data.current.value == 'pop_vehicle' then
        OpenPopVehicleMenu()
      end

      if data.current.value == 'announce' then
          messagenotfinish = true
          Message()
      end

      if data.current.value == 'depop_vehicle' then
        DeleteShopInsideVehicles()
      end

      if data.current.value == 'create_bill' then

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'set_vehicle_owner_sell_amount',
          {
            title = _U('invoice_amount'),
          },
          function (data2, menu)

            local amount = tonumber(data2.value)

            if amount == nil then
              ESX.ShowNotification(_U('invoice_amount'))
            else
              menu.close()

              local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

              if closestPlayer == -1 or closestDistance > 3.0 then
                ESX.ShowNotification(_U('invoice_amount'))
              else
                TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_cardealer', _U('car_dealer'), tonumber(data2.value))
              end
            end
          end,
          function (data2, menu)
            menu.close()
          end
        )
      end

      if data.current.value == 'get_rented_vehicles' then
        OpenRentedVehiclesMenu()
      end

      if data.current.value == 'set_vehicle_owner_sell' then
        local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

        if closestPlayer == -1 or closestDistance > 3.0 then
          ESX.ShowNotification(_U('no_players'))
        else
          SetVehicleNumberPlateText(LastVehicles[#LastVehicles], string.upper(ESX.GetRandomString(8)))
          local vehicleProps = ESX.Game.GetVehicleProperties(LastVehicles[#LastVehicles])
          local model        = CurrentVehicleData.model

          TriggerServerEvent('esx_vehicleshop:sellVehicle', model)

          if Config.EnableOwnedVehicles then
            TriggerServerEvent('esx_vehicleshop:setVehicleOwnedPlayerId', GetPlayerServerId(closestPlayer), vehicleProps)
            ESX.ShowNotification(_U('vehicle_set_owned', vehicleProps.plate, GetPlayerName(closestPlayer)))
          else
            ESX.ShowNotification(_U('vehicle_sold_to', vehicleProps.plate, GetPlayerName(closestPlayer)))
          end
        end
      end

      if data.current.value == 'set_vehicle_owner_sell_society' then
        local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

        if closestPlayer == -1 or closestDistance > 3.0 then
          ESX.ShowNotification(_U('no_players'))
        else
          SetVehicleNumberPlateText(LastVehicles[#LastVehicles], string.upper(ESX.GetRandomString(8)))
          ESX.TriggerServerCallback('esx:getOtherPlayerData', function (xPlayer)
            local vehicleProps = ESX.Game.GetVehicleProperties(LastVehicles[#LastVehicles])
            local model        = CurrentVehicleData.model

            TriggerServerEvent('esx_vehicleshop:sellVehicle', model)

            if Config.EnableSocietyOwnedVehicles then
              TriggerServerEvent('esx_vehicleshop:setVehicleOwnedSociety', xPlayer.job.name, vehicleProps)
              ESX.ShowNotification(_U('vehicle_set_owned', vehicleProps.plate, GetPlayerName(closestPlayer)))
            else
              ESX.ShowNotification(_U('vehicle_sold_to', vehicleProps.plate, GetPlayerName(closestPlayer)))
            end
          end, GetPlayerServerId(closestPlayer))
        end
      end

      if data.current.value == 'set_vehicle_owner_rent' then
        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'set_vehicle_owner_rent_amount',
          {
            title = _U('rental_amount'),
          },
          function (data2, menu)
            local amount = tonumber(data2.value)

            if amount == nil then
              ESX.ShowNotification(_U('invalid_amount'))
            else
              menu.close()

              local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

              if closestPlayer == -1 or closestDistance > 5.0 then
                ESX.ShowNotification(_U('no_players'))
              else
                SetVehicleNumberPlateText(LastVehicles[#LastVehicles], 'LOC ' .. ESX.GetRandomString(5))

                local vehicleProps = ESX.Game.GetVehicleProperties(LastVehicles[#LastVehicles])
                local model        = CurrentVehicleData.model

                TriggerServerEvent('esx_vehicleshop:rentVehicle', model, vehicleProps.plate, GetPlayerName(closestPlayer), CurrentVehicleData.price, amount, GetPlayerServerId(closestPlayer))

                if Config.EnableOwnedVehicles then
                  TriggerServerEvent('esx_vehicleshop:setVehicleOwnedPlayerId', GetPlayerServerId(closestPlayer), vehicleProps)
                end

                ESX.ShowNotification(_U('vehicle_set_rented', vehicleProps.plate, GetPlayerName(closestPlayer)))

                TriggerServerEvent('esx_vehicleshop:setVehicleForAllPlayers', vehicleProps, Config.Zones.ShopInside.Pos.x, Config.Zones.ShopInside.Pos.y, Config.Zones.ShopInside.Pos.z, 5.0)
              end
            end
          end,
          function (data2, menu)
            menu.close()
          end
        )
      end
    end,
    function (data, menu)
      menu.close()

      CurrentAction     = 'reseller_menu'
      CurrentActionMsg  = _U('shop_menu')
      CurrentActionData = {}
    end
  )

end

function OpenPersonnalVehicleMenu ()

  ESX.UI.Menu.CloseAll()

  ESX.TriggerServerCallback('esx_vehicleshop:getPersonnalVehicles', function (vehicles)
    local elements = {}

    for i=1, #vehicles, 1 do
      for j=1, #Vehicles, 1 do
        if vehicles[i].model == GetHashKey(Vehicles[j].model) then
          vehicles[i].name = Vehicles[j].name
        end
      end
    end

    for i=1, #vehicles, 1 do
      table.insert(elements, {label = vehicles[i].name .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'personnal_vehicle',
      {
        title    = _U('personal_vehicle'),
        align    = 'left',
        elements = elements,
      },
      function (data, menu)
        local playerPed   = GetPlayerPed(-1)
        local coords      = GetEntityCoords(playerPed)
        local heading     = GetEntityHeading(playerPed)
        local vehicleData = data.current.value

        menu.close()

        ESX.Game.SpawnVehicle(vehicleData.model, {
          x = coords.x,
          y = coords.y,
          z = coords.z
        }, heading, function (vehicle)
          ESX.Game.SetVehicleProperties(vehicle, vehicleData)
          TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
        end)
      end,
      function (data, menu)
        menu.close()
      end
    )
  end)
end

function OpenPopVehicleMenu ()
  ESX.TriggerServerCallback('esx_vehicleshop:getCommercialVehicles', function (vehicles)
    local elements = {}

    for i=1, #vehicles, 1 do
      table.insert(elements, {label = vehicles[i].name .. ' $' .. vehicles[i].price, value = vehicles[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'commercial_vehicles',
      {
        title    = _U('vehicle_dealer'),
        align    = 'left',
        elements = elements,
      },
      function (data, menu)
        local model = data.current.value

        DeleteShopInsideVehicles()

        ESX.Game.SpawnVehicle(model, {
          x = Config.Zones.ShopInside.Pos.x,
          y = Config.Zones.ShopInside.Pos.y,
          z = Config.Zones.ShopInside.Pos.z
        }, Config.Zones.ShopInside.Heading, function (vehicle)

          table.insert(LastVehicles, vehicle)

          for i=1, #Vehicles, 1 do
            if model == Vehicles[i].model then
              CurrentVehicleData = Vehicles[i]
            end
          end
        end)
      end,
      function (data, menu)
        menu.close()
      end
    )
  end)
end

function OpenRentedVehiclesMenu ()
  ESX.TriggerServerCallback('esx_vehicleshop:getRentedVehicles', function (vehicles)
    local elements = {}

    for i=1, #vehicles, 1 do
      table.insert(elements, {label = vehicles[i].playerName .. ' : ' .. vehicles[i].name .. ' - ' .. vehicles[i].plate, value = vehicles[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'rented_vehicles',
      {
        title    = _U('rent_vehicle'),
        align    = 'left',
        elements = elements,
      },
      nil,
      function (data, menu)
        menu.close()
      end
    )
  end)
end

function OpenBossActionsMenu ()
  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'reseller',
    {
      title    = _U('dealer_boss'),
      align    = 'left',
      elements = {
        {label = 'Tenues', value = 'cloakroom'},
        {label = _U('buy_vehicle'), value = 'buy_vehicle'},
        {label = 'Action Patron',   value = 'boss_actions'},
      },
    },
    function (data, menu)
      if data.current.value == 'cloakroom' then
        OpenCloakroomMenu()
      end

      if data.current.value == 'buy_vehicle' then
        OpenShopMenu()
      end

      if data.current.value == 'boss_actions' then
        TriggerEvent('esx_society:openBossMenu', 'cardealer', function(data, menu)
          menu.close()
        end)
      end
    end,
    function (data, menu)
      menu.close()

      CurrentAction     = 'boss_actions_menu'
      CurrentActionMsg  = _U('shop_menu')
      CurrentActionData = {}
    end
  )
end

function OpenCloakroomMenu()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = 'Tenues',
      align    = 'top-left',
      elements = {
        {label = 'Tenue Civil', value = 'citizen_wear'},
        {label = 'Tenue Concessionaire', value = 'agent_wear'}
      },
    },
    function(data, menu)

      menu.close()

      if data.current.value == 'citizen_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
          TriggerEvent('skinchanger:loadSkin', skin)
        end)

      end

      if data.current.value == 'agent_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

          if skin.sex == 0 then
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
          else
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
          end

        end)

      end
	  
    end,
    function(data, menu)
      menu.close()
    end
  )

end


function OpenGetStocksMenu ()
  ESX.TriggerServerCallback('esx_vehicleshop:getStockItems', function (items)
    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('dealership_stock'),
        elements = elements,
      },
      function (data, menu)
        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('amount'),
          },
          function (data2, menu2)
            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('esx_vehicleshop:getStockItem', itemName, count)
            end
          end,
          function (data2, menu2)
            menu2.close()
          end
        )
      end,
      function (data, menu)
        menu.close()
      end
    )
  end)
end

function OpenPutStocksMenu ()
  ESX.TriggerServerCallback('esx_vehicleshop:getPlayerInventory', function (inventory)
    local elements = {}

    for i=1, #inventory.items, 1 do
      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements,
      },
      function (data, menu)
        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('amount'),
          },
          function (data2, menu2)
            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('esx_vehicleshop:putStockItems', itemName, count)
            end

          end,
          function (data2, menu2)
            menu2.close()
          end
        )
      end,
      function (data, menu)
        menu.close()
      end
    )
  end)
end


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function (xPlayer)
  PlayerData = xPlayer

  if Config.EnablePlayerManagement then
    if PlayerData.job.name == 'cardealer' then
      Config.Zones.ShopEntering.Type = 1

      if PlayerData.job.grade_name == 'boss' then
        Config.Zones.BossActions.Type = 1
      end
    else
      Config.Zones.ShopEntering.Type = -1
      Config.Zones.BossActions.Type  = -1
    end
  end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function (job)
  PlayerData.job = job

  if Config.EnablePlayerManagement then
    if PlayerData.job.name == 'cardealer' then
      Config.Zones.ShopEntering.Type = 1

      if PlayerData.job.grade_name == 'boss' then
        Config.Zones.BossActions.Type = 1
      end

    else
      Config.Zones.ShopEntering.Type = -1
      Config.Zones.BossActions.Type  = -1
    end
  end
end)

RegisterNetEvent('esx_vehicleshop:openPersonnalVehicleMenu')
AddEventHandler('esx_vehicleshop:openPersonnalVehicleMenu', function ()
  OpenPersonnalVehicleMenu()
end)

AddEventHandler('esx_vehicleshop:hasEnteredMarker', function (zone)
  if zone == 'ShopEntering' then
    if Config.EnablePlayerManagement then
      if PlayerData.job ~= nil and PlayerData.job.name == 'cardealer' then
        CurrentAction     = 'reseller_menu'
        CurrentActionMsg  = _U('shop_menu')
        CurrentActionData = {}
      end
    else
      CurrentAction     = 'shop_menu'
      CurrentActionMsg  = _U('shop_menu')
      CurrentActionData = {}
    end
  end

  if zone == 'GiveBackVehicle' and Config.EnablePlayerManagement then
    local playerPed = GetPlayerPed(-1)

    if IsPedInAnyVehicle(playerPed, false) then
      local vehicle = GetVehiclePedIsIn(playerPed, false)

      CurrentAction     = 'give_back_vehicle'
      CurrentActionMsg  = _U('vehicle_menu')

      CurrentActionData = {
        vehicle = vehicle
      }
    end
  end

  if zone == 'ResellVehicle' then
    local playerPed = GetPlayerPed(-1)

    if IsPedInAnyVehicle(playerPed, false) then
      local vehicle     = GetVehiclePedIsIn(playerPed, false)
      local vehicleData = nil

      for i=1, #Vehicles, 1 do
        if GetHashKey(Vehicles[i].model) == GetEntityModel(vehicle) then
          vehicleData = Vehicles[i]
          break
        end
      end

      local resellPrice = math.floor(vehicleData.price / 100 * Config.ResellPercentage)

      CurrentAction     = 'resell_vehicle'
      CurrentActionMsg  = _U('sell_menu').. vehicleData.name .. _U('price') .. resellPrice

      CurrentActionData = {
        vehicle = vehicle,
        price   = resellPrice
      }
    end
  end

  if zone == 'BossActions' and Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.name == 'cardealer' and PlayerData.job.grade_name == 'boss' then
    CurrentAction     = 'boss_actions_menu'
    CurrentActionMsg  = _U('shop_menu')
    CurrentActionData = {}
  end
end)

AddEventHandler('esx_vehicleshop:hasExitedMarker', function (zone)
  if not IsInShopMenu then
    ESX.UI.Menu.CloseAll()
  end

  CurrentAction = nil
end)

-- Display markers
Citizen.CreateThread(function ()
  while true do
    Wait(0)

    local coords = GetEntityCoords(GetPlayerPed(-1))

    for k,v in pairs(Config.Zones) do
      if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
        DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
      end
    end
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function ()
  while true do
    Wait(0)

    local coords      = GetEntityCoords(GetPlayerPed(-1))
    local isInMarker  = false
    local currentZone = nil

    for k,v in pairs(Config.Zones) do
      if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
        isInMarker  = true
        currentZone = k
      end
    end

    if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
      HasAlreadyEnteredMarker = true
      LastZone                = currentZone
      TriggerEvent('esx_vehicleshop:hasEnteredMarker', currentZone)
    end

    if not isInMarker and HasAlreadyEnteredMarker then
      HasAlreadyEnteredMarker = false
      TriggerEvent('esx_vehicleshop:hasExitedMarker', LastZone)
    end
  end
end)

-- Key controls
Citizen.CreateThread(function ()
  while true do
    Citizen.Wait(1)

    if CurrentAction ~= nil then
      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlPressed(0, Keys['E']) and (GetGameTimer() - GUI.Time) > 300 then
        if CurrentAction == 'shop_menu' then
          OpenShopMenu()
        end

        if CurrentAction == 'reseller_menu' then
          OpenResellerMenu()
        end

        if CurrentAction == 'give_back_vehicle' then
          ESX.TriggerServerCallback('esx_vehicleshop:giveBackVehicle', function (isRentedVehicle)
            if isRentedVehicle then
              DeleteVehicle(CurrentActionData.vehicle)
              ESX.ShowNotification(_U('delivered'))
            else
              ESX.ShowNotification(_U('not_rental'))
            end
          end, GetVehicleNumberPlateText(CurrentActionData.vehicle))
        end

        if CurrentAction == 'resell_vehicle' then
          ESX.TriggerServerCallback('esx_vehicleshop:resellVehicle', function (isOwnedVehicle)
            if isOwnedVehicle then
              DeleteVehicle(CurrentActionData.vehicle)
              ESX.ShowNotification(_U('vehicle_sold'))
            else
              ESX.ShowNotification(_U('not_yours'))
            end
          end, GetVehicleNumberPlateText(CurrentActionData.vehicle), CurrentActionData.price)
        end

        if CurrentAction == 'boss_actions_menu' then
          OpenBossActionsMenu()
        end

        CurrentAction = nil
        GUI.Time      = GetGameTimer()
      end
    end
  end
end)

-- Load IPLS
Citizen.CreateThread(function ()
  RemoveIpl('v_carshowroom')
  RemoveIpl('shutter_open')
  RemoveIpl('shutter_closed')
  RemoveIpl('shr_int')
  RemoveIpl('csr_inMission')
  RequestIpl('v_carshowroom')
  RequestIpl('shr_int')
  RequestIpl('shutter_closed')
end)


Citizen.CreateThread(function()
    while true do
        Citizen.Wait(1)
        local myCoords = GetEntityCoords(GetPlayerPed(-1))
        ClearAreaOfCops(myCoords.x, myCoords.y, myCoords.z, 100.0, 0)
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(1)
        DisablePlayerVehicleRewards(PlayerId())
    end
end)


function DrawAdvancedTextCNN (x,y ,w,h,sc, text, r,g,b,a,font,jus)
    SetTextFont(font)
    SetTextProportional(0)
    SetTextScale(sc, sc)
    N_0x4e096588b13ffeca(jus)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - 0.1+w, y - 0.02+h)
end


 Citizen.CreateThread(function()
        while true do
            Citizen.Wait(1)    
                           
                if (affichenews == true) then
               
                    DrawRect(0.494, 0.227, 5.185, 0.118, 0, 0, 0, 150)
                    DrawAdvancedTextCNN(0.588, 0.14, 0.005, 0.0028, 0.8, "~r~ PREMIUM MOTORSPORT ~d~", 255, 255, 255, 255, 1, 0)
                    DrawAdvancedTextCNN(0.586, 0.199, 0.005, 0.0028, 0.6, texteafiche, 255, 255, 255, 255, 7, 0)
                    DrawAdvancedTextCNN(0.588, 0.246, 0.005, 0.0028, 0.4, "", 255, 255, 255, 255, 0, 0)

            end                
       end
    end)



RegisterNetEvent('esx_vehicleshop:annonce')
AddEventHandler('esx_vehicleshop:annonce', function(text)
    texteafiche = text
    affichenews = true
    
  end) 


RegisterNetEvent('esx_vehicleshop:annoncestop')
AddEventHandler('esx_vehicleshop:annoncestop', function()
    affichenews = false
    
  end)