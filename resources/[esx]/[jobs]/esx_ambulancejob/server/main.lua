ESX                 = nil
local CallsList     = {}
PlayersHarvesting   = {}
PlayersTransforming = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'ambulance', Config.MaxInService)
end

RegisterServerEvent('esx_ambulancejob:revive')
AddEventHandler('esx_ambulancejob:revive', function(target)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.addMoney(Config.ReviveReward)
	TriggerClientEvent('esx_ambulancejob:revive', target)
end)

RegisterServerEvent('esx_ambulancejob:heal')
AddEventHandler('esx_ambulancejob:heal', function(target, type)
  TriggerClientEvent('esx_ambulancejob:heal', target, type)
end)

TriggerEvent('esx_phone:registerNumber', 'ambulance', _U('alert_ambulance'), true, true)

TriggerEvent('esx_society:registerSociety', 'ambulance', 'Ambulance', 'society_ambulance', 'society_ambulance', 'society_ambulance', {type = 'public'})

ESX.RegisterServerCallback('esx_ambulancejob:removeItemsAfterRPDeath', function(source, cb)

  local xPlayer = ESX.GetPlayerFromId(source)

  if Config.RemoveCashAfterRPDeath then

    if xPlayer.getMoney() > 0 then
      xPlayer.removeMoney(xPlayer.getMoney())
    end

    if xPlayer.getAccount('black_money').money > 0 then
      xPlayer.setAccountMoney('black_money', 0)
    end

  end

  if Config.RemoveItemsAfterRPDeath then
    for i=1, #xPlayer.inventory, 1 do
      if xPlayer.inventory[i].count > 0 then
        xPlayer.setInventoryItem(xPlayer.inventory[i].name, 0)
      end
    end
  end

  if Config.RemoveWeaponsAfterRPDeath then
    for i=1, #xPlayer.loadout, 1 do
      xPlayer.removeWeapon(xPlayer.loadout[i].name)
    end
  end

  if Config.RespawnFine then
    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('respawn_fine', Config.RespawnFineAmount))
    xPlayer.removeAccountMoney('bank', Config.RespawnFineAmount)
  end

  cb()
end)

ESX.RegisterServerCallback('esx_ambulancejob:getItemAmount', function(source, cb, item)
  local xPlayer = ESX.GetPlayerFromId(source)
  local qtty = xPlayer.getInventoryItem(item).count
  cb(qtty)
end)

RegisterServerEvent('esx_ambulancejob:removeItem')
AddEventHandler('esx_ambulancejob:removeItem', function(item)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  xPlayer.removeInventoryItem(item, 1)
  if item == 'bandage' then
    TriggerClientEvent('esx:showNotification', _source, _U('used_bandage'))
  elseif item == 'medikit' then
    TriggerClientEvent('esx:showNotification', _source, _U('used_medikit'))
  end
end)

RegisterServerEvent('esx_ambulancejob:giveItem')
AddEventHandler('esx_ambulancejob:giveItem', function(item)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  local limit = xPlayer.getInventoryItem(item).limit
  local delta = 1
  local qtty = xPlayer.getInventoryItem(item).count
  if limit ~= -1 then
    delta = limit - qtty
  end
  if qtty < limit then
    xPlayer.addInventoryItem(item, delta)
  else
    TriggerClientEvent('esx:showNotification', _source, _U('max_item'))
  end
end)

TriggerEvent('es:addGroupCommand', 'revive', 'admin', function(source, args, user)
	if args[1] ~= nil then
		if GetPlayerName(tonumber(args[1])) ~= nil then
			print('esx_ambulancejob: ' .. GetPlayerName(source) .. ' is reviving a player!')
			TriggerClientEvent('esx_ambulancejob:revive', tonumber(args[1]))
		end
	else
		TriggerClientEvent('esx_ambulancejob:revive', source)
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient Permissions.")
end, {help = _U('revive_help'), params = {{name = 'id'}}})

ESX.RegisterUsableItem('medikit', function(source)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  xPlayer.removeInventoryItem('medikit', 1)
  TriggerClientEvent('esx_ambulancejob:heal', _source, 'big')
  TriggerClientEvent('esx:showNotification', _source, _U('used_medikit'))
end)

ESX.RegisterUsableItem('bandage', function(source)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  xPlayer.removeInventoryItem('bandage', 1)
  TriggerClientEvent('esx_ambulancejob:heal', _source, 'small')
  TriggerClientEvent('esx:showNotification', _source, _U('used_bandage'))
end)

RegisterServerEvent('esx_ambulancejob:firstSpawn')
AddEventHandler('esx_ambulancejob:firstSpawn', function()
	local _source    = source
	local identifier = GetPlayerIdentifiers(_source)[1]
	MySQL.Async.fetchScalar('SELECT isDead FROM users WHERE identifier=@identifier',
	{
		['@identifier'] = identifier
	}, function(isDead)
		if isDead == 1 then
			print('esx_ambulancejob: ' .. GetPlayerName(_source) .. ' (' .. identifier .. ') attempted combat logging!')
			TriggerClientEvent('esx_ambulancejob:requestDeath', _source)
		end
	end)
end)

RegisterServerEvent('esx_ambulancejob:setDeathStatus')
AddEventHandler('esx_ambulancejob:setDeathStatus', function(isDead)
	local _source = source
	MySQL.Sync.execute("UPDATE users SET isDead=@isDead WHERE identifier=@identifier",
	{
		['@identifier'] = GetPlayerIdentifiers(_source)[1],
		['@isDead'] = isDead
	})
end)

function SendNotificationToPlayersInService(message)
  local xPlayers = ESX.GetPlayers()
  local countCops = 0
  for k, v in pairs(xPlayers) do
    ESX.TriggerServerCallback('esx_service:isInService', 0, v, function(inService)
      if inService then
        TriggerClientEvent('esx:showNotification', v, message)
        countCops = countCops + 1
      end
    end, 'ambulance')
  end
  if countCops <= 0 then
    return false
  end
  return true
end

RegisterNetEvent('esx_ambulancejob:newCall')
AddEventHandler('esx_ambulancejob:newCall', function(type, pos)
  local _source = source
  local message
  if type == "death" then
    message = "Une personne est inconsciente"
  elseif type == "heal" then
    message = "Une Personne a besoin d'un médecin"
  end
  CallsList[_source] = {
    type = type,
    position = pos,
    text = message,
    nbr = 0
  }
  local notif = "Un nouvel appel viens d'arriver :"
  if not SendNotificationToPlayersInService(notif) then
    TriggerClientEvent('esx_ambulancejob:noEMS', _source)
  else
    SendNotificationToPlayersInService("~r~"..message)
  end
end)

ESX.RegisterServerCallback('esx_ambulancejob:getCalls', function(source, cb)
  cb(CallsList)
end)

RegisterNetEvent('esx_ambulancejob:takeCall')
AddEventHandler('esx_ambulancejob:takeCall', function(id)
  if CallsList[id] ~= nil then
    CallsList[id].nbr = CallsList[id].nbr + 1
    TriggerClientEvent('esx_ambulancejob:callTaked', id)
  end
end)

RegisterNetEvent('esx_ambulancejob:endCall')
AddEventHandler('esx_ambulancejob:endCall', function(id)
  TriggerClientEvent('esx_ambulancejob:endCall', id)
  CallsList[id] = nil
end)

RegisterNetEvent('esx_ambulancejob:refuseCall')
AddEventHandler('esx_ambulancejob:refuseCall', function(id)
  TriggerClientEvent('esx_ambulancejob:refuseCall', id)
  CallsList[id] = nil
end)

ESX.RegisterServerCallback("esx_ambulancejob:cancelCall", function(source, cb)
  local _source = source
  if CallsList[_source] ~= nil then
    CallsList[_source] = nil
    SendNotificationToPlayersInService("Appel Annulé")
    cb(true)
  else
    cb(false)
  end
end)

ESX.RegisterServerCallback('esx_ambulancejob:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_ambulance', function(inventory)
    cb(inventory.items)
  end)
end)

RegisterServerEvent('esx_ambulancejob:getStockItem')
AddEventHandler('esx_ambulancejob:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_ambulance', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)
  end)
end)

RegisterServerEvent('esx_ambulancejob:putStockItems')
AddEventHandler('esx_ambulancejob:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_ambulance', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= 0 then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)
  end)
end)

ESX.RegisterServerCallback('esx_ambulancejob:getPlayerInventory', function(source, cb)

  local xPlayer = ESX.GetPlayerFromId(source)
  local items   = xPlayer.inventory

  cb({
    items = items
  })
end)

function HarvestElectrode(source)
  SetTimeout(5000, function()

    if PlayersHarvesting[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local electrod = xPlayer.getInventoryItem('electrod')

      if electrod.limit ~= -1 and electrod.count >= electrod.limit then
        TriggerClientEvent('esx:showNotification', source, "Vous ne pouvez plus récolter")
      else
        xPlayer.addInventoryItem('electrod', 1)
        HarvestElectrode(source)
      end
    end
  end)
end

RegisterNetEvent('esx_ambulancejob:startHarvestElectrode')
AddEventHandler('esx_ambulancejob:startHarvestElectrode', function()
  local _source = source
  PlayersHarvesting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, "Récolte en cours")
  HarvestElectrode(_source)
end)

RegisterNetEvent('esx_ambulancejob:stopHarvestElectrode')
AddEventHandler('esx_ambulancejob:stopHarvestElectrode', function()
  local _source = source
  if PlayersHarvesting[_source] ~= nil then
    PlayersHarvesting[_source] = false
  end
end)

function TransformElectrode(source)
  SetTimeout(5000, function()

    if PlayersTransforming[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local defibrillator = xPlayer.getInventoryItem('defibrillator')
      local electrod      = xPlayer.getInventoryItem('electrod')

      if defibrillator.limit ~= -1 and defibrillator.count >= defibrillator.limit then
        TriggerClientEvent('esx:showNotification', source, "Vous ne pouvez plus transformer")
      elseif electrod.count < 2 then
        TriggerClientEvent('esx:showNotification', source, "Vous n\'avez plus assez d\'élétrodes")
      else
        xPlayer.removeInventoryItem('electrod', 2)
        xPlayer.addInventoryItem('defibrillator', 1)
        TransformElectrode(source)
      end
    end
  end)
end

RegisterNetEvent('esx_ambulancejob:startTransformElectrode')
AddEventHandler('esx_ambulancejob:startTransformElectrode', function()
  local _source = source

  PlayersTransforming[_source] = true

  TriggerClientEvent('esx:showNotification', _source, "Assemblage en cours")

  TransformElectrode(_source)
end)

RegisterNetEvent('esx_ambulancejob:stopTransformElectrode')
AddEventHandler('esx_ambulancejob:stopTransformElectrode', function()
  local _source = source
  if PlayersTransforming[_source] ~= nil then
    PlayersTransforming[_source] = false
  end
end)
