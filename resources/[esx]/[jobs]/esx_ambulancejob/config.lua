Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerColor                = { r = 102, g = 0, b = 102 }
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.ReviveReward               = 0  -- revive reward, set to 0 if you don't want it enabled
Config.AntiCombatLog              = true -- enable anti-combat logging?
Config.MaxInService 			  			= 200
local second 					  					= 1000
local minute 					  					= 60 * second

-- How much time before auto respawn at hospital
Config.RespawnDelayAfterRPDeath   = 120 * minute

-- How much time before a menu opens to ask the player if he wants to respawn at hospital now
-- The player is not obliged to select YES, but he will be auto respawn
-- at the end of RespawnDelayAfterRPDeath just above.
Config.RespawnToHospitalMenuTimer   = false
Config.MenuRespawnToHospitalDelay   = 0

Config.EnablePlayerManagement       = true
Config.EnableSocietyOwnedVehicles   = true

Config.RemoveWeaponsAfterRPDeath    = true
Config.RemoveCashAfterRPDeath       = true
Config.RemoveItemsAfterRPDeath      = true

-- Will display a timer that shows RespawnDelayAfterRPDeath time remaining
Config.ShowDeathTimer               = false

-- Will allow to respawn at any time, don't use with RespawnToHospitalMenuTimer enabled!
Config.EarlyRespawn                 = true
-- The player can have a fine (on bank account)
Config.RespawnFine                  = false
Config.RespawnFineAmount            = 0

Config.Locale                       = 'fr'

Config.Blip = {
	Pos     = { x = 307.76, y = -1433.47, z = 28.97 },
	Sprite  = 61,
	Display = 4,
	Scale   = 1.2,
	Colour  = 2,
}

Config.HelicopterSpawner = {
	SpawnPoint = { x = 313.33, y = -1465.2, z = 45.5 },
	Heading    = 0.0
}

-- https://wiki.fivem.net/wiki/Vehicles
Config.AuthorizedVehicles = {

	{
		model = 'ambulance',
		label = 'Ambulance'
	}
}

Config.Zones = {

	HospitalInteriorEntering1 = { -- Main entrance
		Pos	= { x = 294.2, y = -1448.60, z = 29.0 },
		Type = 27,
	},

	HospitalInteriorInside1 = {
		Pos	= { x = 272.8, y = -1358.8, z = 23.9 },
		Type = -1,
	},

	HospitalInteriorOutside1 = {
		Pos	= { x = 295.8, y = -1446.5, z = 28.9 },
		Type = -1,
	},

	HospitalInteriorExit1 = {
		Pos	= { x = 275.7, y = -1361.5, z = 23.6 },
		Type = 27,
	},

	HospitalInteriorEntering2 = { -- Lift go to the roof
		Pos	= { x = 247.1, y = -1371.4, z = 23.5 },
		Type = 27,
	},

	HospitalInteriorInside2 = { -- Roof outlet
		Pos	= { x = 333.1,	y = -1434.9, z = 45.5 },
		Type = -1,
	},

	HospitalInteriorOutside2 = { -- Lift back from roof
		Pos	= { x = 249.1,	y = -1369.6, z = 23.5 },
		Type = -1,
	},

	HospitalInteriorExit2 = { -- Roof entrance
		Pos	= { x = 335.5, y = -1432.0, z = 45.5 },
		Type = 27,
	},

	AmbulanceActions = { -- Cloakroom
		Pos	= { x = 268.4, y = -1363.330, z = 23.75 },
		Type = 27,
	},

	VehicleSpawner = {
		Pos	= { x = 307.76, y = -1433.47, z = 28.97 },
		Type = 27,
	},

	VehicleSpawnPoint = {
		Pos	= { x = 304.87, y = -1437.69, z = 28.80 },
		Type = -27,
	},

	VehicleDeleter = {
		Pos	= { x = 327.67, y = -1481.1, z = 28.83 },
		Type = 27,
	},

	Pharmacy = {
		Pos	= { x = 230.13, y = -1366.18, z = 38.53 },
		Type = 27,
	},

	ParkingDoorGoOutInside = {
		Pos	= { x = 234.56, y = -1373.77, z = 20.97 },
		Type = 27,
	},

	ParkingDoorGoOutOutside = {
		Pos	= { x = 320.98, y = -1478.62, z = 28.81 },
		Type = -1,
	},

	ParkingDoorGoInInside = {
		Pos	= { x = 238.64, y = -1368.48, z = 23.53 },
		Type = -1,
	},

	ParkingDoorGoInOutside = {
		Pos	= { x = 317.97, y = -1476.13, z = 28.97 },
		Type = 27,
	},

	ElectrodeHarvest = {
		Pos = {x = 3540.056, y = 3667.788, z = 27.42},
		Type = 27,
	},

	ElectrodeTransform = {
		Pos = {x = 247.085, y = -1377.911, z = 38.534},
		Type = 27,
	}
}
