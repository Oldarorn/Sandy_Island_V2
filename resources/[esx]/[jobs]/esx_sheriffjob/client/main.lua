local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData                = {}
local GUI                       = {}
local HasAlreadyEnteredMarker   = false
local LastStation               = nil
local LastPart                  = nil
local LastPartNum               = nil
local LastEntity                = nil
local CurrentAction             = nil
local CurrentActionMsg          = ''
local CurrentActionData         = {}
local IsHandcuffed              = false
local IsDragged                 = false
local CopPed                    = 0
local hasAlreadyJoined          = false
local blipsCops                 = {}
local isDead                    = false
local InService                 = false
local SelectedCall              = nil
local RadarObject               = {}
CurrentTask                     = {}

ESX                             = nil
GUI.Time                        = 0

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end

	Citizen.Wait(5000)
	PlayerData = ESX.GetPlayerData()
end)

function SetVehicleMaxMods(vehicle)
	local props = {
		modEngine       = 2,
		modBrakes       = 2,
		modTransmission = 2,
		modSuspension   = 3,
		modTurbo        = true,
	}

	ESX.Game.SetVehicleProperties(vehicle, props)
end

function cleanPlayer(playerPed)
	SetPedArmour(playerPed, 0)
	ClearPedBloodDamage(playerPed)
	ResetPedVisibleDamage(playerPed)
	ClearPedLastWeaponDamage(playerPed)
	ResetPedMovementClipset(playerPed, 0)
end

function setUniform(job, playerPed)
  TriggerEvent('skinchanger:getSkin', function(skin)

    if job == "bullet_wear" then
      SetPedArmour(playerPed, 100)
    end

    if skin.sex == 0 then
      if Config.Uniforms[job].male ~= nil then
        TriggerEvent('skinchanger:loadClothes', skin, Config.Uniforms[job].male)
      else
        ESX.ShowNotification(_U('no_outfit'))
      end
    else
      if Config.Uniforms[job].female ~= nil then
        TriggerEvent('skinchanger:loadClothes', skin, Config.Uniforms[job].female)
      else
        ESX.ShowNotification(_U('no_outfit'))
      end
    end
    ESX.TriggerServerCallback('esx_service:enableService', function(result)
      if result then
        ESX.ShowNotification('~g~Prise de Service')
        InService = true
      end
    end, 'sheriff')

  end)
end

function OpenCloakroomMenu()

  local playerPed = GetPlayerPed(-1)

  local elements = {
    { label = _U('citizen_wear'), value = 'citizen_wear' },
    { label = _U('bullet_wear'), value = 'bullet_wear' },
  }

  if PlayerData.job.grade_name == 'recruit' then
    table.insert(elements, {label = _U('police_wear'), value = 'cadet_wear'})
  end

  if PlayerData.job.grade_name == 'officer' then
    table.insert(elements, {label = _U('police_wear'), value = 'police_wear'})
  end

  if PlayerData.job.grade_name == 'sergeant' then
    table.insert(elements, {label = _U('police_wear'), value = 'sergeant_wear'})
  end

  if PlayerData.job.grade_name == 'lieutenant' then
    table.insert(elements, {label = _U('police_wear'), value = 'lieutenant_wear'})
  end

  if PlayerData.job.grade_name == 'boss' then
    table.insert(elements, {label = _U('police_wear'), value = 'commandant_wear'})
  end

  if Config.EnableNonFreemodePeds then
    table.insert(elements, {label = _U('sheriff_wear'), value = 'sheriff_wear_freemode'})
    table.insert(elements, {label = _U('lieutenant_wear'), value = 'lieutenant_wear_freemode'})
    table.insert(elements, {label = _U('commandant_wear'), value = 'commandant_wear_freemode'})
  end

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = _U('cloakroom'),
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      cleanPlayer(playerPed)

      if data.current.value == 'citizen_wear' and InService then
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
          TriggerEvent('skinchanger:loadSkin', skin)
          TriggerServerEvent('esx_service:disableService', 'sheriff')
          ESX.ShowNotification("~r~Fin de Service")
          InService = false
        end)
      end

      if
        data.current.value == 'cadet_wear' or
        data.current.value == 'police_wear' or
        data.current.value == 'sergeant_wear' or
        data.current.value == 'lieutenant_wear' or
        data.current.value == 'commandant_wear' or
        data.current.value == 'bullet_wear' or
        data.current.value == 'gilet_wear'
      then
        setUniform(data.current.value, playerPed)
      end

      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = _U('open_cloackroom')
      CurrentActionData = {}

    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = _U('open_cloackroom')
      CurrentActionData = {}
    end
  )
end

function OpenArmoryMenu(station)

  if Config.EnableArmoryManagement then

    local elements = {
      {label = _U('get_weapon'),     value = 'get_weapon'},
      {label = _U('put_weapon'),     value = 'put_weapon'},
      {label = _U('deposit_object'), value = 'put_stock'},
      {label = "Déposer dans l'armoire", value = 'put_public_stock'},
      {label = "Prendre dans l'armoire'", value = 'get_public_stock'},
    }

    if PlayerData.job.grade_name == 'boss' then
      table.insert(elements, {label = _U('remove_object'),  value = 'get_stock'})
      table.insert(elements, {label = _U('buy_weapons'), value = 'buy_weapons'})
    end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'armory',
      {
        title    = _U('armory'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        if data.current.value == 'get_weapon' then
          OpenGetWeaponMenu()
        end

        if data.current.value == 'put_weapon' then
          OpenPutWeaponMenu()
        end

        if data.current.value == 'buy_weapons' then
          OpenBuyWeaponsMenu(station)
        end

        if data.current.value == 'put_stock' then
          OpenPutStocksMenu()
        end

        if data.current.value == 'get_stock' then
          OpenGetStocksMenu()
        end

        if data.current.value == 'get_public_stock' then
          OpenGetPublicStocksMenu()
        end

        if data.current.value == 'put_public_stock' then
          OpenPutPublicStocksMenu()
        end
      end,
      function(data, menu)
        menu.close()
        CurrentAction     = 'menu_armory'
        CurrentActionMsg  = _U('open_armory')
        CurrentActionData = {station = station}
      end
    )
  else

    local elements = {}

    for i=1, #Config.PoliceStations[station].AuthorizedWeapons, 1 do
      local weapon = Config.PoliceStations[station].AuthorizedWeapons[i]
      table.insert(elements, {label = ESX.GetWeaponLabel(weapon.name), value = weapon.name})
    end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'armory',
      {
        title    = _U('armory'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)
        local weapon = data.current.value
        TriggerServerEvent('esx_policejob:giveWeapon', weapon,  1000)
      end,
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_armory'
        CurrentActionMsg  = _U('open_armory')
        CurrentActionData = {station = station}
      end
    )
  end
end

function OpenVehicleSpawnerMenu(station, partNum)

  local vehicles = Config.PoliceStations[station].Vehicles

  ESX.UI.Menu.CloseAll()

  if Config.EnableSocietyOwnedVehicles then

    local elements = {}

    ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

      for i=1, #garageVehicles, 1 do
        table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'vehicle_spawner',
        {
          title    = _U('vehicle_menu'),
          align    = 'top-left',
          elements = elements,
        },
        function(data, menu)

          menu.close()

          local vehicleProps = data.current.value

          -- Citizen.CreateThread(function()
          --   ESX.Game.SpawnVehicle(vehicleProps.model, {x = 463.74, y = -1014.53, z = 28.07}, 90.0, function(vehicle)
          --     ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
          --     ESX.ShowNotification("Votre véhicule va arriver")
          --     Citizen.Wait(1500)
          --     local driver = 368603149
          --     RequestModel(driver)
          --     while not RequestModel(driver) do
          --       RequestModel(driver)
          --       Citizen.Wait(0)
          --     end
          --     driver = CreatePedInsideVehicle(vehicle, 6, driver , -1, true, false)
          --     --SetPedIntoVehicle(driver, vehicle, -1)
          --     SetVehicleFixed(vehicle)
          --     SetVehicleOnGroundProperly(vehicle)
          --     SetEntityAsMissionEntity(driver, true, true)
          --     TaskVehicleDriveToCoord(driver, vehicle, 424.467, -1020.0468, 28.988, 5.0, 0, GetHashKey("police3"), 8388614, 1.0, true)
          --     enroute = true
          --     while (enroute) do
          --       Citizen.Wait(300)
          --       local vehCoords = GetEntityCoords(vehicle)
          --       local distanceToPos = GetDistanceBetweenCoords(vehCoords.x, vehCoords.y, vehCoords.z, 424.467, -1020.0468, 28.988, 1)
          --       SetDriveTaskDrivingStyle(driver, 8388614)
          --       SetEntityInvincible(vehicle, true)
          --       SetEntityInvincible(driver, true)
          --       if distanceToPos <= 5 then
          --         enroute = false
          --         Citizen.Wait(1000)
          --         TaskLeaveVehicle(driver, vehicle, 256)
          --         ClearPedTasks(driver)
          --         SetEntityInvincible(vehicle, false)
          --         SetVehicleFixed(vehicle)
          --         Citizen.Wait(1000)
          --         while true do
          --           TaskGoStraightToCoord(driver,  451.464,  -1017.978,  28.49,  1.0,  -1,  0.0,  0.0)
          --           local driverPos = GetEntityCoords(driver)
          --           if GetDistanceBetweenCoords(driverPos.x, driverPos.y, driverPos.z, 451.464, -1017.978, 28.49, 1) < 5 then
          --             DeletePed(driver)
          --             break
          --           end
          --           Citizen.Wait(5)
          --         end
          --       end
          --     end
          --   end)
          -- end)

          ESX.Game.SpawnVehicle(vehicleProps.model, vehicles[partNum].SpawnPoint, 270.0, function(vehicle)
            ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
            local playerPed = GetPlayerPed(-1)
            TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
          end)

          TriggerServerEvent('esx_society:removeVehicleFromGarage', 'sheriff', vehicleProps)

        end,
        function(data, menu)

          menu.close()

          CurrentAction     = 'menu_vehicle_spawner'
          CurrentActionMsg  = _U('vehicle_spawner')
          CurrentActionData = {station = station, partNum = partNum}
        end
      )
    end, 'sheriff')
  else

    local elements = {}

    table.insert(elements, { label = 'Vélo', value = 'fixter' })
    table.insert(elements, { label = 'Cruiser', value = 'police' })
    table.insert(elements, { label = 'Sheriff Cruiser', value = 'sheriff' })

    if PlayerData.job.grade_name == 'officer' then
      table.insert(elements, { label = 'Interceptor', value = 'police3'})
    end

    if PlayerData.job.grade_name == 'sergeant' then
      table.insert(elements, { label = 'Sheriff SUV', value = 'sheriff2'})
      table.insert(elements, { label = 'Interceptor', value = 'police3'})
      table.insert(elements, { label = 'Buffalo', value = 'police2'})
      table.insert(elements, { label = 'Moto', value = 'policeb'})
      table.insert(elements, { label = 'Bus pénitentiaire', value = 'pbus'})
      table.insert(elements, { label = 'Bus de transport', value = 'policet'})
      table.insert(elements, { label = 'Antiémeute', value = 'riot'})
    end

    if PlayerData.job.grade_name == 'lieutenant' then
      table.insert(elements, { label = 'Sheriff SUV', value = 'sheriff2'})
      table.insert(elements, { label = 'Interceptor', value = 'police3'})
      table.insert(elements, { label = 'Buffalo', value = 'police2'})
      table.insert(elements, { label = 'Moto', value = 'policeb'})
      table.insert(elements, { label = 'Bus pénitentiaire', value = 'pbus'})
      table.insert(elements, { label = 'Bus de transport', value = 'policet'})
      table.insert(elements, { label = 'Antiémeute', value = 'riot'})
      table.insert(elements, { label = 'FBI', value = 'fbi'})
      table.insert(elements, { label = 'FBI SUV', value = 'fbi2'})
    end

    if PlayerData.job.grade_name == 'boss' then
      table.insert(elements, { label = 'Sheriff SUV', value = 'sheriff2'})
      table.insert(elements, { label = 'Interceptor', value = 'police3'})
      table.insert(elements, { label = 'Buffalo', value = 'police2'})
      table.insert(elements, { label = 'Moto', value = 'policeb'})
      table.insert(elements, { label = 'Bus pénitentiaire', value = 'pbus'})
      table.insert(elements, { label = 'Bus de transport', value = 'policet'})
      table.insert(elements, { label = 'Antiémeute', value = 'riot'})
      table.insert(elements, { label = 'FBI', value = 'fbi'})
      table.insert(elements, { label = 'FBI SUV', value = 'fbi2'})
      table.insert(elements, { label = 'Voiture Banalisée ', value = 'police4'})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vehicle_spawner',
      {
        title    = _U('vehicle_menu'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()
        local model = data.current.value
        local vehicle = GetClosestVehicle(vehicles[partNum].SpawnPoint.x,  vehicles[partNum].SpawnPoint.y,  vehicles[partNum].SpawnPoint.z,  3.0,  0,  71)

        if not DoesEntityExist(vehicle) then

          local playerPed = GetPlayerPed(-1)

          if Config.MaxInService == -1 then

            ESX.Game.SpawnVehicle(model, {
              x = vehicles[partNum].SpawnPoint.x,
              y = vehicles[partNum].SpawnPoint.y,
              z = vehicles[partNum].SpawnPoint.z
            }, vehicles[partNum].Heading, function(vehicle)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
              SetVehicleMaxMods(vehicle)
            end)
          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                ESX.Game.SpawnVehicle(model, {
                  x = vehicles[partNum].SpawnPoint.x,
                  y = vehicles[partNum].SpawnPoint.y,
                  z = vehicles[partNum].SpawnPoint.z
                }, vehicles[partNum].Heading, function(vehicle)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
                  SetVehicleMaxMods(vehicle)
                end)
              else
                ESX.ShowNotification(_U('service_max') .. inServiceCount .. '/' .. maxInService)
              end
            end, 'police')
          end
        else
          ESX.ShowNotification(_U('vehicle_out'))
        end
      end,
      function(data, menu)
        menu.close()
        CurrentAction     = 'menu_vehicle_spawner'
        CurrentActionMsg  = _U('vehicle_spawner')
        CurrentActionData = {station = station, partNum = partNum}
      end
    )
  end
end

function OpenPoliceActionsMenu()
	ESX.UI.Menu.CloseAll()

  local options = {
    {label = "Appels", value = 'calls'},
    {label = _U('citizen_interaction'), value = 'citizen_interaction'},
    {label = _U('vehicle_interaction'), value = 'vehicle_interaction'},
    {label = "Retirer le radar", value = "remove_radar"},
  }

  if PlayerData.job.grade_name == "boss" or PlayerData.job.grade_name == "lieutenant" then
    table.insert(options, {label = "Publier un Avis de recherche", value = "warrant"})
  end

	ESX.UI.Menu.Open(
	'default', GetCurrentResourceName(), 'police_actions',
	{
		title    = 'Sheriff',
		align    = 'top-left',
		elements = options
	}, function(data, menu)

    if data.current.value == "remove_radar" then
      if RadarObject.Object ~= nil then
        if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), RadarPos.x, RadarPos.y, RadarPos.z, true) < 5 then
          RequestAnimDict("anim@apt_trans@garage")
          while not HasAnimDictLoaded("anim@apt_trans@garage") do
            Citizen.Wait(1)
          end
          TaskPlayAnim(GetPlayerPed(-1), "anim@apt_trans@garage", "gar_open_1_left", 1.0, -1.0, 5000, 0, 1, true, true, true)
          Citizen.Wait(1000)
          if DoesEntityExist(RadarObject.Object) then
            DeleteEntity(RadarObject.Object)
          end
          TriggerServerEvent("esx_policejob:radarRemoved")
        end
      end
    end

    if data.current.value == 'calls' then
      ESX.TriggerServerCallback('esx_sheriffjob:getCallsList', function(calls)
        local options = {}

        if calls ~= nil then
          for k, v in pairs(calls) do
            if k == SelectedCall then
              table.insert(options, {label = v.nbr.."x Unité(s) en route - "..v.text, value = k})
            else
              table.insert(options, {label = v.nbr.."x Unité(s) en route - "..v.text, value = k})
            end
          end
        else
          table.insert(options, {label = "Aucun Appel en Attente"})
        end
          ESX.UI.Menu.Open(
          'default', GetCurrentResourceName(), 'police_calls',
          {
            title    = 'Sheriff - Appels',
            align    = 'top-left',
            elements = options
          }, function(data2, menu2)
            local callid = data2.current.value
            local options2 = {
              {label = "Prendre l'appel", value="take_call"},
              {label = "Position de l'appel", value="pos_call"},
              {label = "Appel Résolu", value="end_call"},
              {label = "Refuser l'appel", value="refused_call"}
            }
            ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'police_call',
            {
              title    = 'Actions',
              align    = 'top-left',
              elements = options2
            }, function(data3, menu3)
            if data3.current.value == "take_call" and SelectedCall ~= data2.current.value then
              SelectedCall = data2.current.value
              TriggerServerEvent('esx_sheriffjob:takeCall', data2.current.value)
              local x = calls[data2.current.value].position.x
              local y = calls[data2.current.value].position.y
              SetNewWaypoint(x, y)
            end
            if data3.current.value == "pos_call" then
              local x = calls[data2.current.value].position.x
              local y = calls[data2.current.value].position.y
              SetNewWaypoint(x, y)
            end
            if data3.current.value == "end_call" then
              TriggerServerEvent('esx_sheriffjob:endCall', callid)
              ESX.ShowNotification("~g~Appel Résolu")
            end
            if data3.current.value == "refused_call" then
              TriggerServerEvent("esx_sheriffjob:refuseCall", callid)
            end
          end,
          function(data3, menu3)
            menu3.close()
          end)
        end,
        function(data2, menu2)
          menu2.close()
        end)
      end)
    end

		if data.current.value == 'citizen_interaction' then
			local elements = {
				{label = _U('id_card'),			value = 'identity_card'},
				{label = _U('search'),			value = 'body_search'},
				{label = _U('handcuff'),		value = 'handcuff'},
				{label = _U('drag'),			value = 'drag'},
				{label = _U('put_in_vehicle'),	value = 'put_in_vehicle'},
				{label = _U('out_the_vehicle'),	value = 'out_the_vehicle'},
				{label = _U('fine'),			value = 'fine'}
			}

			if Config.EnableLicenses then
				table.insert(elements, { label = _U('license_check'), value = 'license' })
			end

			ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'citizen_interaction',
			{
				title    = _U('citizen_interaction'),
				align    = 'top-left',
				elements = elements
			}, function(data2, menu2)
				local player, distance = ESX.Game.GetClosestPlayer()
				if distance ~= -1 and distance <= 3.0 then
					local action = data2.current.value

					if action == 'identity_card' then
						OpenIdentityCardMenu(player)
					elseif action == 'body_search' then
						OpenBodySearchMenu(player)
					elseif action == 'handcuff' then
						TriggerServerEvent('esx_policejob:handcuff', GetPlayerServerId(player))
					elseif action == 'drag' then
						TriggerServerEvent('esx_policejob:drag', GetPlayerServerId(player))
					elseif action == 'put_in_vehicle' then
						TriggerServerEvent('esx_policejob:putInVehicle', GetPlayerServerId(player))
					elseif action == 'out_the_vehicle' then
						TriggerServerEvent('esx_policejob:OutVehicle', GetPlayerServerId(player))
					elseif action == 'fine' then
						OpenFineMenu(player)
					elseif action == 'license' then
						ShowPlayerLicense(player)
					end
				else
					ESX.ShowNotification(_U('no_players_nearby'))
				end
			end, function(data2, menu2)
				menu2.close()
			end)
		elseif data.current.value == 'vehicle_interaction' then
			local elements = {}
			local playerPed = GetPlayerPed(-1)
			local coords    = GetEntityCoords(playerPed)
			local vehicle   = GetClosestVehicle(coords.x,  coords.y,  coords.z,  3.0,  0,  71)

			if DoesEntityExist(vehicle) then
				table.insert(elements, {label = _U('vehicle_info'),	value = 'vehicle_infos'})
				table.insert(elements, {label = _U('pick_lock'),	value = 'hijack_vehicle'})
				table.insert(elements, {label = _U('impound'),		value = 'impound'})
			end

			--table.insert(elements, {label = _U('search_database'), value = 'search_database'})

			ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'vehicle_interaction',
			{
				title    = _U('vehicle_interaction'),
				align    = 'top-left',
				elements = elements
			}, function(data2, menu2)
				coords    = GetEntityCoords(playerPed)
				vehicle   = GetClosestVehicle(coords.x, coords.y, coords.z, 3.0,  0, 71)
				action    = data2.current.value

				if action == 'search_database' then
					LookupVehicle()
				elseif DoesEntityExist(vehicle) then
					local vehicleData = ESX.Game.GetVehicleProperties(vehicle)
					if action == 'vehicle_infos' then
						OpenVehicleInfosMenu(vehicleData)

					elseif action == 'hijack_vehicle' then
						if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 3.0) then
							TaskStartScenarioInPlace(playerPed, "WORLD_HUMAN_WELDING", 0, true)
							Citizen.Wait(20000)
							ClearPedTasksImmediately(playerPed)

							SetVehicleDoorsLocked(vehicle, 1)
							SetVehicleDoorsLockedForAllPlayers(vehicle, false)
							ESX.ShowNotification(_U('vehicle_unlocked'))
						end
					elseif action == 'impound' then

						-- is the script busy?
						if CurrentTask.Busy then
							return
						end

						SetTextComponentFormat('STRING')
						AddTextComponentString(_U('impound_prompt')) --press ~INPUT_CONTEXT~ to cancel the impound
						DisplayHelpTextFromStringLabel(0, 0, 1, -1)

						TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TEND_TO_DEAD', 0, true)

						CurrentTask.Busy = true
						CurrentTask.Task = ESX.SetTimeout(10000, function()
							ClearPedTasks(playerPed)
							ImpoundVehicle(vehicle)
							Citizen.Wait(100) -- sleep the entire script to let stuff sink back to reality
						end)

						-- keep track of that vehicle!
						Citizen.CreateThread(function()
							while CurrentTask.Busy do
								Citizen.Wait(1000)

								vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 3.0, 0, 71)
								if not DoesEntityExist(vehicle) and CurrentTask.Busy then
									ESX.ShowNotification(_U('impound_canceled_moved'))
									ESX.ClearTimeout(CurrentTask.Task)
									ClearPedTasks(playerPed)
									CurrentTask.Busy = false
									break
								end
							end
						end)
					end
				else
					ESX.ShowNotification(_U('no_vehicles_nearby'))
				end

			end, function(data2, menu2)
				menu2.close()
			end
			)
		end

    if data.current.value == "warrant" then
      DisplayOnscreenKeyboard(1, "FMMC_MPM_NA", "", "", "", "", "", 100)
      while (UpdateOnscreenKeyboard() == 0) do
          DisableAllControlActions(0);
          Citizen.Wait(0);
      end
      if (GetOnscreenKeyboardResult()) then
          text = GetOnscreenKeyboardResult()
      end
      TriggerServerEvent("esx_sheriffjob:addWarrant", text)
    end
	end, function(data, menu)
		menu.close()
	end)
end

function OpenIdentityCardMenu(player)

  if Config.EnableESXIdentity then

    ESX.TriggerServerCallback('esx_policejob:getOtherPlayerData', function(data)

      local jobLabel    = nil
      local sexLabel    = nil
      local sex         = nil
      local dobLabel    = nil
      local heightLabel = nil
      local idLabel     = nil

      if data.job.grade_label ~= nil and  data.job.grade_label ~= '' then
        jobLabel = 'Job: ' .. data.job.label .. ' - ' .. data.job.grade_label
      else
        jobLabel = 'Job: ' .. data.job.label
      end

      if data.sex ~= nil then
        if (data.sex == 'm') or (data.sex == 'M') then
          sex = 'Male'
        else
          sex = 'Female'
        end
        sexLabel = 'Sex: ' .. sex
      else
        sexLabel = 'Sex: Unknown'
      end

      if data.dob ~= nil then
        dobLabel = 'DOB: ' .. data.dob
      else
        dobLabel = 'DOB: Unknown'
      end

      if data.height ~= nil then
        heightLabel = 'Height: ' .. data.height
      else
        heightLabel = 'Height: Unknown'
      end

      if data.name ~= nil then
        idLabel = 'ID: ' .. data.name
      else
        idLabel = 'ID: Unknown'
      end

      local elements = {
        {label = _U('name') .. data.firstname .. " " .. data.lastname, value = nil},
        {label = sexLabel,    value = nil},
        {label = dobLabel,    value = nil},
        {label = heightLabel, value = nil},
        {label = jobLabel,    value = nil},
        {label = idLabel,     value = nil},
      }

      if data.drunk ~= nil then
        table.insert(elements, {label = _U('bac') .. data.drunk .. '%', value = nil})
      end

      if data.licenses ~= nil then

        table.insert(elements, {label = '--- Licenses ---', value = nil})

        for i=1, #data.licenses, 1 do
          table.insert(elements, {label = data.licenses[i].label .. "(" .. data.licenses[i].points .. ")", value = nil})
        end
      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'citizen_interaction',
        {
          title    = _U('citizen_interaction'),
          align    = 'top-left',
          elements = elements,
        },
        function(data, menu)

        end,
        function(data, menu)
          menu.close()
        end
      )
    end, GetPlayerServerId(player))
  else

    ESX.TriggerServerCallback('esx_policejob:getOtherPlayerData', function(data)

      local jobLabel = nil

      if data.job.grade_label ~= nil and  data.job.grade_label ~= '' then
        jobLabel = 'Job: ' .. data.job.label .. ' - ' .. data.job.grade_label
      else
        jobLabel = 'Job: ' .. data.job.label
      end

        local elements = {
          {label = _U('name') .. data.name, value = nil},
          {label = jobLabel,              value = nil},
        }

      if data.drunk ~= nil then
        table.insert(elements, {label = _U('bac') .. data.drunk .. '%', value = nil})
      end

      if data.licenses ~= nil then

        table.insert(elements, {label = '--- Licenses ---', value = nil})

        for i=1, #data.licenses, 1 do
          table.insert(elements, {label = data.licenses[i].label .. "(" .. data.licenses[i].points .. ")", value = nil})
        end
      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'citizen_interaction',
        {
          title    = _U('citizen_interaction'),
          align    = 'top-left',
          elements = elements,
        },
        function(data, menu)

        end,
        function(data, menu)
          menu.close()
        end
      )
    end, GetPlayerServerId(player))
  end
end

function OpenBodySearchMenu(player)

  ESX.TriggerServerCallback('esx_policejob:getOtherPlayerData', function(data)

    local elements = {}

    local blackMoney = 0

    for i=1, #data.accounts, 1 do
      if data.accounts[i].name == 'black_money' then
        blackMoney = data.accounts[i].money
      end
    end

    table.insert(elements, {
      label          = _U('confiscate_dirty') .. blackMoney,
      value          = 'black_money',
      itemType       = 'item_account',
      amount         = blackMoney
    })

    table.insert(elements, {label = '--- Armes ---', value = nil})

    for i=1, #data.weapons, 1 do
      table.insert(elements, {
        label          = _U('confiscate') .. ESX.GetWeaponLabel(data.weapons[i].name),
        value          = data.weapons[i].name,
        itemType       = 'item_weapon',
        amount         = data.ammo,
      })
    end

    table.insert(elements, {label = _U('inventory_label'), value = nil})

    for i=1, #data.inventory, 1 do
      if data.inventory[i].count > 0 then
        table.insert(elements, {
          label          = _U('confiscate_inv') .. data.inventory[i].count .. ' ' .. data.inventory[i].label,
          value          = data.inventory[i].name,
          itemType       = 'item_standard',
          amount         = data.inventory[i].count,
        })
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'body_search',
      {
        title    = _U('search'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        local itemType = data.current.itemType
        local itemName = data.current.value
        local amount   = data.current.amount

        if data.current.value ~= nil then

          TriggerServerEvent('esx_policejob:confiscatePlayerItem', GetPlayerServerId(player), itemType, itemName, amount)

          OpenBodySearchMenu(player)
        end
      end,
      function(data, menu)
        menu.close()
      end
    )
  end, GetPlayerServerId(player))
end

function OpenFineMenu(player)

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'fine',
    {
      title    = _U('fine'),
      align    = 'top-left',
      elements = {
        {label = _U('traffic_offense'),   value = 0},
        {label = _U('minor_offense'),     value = 1},
        {label = _U('average_offense'),   value = 2},
        {label = _U('major_offense'),     value = 3}
      },
    },
    function(data, menu)
      OpenFineCategoryMenu(player, data.current.value)
    end,
    function(data, menu)
      menu.close()
    end
  )
end

function OpenFineCategoryMenu(player, category)

  ESX.TriggerServerCallback('esx_policejob:getFineList', function(fines)

    local elements = {}

    for i=1, #fines, 1 do
      table.insert(elements, {
        label     = fines[i].label .. ' $' .. fines[i].amount,
        value     = fines[i].id,
        amount    = fines[i].amount,
        fineLabel = fines[i].label
      })
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'fine_category',
      {
        title    = _U('fine'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        local label  = data.current.fineLabel
        local amount = data.current.amount

        menu.close()

        if Config.EnablePlayerManagement then
          TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), 'society_sheriff', _U('fine_total') .. label, amount)
        else
          TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), '', _U('fine_total') .. label, amount)
        end

        ESX.SetTimeout(300, function()
          OpenFineCategoryMenu(player, category)
        end)
      end,
      function(data, menu)
        menu.close()
      end
    )
  end, category)
end

function LookupVehicle()
	ESX.UI.Menu.Open(
	'dialog', GetCurrentResourceName(), 'lookup_vehicle',
	{
		title = _U('search_database_title'),
	}, function (data, menu)
		local length = string.len(data.value)
		if data.value == nil or length < 8 or length > 13 then
			ESX.ShowNotification(_U('search_database_error_invalid'))
		else
			ESX.TriggerServerCallback('esx_policejob:getVehicleFromPlate', function(owner, found)
				if found then
					ESX.ShowNotification(_U('search_database_found', owner))
				else
					ESX.ShowNotification(_U('search_database_error_not_found'))
				end
			end, data.value)
			menu.close()
		end
	end, function (data, menu)
		menu.close()
	end
	)
end

function ShowPlayerLicense(player)
	local elements = {}
	local targetName

	ESX.TriggerServerCallback('esx_policejob:getOtherPlayerData', function(data)
		if data.licenses ~= nil then
			for i=1, #data.licenses, 1 do
				if data.licenses[i].label ~= nil and data.licenses[i].type ~= nil then
					table.insert(elements, {label = data.licenses[i].label, value = data.licenses[i].type})
				end
			end
		end

		if Config.EnableESXIdentity then
			targetName = data.firstname .. ' ' .. data.lastname
		else
			targetName = data.name
		end

		ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'manage_license',
		{
			title    = _U('license_revoke'),
			align    = 'top-left',
			elements = elements,
		},
		function(data, menu)
			ESX.ShowNotification(_U('licence_you_revoked', data.current.label, targetName))
			TriggerServerEvent('esx_policejob:message', GetPlayerServerId(player), _U('license_revoked', data.current.label))

			TriggerServerEvent('esx_license:removeLicense', GetPlayerServerId(player), data.current.value)

			ESX.SetTimeout(300, function()
				ShowPlayerLicense(player)
			end)
		end,
		function(data, menu)
			menu.close()
		end
		)
	end, GetPlayerServerId(player))
end


function OpenVehicleInfosMenu(vehicleData)

  ESX.TriggerServerCallback('esx_policejob:getVehicleInfos', function(infos)

    local elements = {}

    table.insert(elements, {label = _U('plate') .. infos.plate, value = nil})

    if infos.owner == nil then
      table.insert(elements, {label = _U('owner_unknown'), value = nil})
    else
      table.insert(elements, {label = _U('owner') .. infos.owner, value = nil})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vehicle_infos',
      {
        title    = _U('vehicle_info'),
        align    = 'top-left',
        elements = elements,
      },
      nil,
      function(data, menu)
        menu.close()
      end
    )
  end, vehicleData.plate)
end

function OpenGetWeaponMenu()

  ESX.TriggerServerCallback('esx_sheriffjob:getArmoryWeapons', function(weapons)

    local elements = {}

    for i=1, #weapons, 1 do
      if weapons[i].count > 0 then
        table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'armory_get_weapon',
      {
        title    = _U('get_weapon_menu'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        ESX.TriggerServerCallback('esx_sheriffjob:removeArmoryWeapon', function()
          OpenGetWeaponMenu()
        end, data.current.value)
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

function OpenPutWeaponMenu()

  local elements   = {}
  local playerPed  = GetPlayerPed(-1)
  local weaponList = ESX.GetWeaponList()

  for i=1, #weaponList, 1 do

    local weaponHash = GetHashKey(weaponList[i].name)

    if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
      local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
      table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
    end

  end

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'armory_put_weapon',
    {
      title    = _U('put_weapon_menu'),
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      menu.close()

      ESX.TriggerServerCallback('esx_sheriffjob:addArmoryWeapon', function()
        OpenPutWeaponMenu()
      end, data.current.value)
    end,
    function(data, menu)
      menu.close()
    end
  )
end

function OpenBuyWeaponsMenu(station)

  ESX.TriggerServerCallback('esx_sheriffjob:getArmoryWeapons', function(weapons)

    local elements = {}

    for i=1, #Config.PoliceStations[station].AuthorizedWeapons, 1 do

      local weapon = Config.PoliceStations[station].AuthorizedWeapons[i]
      local count  = 0

      for i=1, #weapons, 1 do
        if weapons[i].name == weapon.name then
          count = weapons[i].count
          break
        end
      end

      table.insert(elements, {label = 'x' .. count .. ' ' .. ESX.GetWeaponLabel(weapon.name) .. ' $' .. weapon.price, value = weapon.name, price = weapon.price})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'armory_buy_weapons',
      {
        title    = _U('buy_weapon_menu'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        ESX.TriggerServerCallback('esx_sheriffjob:buy', function(hasEnoughMoney)

          if hasEnoughMoney then
            ESX.TriggerServerCallback('esx_sheriffjob:addArmoryWeapon', function()
              OpenBuyWeaponsMenu(station)
            end, data.current.value)
          else
            ESX.ShowNotification(_U('not_enough_money'))
          end
        end, data.current.price)
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('esx_sheriffjob:getStockItems', function(items)


    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('police_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('esx_sheriffjob:getStockItem', itemName, count)
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

function OpenGetPublicStocksMenu()

  ESX.TriggerServerCallback('esx_sheriffjob:getPublicStockItems', function(items)


    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('police_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              OpenGetPublicStocksMenu()

              TriggerServerEvent('esx_sheriffjob:getPublicStockItem', itemName, count)
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

function OpenPutStocksMenu()

  ESX.TriggerServerCallback('esx_policejob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('esx_sheriffjob:putStockItems', itemName, count)
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

function OpenPutPublicStocksMenu()

  ESX.TriggerServerCallback('esx_policejob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('esx_sheriffjob:putPublicStockItems', itemName, count)
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job

	-- Citizen.Wait(5000)
	-- TriggerServerEvent('esx_policejob:forceBlip')
end)

AddEventHandler('esx_sheriffjob:hasEnteredMarker', function(station, part, partNum)

  if part == 'Cloakroom' then
    CurrentAction     = 'menu_cloakroom'
    CurrentActionMsg  = _U('open_cloackroom')
    CurrentActionData = {}
  end

  if part == 'Armory' then
    CurrentAction     = 'menu_armory'
    CurrentActionMsg  = _U('open_armory')
    CurrentActionData = {station = station}
  end

  if part == 'VehicleSpawner' then
    CurrentAction     = 'menu_vehicle_spawner'
    CurrentActionMsg  = _U('vehicle_spawner')
    CurrentActionData = {station = station, partNum = partNum}
  end

  if part == 'VehicleDeleter' then

    local playerPed = GetPlayerPed(-1)
    local coords    = GetEntityCoords(playerPed)

    if IsPedInAnyVehicle(playerPed,  false) then

      local vehicle = GetVehiclePedIsIn(playerPed, false)

      if DoesEntityExist(vehicle) then
        CurrentAction     = 'delete_vehicle'
        CurrentActionMsg  = _U('store_vehicle')
        CurrentActionData = {vehicle = vehicle}
      end
    end
  end

  if part == 'BossActions' then
    CurrentAction     = 'menu_boss_actions'
    CurrentActionMsg  = _U('open_bossmenu')
    CurrentActionData = {}
  end
end)

AddEventHandler('esx_sheriffjob:hasExitedMarker', function(station, part, partNum)
  ESX.UI.Menu.CloseAll()
  CurrentAction = nil
end)

-- Create blips
Citizen.CreateThread(function()

  for k,v in pairs(Config.PoliceStations) do

    local blip = AddBlipForCoord(v.Blip.Pos.x, v.Blip.Pos.y, v.Blip.Pos.z)

    SetBlipSprite (blip, v.Blip.Sprite)
    SetBlipDisplay(blip, v.Blip.Display)
    SetBlipScale  (blip, v.Blip.Scale)
    SetBlipColour (blip, v.Blip.Colour)
    SetBlipAsShortRange(blip, true)

    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('map_blip'))
    EndTextCommandSetBlipName(blip)
  end
end)

-- Display markers
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    if PlayerData.job ~= nil and PlayerData.job.name == 'sheriff' then

      local playerPed = GetPlayerPed(-1)
      local coords    = GetEntityCoords(playerPed)

      for k,v in pairs(Config.PoliceStations) do

        for i=1, #v.Cloakrooms, 1 do
          if GetDistanceBetweenCoords(coords,  v.Cloakrooms[i].x,  v.Cloakrooms[i].y,  v.Cloakrooms[i].z,  true) < Config.DrawDistance then
            DrawMarker(Config.MarkerType, v.Cloakrooms[i].x, v.Cloakrooms[i].y, v.Cloakrooms[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
          end
        end

        if InService then
          for i=1, #v.Armories, 1 do
            if GetDistanceBetweenCoords(coords,  v.Armories[i].x,  v.Armories[i].y,  v.Armories[i].z,  true) < Config.DrawDistance then
              DrawMarker(Config.MarkerType, v.Armories[i].x, v.Armories[i].y, v.Armories[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
            end
          end

          for i=1, #v.Vehicles, 1 do
            if GetDistanceBetweenCoords(coords,  v.Vehicles[i].Spawner.x,  v.Vehicles[i].Spawner.y,  v.Vehicles[i].Spawner.z,  true) < Config.DrawDistance then
              DrawMarker(Config.MarkerType, v.Vehicles[i].Spawner.x, v.Vehicles[i].Spawner.y, v.Vehicles[i].Spawner.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
            end
          end

          for i=1, #v.HelicoptersSpawner, 1 do
            if GetDistanceBetweenCoords(coords,  v.HelicoptersSpawner[i].x,  v.HelicoptersSpawner[i].y,  v.HelicoptersSpawner[i].z,  true) < Config.DrawDistance then
              DrawMarker(Config.MarkerType, v.HelicoptersSpawner[i].x, v.HelicoptersSpawner[i].y, v.HelicoptersSpawner[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
            end
          end

          for i=1, #v.HelicoptersSpawnpoint, 1 do
            if IsPedInAnyHeli(playerPed) and GetDistanceBetweenCoords(coords,  v.HelicoptersSpawnpoint[i].x,  v.HelicoptersSpawnpoint[i].y,  v.HelicoptersSpawnpoint[i].z,  true) < Config.DrawDistance then
              DrawMarker(27, v.HelicoptersSpawnpoint[i].x, v.HelicoptersSpawnpoint[i].y, v.HelicoptersSpawnpoint[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 8.5, 8.5, 1.0, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
            end
          end

          for i=1, #v.VehicleDeleters, 1 do
            if GetDistanceBetweenCoords(coords,  v.VehicleDeleters[i].x,  v.VehicleDeleters[i].y,  v.VehicleDeleters[i].z,  true) < Config.DrawDistance then
              DrawMarker(Config.MarkerType, v.VehicleDeleters[i].x, v.VehicleDeleters[i].y, v.VehicleDeleters[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
            end
          end
        end

        if Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.name == 'sheriff' and PlayerData.job.grade_name == 'boss' then

          for i=1, #v.BossActions, 1 do
            if not v.BossActions[i].disabled and GetDistanceBetweenCoords(coords,  v.BossActions[i].x,  v.BossActions[i].y,  v.BossActions[i].z,  true) < Config.DrawDistance then
              DrawMarker(Config.MarkerType, v.BossActions[i].x, v.BossActions[i].y, v.BossActions[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
            end
          end
        end
      end
    end
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    if PlayerData.job ~= nil and PlayerData.job.name == 'sheriff' then

      local playerPed      = GetPlayerPed(-1)
      local coords         = GetEntityCoords(playerPed)
      local isInMarker     = false
      local currentStation = nil
      local currentPart    = nil
      local currentPartNum = nil

      for k,v in pairs(Config.PoliceStations) do

        for i=1, #v.Cloakrooms, 1 do
          if GetDistanceBetweenCoords(coords,  v.Cloakrooms[i].x,  v.Cloakrooms[i].y,  v.Cloakrooms[i].z,  true) < Config.MarkerSize.x then
            isInMarker     = true
            currentStation = k
            currentPart    = 'Cloakroom'
            currentPartNum = i
          end
        end

        if InService then
          for i=1, #v.Armories, 1 do
            if GetDistanceBetweenCoords(coords,  v.Armories[i].x,  v.Armories[i].y,  v.Armories[i].z,  true) < Config.MarkerSize.x then
              isInMarker     = true
              currentStation = k
              currentPart    = 'Armory'
              currentPartNum = i
            end
          end

          for i=1, #v.Vehicles, 1 do

            if GetDistanceBetweenCoords(coords,  v.Vehicles[i].Spawner.x,  v.Vehicles[i].Spawner.y,  v.Vehicles[i].Spawner.z,  true) < Config.MarkerSize.x then
              isInMarker     = true
              currentStation = k
              currentPart    = 'VehicleSpawner'
              currentPartNum = i
            end

            if GetDistanceBetweenCoords(coords,  v.Vehicles[i].SpawnPoint.x,  v.Vehicles[i].SpawnPoint.y,  v.Vehicles[i].SpawnPoint.z,  true) < Config.MarkerSize.x then
              isInMarker     = true
              currentStation = k
              currentPart    = 'VehicleSpawnPoint'
              currentPartNum = i
            end
          end

          for i=1, #v.VehicleDeleters, 1 do
            if GetDistanceBetweenCoords(coords,  v.VehicleDeleters[i].x,  v.VehicleDeleters[i].y,  v.VehicleDeleters[i].z,  true) < Config.MarkerSize.x then
              isInMarker     = true
              currentStation = k
              currentPart    = 'VehicleDeleter'
              currentPartNum = i
            end
          end
        end

        if Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.name == 'sheriff' and PlayerData.job.grade_name == 'boss' then

          for i=1, #v.BossActions, 1 do
            if GetDistanceBetweenCoords(coords,  v.BossActions[i].x,  v.BossActions[i].y,  v.BossActions[i].z,  true) < Config.MarkerSize.x then
              isInMarker     = true
              currentStation = k
              currentPart    = 'BossActions'
              currentPartNum = i
            end
          end
        end
      end

      local hasExited = false

      if isInMarker and not HasAlreadyEnteredMarker or (isInMarker and (LastStation ~= currentStation or LastPart ~= currentPart or LastPartNum ~= currentPartNum) ) then

        if
          (LastStation ~= nil and LastPart ~= nil and LastPartNum ~= nil) and
          (LastStation ~= currentStation or LastPart ~= currentPart or LastPartNum ~= currentPartNum)
        then
          TriggerEvent('esx_sheriffjob:hasExitedMarker', LastStation, LastPart, LastPartNum)
          hasExited = true
        end

        HasAlreadyEnteredMarker = true
        LastStation             = currentStation
        LastPart                = currentPart
        LastPartNum             = currentPartNum

        TriggerEvent('esx_sheriffjob:hasEnteredMarker', currentStation, currentPart, currentPartNum)
      end

      if not hasExited and not isInMarker and HasAlreadyEnteredMarker then

        HasAlreadyEnteredMarker = false

        TriggerEvent('esx_sheriffjob:hasExitedMarker', LastStation, LastPart, LastPartNum)
      end
    end
  end
end)

-- Key Controls
Citizen.CreateThread(function()
	while true do

    --SetWeaponDrops()
		Citizen.Wait(5)

		if CurrentAction ~= nil then
			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0, Keys['E']) and PlayerData.job ~= nil and PlayerData.job.name == 'sheriff' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'menu_cloakroom' then
					OpenCloakroomMenu()
				elseif CurrentAction == 'menu_armory' then
					OpenArmoryMenu(CurrentActionData.station)
				elseif CurrentAction == 'menu_vehicle_spawner' then
					OpenVehicleSpawnerMenu(CurrentActionData.station, CurrentActionData.partNum)
        elseif CurrentAction == 'menu_helicopter_spawner' then
          OpenHelicopterSpawnerMenu(CurrentActionData.station, CurrentActionData.partNum)
				elseif CurrentAction == 'delete_vehicle' then
					local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
					TriggerServerEvent('esx_society:putVehicleInGarage', 'sheriff', vehicleProps)
					ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
				elseif CurrentAction == 'menu_boss_actions' then
					ESX.UI.Menu.CloseAll()
					TriggerEvent('esx_society:openBossMenu', 'sheriff', function(data, menu)
						menu.close()
						CurrentAction     = 'menu_boss_actions'
						CurrentActionMsg  = _U('open_bossmenu')
						CurrentActionData = {}
					end)
				end

				CurrentAction = nil
				GUI.Time      = GetGameTimer()
			end
		end -- CurrentAction end

		if IsControlPressed(0, Keys['F6']) and InService and not isDead and PlayerData.job ~= nil and PlayerData.job.name == 'sheriff' and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'police_actions') and (GetGameTimer() - GUI.Time) > 150 then
			OpenPoliceActionsMenu()
			GUI.Time = GetGameTimer()
		end

		if IsControlPressed(0, Keys['E']) and CurrentTask.Busy then
			ESX.ShowNotification(_U('impound_canceled'))
			ESX.ClearTimeout(CurrentTask.Task)
			ClearPedTasks(GetPlayerPed(-1))

			CurrentTask.Busy = false
		end
	end
end)

-- function createBlip(id)
-- 	ped = GetPlayerPed(id)
-- 	blip = GetBlipFromEntity(ped)

-- 	if not DoesBlipExist(blip) then -- Add blip and create head display on player
-- 		blip = AddBlipForEntity(ped)
-- 		SetBlipSprite(blip, 1)
-- 		Citizen.InvokeNative(0x5FBCA48327B914DF, blip, true) -- Player Blip indicator
-- 		SetBlipRotation(blip, math.ceil(GetEntityHeading(veh))) -- update rotation
-- 		SetBlipNameToPlayerName(blip, id) -- update blip name
-- 		SetBlipScale(blip, 0.85) -- set scale
-- 		SetBlipAsShortRange(blip, true)

-- 		table.insert(blipsCops, blip) -- add blip to array so we can remove it later
-- 	end
-- end

-- RegisterNetEvent('esx_policejob:updateBlip')
-- AddEventHandler('esx_policejob:updateBlip', function()

-- 	-- Refresh all blips
-- 	for k, existingBlip in pairs(blipsCops) do
-- 		RemoveBlip(existingBlip)
-- 	end

-- 	-- Clean the blip table
-- 	blipsCops = {}

-- 	-- Is the player a cop? In that case show all the blips for other cops
-- 	if PlayerData.job ~= nil and PlayerData.job.name == 'police' then
-- 		ESX.TriggerServerCallback('esx_society:getOnlinePlayers', function(players)
-- 			for i=1, #players, 1 do
-- 				if players[i].job.name == 'police' then
-- 					for id = 0, 32 do
-- 						if NetworkIsPlayerActive(id) and GetPlayerPed(id) ~= GetPlayerPed(-1) and GetPlayerName(id) == players[i].name then
-- 							createBlip(id)
-- 						end
-- 					end
-- 				end
-- 			end
-- 		end)
-- 	end

-- end)

-- AddEventHandler('playerSpawned', function(spawn)
-- 	isDead = false
-- 	if not hasAlreadyJoined then
-- 		TriggerServerEvent('esx_policejob:spawned')
-- 	end
-- 	hasAlreadyJoined = true
-- end)

AddEventHandler('esx:onPlayerDeath', function()
	isDead = true
end)

-- TODO
--   - return to garage if owned
--   - message owner that his vehicle has been impounded
function ImpoundVehicle(vehicle)
	--local vehicleName = GetLabelText(GetDisplayNameFromVehicleModel(GetEntityModel(vehicle)))
	ESX.Game.DeleteVehicle(vehicle)
	ESX.ShowNotification(_U('impound_successful'))
	CurrentTask.Busy = false
end

RegisterNetEvent('esx_sheriffjob:callPolice')
AddEventHandler('esx_sheriffjob:callPolice', function(datas)
  local pos = GetEntityCoords(GetPlayerPed(-1), false)

  TriggerServerEvent('esx_sheriffjob:newCall', datas.type, {x=pos.x, y=pos.y})

  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)

RegisterNetEvent('esx_sheriffjob:callPoliceCustom')
AddEventHandler('esx_sheriffjob:callPoliceCustom', function()
  local text = ""
  DisplayOnscreenKeyboard(1, "FMMC_MPM_NA", "", text, "", "", "", 100)
  while (UpdateOnscreenKeyboard() == 0) do
      DisableAllControlActions(0);
      Citizen.Wait(0);
  end
  if (GetOnscreenKeyboardResult()) then
      text = GetOnscreenKeyboardResult()
  end
  local pos = GetEntityCoords(GetPlayerPed(-1), false)
  TriggerServerEvent('esx_sheriffjob:newCall', "custom", {x = pos.x, y = pos.y}, text)
  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)

RegisterNetEvent('esx_sheriffjob:noCops')
AddEventHandler('esx_sheriffjob:noCops', function()
  ESX.ShowNotification("~r~Aucun Sheriff en Service Actuellement")
end)

RegisterNetEvent('esx_sheriffjob:callTaked')
AddEventHandler('esx_sheriffjob:callTaked', function()
  ESX.ShowNotification("~g~Votre appel viens d'être accepter, une unité est en route")
end)

RegisterNetEvent('esx_sheriffjob:endTaked')
AddEventHandler('esx_sheriffjob:endTaked', function()
  ESX.ShowNotification("~g~Appel Terminer")
end)

RegisterNetEvent('esx_sheriffjob:refuseTaked')
AddEventHandler('esx_sheriffjob:refuseTaked', function()
  ESX.ShowNotification("~r~Votre Appel à été refusé")
end)

RegisterNetEvent('esx_sheriffjob:cancelCall')
AddEventHandler('esx_sheriffjob:cancelCall', function()
  ESX.TriggerServerCallback("esx_sheriffjob:cancelCall", function(result)
    if result then
      ESX.ShowNotification("~g~Appel Annulé")
    end
  end)
end)
