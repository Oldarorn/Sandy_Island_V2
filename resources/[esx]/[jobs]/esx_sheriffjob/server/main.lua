ESX = nil
local CallsList = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

TriggerEvent('esx_society:registerSociety', 'sheriff', 'Sheriff', 'society_sheriff', 'society_sheriff', 'society_sheriff', {type = 'public'})

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'sheriff', Config.MaxInService)
end

RegisterServerEvent('esx_sheriffjob:getStockItem')
AddEventHandler('esx_sheriffjob:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_sheriff', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)

  end)

end)

RegisterServerEvent('esx_sheriffjob:getPublicStockItem')
AddEventHandler('esx_sheriffjob:getPublicStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_sheriff_public', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)

  end)

end)


RegisterServerEvent('esx_sheriffjob:putStockItems')
AddEventHandler('esx_sheriffjob:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_sheriff', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= 0 then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)

  end)

end)

RegisterServerEvent('esx_sheriffjob:putPublicStockItems')
AddEventHandler('esx_sheriffjob:putPublicStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_sheriff_public', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= 0 then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_sheriffjob:getArmoryWeapons', function(source, cb)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_sheriff', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    cb(weapons)

  end)

end)

ESX.RegisterServerCallback('esx_sheriffjob:addArmoryWeapon', function(source, cb, weaponName)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeWeapon(weaponName)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_sheriff', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = weapons[i].count + 1
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 1
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

ESX.RegisterServerCallback('esx_sheriffjob:removeArmoryWeapon', function(source, cb, weaponName)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.addWeapon(weaponName, 1000)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_sheriff', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 0
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)


ESX.RegisterServerCallback('esx_sheriffjob:buy', function(source, cb, amount)

  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_sheriff', function(account)

    if account.money >= amount then
      account.removeMoney(amount)
      cb(true)
    else
      cb(false)
    end

  end)

end)

ESX.RegisterServerCallback('esx_sheriffjob:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_sheriff', function(inventory)
    cb(inventory.items)
  end)

end)

ESX.RegisterServerCallback('esx_sheriffjob:getPublicStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_sheriff_public', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_sheriffjob:message')
AddEventHandler('esx_sheriffjob:message', function(target, msg)
	TriggerClientEvent('esx:showNotification', target, msg)
end)

function SendNotificationToPlayersInService(message)
  local xPlayers = ESX.GetPlayers()
  local countCops = 0
  for k, v in pairs(xPlayers) do
    ESX.TriggerServerCallback('esx_service:isInService', 0, v, function(inService)
      if inService then
        TriggerClientEvent('esx:showNotification', v, message)
        countCops = countCops + 1
      end
    end, 'sheriff')
  end
  if countCops <= 0 then
    return false
  end
  return true
end

ESX.RegisterServerCallback('esx_sheriffjob:getCallsList', function(source, cb)
  local _source = source
  cb(CallsList)
end)

RegisterNetEvent('esx_sheriffjob:newCall')
AddEventHandler('esx_sheriffjob:newCall', function(type, pos, message)
  local _source = source
  if type ~= "custom" then
    if type == "rob" then
      message = "Signalement d'un vol"
    elseif type == "attack" then
      message = "Signalement d'une agression"
    end
  end
  CallsList[_source] = {
    type = type,
    position = pos,
    text = message,
    nbr = 0
  }
  local notif = "Un nouvel appel viens d'arriver :"
  if not SendNotificationToPlayersInService(notif) then
    TriggerClientEvent('esx_sheriffjob:noCops', _source)
  else
    SendNotificationToPlayersInService("~r~"..message)
  end
end)

RegisterNetEvent('esx_sheriffjob:takeCall')
AddEventHandler('esx_sheriffjob:takeCall', function(id)
  CallsList[id].nbr = CallsList[id].nbr + 1
  TriggerClientEvent('esx_sheriffjob:callTaked', id)
end)

RegisterNetEvent('esx_sheriffjob:endCall')
AddEventHandler('esx_sheriffjob:endCall', function(id)
  TriggerClientEvent('esx_sheriffjob:endCall', id)
  CallsList[id] = nil
end)

RegisterNetEvent('esx_sheriffjob:refuseCall')
AddEventHandler('esx_sheriffjob:refuseCall', function(id)
  TriggerClientEvent('esx_sheriffjob:refuseCall', id)
  CallsList[id] = nil
end)

ESX.RegisterServerCallback("esx_sheriffjob:cancelCall", function(source, cb)
  local _source = source
  if CallsList[_source] ~= nil then
    CallsList[_source] = nil
    SendNotificationToPlayersInService("Appel Annulé")
    cb(true)
  else
    cb(false)
  end
end)

RegisterNetEvent("esx_sheriffjob:addWarrant")
AddEventHandler("esx_sheriffjob:addWarrant", function(message)
  TriggerClientEvent("esx_alerts:sheriffWarrant", -1, message)
end)