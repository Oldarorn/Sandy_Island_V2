Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 27
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = true
Config.EnableLicenses             = false -- only turn this on if you are using esx_license
Config.MaxInService               = 200
Config.Locale                     = 'fr'

Config.PoliceStations = {

  Sandy = {

    Blip = {
      Pos     = { x = -442.330, y = 6017.543, z = 31.68 },
      Sprite  = 461,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'GADGET_PARACHUTE',        price = 1000 },
      { name = 'WEAPON_NIGHTSTICK',       price = 1500 },
      { name = 'WEAPON_FLASHLIGHT',       price = 1500 },
      { name = 'WEAPON_FLAREGUN',         price = 5000 },
      { name = 'WEAPON_STUNGUN',          price = 10000 },
      { name = 'WEAPON_PISTOL',           price = 25000 },
      { name = 'WEAPON_COMBATPISTOL',     price = 30000 },
      { name = 'WEAPON_PUMPSHOTGUN',      price = 45000 },
      { name = 'WEAPON_ASSAULTSMG',       price = 60000 },
      { name = 'WEAPON_ASSAULTRIFLE',     price = 90000 },
    },

    AuthorizedVehicles = {
      { name = 'police',  label = 'Véhicule de patrouille 1' },
      { name = 'police2', label = 'Véhicule de patrouille 2' },
      { name = 'police3', label = 'Véhicule de patrouille 3' },
      { name = 'police4', label = 'Véhicule civil' },
      { name = 'policeb', label = 'Moto' },
      { name = 'policet', label = 'Van de transport' },
    },

    Cloakrooms = {
      { x = -450.049, y = 6016.197, z = 30.51 },
    },

    Armories = {
      { x = -447.361, y = 6007.817, z = 30.51 },
    },

    Vehicles = {
      {
        Spawner    = { x = -451.78, y = 6006.101, z = 30.84 },
        SpawnPoint = { x = -465.365, y = 6001.601, z = 30.84 },
        Heading    = 90.0,
      }
    },

    HelicoptersSpawner = {
      
    },
    HelicoptersSpawnpoint = {
      
    },

    VehicleDeleters = {
      { x = -445.775, y = 5984.453, z = 29.990 },
    },

    BossActions = {
      { x = -450.550, y = 6011.505, z = 30.51 }
    },

  },

}


-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Config.Uniforms = {
    
  cadet_wear = {
    male = {
        ['tshirt_1'] = 58,  ['tshirt_2'] = 0,
        ['torso_1'] = 26,   ['torso_2'] = 1,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 37,
        ['pants_1'] = 31,   ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 60,  ['helmet_2'] = 0,
        ['chain_1'] = 0,    ['chain_2'] = 0,
        ['ears_1'] = -1,     ['ears_2'] = 0
    },
    female = {
        ['tshirt_1'] = 35,  ['tshirt_2'] = 0,
        ['torso_1'] = 88,   ['torso_2'] = 1,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 33,
        ['pants_1'] = 30,   ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 60,  ['helmet_2'] = 0,
        ['chain_1'] = 0,    ['chain_2'] = 0,
        ['ears_1'] = -1,     ['ears_2'] = 0
    }
  },
  police_wear = {
    male = {
        ['tshirt_1'] = 58,  ['tshirt_2'] = 0,
        ['torso_1'] = 26,   ['torso_2'] = 2,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 37,
        ['pants_1'] = 31,   ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 6,  ['helmet_2'] = 7,
        ['chain_1'] = 0,    ['chain_2'] = 0,
        ['ears_1'] = -1,     ['ears_2'] = 0
    },
    female = {
        ['tshirt_1'] = 35,  ['tshirt_2'] = 0,
        ['torso_1'] = 27,   ['torso_2'] = 2,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 33,
        ['pants_1'] = 30,   ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 60,  ['helmet_2'] = 7,
        ['chain_1'] = 0,    ['chain_2'] = 0,
        ['ears_1'] = -1,     ['ears_2'] = 0
    }
  },
  sergeant_wear = {
    male = {
        ['tshirt_1'] = 58,  ['tshirt_2'] = 0,
        ['torso_1'] = 26,   ['torso_2'] = 4,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 37,
        ['pants_1'] = 31,   ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 13,  ['helmet_2'] = 7,
        ['chain_1'] = 0,    ['chain_2'] = 0,
        ['ears_1'] = -1,     ['ears_2'] = 0
    },
    female = {
        ['tshirt_1'] = 35,  ['tshirt_2'] = 0,
        ['torso_1'] = 27,   ['torso_2'] = 2,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 33,
        ['pants_1'] = 30,   ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 60,  ['helmet_2'] = 7,
        ['chain_1'] = 0,    ['chain_2'] = 0,
        ['ears_1'] = -1,     ['ears_2'] = 0
    }
  },
  lieutenant_wear = {
    male = {
        ['tshirt_1'] = 58,  ['tshirt_2'] = 0,
        ['torso_1'] = 26,   ['torso_2'] = 4,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 37,
        ['pants_1'] = 31,   ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 13,  ['helmet_2'] = 7,
        ['chain_1'] = 0,    ['chain_2'] = 0,
        ['ears_1'] = -1,     ['ears_2'] = 0
    },
    female = {
        ['tshirt_1'] = 35,  ['tshirt_2'] = 0,
        ['torso_1'] = 27,   ['torso_2'] = 2,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 33,
        ['pants_1'] = 30,   ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 60,  ['helmet_2'] = 7,
        ['chain_1'] = 0,    ['chain_2'] = 0,
        ['ears_1'] = -1,     ['ears_2'] = 0
    }
  },
  commandant_wear = {
    male = {
        ['tshirt_1'] = 58,  ['tshirt_2'] = 0,
        ['torso_1'] = 26,   ['torso_2'] = 4,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 37,
        ['pants_1'] = 31,   ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 13,  ['helmet_2'] = 7,
        ['chain_1'] = 0,    ['chain_2'] = 0,
        ['ears_1'] = -1,     ['ears_2'] = 0
    },
    female = {
        ['tshirt_1'] = 35,  ['tshirt_2'] = 0,
        ['torso_1'] = 27,   ['torso_2'] = 2,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 33,
        ['pants_1'] = 30,   ['pants_2'] = 0,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 20,  ['helmet_2'] = 1,
        ['chain_1'] = 0,    ['chain_2'] = 0,
        ['ears_1'] = -1,     ['ears_2'] = 0
    }
  },
  bullet_wear = {
    male = {
        ['bproof_1'] = 20,  ['bproof_2'] = 7
    },
    female = {
        ['bproof_1'] = 20,  ['bproof_2'] = 7
    }
  },
  gilet_wear = {
    male = {
        ['tshirt_1'] = 59,  ['tshirt_2'] = 1
    },
    female = {
        ['tshirt_1'] = 36,  ['tshirt_2'] = 1
    }
  }

}