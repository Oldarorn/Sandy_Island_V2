Config                            = {}
Config.DrawDistance               = 100.0

Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = false
Config.MaxInService 			  		  = 200
Config.Locale                     = 'fr'

Config.Zones = {

	SocietyActions = {
		Pos     = {x =110.643, y = -1314.717, z = 28.354},
		Size    = {x = 1.5, y = 1.5, z = 1.0},
		Color   = {r = 30, g = 144, b = 255 },
		Type    = 27,
	},

	VehicleSpawnPoint = {
		Pos  = {x = 114.973, y = -1316.988, z = 28.300},
		Size = {x = 1.5, y = 1.5, z = 1.0},
		Type = -1,
	},

	VehicleDeleter = {
		Pos   = {x = 114.973, y = -1316.988, z = 28.300},
		Size  = { x = 4.1, y = 4.1, z = 1.0 },
		Color = { r = 30, g = 144, b = 255 },
		Type  = 27,
	},

	Harvest1 = {
		Pos   = { x = 2413.522, y = 4991.765, z = 45.502 },
		Size  = { x = 4.1, y = 4.1, z = 1.0 },
		Color = { r = 30, g = 144, b = 255 },
		Type  = 27,
	},

	Craft1 = {
		Pos   = { x = 837.185, y = -1938.010, z = 28.05 },
		Size  = { x = 4.1, y = 4.1, z = 1.0 },
		Color = { r = 30, g = 144, b = 255 },
		Type  = 27,
	},

	Resell1 = {
		Pos   = { x = 141.3200, y = -1277.928, z = 28.350 },
		Size  = { x = 4.1, y = 4.1, z = 1.0 },
		Color = { r = 30, g = 144, b = 255 },
		Type  = 27,
	},

  Harvest = {
    Pos   = { x = 2326.955, y = 4999.946, z = 41.200 },
    Size  = { x = 4.1, y = 4.1, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },

  Craft = {
    Pos   = { x = 799.2905, y = -2502.9019, z = 20.862 },
    Size  = { x = 4.1, y = 4.1, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },

  Resell = {
    Pos   = { x = -564.5007, y = 302.0839, z = 82.1522 },
    Size  = { x = 4.1, y = 4.1, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },

  BossActions = {
    Pos   = { x = 98.799, y = -1308.683, z = 28.374 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },

  Stocks = {
    Pos   = { x =100.0973, y = -1316.980, z = 28.354 },
    Size  = { x = 1.3, y = 1.3, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },

  Cloakrooms = {
    Pos   = { x = 87.640, y = -1295.095, z = 28.300},
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },
}
