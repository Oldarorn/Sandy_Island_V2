ESX 			   			 = nil
local CallsList    = {}
PlayersHarvesting  = {}
PlayersHarvesting1 = {}
PlayersCrafting    = {}
PlayersCrafting1   = {}
PlayersSelling     = {}
PlayersSelling1    = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'brasseur', Config.MaxInService)
end

TriggerEvent('esx_society:registerSociety', 'brasseur', 'Brasseur', 'society_brasseur', 'society_brasseur', 'society_brasseur', {type = 'public'})

-- [[ Run ]] --
-- Harvest
  -- 1
local function Harvest(source)

  SetTimeout(4000, function()

    if PlayersHarvesting[source] == true then

      local xPlayer  		 = ESX.GetPlayerFromId(source)
      local orangeQuantity = xPlayer.getInventoryItem('orange2').count

      if orangeQuantity >= 20 then
        TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
      else
        xPlayer.addInventoryItem('orange2', 1)
        Harvest(source)
      end
    end
  end)
end

RegisterServerEvent('sandy_brasseur:startHarvest')
AddEventHandler('sandy_brasseur:startHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('recovery_orange'))
  Harvest(source)
end)

RegisterServerEvent('sandy_brasseur:stopHarvest')
AddEventHandler('sandy_brasseur:stopHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = false
end)
  -- 2
local function Harvest1(source)

  SetTimeout(4000, function()

    if PlayersHarvesting1[source] == true then

      local xPlayer      = ESX.GetPlayerFromId(source)
      local orgeQuantity = xPlayer.getInventoryItem('orge').count

      if orgeQuantity >= 30 then
        TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
      else
        xPlayer.addInventoryItem('orge', 1)
        Harvest1(source)
      end
    end
  end)
end

RegisterServerEvent('sandy_brasseur:startHarvest1')
AddEventHandler('sandy_brasseur:startHarvest1', function()
  local _source = source
  PlayersHarvesting1[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('recovery_orge'))
  Harvest1(source)
end)

RegisterServerEvent('sandy_brasseur:stopHarvest1')
AddEventHandler('sandy_brasseur:stopHarvest1', function()
  local _source = source
  PlayersHarvesting1[_source] = false
end)

-- Craft
  -- 1
local function Craft(source)

  SetTimeout(4000, function()

    if PlayersCrafting[source] == true then

      local xPlayer  	   = ESX.GetPlayerFromId(source)
      local orangeQuantity = xPlayer.getInventoryItem('orange2').count

      if orangeQuantity.limit ~= -1 and orangeQuantity.count >= orangeQuantity.limit then
        TriggerClientEvent('esx:showNotification', source, _U('not_enough_orange'))
      else
        xPlayer.removeInventoryItem('orange2', 1)
        xPlayer.addInventoryItem('morangina', 1)
        Craft(source)
      end
    end
  end)
end

RegisterServerEvent('sandy_brasseur:startCraft')
AddEventHandler('sandy_brasseur:startCraft', function()
  local _source = source
  PlayersCrafting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('assembling_orange'))
  Craft(_source)
end)

RegisterServerEvent('sandy_brasseur:stopCraft')
AddEventHandler('sandy_brasseur:stopCraft', function()
  local _source = source
  PlayersCrafting[_source] = false
end)

  -- 2
local function Craft1(source)

  SetTimeout(4000, function()

    if PlayersCrafting1[source] == true then

      local xPlayer      = ESX.GetPlayerFromId(source)
      local orgeQuantity = xPlayer.getInventoryItem('orge').count

      if orgeQuantity.limit ~= -1 and orgeQuantity.count >= orgeQuantity.limit then
        TriggerClientEvent('esx:showNotification', source, _U('not_enough_orge'))
      else
        xPlayer.removeInventoryItem('orge', 1)
        xPlayer.addInventoryItem('beer', 1)
        Craft1(source)
      end
    end
  end)
end

RegisterServerEvent('sandy_brasseur:startCraf1t')
AddEventHandler('sandy_brasseur:startCraft1', function()
  local _source = source
  PlayersCrafting1[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('assembling_orge'))
  Craft1(_source)
end)

RegisterServerEvent('sandy_brasseur:stopCraft1')
AddEventHandler('sandy_brasseur:stopCraft1', function()
  local _source = source
  PlayersCrafting1[_source] = false
end)

-- Sell
  -- 1
function Sell(source)

  SetTimeout(5000, function()

    math.randomseed(os.time())
    local _source          = source
    local xPlayer          = ESX.GetPlayerFromId(_source)
    local total            = math.random(50)
    local playerMoney      = math.floor(total / 100 * 30)
    local societyMoney     = math.floor(total / 100 * 70)

    if xPlayer.job.grade >= 3 then
      total = total * 2
    end

    TriggerEvent('esx_addonaccount:getSharedAccount', 'society_brasseur', function(account)
      societyAccount = account
    end)

    if societyAccount ~= nil then

      if PlayersSelling[source] == true then

        local beerQuantity = xPlayer.getInventoryItem('beer').count

        if beerQuantity == 0 then
          TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas de Hannakein à vendre !')
        else
          xPlayer.removeInventoryItem('beer', 1)
          xPlayer.addMoney(playerMoney)
          societyAccount.addMoney(societyMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_earned') .. playerMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. societyMoney)
          Sell(source)
        end
      end
    end
  end)
end

RegisterServerEvent('sandy_brasseur:startSell')
AddEventHandler('sandy_brasseur:startSell', function()

  local _source = source
  PlayersSelling[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))
  Sell(_source)
end)

RegisterServerEvent('sandy_brasseur:stopSell')
AddEventHandler('sandy_brasseur:stopSell', function()

  local _source = source
  PlayersSelling[_source] = false
end)

  -- 2
function Sell1(source)

  SetTimeout(5000, function()

    math.randomseed(os.time())
    local _source          = source
    local xPlayer          = ESX.GetPlayerFromId(_source)
    local total            = math.random(50)
    local playerMoney      = math.floor(total / 100 * 30)
    local societyMoney     = math.floor(total / 100 * 70)

    if xPlayer.job.grade >= 3 then
      total = total * 2
    end

    TriggerEvent('esx_addonaccount:getSharedAccount', 'society_brasseur', function(account)
      societyAccount = account
    end)

    if societyAccount ~= nil then

      if PlayersSelling[source] == true then

        local fishStickQuantity = xPlayer.getInventoryItem('morangina').count

        if moranginaQuantity == 0 then
          TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas de Morangina à vendre !')
        else
          xPlayer.removeInventoryItem('morangina', 1)
          xPlayer.addMoney(playerMoney)
          societyAccount.addMoney(societyMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_earned') .. playerMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. societyMoney)
          Sell1(source)
        end
      end
    end
  end)
end

RegisterServerEvent('sandy_brasseur:startSell1')
AddEventHandler('sandy_brasseur:startSell1', function()

  local _source = source
  PlayersSelling1[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))
  Sell1(_source)
end)

RegisterServerEvent('sandy_brasseur:stopSell1')
AddEventHandler('sandy_brasseur:stopSell1', function()

  local _source = source
  PlayersSelling1[_source] = false
end)
-- [[ Frigo ]] --
-- Retrait
RegisterServerEvent('sandy_brasseur:getStockItem')
AddEventHandler('sandy_brasseur:getStockItem', function(itemName, count)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_brasseur', function(inventory)
		local item = inventory.getItem(itemName)
		local sourceItem = xPlayer.getInventoryItem(itemName)

		if count > 0 and item.count >= count then

			if sourceItem.limit ~= -1 and (sourceItem.count + count) > sourceItem.limit then
				TriggerClientEvent('esx:showNotification', xPlayer.source, _U('player_cannot_hold'))
			else
				inventory.removeItem(itemName, count)
				xPlayer.addInventoryItem(itemName, count)
				TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)
			end
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end
	end)
end)

ESX.RegisterServerCallback('sandy_brasseur:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_brasseur', function(inventory)
    cb(inventory.items)
  end)
end)

-- Dépot
RegisterServerEvent('sandy_brasseur:putStockItems')
AddEventHandler('sandy_brasseur:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_brasseur', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= 0 then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)
  end)
end)

ESX.RegisterServerCallback('sandy_brasseur:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })
end)

ESX.RegisterServerCallback('sandy_brasseur:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })
end)

RegisterServerEvent('sandy_brasseur:message')
AddEventHandler('sandy_brasseur:message', function(target, msg)
  TriggerClientEvent('esx:showNotification', target, msg)
end)

function SendNotificationToPlayersInService(message)
  local xPlayers 	= ESX.GetPlayers()
  local countCops = 0

  for k, v in pairs(xPlayers) do
    ESX.TriggerServerCallback('esx_service:isInService', 0, v, function(inService)
      if inService then
        TriggerClientEvent('esx:showNotification', v, message)
        countCops = countCops + 1
      end
    end, 'brasseur')
  end

  if countCops <= 0 then
    return false
  end
  return true
end

ESX.RegisterServerCallback('sandy_brasseur:getCallsList', function(source, cb)
  local _source = source
  cb(CallsList)
end)

RegisterNetEvent('sandy_brasseur:newCall')
AddEventHandler('sandy_brasseur:newCall', function(type, pos, message)
  local _source = source

  if type ~= "custom" then
    if type == "contact" then
      print(contact)
      message = "Une personne souhaite prendre contact"
    elseif type == "Commande" then
      print(Commande)
      message = "Une personne souhaite passer une commande"
    end
  end

  CallsList[_source] = {
    type = type,
    position = pos,
    text = message,
    nbr = 0
  }

  local notif = "Un nouvel appel viens d'arriver : "

  if not SendNotificationToPlayersInService(notif) then
    TriggerClientEvent('sandy_brasseur:noBrasseur', _source)
  else
    SendNotificationToPlayersInService("~y~".. message)
  end
end)

RegisterNetEvent('sandy_brasseur:takeCall')
AddEventHandler('sandy_brasseur:takeCall', function(id)
  CallsList[id].nbr = CallsList[id].nbr + 1
  TriggerClientEvent('sandy_brasseur:callTaked', id)
end)

RegisterNetEvent('sandy_brasseur:endCall')
AddEventHandler('sandy_brasseur:endCall', function(id)
  TriggerClientEvent('sandy_brasseur:endCall', id)
  CallsList[id] = nil
end)

RegisterNetEvent('sandy_brasseur:refuseCall')
AddEventHandler('sandy_brasseur:refuseCall', function(id)
  TriggerClientEvent('sandy_brasseur:refuseCall', id)
  CallsList[id] = nil
end)

ESX.RegisterServerCallback("sandy_brasseur:cancelCall", function(source, cb)
  local _source = source
  if CallsList[_source] ~= nil then
    CallsList[_source] = nil
    SendNotificationToPlayersInService("Appel Annulé")
    cb(true)
  else
    cb(false)
  end
end)
