local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData                = {}
local GUI                       = {}
local HasAlreadyEnteredMarker   = false
local LastZone                  = nil
local CurrentAction             = nil

local CurrentActionMsg          = ''
local CurrentActionData         = {}
local IsDead                    = false
local InService                 = false
local SelectedCall              = nil

ESX                             = nil
GUI.Time                        = 0

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(1)
  end
  Citizen.Wait(5000)
  PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

function OpenVehicleMenu()

  local elements = {
    {label = _U('spawn_veh'), value = 'spawn_vehicle'}
  }

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'brasseur_actions',
    {
      title    = 'Garage véhicules',
      align    = 'top-left',
      elements = elements
    },
    function(data, menu)

      if data.current.value == 'spawn_vehicle' then

        if Config.EnableSocietyOwnedVehicles then

          local elements = {}

          ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

            for i=1, #vehicles, 1 do
              table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
            end

            ESX.UI.Menu.Open(
              'default', GetCurrentResourceName(), 'vehicle_spawner',
              {
                title    = _U('spawn_veh'),
                align    = 'top-left',
                elements = elements
              },
              function(data, menu)

                menu.close()

                local vehicleProps = data.current.value

                ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
                  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                  local playerPed = GetPlayerPed(-1)
                  TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
                end)

                TriggerServerEvent('esx_society:removeVehicleFromGarage', 'brasseur', vehicleProps)
              end,
              function(data, menu)
                menu.close()
              end
            )
          end, 'brasseur')
        else

          menu.close()

          if Config.MaxInService == -1 then

            local playerPed = GetPlayerPed(-1)
            local coords    = Config.Zones.VehicleSpawnPoint.Pos

            ESX.Game.SpawnVehicle(2112052861, coords, 117.494, function(vehicle)
              TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
            end)
          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                local playerPed = GetPlayerPed(-1)
                local coords    = Config.Zones.VehicleSpawnPoint.Pos

                ESX.Game.SpawnVehicle(2112052861, coords, 117.494, function(vehicle)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                end)
              else
                ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)
              end
            end, 'brasseur')
          end
        end
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'vehicle_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}
    end
  )
end

function OpenStockMenu()

  local elements = {
    {label = _U('get_object'), value = 'get_stock'},
    {label = _U('put_object'), value = 'put_stock'}
  }

  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'vault',
    {
      title    = 'Coffre',
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      if data.current.value == 'put_stock' then
        OpenPutStocksMenu()
      elseif data.current.value == 'get_stock' then
        OpenGetStocksMenu()
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_stock'
      CurrentActionMsg  = _U('open_stock')
      CurrentActionData = {}
    end
  )
end

function OpenCloakroomMenu()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = _U('cloakroom'),
      align    = 'top-left',
      elements = {
        {label = _U('citizen_wear'), value = 'citizen_wear'},
        {label = _U('barman_outfit'),   value = 'work_wear'},
      },
    },
    function(data, menu)
      menu.close()

      if data.current.value == 'citizen_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
          TriggerEvent('skinchanger:loadSkin', skin)
          TriggerServerEvent('esx_service:disableService', 'brasseur')
          ESX.ShowNotification("~r~Fin de Service")
          InService = false
        end)
      end

      if data.current.value == 'work_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

          if skin.sex == 0 then
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
          else
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
          end
          ESX.TriggerServerCallback('esx_service:enableService', function(result)
            if result then
              ESX.ShowNotification('~g~Prise de Service')
              InService = true
            end
          end, 'brasseur')
        end)
      end
      CurrentAction     = 'cloackroom_menu'
      CurrentActionMsg  = _U('open_menu')
      CurrentActionData = {}
    end,
    function(data, menu)
      menu.close()
    end
  )
end

function OpenMobileActionsMenu()

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'mobile_actions',
    {
      title    = 'Moraa\'s',
      align    = 'top-left',
      elements = {
        {label = "Appels",      value = 'calls'},
        {label = _U('billing'), value = 'billing'},
        {label = _U('gps'),     value = 'gps'}
      }
    },
    function(data, menu)

      if data.current.value == 'calls' then
        ESX.TriggerServerCallback('sandy_brasseur:getCallsList', function(calls)
          local options = {}

          if calls ~= nil then
            for k, v in pairs(calls) do
              if k == SelectedCall then
                table.insert(options, {label = "~g~"..v.nbr.."x Unité(s) en route - "..v.text, value = k})
              else
                table.insert(options, {label = v.nbr.."x Unité(s) en route - "..v.text, value = k})
              end
            end
          else
            table.insert(options, {label = "Aucun Appel en Attente"})
          end
          ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'brasseur_calls',
            {
              title    = 'Appels',
              align    = 'top-left',
              elements = options
            }, function(data2, menu2)
              local callid = data2.current.value
              local options2 = {
                {label = "Prendre l'appel", value="take_call"},
                {label = "Position de l'appel", value="pos_call"},
                {label = "Appel Résolu", value="end_call"},
                {label = "Refuser l'appel", value="refused_call"}
              }
              ESX.UI.Menu.Open(
                'default', GetCurrentResourceName(), 'brasseur_call',
                {
                  title    = 'Actions',
                  align    = 'top-left',
                  elements = options2
                }, function(data3, menu3)
                  if data3.current.value == "take_call" and SelectedCall ~= data2.current.value then
                    SelectedCall = data2.current.value
                    TriggerServerEvent('sandy_brasseur:takeCall', data2.current.value)
                    local x = calls[data2.current.value].position.x
                    local y = calls[data2.current.value].position.y
                    SetNewWaypoint(x, y)
                  end
                  if data3.current.value == "pos_call" then
                    local x = calls[data2.current.value].position.x
                    local y = calls[data2.current.value].position.y
                    SetNewWaypoint(x, y)
                  end
                  if data3.current.value == "end_call" then
                    TriggerServerEvent('sandy_brasseur:endCall', callid)
                    ESX.ShowNotification("~g~Appel Résolu")
                  end
                  if data3.current.value == "refused_call" then
                    TriggerServerEvent("sandy_brasseur:refuseCall", callid)
                  end
                end
              )
            end
          )
        end)
      elseif data.current.value == 'billing' then

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'billing',
          {
            title = _U('invoice_amount')
          },
          function(data, menu)

            local amount = tonumber(data.value)

            if amount == nil then
              ESX.ShowNotification(_U('amount_invalid'))
            else

              menu.close()
              local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

              if closestPlayer == -1 or closestDistance > 3.0 then
                ESX.ShowNotification(_U('no_players_near'))
              else
                TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_brasseur', 'brasseur', amount)
              end
            end
          end,
          function(data, menu)
            menu.close()
          end
        )
      elseif data.current.value == 'gps' then
        if PlayerData.job ~= nil and PlayerData.job.name == 'brasseur' then
          local blips = {
            {title="Récolte Orge",            x= 2413.522, y = 4991.765, z =45.502 },
            {title="Fabrication Hannakien",   x= 837.185, y = -1938.010, z = 27.776 },
            {title="Revente Hannakien",       x = 141.3200, y = -1277.928, z = 28.109 },
            {title="Récolte Orange",          x = 2326.955, y = 4999.946, z = 42.153 },
            {title="Fabrication Morange",     x = 799.290, y = -2502.901, z = 21.862 },
            {title="Revente Morange",         x = -564.500, y = 302.083, z = 83.152 }
          }

          for _, info in pairs(blips) do
            info.blip = AddBlipForCoord(info.x, info.y, info.z)
            SetBlipSprite(info.blip, 1)
            SetBlipDisplay(info.blip, 4)
            SetBlipScale(info.blip, 0.8)
            SetBlipColour(info.blip, 27)
            SetBlipAsShortRange(info.blip, true)

            BeginTextCommandSetBlipName("STRING")
            AddTextComponentString(info.title)
            EndTextCommandSetBlipName(info.blip)
          end
          ESX.ShowNotification("Position ajouté à votre gps !")
        else
          RemoveBlip(blips)
          ESX.ShowNotification("Position retirer de votre gps !")
        end
      end
    end,
    function(data, menu)
      menu.close()
    end
  )
end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('sandy_brasseur:getStockItems', function(items)
    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = 'Mora\'s Beer Frigo',
        align    = 'top-left',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'get_item',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              TriggerServerEvent('sandy_brasseur:getStockItem', itemName, count)

              Citizen.Wait(1000)
              OpenGetStocksMenu()
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

function OpenPutStocksMenu()

  ESX.TriggerServerCallback('sandy_brasseur:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        align    = 'top-left',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              TriggerServerEvent('sandy_brasseur:putStockItems', itemName, count)

              Citizen.Wait(1000)
              OpenPutStocksMenu()
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

AddEventHandler('sandy_brasseur:hasEnteredMarker', function(zone)

  if zone == 'Cloakrooms' then
    CurrentAction     = 'menu_cloakroom'
    CurrentActionMsg  = _U('open_cloackroom')
    CurrentActionData = {}
  end

  if zone == 'Harvest' then
    CurrentAction     = 'harvest_menu'
    CurrentActionMsg  = _U('start_harvest')
    CurrentActionData = {}
  end

  if zone == 'Harvest1' then
    CurrentAction     = 'harvest_menu1'
    CurrentActionMsg  = _U('start_harvest')
    CurrentActionData = {}
  end

  if zone == 'Craft' then
    CurrentAction     = 'craft_menu'
    CurrentActionMsg  = _U('start_craft')
    CurrentActionData = {}
  end

  if zone == 'Craft1' then
    CurrentAction     = 'craft_menu1'
    CurrentActionMsg  = _U('start_craft')
    CurrentActionData = {}
  end

  if zone == 'Resell' then
    CurrentAction     = 'resell'
    CurrentActionMsg  = _U('start_sell')
    CurrentActionData = {}
  end

  if zone == 'Resell1' then
    CurrentAction     = 'sell1'
    CurrentActionMsg  = _U('start_sell')
    CurrentActionData = {}
  end

  --if InService then
    if zone == 'SocietyActions' then
      CurrentAction     = 'actions_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}
    end

    if zone == 'BossActions' then
      CurrentAction     = 'menu_boss_actions'
      CurrentActionMsg  = _U('open_bossmenu')
      CurrentActionData = {}
    end

    if zone == 'Stocks' then
      CurrentAction     = 'menu_stock'
      CurrentActionMsg  = _U('open_stock')
      CurrentActionData = {}
    end

    if zone == 'VehicleDeleter' then

      local playerPed = GetPlayerPed(-1)
      local vehicle = GetVehiclePedIsIn(playerPed, false)

      if IsPedInAnyVehicle(playerPed,  false) then
        CurrentAction     = 'delete_vehicle'
        CurrentActionMsg  = _U('store_veh')
        CurrentActionData = { vehicle = vehicle }
      end
    end
  --end
end)

AddEventHandler('sandy_brasseur:hasExitedMarker', function(zone)
  ESX.UI.Menu.CloseAll()
  CurrentAction = nil

  TriggerServerEvent('sandy_brasseur:stopHarvest')
  TriggerServerEvent('sandy_brasseur:stopHarvest1')
  TriggerServerEvent('sandy_brasseur:stopCraft')
  TriggerServerEvent('sandy_brasseur:stopCraft1')
  TriggerServerEvent('sandy_brasseur:stopSelling')
  TriggerServerEvent('sandy_brasseur:stopSelling1')
end)

-- Create Blips
Citizen.CreateThread(function()

  local blip = AddBlipForCoord(Config.Zones.SocietyActions.Pos.x, Config.Zones.SocietyActions.Pos.y, Config.Zones.SocietyActions.Pos.z)

  SetBlipSprite (blip, 198)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 1.0)
  SetBlipColour (blip, 5)
  SetBlipAsShortRange(blip, true)

  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString("Moraa's Company")
  EndTextCommandSetBlipName(blip)
end)

-- Display markers
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if PlayerData.job ~= nil and PlayerData.job.name == 'brasseur' then

      local coords = GetEntityCoords(GetPlayerPed(-1))

      for k,v in pairs(Config.Zones) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end
    end
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if PlayerData.job ~= nil and PlayerData.job.name == 'brasseur' then

      local coords      = GetEntityCoords(GetPlayerPed(-1))
      local isInMarker  = false
      local currentZone = nil

      for k,v in pairs(Config.Zones) do
        if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
          isInMarker  = true
          currentZone = k
        end
      end

      if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
        HasAlreadyEnteredMarker = true
        LastZone                = currentZone
        TriggerEvent('sandy_brasseur:hasEnteredMarker', currentZone)
      end

      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('sandy_brasseur:hasExitedMarker', LastZone)
      end
    end
  end
end)

-- Key Controls
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlPressed(0, Keys['E']) and PlayerData.job ~= nil and PlayerData.job.name == 'brasseur' and (GetGameTimer() - GUI.Time) > 300 then

        if CurrentAction == 'menu_cloakroom' then
          OpenCloakroomMenu()
        end

        --if InService then

          if CurrentAction == 'actions_menu' then
            OpenVehicleMenu()
          elseif CurrentAction == 'craft_menu' then
            TriggerServerEvent('sandy_brasseur:startCraft')
          elseif CurrentAction == 'resell' then
            TriggerServerEvent('sandy_brasseur:startSell')
          elseif CurrentAction == 'harvest_menu' then
            TriggerServerEvent('sandy_brasseur:startHarvest')
          elseif CurrentAction == 'craft_menu1' then
            TriggerServerEvent('sandy_brasseur:startCraft1')
          elseif CurrentAction == 'resell1' then
            TriggerServerEvent('sandy_brasseur:startSell1')
          elseif CurrentAction == 'harvest_menu1' then
            TriggerServerEvent('sandy_brasseur:startHarvest1')
          elseif CurrentAction == 'menu_stock' then
            OpenStockMenu()
          elseif CurrentAction == 'menu_boss_actions' then

            ESX.UI.Menu.CloseAll()

            TriggerEvent('esx_society:openBossMenu', 'brasseur', function(data, menu)

              menu.close()
              CurrentAction     = 'menu_boss_actions'
              CurrentActionMsg  = _U('open_bossmenu')
              CurrentActionData = {}
            end)
          elseif CurrentAction == 'delete_vehicle' then
            if Config.EnableSocietyOwnedVehicles then
              local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
              TriggerServerEvent('esx_society:putVehicleInGarage', 'brasseur', vehicleProps)
            end
            ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
          end
        --end and InService
        CurrentAction = nil
        GUI.Time      = GetGameTimer()
      end
    end

    if IsControlPressed(0, Keys['F6']) and not IsDead and PlayerData.job ~= nil and PlayerData.job.name == 'brasseur' and (GetGameTimer() - GUI.Time) > 150 then
      OpenMobileActionsMenu()
      GUI.Time = GetGameTimer()
    end
  end
end)

AddEventHandler('esx:onPlayerDeath', function()
  IsDead = true
end)

AddEventHandler('playerSpawned', function(spawn)
  IsDead = false
end)

RegisterNetEvent('sandy_brasseur:callBrasseur')
AddEventHandler('sandy_brasseur:callBrasseur', function(datas)
  local pos = GetEntityCoords(GetPlayerPed(-1), false)

  TriggerServerEvent('sandy_brasseur:newCall', datas.type, {x=pos.x, y=pos.y})

  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)

RegisterNetEvent('sandy_brasseur:noBrasseur')
AddEventHandler('sandy_brasseur:noBrasseur', function()
  ESX.ShowNotification("~r~Aucun employé n'est en service actuellement")
end)

RegisterNetEvent('sandy_brasseur:callTaked')
AddEventHandler('sandy_brasseur:callTaked', function()
  ESX.ShowNotification("~g~Votre appel viens d'être accepter, un employé est en route")
end)

RegisterNetEvent('sandy_brasseur:endTaked')
AddEventHandler('sandy_brasseur:endTaked', function()
  ESX.ShowNotification("~g~Appel terminer")
end)

RegisterNetEvent('sandy_brasseur:refuseTaked')
AddEventHandler('sandy_brasseur:refuseTaked', function()
  ESX.ShowNotification("~r~Votre Appel à été refusé")
end)

RegisterNetEvent('sandy_brasseur:cancelCall')
AddEventHandler('sandy_brasseur:cancelCall', function()
  ESX.TriggerServerCallback("sandy_brasseur:cancelCall", function(result)
    if result then
      ESX.ShowNotification("~g~Appel Annulé")
    end
  end)
end)

RegisterNetEvent('sandy_brasseur:callBrasseurCustom')
AddEventHandler('sandy_brasseur:callBrasseurCustom', function()
  local text = ""
  DisplayOnscreenKeyboard(1, "FMMC_MPM_NA", "", text, "", "", "", 100)
  while (UpdateOnscreenKeyboard() == 0) do
    DisableAllControlActions(0);
    Citizen.Wait(1);
  end
  if (GetOnscreenKeyboardResult()) then
    text = GetOnscreenKeyboardResult()
  end
  local pos = GetEntityCoords(GetPlayerPed(-1), false)
  TriggerServerEvent('sandy_brasseur:newCall', "custom", {x = pos.x, y = pos.y}, text)
  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)
