SET @job_name = 'brasseur';
SET @society_name = 'society_brasseur';
SET @job_Name_Caps = 'Moras Beer';

INSERT INTO `addon_account` (name, label, shared) VALUES
  (@society_name, @job_Name_Caps, 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  ('society_brasseur', 'Moras (frigo)', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
    (@society_name, @job_Name_Caps, 1)
;

INSERT INTO `jobs` (name, label, whitelisted) VALUES
  (@job_name, @job_Name_Caps, 1)
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  (@job_name, 0, 'interim', 'Intérimaire', 1000, '{"tshirt_2":0,"shoes_1":25,"shoes_2":0,"torso_2":0,"pants_1":7,"sex":0,"decals_2":0,"tshirt_1":57,
"arms":66,"decals_1":0,"torso_1":41,"pants_2":0}', '{"tshirt_1":36,"torso_1":9,"arms":72,"pants_1":3,"helmet_2":4,"shoes_1":25,"shoes_2":0,"torso_2":7,
"sex":1,"pants_2":0,"tshirt_2":0}'),
  (@job_name, 1, 'employe', 'Employé', 1300, '{"tshirt_2":0,"shoes_1":25,"shoes_2":0,"torso_2":0,"pants_1":7,"sex":0,"decals_2":0,"tshirt_1":57,
"arms":66,"decals_1":0,"torso_1":41,"pants_2":0}', '{"tshirt_1":36,"torso_1":9,"arms":72,"pants_1":3,"helmet_2":4,"shoes_1":25,"shoes_2":0,"torso_2":7,
"sex":1,"pants_2":0,"tshirt_2":0}'),
  (@job_name, 2, 'seller', 'Vendeur', 1500, '{"tshirt_2":0,"shoes_1":25,"shoes_2":0,"torso_2":0,"pants_1":7,"sex":0,"decals_2":0,"tshirt_1":57,
"arms":66,"decals_1":0,"torso_1":41,"pants_2":0}', '{"tshirt_1":36,"torso_1":9,"arms":72,"pants_1":3,"helmet_2":4,"shoes_1":25,"shoes_2":0,"torso_2":7,
"sex":1,"pants_2":0,"tshirt_2":0}'),
  (@job_name, 3, 'shareholder', 'Actionnaire', 1800, '{"tshirt_2":0,"shoes_1":25,"shoes_2":0,"torso_2":0,"pants_1":7,"sex":0,"decals_2":0,"tshirt_1":57,
"arms":66,"decals_1":0,"torso_1":41,"pants_2":0}', '{"tshirt_1":36,"torso_1":9,"arms":72,"pants_1":3,"helmet_2":4,"shoes_1":25,"shoes_2":0,"torso_2":7,
"sex":1,"pants_2":0,"tshirt_2":0}'),
  (@job_name, 4, 'boss', 'Gérant', 2000, '{"tshirt_2":0,"shoes_1":25,"shoes_2":0,"torso_2":0,"pants_1":7,"sex":0,"decals_2":0,"tshirt_1":57,
"arms":66,"decals_1":0,"torso_1":41,"pants_2":0}', '{"tshirt_1":36,"torso_1":9,"arms":72,"pants_1":3,"helmet_2":4,"shoes_1":25,"shoes_2":0,"torso_2":7,
"sex":1,"pants_2":0,"tshirt_2":0}')
;

INSERT INTO `items` (`name`, `label`) VALUES
	('orge', 'Orge'),
	('beer', 'Hannakein'),
  ('orange', 'Orange'),
  ('morange', 'Morange')
;

