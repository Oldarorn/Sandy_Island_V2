local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData                = {}
local GUI                       = {}
local HasAlreadyEnteredMarker   = false
local LastZone                  = nil
local CurrentAction             = nil

local CurrentActionMsg          = ''
local CurrentActionData         = {}
local IsDead                    = false
local InService                 = false
local SelectedCall              = nil

ESX                             = nil
GUI.Time                        = 0

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end
	Citizen.Wait(5000)
	PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

function OpenVehicleMenu()

  local elements = {
    {label = _U('spawn_veh'), value = 'spawn_vehicle'}
  }

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'faimdus_actions',
    {
      title    = 'Garage véhicules',
      align    = 'top-left',
      elements = elements
    },
    function(data, menu)

      if data.current.value == 'spawn_vehicle' then

        if Config.EnableSocietyOwnedVehicles then

          local elements = {}

          ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

            for i=1, #vehicles, 1 do
              table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
            end

            ESX.UI.Menu.Open(
              'default', GetCurrentResourceName(), 'vehicle_spawner',
              {
                title    = _U('spawn_veh'),
                align    = 'top-left',
                elements = elements
              },
              function(data, menu)

                menu.close()

                local vehicleProps = data.current.value

                ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
                  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                  local playerPed = GetPlayerPed(-1)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
                end)

                TriggerServerEvent('esx_society:removeVehicleFromGarage', 'faimdus', vehicleProps)
              end,
              function(data, menu)
                menu.close()
              end
            )
          end, 'faimdus')
        else

          menu.close()

          if Config.MaxInService == -1 then

            local playerPed = GetPlayerPed(-1)
            local coords    = Config.Zones.VehicleSpawnPoint.Pos

            while not HasModelLoaded(-877478386) do
              Citizen.Wait(0)
            end

            ESX.Game.SpawnVehicle(-2100640717, coords, 225.0, function(vehicle)
              TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
              local trailer = CreateVehicle(-877478386, coords.x, coords.y, coords.z, spawnPoint.Heading, true, false)
              AttachVehicleToTrailer(veh, trailer, 1.1)
            end)
          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                local playerPed = GetPlayerPed(-1)
                local coords    = Config.Zones.VehicleSpawnPoint.Pos

                ESX.Game.SpawnVehicle(-2100640717, coords, 225.0, function(vehicle)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                  local trailer = CreateVehicle(-877478386, coords.x, coords.y, coords.z, spawnPoint.Heading, true, false)
                  AttachVehicleToTrailer(veh, trailer, 1.1)
                end)
              else
                ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)
              end
            end, 'faimdus')
          end
        end
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'vehicle_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}
    end
  )
end

function OpenBoatVehicleMenu()

  local elements = {
    {label = "Garage bateaux", value = 'spawn_vehicle'}
  }

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'faimdus_actions',
    {
      title    = 'Garage bateaux',
      align    = 'top-left',
      elements = elements
    },
    function(data, menu)

      if data.current.value == 'spawn_vehicle' then

        if Config.EnableSocietyOwnedVehicles then

          local elements = {}

          ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

            for i=1, #vehicles, 1 do
              table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
            end

            ESX.UI.Menu.Open(
              'default', GetCurrentResourceName(), 'vehicle_spawner',
              {
                title    = _U('spawn_veh'),
                align    = 'top-left',
                elements = elements
              },
              function(data, menu)

                menu.close()

                local vehicleProps = data.current.value

                ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
                  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                  local playerPed = GetPlayerPed(-1)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
                end)

                TriggerServerEvent('esx_society:removeVehicleFromGarage', 'faimdus2', vehicleProps)
              end,
              function(data, menu)
                menu.close()
              end
            )
          end, 'faimdus2')
        else

          menu.close()

          if Config.MaxInService == -1 then

            local playerPed = GetPlayerPed(-1)
            local coords    = Config.Zones.VehicleSpawnPoint.Pos

            ESX.Game.SpawnVehicle(-2100640717, coords, 29.194, function(vehicle)
              TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
            end)
          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                local playerPed = GetPlayerPed(-1)
                local coords    = Config.Zones.VehicleSpawnPoint.Pos

                ESX.Game.SpawnVehicle(-2100640717, coords, 29.194, function(vehicle)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                end)
              else
                ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)
              end
            end, 'faimdus')
          end
        end
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'boat_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}
    end
  )
end

function OpenStockMenu()

  local elements = {
    {label = _U('get_object'), value = 'get_stock'},
    {label = _U('put_object'), value = 'put_stock'}
  }

  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'vault',
    {
      title    = 'Coffre',
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      if data.current.value == 'put_stock' then
        OpenPutStocksMenu()
      elseif data.current.value == 'get_stock' then
        OpenGetStocksMenu()
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_stock'
      CurrentActionMsg  = _U('open_stock')
      CurrentActionData = {}
    end
  )
end

function OpenCloakroomMenu()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = _U('cloakroom'),
      align    = 'top-left',
      elements = {
        {label = _U('citizen_wear'), value = 'citizen_wear'},
        {label = _U('barman_outfit'),   value = 'work_wear'},
      },
    },
    function(data, menu)
      menu.close()

      if data.current.value == 'citizen_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
          TriggerEvent('skinchanger:loadSkin', skin)
          TriggerServerEvent('esx_service:disableService', 'faimdus')
          ESX.ShowNotification("~r~Fin de Service")
          InService = false
        end)
      end

      if data.current.value == 'work_wear' then

        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

          if skin.sex == 0 then
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
          else
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
          end
          ESX.TriggerServerCallback('esx_service:enableService', function(result)
  		      if result then
  		        ESX.ShowNotification('~g~Prise de Service')
  		        InService = true
  		      end
  		    end, 'faimdus')
        end)
      end
      CurrentAction     = 'cloackroom_menu'
      CurrentActionMsg  = _U('open_menu')
      CurrentActionData = {}
    end,
    function(data, menu)
      menu.close()
    end
  )
end

function OpenMobileActionsMenu()

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'mobile_taxi_actions',
    {
      title    = 'Taxi',
      align    = 'top-left',
      elements = {
        {label = "Appels", 			value = 'calls'},
        {label = _U('billing'), value = 'billing'},
        {label = _U('gps'),     value = 'gps'}
      }
    },
    function(data, menu)

      if data.current.value == 'calls' then
        ESX.TriggerServerCallback('sandy_faimdus:getCallsList', function(calls)
          local options = {}

          if calls ~= nil then
            for k, v in pairs(calls) do
              table.insert(options, {label = v.text, value = k})
            end
          else
            table.insert(options, {label = "Aucun Appel en Attente"})
          end
            ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'faimdus_calls',
            {
              title    = 'Faim Dus - Appels',
              align    = 'top-left',
              elements = options
            }, function(data2, menu2)

              local callid = data2.current.value
              local options2 = {
                {label = "Prendre l'appel", 		value="take_call"},
                {label = "Position de l'appel", value="pos_call"},
                {label = "Appel Résolu", 				value="end_call"},
                {label = "Refuser l'appel", 		value="refused_call"}
              }

              ESX.UI.Menu.Open(
	              'default', GetCurrentResourceName(), 'taxi_call',
	              {
	                title    = 'Actions',
	                align    = 'top-left',
	                elements = options2
	              }, function(data3, menu3)

		              if data3.current.value == "take_call" and SelectedCall ~= data2.current.value then

		                SelectedCall = data2.current.value
		                TriggerServerEvent('sandy_faimdus:takeCall', data2.current.value)
		                local x = calls[data2.current.value].position.x
		                local y = calls[data2.current.value].position.y
		                SetNewWaypoint(x, y)
		              elseif data3.current.value == "pos_call" then

		                local x = calls[data2.current.value].position.x
		                local y = calls[data2.current.value].position.y
		                SetNewWaypoint(x, y)
		              elseif data3.current.value == "end_call" then

		                TriggerServerEvent('sandy_faimdus:endCall', callid)
		                ESX.ShowNotification("~g~Appel Résolu")
		              elseif data3.current.value == "refused_call" then

		                TriggerServerEvent("sandy_faimdus:refuseCall", callid)
		              end
		            end,
		            function(data3, menu3)
		              menu3.close()
		            end
	          	)
	          end,
	          function(data2, menu2)
	            menu2.close()
	          end
	        )
        end)
      elseif data.current.value == 'billing' then

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'billing',
          {
            title = _U('invoice_amount')
          },
          function(data, menu)

            local amount = tonumber(data.value)

            if amount == nil then
              ESX.ShowNotification(_U('amount_invalid'))
            else

              menu.close()
              local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

              if closestPlayer == -1 or closestDistance > 3.0 then
                ESX.ShowNotification(_U('no_players_near'))
              else
                TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_faimdus', 'faimdus', amount)
              end
            end
          end,
          function(data, menu)
            menu.close()
          end
        )
      elseif data.current.value == 'gps' then

        local blips = {
          {title="Pêche",       x=-1999.110, y=5186.249,  z=1.100},
          {title="Fabrication", x=874.353,   y=-1682.482, z=29.650},
          {title="Vente",       x=-380.250,  y=278.592,   z=83.896}
        }

        for _, info in pairs(blips) do
          info.blip = AddBlipForCoord(info.x, info.y, info.z)
          SetBlipSprite(info.blip, 478)
          SetBlipDisplay(info.blip, 4)
          SetBlipScale(info.blip, 0.8)
          SetBlipColour(info.blip, 28)
          SetBlipAsShortRange(info.blip, true)

          BeginTextCommandSetBlipName("STRING")
          AddTextComponentString(info.title)
          EndTextCommandSetBlipName(info.blip)
        end
        ESX.ShowNotification("Position ajouté à votre gps !")
      end
    end,
    function(data, menu)
      menu.close()
    end
  )
end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('sandy_faimdus:getStockItems', function(items)
    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = 'Faim Dus Frigo',
        align    = 'top-left',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'get_item',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              TriggerServerEvent('sandy_faimdus:getStockItem', itemName, count)

              Citizen.Wait(1000)
              OpenGetStocksMenu()
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

function OpenPutStocksMenu()

  ESX.TriggerServerCallback('sandy_faimdus:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        align    = 'top-left',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              TriggerServerEvent('sandy_faimdus:putStockItems', itemName, count)

              Citizen.Wait(1000)
              OpenPutStocksMenu()
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

AddEventHandler('sandy_faimdus:hasEnteredMarker', function(zone)

  if zone == 'Cloakrooms' then
    CurrentAction     = 'menu_cloakroom'
    CurrentActionMsg  = _U('open_cloackroom')
    CurrentActionData = {}
  end

  if InService then
    if zone == 'Harvest' then
      CurrentAction     = 'harvest_menu'
      CurrentActionMsg  = _U('press_collect')
      CurrentActionData = {}
    end

    if zone == 'Craft' then
      CurrentAction     = 'craft_menu'
      CurrentActionMsg  = _U('press_craft')
      CurrentActionData = {}
    end

    if zone == 'Resell' then
      CurrentAction     = 'resell'
      CurrentActionMsg  = _U('press_resell')
      CurrentActionData = {}
    end

    if zone == 'SocietyActions' then
      CurrentAction     = 'actions_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}
    end

    if zone == 'BossActions' then
      CurrentAction     = 'menu_boss_actions'
      CurrentActionMsg  = _U('open_bossmenu')
      CurrentActionData = {}
    end

    if zone == 'Stocks' then
      CurrentAction     = 'menu_stock'
      CurrentActionMsg  = _U('open_stock')
      CurrentActionData = {}
    end

    if zone == 'VehicleDeleter' or zone == 'BateauDeleter' then

      local playerPed = GetPlayerPed(-1)
      local vehicle = GetVehiclePedIsIn(playerPed, false)

      if IsPedInAnyVehicle(playerPed,  false) then
        CurrentAction     = 'delete_vehicle'
        CurrentActionMsg  = _U('store_veh')
        CurrentActionData = { vehicle = vehicle }
      end
    end

    if zone == 'BateauActions' then
      CurrentAction     = 'boat_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}
    end
  end
end)

AddEventHandler('sandy_faimdus:hasExitedMarker', function(zone)
  ESX.UI.Menu.CloseAll()
  CurrentAction = nil

  TriggerServerEvent('sandy_faimdus:stopHarvest')
  TriggerServerEvent('sandy_faimdus:stopCraft')
  TriggerServerEvent('sandy_faimdus:stopSell')
end)

-- Create Blips
Citizen.CreateThread(function()

  local blip = AddBlipForCoord(Config.Zones.SocietyActions.Pos.x, Config.Zones.SocietyActions.Pos.y, Config.Zones.SocietyActions.Pos.z)

  SetBlipSprite (blip, 67)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 1.0)
  SetBlipColour (blip, 5)
  SetBlipAsShortRange(blip, true)

  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString("Faim Dus Company")
  EndTextCommandSetBlipName(blip)
end)

-- Display markers
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if PlayerData.job ~= nil and PlayerData.job.name == 'faimdus' then

      local coords = GetEntityCoords(GetPlayerPed(-1))

      for k,v in pairs(Config.Zones) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end
    end
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if PlayerData.job ~= nil and PlayerData.job.name == 'faimdus' then

      local coords      = GetEntityCoords(GetPlayerPed(-1))
      local isInMarker  = false
      local currentZone = nil

      for k,v in pairs(Config.Zones) do
        if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
          isInMarker  = true
          currentZone = k
        end
      end

      if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
        HasAlreadyEnteredMarker = true
        LastZone                = currentZone
        TriggerEvent('sandy_faimdus:hasEnteredMarker', currentZone)
      end

      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('sandy_faimdus:hasExitedMarker', LastZone)
      end
    end
  end
end)

-- Key Controls
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlPressed(0, Keys['E']) and PlayerData.job ~= nil and PlayerData.job.name == 'faimdus' and (GetGameTimer() - GUI.Time) > 300 then

        if CurrentAction == 'menu_cloakroom' then
          OpenCloakroomMenu()
        end

        if InService then

          if CurrentAction == 'actions_menu' then
            OpenVehicleMenu()
          elseif CurrentAction == 'craft_menu' then
            TriggerServerEvent('sandy_faimdus:startCraft')
          elseif CurrentAction == 'resell' then
            TriggerServerEvent('sandy_faimdus:startSell')
          elseif CurrentAction == 'harvest_menu' then
            TriggerServerEvent('sandy_faimdus:startHarvest')
          elseif CurrentAction == 'menu_stock' then
            OpenStockMenu()
          elseif CurrentAction == 'boat_menu' then
            OpenBoatVehicleMenu()
          elseif CurrentAction == 'menu_boss_actions' then

            ESX.UI.Menu.CloseAll()

            TriggerEvent('esx_society:openBossMenu', 'faimdus', function(data, menu)

              menu.close()
              CurrentAction     = 'menu_boss_actions'
              CurrentActionMsg  = _U('open_bossmenu')
              CurrentActionData = {}
            end)
          elseif CurrentAction == 'delete_vehicle' then
	          if Config.EnableSocietyOwnedVehicles then
	            local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
	            TriggerServerEvent('esx_society:putVehicleInGarage', 'faimdus', vehicleProps)
	          end
	          ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
	        end
        end
        CurrentAction = nil
        GUI.Time      = GetGameTimer()
      end
    end

    if IsControlPressed(0, Keys['F6']) and InService and not IsDead and PlayerData.job ~= nil and PlayerData.job.name == 'faimdus' and (GetGameTimer() - GUI.Time) > 150 then
      OpenMobileActionsMenu()
      GUI.Time = GetGameTimer()
    end
  end
end)

AddEventHandler('esx:onPlayerDeath', function()
	IsDead = true
end)

AddEventHandler('playerSpawned', function(spawn)
	IsDead = false
end)

RegisterNetEvent('sandy_faimdus:callFaimdus')
AddEventHandler('sandy_faimdus:callFaimdus', function(datas)
  local pos = GetEntityCoords(GetPlayerPed(-1), false)

  TriggerServerEvent('sandy_faimdus:newCall', datas.type, {x=pos.x, y=pos.y})

  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)

RegisterNetEvent('sandy_faimdus:noFaimdus')
AddEventHandler('sandy_faimdus:noFaimdus', function()
  ESX.ShowNotification("~r~Aucun employer n'est en service actuellement")
end)

RegisterNetEvent('sandy_faimdus:callTaked')
AddEventHandler('sandy_faimdus:callTaked', function()
  ESX.ShowNotification("~g~Votre appel viens d'être accepter, un employer est en route")
end)

RegisterNetEvent('sandy_faimdus:endTaked')
AddEventHandler('sandy_faimdus:endTaked', function()
  ESX.ShowNotification("~g~Appel terminer")
end)

RegisterNetEvent('sandy_faimdus:refuseTaked')
AddEventHandler('sandy_faimdus:refuseTaked', function()
  ESX.ShowNotification("~r~Votre Appel à été refusé")
end)

RegisterNetEvent('sandy_faimdus:cancelCall')
AddEventHandler('sandy_faimdus:cancelCall', function()
  ESX.TriggerServerCallback("sandy_faimdus:cancelCall", function(result)
    if result then
      ESX.ShowNotification("~g~Appel Annulé")
    end
  end)
end)

RegisterNetEvent('sandy_faimdus:callFaimdusCustom')
AddEventHandler('sandy_faimdus:callFaimdusCustom', function()
  local text = ""
  DisplayOnscreenKeyboard(1, "FMMC_MPM_NA", "", text, "", "", "", 100)
  while (UpdateOnscreenKeyboard() == 0) do
    DisableAllControlActions(0);
    Citizen.Wait(1);
  end
  if (GetOnscreenKeyboardResult()) then
    text = GetOnscreenKeyboardResult()
  end
  local pos = GetEntityCoords(GetPlayerPed(-1), false)
  TriggerServerEvent('sandy_faimdus:newCall', "custom", {x = pos.x, y = pos.y}, text)
  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)
