Config                            = {}
Config.DrawDistance               = 100.0
Config.MaxInService               = -1

Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = false
Config.MaxInService 			        = 200
Config.Locale                     = 'fr'

Config.Zones = {

	SocietyActions = {
		Pos     = {x = -1039.802, y = -1352.886, z = 4.700},
		Size    = {x = 1.5, y = 1.5, z = 1.0},
		Color   = {r = 30, g = 144, b = 255 },
		Type    = 27,
	},

	VehicleSpawnPoint = {
		Pos  = {x = -1040.215, y = -1329.037, z = 4.447},
		Size = {x = 1.5, y = 1.5, z = 1.0},
		Type = -1,
	},

	VehicleDeleter = {
		Pos   = {x = -1040.215, y = -1329.037, z = 4.500},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = { r = 30, g = 144, b = 255 },
		Type  = 27,
	},

	Harvest = {
		Pos   = { x = -1999.110, y = 5186.249, z = 1.100 },
		Size  = { x = 12.0, y = 12.0, z = 1.0 },
		Color = { r = 30, g = 144, b = 255 },
		Type  = 27,
	},

	Craft = {
		Pos   = { x = 874.353, y = -1682.482, z = 29.650 },
		Size  = { x = 2.5, y = 2.5, z = 1.0 },
		Color = { r = 30, g = 144, b = 255 },
		Type  = 27,
	},

	Resell = {
		Pos   = { x = -380.250, y = 278.592, z = 83.896 },
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Color = { r = 30, g = 144, b = 255 },
		Type  = 27,
	},

  BossActions = {
    Pos   = { x = -1038.475, y = -1395.816, z = 4.553 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },

  Stocks = {
    Pos   = { x =-1056.204, y = -1373.713, z = 4.553 },
    Size  = { x = 1.3, y = 1.3, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },

  Cloakrooms = {
    Pos   = { x = -1039.080, y = -1398.147, z = 4.553},
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 30, g = 144, b = 255 },
    Type  = 27,
  },

  -- Bateau
  BateauActions = {
		Pos     = {x = -1605.694, y = 5259.289, z = 1.089},
		Size    = {x = 1.5, y = 1.5, z = 1.0},
		Color   = {r = 30, g = 144, b = 255 },
		Type    = 27,
	},

	BateauSpawnPoint = {
		Pos  = {x = -1599.997, y = 5263.766, z = 0.962},
		Size = {x = 1.5, y = 1.5, z = 1.0},
		Type = -1,
	},

	BateauDeleter = {
		Pos   = {x = -1599.997, y = 5263.766, z = 0.962},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = { r = 30, g = 144, b = 255 },
		Type  = 27,
	},
}
