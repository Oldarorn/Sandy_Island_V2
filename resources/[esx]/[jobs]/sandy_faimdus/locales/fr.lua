Locales['fr'] = {
    -- Cloakroom
    ['cloakroom']                = 'Vestiaire',
    ['citizen_wear']             = 'Tenue civile',
    ['barman_outfit']            = 'Tenue de travail',
    ['open_cloackroom']          = 'Appuyez sur ~INPUT_CONTEXT~ pour vous changer',

    -- Fridge
    ['get_object']               = 'Prendre Objet',
    ['put_object']               = 'Déposer Objet',
    ['fridge']                   = 'Frigo',
    ['open_stock']              = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au frigo',
    ['theater_fridge_stock']     = 'Contenu du Frigo',
    ['fridge_inventory']         = 'Contenu du frigo',
    ['have_withdrawn']           = 'Vous avez retiré ',
    ['added']                    = 'Vous avez ajouté ',
    ['player_cannot_hold']       = 'Vous n\'avez ~r~pas~w~ assez ~y~d\'espace libre~w~ dans votre inventaire!',

    -- Shops
    ['shop']                     = 'Boutique',
    ['shop_menu']                = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder à la boutique.',
    ['bought']                   = 'Vous avez acheté ~b~1x ',
    ['not_enough_money']         = 'Vous n\'avez ~r~pas assez~s~ d\'argent.',
    ['max_item']                 = 'Vous en portez déjà assez sur vous.',

    -- Vehicles
    ['spawn_veh']                = 'Sortir un véhicule',
    ['press_to_open']            = 'Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le garage',
    ['vehicle_menu']             = 'Véhicule',
    ['vehicle_out']              = 'Il y a déja un véhicule dehors',
    ['vehicle_spawner']          = 'Appuyez sur ~INPUT_CONTEXT~ pour sortir un véhicule',
    ['store_veh']                = 'Appuyez sur ~INPUT_CONTEXT~ pour ranger le véhicule',
    ['service_max']              = 'Service complet : ',
    ['spawn_point_busy']         = 'Un véhicule occupe le point de sortie',

    -- Boss Menu
    ['take_company_money']       = 'Retirer argent société',
    ['deposit_money']            = 'Déposer argent',
    ['amount_of_withdrawal']     = 'Montant du retrait',
    ['invalid_amount']           = 'Montant invalide',
    ['amount_of_deposit']        = 'Montant du dépôt',
    ['open_bossmenu']            = 'Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le menu',
    ['invalid_quantity']         = 'Quantité invalide',
    ['you_removed']              = 'Vous avez retiré x',
    ['you_added']                = 'Vous avez ajouté x',
    ['quantity']                 = 'Quantité',
    ['inventory']                = 'Inventaire',
    ['theater_stock']            = 'Stock du Théatre',
    ['boss_actions']             = 'Action Patron',

    -- Billing Menu
    ['billing']                  = 'Facture',
    ['gps']                      = 'Itinéraire fournisseur',
    ['no_players_nearby']        = 'Aucun joueur à proximité',
    ['invoice_amount']           = 'Montant de la facture',
    ['amount_invalid']           = 'Montant invalide',

    -- Farm
    ['press_craft']              = 'Appuyez sur ~INPUT_CONTEXT~ pour fabriquer',
    ['press_harvest']            = 'Appuyez sur ~INPUT_CONTEXT~ pour récolter',
    ['press_sell']               = 'Appuyez sur ~INPUT_CONTEXT~ pour vendre',
    ['sale_in_prog']             = '~g~Vente en cours~s~...',
    ['have_earned']              = 'vous avez gagné ~g~$',
    ['comp_earned']              = 'votre société a gagné ~g~$',
}
