ESX 			   			 = nil
local CallsList    = {}
PlayersHarvesting  = {}
PlayersCrafting    = {}
PlayersSelling     = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'faimdus', Config.MaxInService)
end

TriggerEvent('esx_society:registerSociety', 'faimdus', 'Faimdus', 'society_faimdus', 'society_faimdus', 'society_faimdus', {type = 'public'})

-- [[ Run ]] --
-- Harvest
local function Harvest(source)

  SetTimeout(4000, function()

    if PlayersHarvesting[source] == true then

      local xPlayer  		 = ESX.GetPlayerFromId(source)
      local fishQuantity = xPlayer.getInventoryItem('fish').count

      if fishQuantity >= 50 then
        TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
      else
        xPlayer.addInventoryItem('fish', 1)
        Harvest(source)
      end
    end
  end)
end

RegisterServerEvent('sandy_faimdus:startHarvest')
AddEventHandler('sandy_faimdus:startHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('recovery_fish'))
  Harvest(source)
end)

RegisterServerEvent('sandy_faimdus:stopHarvest')
AddEventHandler('sandy_faimdus:stopHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = false
end)

-- Craft
local function Craft(source)

  SetTimeout(4000, function()

    if PlayersCrafting[source] == true then

      local xPlayer  	   = ESX.GetPlayerFromId(source)
      local fishQuantity = xPlayer.getInventoryItem('fish').count

      if fishQuantity.limit ~= -1 and fishQuantity.count >= fishQuantity.limit then
        TriggerClientEvent('esx:showNotification', source, _U('not_enough_fish'))
      else
        xPlayer.removeInventoryItem('fish', 1)
        xPlayer.addInventoryItem('fish_stick', 1)

        Craft(source)
      end
    end
  end)
end

RegisterServerEvent('sandy_faimdus:startCraft')
AddEventHandler('sandy_faimdus:startCraft', function()
  local _source = source
  PlayersCrafting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('assembling_fish'))
  Craft(_source)
end)

RegisterServerEvent('sandy_faimdus:stopCraft')
AddEventHandler('sandy_faimdus:stopCraft', function()
  local _source = source
  PlayersCrafting[_source] = false
end)

-- Sell
function Sell(source)

  SetTimeout(5000, function()

    math.randomseed(os.time())
    local _source          = source
    local xPlayer          = ESX.GetPlayerFromId(_source)
    local total            = math.random(50)
    local playerMoney      = math.floor(total / 100 * 30)
    local societyMoney     = math.floor(total / 100 * 70)

    if xPlayer.job.grade >= 3 then
      total = total * 2
    end

    TriggerEvent('esx_addonaccount:getSharedAccount', 'society_faimdus', function(account)
      societyAccount = account
    end)

    if societyAccount ~= nil then

      if PlayersSelling[source] == true then

        local fishStickQuantity = xPlayer.getInventoryItem('fish_stick').count

        if endromedQuantity == 0 then
          TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas de GPS à vendre !')
        else
          xPlayer.removeInventoryItem('fish_stick', 1)
          xPlayer.addMoney(playerMoney)
          societyAccount.addMoney(societyMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_earned') .. playerMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. societyMoney)
          Sell(source)
        end
      end
    end
  end)
end

RegisterServerEvent('sandy_faimdus:startSell')
AddEventHandler('sandy_faimdus:startSell', function()

  local _source = source
  PlayersSelling[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))
  Sell(_source)
end)

RegisterServerEvent('sandy_faimdus:stopSell')
AddEventHandler('sandy_faimdus:stopSell', function()

  local _source = source
  PlayersSelling[_source] = false
end)

-- [[ Frigo ]] --
-- Retrait
RegisterServerEvent('sandy_faimdus:getStockItem')
AddEventHandler('sandy_faimdus:getStockItem', function(itemName, count)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_faimdus', function(inventory)
		local item = inventory.getItem(itemName)
		local sourceItem = xPlayer.getInventoryItem(itemName)

		if count > 0 and item.count >= count then

			if sourceItem.limit ~= -1 and (sourceItem.count + count) > sourceItem.limit then
				TriggerClientEvent('esx:showNotification', xPlayer.source, _U('player_cannot_hold'))
			else
				inventory.removeItem(itemName, count)
				xPlayer.addInventoryItem(itemName, count)
				TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)
			end
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end
	end)
end)

ESX.RegisterServerCallback('sandy_faimdus:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_faimdus', function(inventory)
    cb(inventory.items)
  end)
end)

-- Dépot
RegisterServerEvent('sandy_faimdus:putStockItems')
AddEventHandler('sandy_faimdus:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_faimdus', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= 0 then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)
  end)
end)

ESX.RegisterServerCallback('sandy_faimdus:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })
end)

RegisterServerEvent('sandy_faimdus:message')
AddEventHandler('sandy_faimdus:message', function(target, msg)
  TriggerClientEvent('esx:showNotification', target, msg)
end)

function SendNotificationToPlayersInService(message)
  local xPlayers 	= ESX.GetPlayers()
  local countCops = 0

  for k, v in pairs(xPlayers) do
    ESX.TriggerServerCallback('esx_service:isInService', 0, v, function(inService)
      if inService then
        TriggerClientEvent('esx:showNotification', v, message)
        countCops = countCops + 1
      end
    end, 'faimdus')
  end

  if countCops <= 0 then
    return false
  end
  return true
end

ESX.RegisterServerCallback('sandy_faimdus:getCallsList', function(source, cb)
  local _source = source
  cb(CallsList)
end)

RegisterNetEvent('sandy_faimdus:newCall')
AddEventHandler('sandy_faimdus:newCall', function(type, pos, message)
  local _source = source

  if type ~= "custom" then
    if type == "contact" then
      print(contact)
      message = "Une personne souhaite prendre contact"
    elseif type == "Commande" then
      print(Commande)
      message = "Une personne souhaite passer une commande"
    end
  end

  CallsList[_source] = {
    type = type,
    position = pos,
    text = message,
    nbr = 0
  }

  local notif = "Un nouvel appel viens d'arriver : "

  if not SendNotificationToPlayersInService(notif) then
    TriggerClientEvent('sandy_faimdus:noFaimdus', _source)
  else
    SendNotificationToPlayersInService("~y~".. message)
  end
end)

RegisterNetEvent('sandy_faimdus:takeCall')
AddEventHandler('sandy_faimdus:takeCall', function(id)
  CallsList[id].nbr = CallsList[id].nbr + 1
  TriggerClientEvent('sandy_faimdus:callTaked', id)
end)

RegisterNetEvent('sandy_faimdus:endCall')
AddEventHandler('sandy_faimdus:endCall', function(id)
  TriggerClientEvent('sandy_faimdus:endCall', id)
  CallsList[id] = nil
end)

RegisterNetEvent('sandy_faimdus:refuseCall')
AddEventHandler('sandy_faimdus:refuseCall', function(id)
  TriggerClientEvent('sandy_faimdus:refuseCall', id)
  CallsList[id] = nil
end)

ESX.RegisterServerCallback("sandy_faimdus:cancelCall", function(source, cb)
  local _source = source
  if CallsList[_source] ~= nil then
    CallsList[_source] = nil
    SendNotificationToPlayersInService("Appel Annulé")
    cb(true)
  else
    cb(false)
  end
end)
