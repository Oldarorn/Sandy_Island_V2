SET @job_name = 'faimdus';
SET @society_name = 'society_faimdus';
SET @job_Name_Caps = 'Faim Dus';

INSERT INTO `addon_account` (name, label, shared) VALUES
  (@society_name, @job_Name_Caps, 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  ('society_faimdus', 'Faim Dus (frigo)', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
    (@society_name, @job_Name_Caps, 1)
;

INSERT INTO `jobs` (name, label, whitelisted) VALUES
  (@job_name, @job_Name_Caps, 1)
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  (@job_name, 0, 'interim', 'Intérimaire', 1000, '{}', '{}'),
  (@job_name, 1, 'employe', 'Employé', 1300, '{}', '{}'),
  (@job_name, 2, 'seller', 'Vendeur', 1500, '{}', '{}'),
  (@job_name, 3, 'shareholder', 'Actionnaire', 1800, '{}', '{}'),
  (@job_name, 4, 'boss', 'Gérant', 2000, '{}', '{}')
;

INSERT INTO `items` (`name`, `label`) VALUES
	('fish', 'Poisson'),
	('fish_stick', 'Bâtonnet de poisson')
;