SET @job_name = 'theater';
SET @society_name = 'society_theater';
SET @job_Name_Caps = 'Theater';

INSERT INTO `addon_account` (name, label, shared) VALUES
  (@society_name, @job_Name_Caps, 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  ('society_theater_fridge', 'Theater (frigo)', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
    (@society_name, @job_Name_Caps, 1)
;

INSERT INTO `jobs` (name, label, whitelisted) VALUES
  (@job_name, @job_Name_Caps, 1)
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  (@job_name, 0, 'barman', 'Barman', 1100, '{"tshirt_2":1,"shoes_1":9,"shoes_2":2,"torso_2":4,"pants_1":24,"sex":0,"decals_2":0,"tshirt_1":22,"arms":,"decals_1":0,"torso_1":25,"pants_2":5,"chain_1":11,"chain_2":2}', '{"tshirt_1":38,"torso_1":7,"arms":3,"pants_1":8,"helmet_2":4,"shoes_1":42,"shoes_2":2,"torso_2":8,"sex":1,"pants_2":1,"tshirt_2":0,"helmet_1":29,"chain_1":23,"chain_2":0}'),
  (@job_name, 1, 'danser', 'Danseuse', 1000, '{"tshirt_2":1,"shoes_1":9,"shoes_2":2,"torso_2":4,"pants_1":24,"sex":0,"decals_2":0,"tshirt_1":22,"arms":,"decals_1":0,"torso_1":25,"pants_2":5,"chain_1":11,"chain_2":2}', '{"tshirt_1":38,"torso_1":7,"arms":3,"pants_1":8,"helmet_2":4,"shoes_1":42,"shoes_2":2,"torso_2":8,"sex":1,"pants_2":1,"tshirt_2":0,"helmet_1":29,"chain_1":23,"chain_2":0}'),
  (@job_name, 2, 'guard', 'Vigile', 1700, '{"tshirt_2":1,"shoes_1":9,"shoes_2":2,"torso_2":4,"pants_1":24,"sex":0,"decals_2":0,"tshirt_1":22,"arms":,"decals_1":0,"torso_1":25,"pants_2":5,"chain_1":11,"chain_2":2}', '{"tshirt_1":38,"torso_1":7,"arms":3,"pants_1":8,"helmet_2":4,"shoes_1":42,"shoes_2":2,"torso_2":8,"sex":1,"pants_2":1,"tshirt_2":0,"helmet_1":29,"chain_1":23,"chain_2":0}'),
  (@job_name, 3, 'boss', 'Gérant', 2000, '{"tshirt_2":1,"shoes_1":9,"shoes_2":2,"torso_2":4,"pants_1":24,"sex":0,"decals_2":0,"tshirt_1":22,"arms":,"decals_1":0,"torso_1":25,"pants_2":5,"chain_1":11,"chain_2":2}', '{"tshirt_1":38,"torso_1":7,"arms":3,"pants_1":8,"helmet_2":4,"shoes_1":42,"shoes_2":2,"torso_2":8,"sex":1,"pants_2":1,"tshirt_2":0,"helmet_1":29,"chain_1":23,"chain_2":0}')
;

INSERT INTO `items` (name, label) VALUES
  ('cocktail', 'Cocktail Abibi\’s'),
  ('orange2', 'Orange sanguine')
;
