local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local Blips                   = {}

local isBarman                = false
local isInMarker              = false
local isInPublicMarker        = false
local hintIsShowed            = false
local hintToDisplay           = "no hint to display"
local isDead                  = false
local InService               = false
local SelectedCall            = nil

ESX                           = nil

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)

function IsJobTrue()
  if PlayerData ~= nil then
    local IsJobTrue = false
    if PlayerData.job ~= nil and PlayerData.job.name == 'theater' then
      IsJobTrue = true
    end
    return IsJobTrue
  end
end

function IsGradeBoss()
  if PlayerData ~= nil then
    local IsGradeBoss = false
    if PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'viceboss' then
      IsGradeBoss = true
    end
    return IsGradeBoss
  end
end

function SetVehicleMaxMods(vehicle)

  local props = {
    modEngine       = 0,
    modBrakes       = 0,
    modTransmission = 0,
    modSuspension   = 0,
    modTurbo        = false,
  }
  ESX.Game.SetVehicleProperties(vehicle, props)
end

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer

end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)


function OpenCloakroomMenu()

  local elements = {
    {label = _U('citizen_wear'), value = 'citizen_wear'},
    {label = _U('barman_outfit'), value = 'barman_outfit'},
  }

  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = _U('cloakroom'),
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)
      --menu.close()

      if data.current.value == 'citizen_wear' then
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
          local playerPed = GetPlayerPed(-1)
          TriggerEvent('skinchanger:loadSkin', skin)
          ClearPedBloodDamage(playerPed)
          ResetPedVisibleDamage(playerPed)
          ClearPedLastWeaponDamage(playerPed)
          ResetPedMovementClipset(playerPed, 0)
          isBarman = false
        end)
      elseif data.current.value == 'barman_outfit' then
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
          local playerPed = GetPlayerPed(-1)
          if skin.sex == 0 then
            SetPedComponentVariation(playerPed, 8, 15, 0, 0) --T-shirt
            SetPedComponentVariation(playerPed, 11, 9, 1, 0) --Torse
            SetPedComponentVariation(playerPed, 10, 0, 0, 0) --Decals
            SetPedComponentVariation(playerPed, 3, 30 , 0, 0) --Bras
            SetPedComponentVariation(playerPed, 4, 63, 0, 0) --Pantalon
            SetPedComponentVariation(playerPed, 6, 51, 0, 0) --Chaussures
            SetPedComponentVariation(playerPed, 7, 0, 0, 0) --Collier

            ClearPedBloodDamage(playerPed)
            ResetPedVisibleDamage(playerPed)
            ClearPedLastWeaponDamage(playerPed)
            ResetPedMovementClipset(playerPed, 0)
            isBarman = true
          else
            SetPedComponentVariation(playerPed, 8, 3, 0, 0) --T-shirt
            SetPedComponentVariation(playerPed, 11, 8, 2, 0) --Torse
            SetPedComponentVariation(playerPed, 10, 0, 0, 0) --Decals
            SetPedComponentVariation(playerPed, 3, 5, 0, 0) --Bras
            SetPedComponentVariation(playerPed, 4, 44, 4, 0) --Pantalon
            SetPedComponentVariation(playerPed, 6, 0, 0, 0) --Chaussures
            SetPedComponentVariation(playerPed, 7, 0, 0, 0) --Collier

            ClearPedBloodDamage(playerPed)
            ResetPedVisibleDamage(playerPed)
            ClearPedLastWeaponDamage(playerPed)
            ResetPedMovementClipset(playerPed, 0)
            isBarman = true
          end
        end)
      end
      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = _U('open_cloackroom')
      CurrentActionData = {}
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = _U('open_cloackroom')
      CurrentActionData = {}
    end
    )
end

function OpenFridgeMenu()

  local elements = {
    {label = _U('get_object'), value = 'get_stock'},
    {label = _U('put_object'), value = 'put_stock'}
  }

  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'fridge',
    {
      title    = _U('fridge'),
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      if data.current.value == 'put_stock' then
        OpenPutFridgeStocksMenu()
      elseif data.current.value == 'get_stock' then
        OpenGetFridgeStocksMenu()
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_fridge'
      CurrentActionMsg  = _U('open_fridge')
      CurrentActionData = {}
    end
  )
end

function OpenVehicleSpawnerMenu()

  local vehicles = Config.Zones.Vehicles
  ESX.UI.Menu.CloseAll()
  if Config.EnableSocietyOwnedVehicles then

    local elements = {}

    ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

      for i=1, #garageVehicles, 1 do
        table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'vehicle_spawner',
        {
          title    = _U('vehicle_menu'),
          align    = 'top-left',
          elements = elements,
        },
        function(data, menu)
          menu.close()

          local vehicleProps = data.current.value
          ESX.Game.SpawnVehicle(vehicleProps.model, vehicles.SpawnPoint, vehicles.Heading, function(vehicle)
            ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
            local playerPed = GetPlayerPed(-1)
            --TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
          end)
          TriggerServerEvent('esx_society:removeVehicleFromGarage', 'theater', vehicleProps)
        end,
        function(data, menu)
          menu.close()
          CurrentAction     = 'menu_vehicle_spawner'
          CurrentActionMsg  = _U('vehicle_spawner')
          CurrentActionData = {}
        end
      )
    end, 'theater')
  else

    local elements = {}

    for i=1, #Config.AuthorizedVehicles, 1 do
      local vehicle = Config.AuthorizedVehicles[i]
      table.insert(elements, {label = vehicle.label, value = vehicle.name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vehicle_spawner',
      {
        title    = _U('vehicle_menu'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)
        menu.close()
        local model = data.current.value
        local vehicle = GetClosestVehicle(vehicles.SpawnPoint.x,  vehicles.SpawnPoint.y,  vehicles.SpawnPoint.z,  3.0,  0,  71)

        if not DoesEntityExist(vehicle) then
          local playerPed = GetPlayerPed(-1)
          ESX.Game.SpawnVehicle(model, {
            x = vehicles.SpawnPoint.x,
            y = vehicles.SpawnPoint.y,
            z = vehicles.SpawnPoint.z
          }, vehicles.Heading, function(vehicle)
            --TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1) -- teleport into vehicle
            SetVehicleMaxMods(vehicle)
            SetVehicleDirtLevel(vehicle, 0)
          end)
        else
          ESX.ShowNotification(_U('vehicle_out'))
        end
      end,
      function(data, menu)
        menu.close()
        CurrentAction     = 'menu_vehicle_spawner'
        CurrentActionMsg  = _U('vehicle_spawner')
        CurrentActionData = {}
      end
    )
  end
end

function OpenSocietyActionsMenu()
  local elements = {}

  table.insert(elements, {label = "Appels",      value = 'calls'})
  table.insert(elements, {label = _U('billing'), value = 'billing'})
  table.insert(elements, {label = _U('gps'),     value = 'gps'})

  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'theater_actions',
    {
      title    = _U('theater'),
      align    = 'top-left',
      elements = elements
    },
    function(data, menu)

      if data.current.value == 'billing' then
        OpenBillingMenu()
      end

      if data.current.value == 'calls' then
        ESX.TriggerServerCallback('sandy_abys:getCallsList', function(calls)
          local options = {}

          if calls ~= nil then
            for k, v in pairs(calls) do
              if k == SelectedCall then
                table.insert(options, {label = "~g~"..v.nbr.."x Barman en route - "..v.text, value = k})
              else
                table.insert(options, {label = v.nbr.."x Barman en route - "..v.text, value = k})
              end
            end
          else
            table.insert(options, {label = "Aucun Appel en Attente"})
          end
          ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'abys_calls',
            {
              title    = 'Appels',
              align    = 'top-left',
              elements = options
            }, function(data2, menu2)
              local callid = data2.current.value
              local options2 = {
                {label = "Prendre l'appel", value="take_call"},
                {label = "Position de l'appel", value="pos_call"},
                {label = "Appel Résolu", value="end_call"},
                {label = "Refuser l'appel", value="refused_call"}
              }
              ESX.UI.Menu.Open(
                'default', GetCurrentResourceName(), 'brasseur_call',
                {
                  title    = 'Actions',
                  align    = 'top-left',
                  elements = options2
                }, function(data3, menu3)
                  if data3.current.value == "take_call" and SelectedCall ~= data2.current.value then
                    SelectedCall = data2.current.value
                    TriggerServerEvent('sandy_abys:takeCall', data2.current.value)
                    local x = calls[data2.current.value].position.x
                    local y = calls[data2.current.value].position.y
                    SetNewWaypoint(x, y)
                  end
                  if data3.current.value == "pos_call" then
                    local x = calls[data2.current.value].position.x
                    local y = calls[data2.current.value].position.y
                    SetNewWaypoint(x, y)
                  end
                  if data3.current.value == "end_call" then
                    TriggerServerEvent('sandy_abys:endCall', callid)
                    ESX.ShowNotification("~g~Appel Résolu")
                  end
                  if data3.current.value == "refused_call" then
                    TriggerServerEvent("sandy_abys:refuseCall", callid)
                  end
                end
              )
            end
          )
        end)
      end

      if data.current.value == 'gps' then

        local blips = {
          {title="Récolte",              x = -1937.794, y = 1792.198, z = 171.715 },
          {title="Fabrication",          x = -109.392, y = 6396.541, z = 30.650 },
          {title="Revente de Cocktail",  x = 138.468, y = -1639.662, z = 28.50 }
        }

        for _, info in pairs(blips) do
          info.blip = AddBlipForCoord(info.x, info.y, info.z)
          SetBlipSprite(info.blip, 1)
          SetBlipDisplay(info.blip, 4)
          SetBlipScale(info.blip, 0.8)
          SetBlipColour(info.blip, 27)
          SetBlipAsShortRange(info.blip, true)

          BeginTextCommandSetBlipName("STRING")
          AddTextComponentString(info.title)
          EndTextCommandSetBlipName(info.blip)
        end
        ESX.ShowNotification("Position ajouté à votre gps !")
      end
    end,
    function(data, menu)
      menu.close()
    end
  )
end

function OpenBillingMenu()

  ESX.UI.Menu.Open(
    'dialog', GetCurrentResourceName(), 'billing',
    {
      title = _U('billing_amount')
    },
    function(data, menu)

      local amount = tonumber(data.value)
      local player, distance = ESX.Game.GetClosestPlayer()

      if player ~= -1 and distance <= 3.0 then

        menu.close()
        if amount == nil then
          ESX.ShowNotification(_U('amount_invalid'))
        else
          TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), 'society_theater', _U('billing'), amount)
        end
      else
        ESX.ShowNotification(_U('no_players_nearby'))
      end
    end,
    function(data, menu)
        menu.close()
    end
  )
end

function OpenGetFridgeStocksMenu()

  ESX.TriggerServerCallback('sandy_abys:getFridgeStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('theater_fridge_stock'),
        elements = elements
      },
      function(data, menu)
        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetFridgeStocksMenu()

              TriggerServerEvent('sandy_abys:getFridgeStockItem', itemName, count)
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

function OpenPutFridgeStocksMenu()
  ESX.TriggerServerCallback('sandy_abys:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('fridge_inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutFridgeStocksMenu()

              TriggerServerEvent('sandy_abys:putFridgeStockItems', itemName, count)
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )
  end)
end

-- Create Blips
Citizen.CreateThread(function()

  local blip = AddBlipForCoord(Config.Zones.Cloakrooms.Pos.x, Config.Zones.Cloakrooms.Pos.y, Config.Zones.Cloakrooms.Pos.z)

  SetBlipSprite (blip, 269)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 0.8)
  SetBlipColour (blip, 83)
  SetBlipAsShortRange(blip, true)

  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString("Abi's Club")
  EndTextCommandSetBlipName(blip)
end)

AddEventHandler('sandy_abys:hasEnteredMarker', function(zone)
  if zone == 'Cloakrooms' then
    CurrentAction     = 'menu_cloakroom'
    CurrentActionMsg  = _U('open_cloackroom')
    CurrentActionData = {}
  end

  if InService then
    if zone == 'Fridge' then
      CurrentAction     = 'menu_fridge'
      CurrentActionMsg  = _U('open_fridge')
      CurrentActionData = {}
    end

    if zone == 'Vehicles' then
      CurrentAction     = 'menu_vehicle_spawner'
      CurrentActionMsg  = _U('vehicle_spawner')
      CurrentActionData = {}
    end

    if zone == 'VehicleDeleters' then
      local playerPed = GetPlayerPed(-1)
      if IsPedInAnyVehicle(playerPed,  false) then
        local vehicle = GetVehiclePedIsIn(playerPed,  false)
        CurrentAction     = 'delete_vehicle'
        CurrentActionMsg  = _U('store_vehicle')
        CurrentActionData = {vehicle = vehicle}
      end
    end

    if zone == 'Craft' then
      CurrentAction     = 'harvest_menu'
      CurrentActionMsg  = _U('press_craft')
      CurrentActionData = {}
    end

    if zone == 'Harvest' then
      CurrentAction     = 'craft_menu'
      CurrentActionMsg  = _U('press_harvest')
      CurrentActionData = {}
    end

    if zone == 'Resell' then
      CurrentAction     = 'resell'
      CurrentActionMsg  = _U('press_sell')
      CurrentActionData = {}
    end

    if zone == 'BossActions' then
      CurrentAction     = 'menu_boss_actions'
      CurrentActionMsg  = _U('open_bossmenu')
      CurrentActionData = {}
    end
  end
end)

AddEventHandler('sandy_abys:hasExitedMarker', function(zone)
  CurrentAction = nil
  ESX.UI.Menu.CloseAll()
  TriggerServerEvent('sandy_abys:stopHarvest')
  TriggerServerEvent('sandy_abys:stopCraft')
  TriggerServerEvent('sandy_abys:stopSell')
end)

-- Display markers
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    if IsJobTrue() then
      local coords = GetEntityCoords(GetPlayerPed(-1))
      for k,v in pairs(Config.Zones) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, false, 2, false, false, false, false)
        end
      end

      local isInMarker  = false
      local currentZone = nil

      for k,v in pairs(Config.Zones) do
        if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
          isInMarker  = true
          currentZone = k
        end
      end

      if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
        HasAlreadyEnteredMarker = true
        LastZone                = currentZone
        TriggerEvent('sandy_abys:hasEnteredMarker', currentZone)
      end

      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('sandy_abys:hasExitedMarker', LastZone)
      end
    end

    local coords      = GetEntityCoords(GetPlayerPed(-1))
    local position    = nil
    local zone        = nil

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlJustReleased(0,  Keys['E']) and IsJobTrue() then

        if CurrentAction == 'menu_cloakroom' then
          OpenCloakroomMenu()
        end

        if InService then
          if CurrentAction == 'resell' then
            TriggerServerEvent('sandy_abys:startSell')
          elseif CurrentAction == 'menu_fridge' then
            OpenFridgeMenu()
          elseif CurrentAction == 'harvest_menu' then
            TriggerServerEvent('sandy_abys:startHarvest')
          elseif CurrentAction == 'menu_vehicle_spawner' then
            OpenVehicleSpawnerMenu()
          elseif CurrentAction == 'craft_menu' then
            TriggerServerEvent('sandy_abys:startCraft')
          elseif CurrentAction == 'menu_boss_actions' then

            ESX.UI.Menu.CloseAll()
            TriggerEvent('esx_society:openBossMenu', 'theater', function(data, menu)

              menu.close()
              CurrentAction     = 'menu_boss_actions'
              CurrentActionMsg  = _U('open_bossmenu')
              CurrentActionData = {}
            end)
          elseif CurrentAction == 'delete_vehicle' then

            local playerPed = GetPlayerPed(-1)

            if Config.EnableSocietyOwnedVehicles then
              local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
              TriggerServerEvent('esx_society:putVehicleInGarage', 'theater', vehicleProps)
            else
              if GetEntityModel(CurrentActionData.vehicle) == GetHashKey('burrito4') then
                if Config.MaxInService ~= -1 then
                  TriggerServerEvent('esx_service:disableService', 'theater')
                end
              end
            end
            ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
          end
        end
        CurrentAction = nil
      end
    end

    if IsControlJustReleased(0, Keys['F6']) and not isDead and PlayerData.job ~= nil and PlayerData.job.name == 'theater' then
      OpenSocietyActionsMenu()
    end
  end
end)

AddEventHandler('esx:onPlayerDeath', function()
  isDead = true
end)

AddEventHandler('playerSpawned', function(spawn)
 isDead = false
end)

RegisterNetEvent('sandy_abys:callAbys')
AddEventHandler('sandy_abys:callAbys', function(datas)
  local pos = GetEntityCoords(GetPlayerPed(-1), false)

  TriggerServerEvent('sandy_abys:newCall', datas.type, {x=pos.x, y=pos.y})

  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)

RegisterNetEvent('sandy_abys:noAbys')
AddEventHandler('sandy_abys:noAbys', function()
  ESX.ShowNotification("~r~Aucun employer n'est en service actuellement")
end)

RegisterNetEvent('sandy_abys:callTaked')
AddEventHandler('sandy_abys:callTaked', function()
  ESX.ShowNotification("~g~Votre appel viens d'être accepter, un employer est en route")
end)

RegisterNetEvent('sandy_abys:endTaked')
AddEventHandler('sandy_abys:endTaked', function()
  ESX.ShowNotification("~g~Appel terminer")
end)

RegisterNetEvent('sandy_abys:refuseTaked')
AddEventHandler('sandy_abys:refuseTaked', function()
  ESX.ShowNotification("~r~Votre Appel à été refusé")
end)

RegisterNetEvent('sandy_abys:cancelCall')
AddEventHandler('sandy_abys:cancelCall', function()
  ESX.TriggerServerCallback("sandy_abys:cancelCall", function(result)
    if result then
      ESX.ShowNotification("~g~Appel Annulé")
    end
  end)
end)

RegisterNetEvent('sandy_abys:callFaimdusCustom')
AddEventHandler('sandy_abys:callFaimdusCustom', function()
  local text = ""
  DisplayOnscreenKeyboard(1, "FMMC_MPM_NA", "", text, "", "", "", 100)
  while (UpdateOnscreenKeyboard() == 0) do
    DisableAllControlActions(0);
    Citizen.Wait(1);
  end

  if (GetOnscreenKeyboardResult()) then
    text = GetOnscreenKeyboardResult()
  end

  local pos = GetEntityCoords(GetPlayerPed(-1), false)
  TriggerServerEvent('sandy_abys:newCall', "custom", {x = pos.x, y = pos.y}, text)
  ESX.ShowNotification("~g~Votre appel a été enregistrer")
end)

