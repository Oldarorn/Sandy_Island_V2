Config      = {}

Config.DrawDistance               = 100.0
Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = false
Config.MissCraft                  = 15 -- %
Config.NPCJobEarnings             = {min = 20, max = 40}
Config.Locale                     = 'fr'

Config.AuthorizedVehicles = {
  { name = 'dubsta2',  label = 'Véhicule de fonction' },
}

Config.Zones = {

  Cloakrooms = {
    Pos   = { x = -332.149, y = 184.667, z = 86.918 },
    Size  = { x = 1.2, y = 1.2, z = 0.1 },
    Color = { r = 255, g = 187, b = 255 },
    Type  = 27,
  },

  Fridge = {
    Pos   = { x = -361.052, y = 154.777, z = 86.107 },
    Size  = { x = 1.2, y = 1.2, z = 0.1 },
    Color = { r = 255, g = 187, b = 255 },
    Type  = 27,
  },

  BossActions = {
    Pos   = { x =  -309.437, y = 179.677, z = 86.918},
    Size  = { x = 1.2, y = 1.2, z = 0.1 },
    Color = { r = 255, g = 187, b = 255 },
    Type  = 27,
  },

  Vehicles = {
    Pos          = { x = -355.048, y = 212.965, z = 85.750 },
    SpawnPoint   = { x = -342.958, y = 216.564, z = 86.970 },
    Size         = { x = 1.8, y = 1.8, z = 1.0 },
    Color        = { r = 255, g = 187, b = 255 },
    Type         = 27,
    Heading      = 284.5786,
  },

  VehicleDeleters = {
    Pos   = { x = -342.958, y = 216.564, z = 86.970 },
    Size  = { x = 3.0, y = 3.0, z = 1.0 },
    Color = { r = 255, g = 187, b = 255 },
    Type  = 27,
  },

  Resell = {
    Pos   = { x = 138.468, y = -1639.662, z = 28.50 },
    Size  = { x = 2.0, y = 2.0, z = 1.0 },
    Color = { r = 255, g = 187, b = 255 },
    Type  = 27,
  },

  Harvest = {
    Pos   = { x = -109.392, y = 6396.541, z = 30.650 },
    Size  = { x = 2.0, y = 2.0, z = 1.0 },
    Color = { r = 255, g = 187, b = 255 },
    Type  = 27,
  },

  Craft = {
    Pos   = { x = -1937.794, y = 1792.198, z = 171.715 },
    Size  = { x = 5.0, y = 5.0, z = 1.0 },
    Color = { r = 255, g = 187, b = 255 },
    Type  = 27,
  },
}
