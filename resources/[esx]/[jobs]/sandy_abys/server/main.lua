ESX                = nil
local CallsList    = {}
PlayersHarvesting  = {}
PlayersCrafting    = {}
PlayersSelling     = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'theater', 'Theater', 'society_theater', 'society_theater', 'society_theater', {type = 'private'})

RegisterServerEvent('sandy_abys:getFridgeStockItem')
AddEventHandler('sandy_abys:getFridgeStockItem', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  local sourceItem = xPlayer.getInventoryItem(itemName)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_theater_fridge', function(inventory)
    local item = inventory.getItem(itemName)

    -- is there enough in the society?
    if count > 0 and item.count >= count then

      if sourceItem.limit ~= -1 and (sourceItem.count + count) > sourceItem.limit then
       TriggerClientEvent('esx:showNotification', _source, _U('player_cannot_hold'))
      else
       inventory.removeItem(itemName, count)
       xPlayer.addInventoryItem(itemName, count)
       TriggerClientEvent('esx:showNotification', _source, _U('have_withdrawn', count, item.label))
      end
    else
     TriggerClientEvent('esx:showNotification', _source, _U('not_enough_in_society'))
    end
  end)
end)

ESX.RegisterServerCallback('sandy_abys:getFridgeStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_theater_fridge', function(inventory)
    cb(inventory.items)
  end)
end)

RegisterServerEvent('sandy_abys:putFridgeStockItems')
AddEventHandler('sandy_abys:putFridgeStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_theater_fridge', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end
    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)
  end)
end)

ESX.RegisterServerCallback('sandy_abys:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })
end)

function Sell(source)

  SetTimeout(5000, function()

    math.randomseed(os.time())
    local _source          = source
    local xPlayer          = ESX.GetPlayerFromId(_source)
    local total            = math.random(Config.NPCJobEarnings.min, Config.NPCJobEarnings.max);
    local playerMoney      = math.floor(total / 100 * 30)
    local societyMoney     = math.floor(total / 100 * 70)

    if xPlayer.job.grade >= 3 then
      total = total * 2
    end

    TriggerEvent('esx_addonaccount:getSharedAccount', 'society_theater', function(account)
      societyAccount = account
    end)

    if societyAccount ~= nil then

      if PlayersSelling[source] == true then

        local cocktailQuantity = xPlayer.getInventoryItem('cocktail').count

        if cocktailQuantity == 0 then
          TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez plus de cocktail à vendre !')
        else
          xPlayer.removeInventoryItem('cocktail', 1)
          xPlayer.addMoney(playerMoney)
          societyAccount.addMoney(societyMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_earned') .. playerMoney)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. societyMoney)
          Sell(source)
        end
      end
    end
  end)
end

RegisterServerEvent('sandy_abys:startSell')
AddEventHandler('sandy_abys:startSell', function()

  local _source = source
  PlayersSelling[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))
  Sell(_source)
end)

RegisterServerEvent('sandy_abys:stopSell')
AddEventHandler('sandy_abys:stopSell', function()

  local _source = source
  PlayersSelling[_source] = false
end)

-- [[ Run ]] --
-- Harvest
  -- 1
local function Harvest(source)

  SetTimeout(4000, function()

    if PlayersHarvesting[source] == true then

      local xPlayer      = ESX.GetPlayerFromId(source)
      local orangeQuantity = xPlayer.getInventoryItem('orange').count

      if orangeQuantity >= 20 then
        TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
      else
        xPlayer.addInventoryItem('orange', 1)
        Harvest(source)
      end
    end
  end)
end

RegisterServerEvent('sandy_abys:startHarvest')
AddEventHandler('sandy_abys:startHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('recovery_orange'))
  Harvest(source)
end)

RegisterServerEvent('sandy_abys:stopHarvest')
AddEventHandler('sandy_abys:stopHarvest', function()
  local _source = source
  PlayersHarvesting[_source] = false
end)

-- Craft
local function Craft(source)

  SetTimeout(4000, function()

    if PlayersCrafting[source] == true then

      local xPlayer      = ESX.GetPlayerFromId(source)
      local orangeQuantity = xPlayer.getInventoryItem('orange').count

      if orangeQuantity.limit ~= -1 and orangeQuantity.count >= orangeQuantity.limit then
        TriggerClientEvent('esx:showNotification', source, _U('not_enough_orange'))
      else
        xPlayer.removeInventoryItem('orange', 1)
        xPlayer.addInventoryItem('cocktail', 1)
        Craft(source)
      end
    end
  end)
end

RegisterServerEvent('sandy_abys:startCraft')
AddEventHandler('sandy_abys:startCraft', function()
  local _source = source
  PlayersCrafting[_source] = true
  TriggerClientEvent('esx:showNotification', _source, _U('assembling_orange'))
  Craft(_source)
end)

RegisterServerEvent('sandy_abys:stopCraft')
AddEventHandler('sandy_abys:stopCraft', function()
  local _source = source
  PlayersCrafting[_source] = false
end)

RegisterServerEvent('sandy_abys:message')
AddEventHandler('sandy_abys:message', function(target, msg)
  TriggerClientEvent('esx:showNotification', target, msg)
end)

function SendNotificationToPlayersInService(message)
  local xPlayers 	= ESX.GetPlayers()
  local countCops = 0

  for k, v in pairs(xPlayers) do
    ESX.TriggerServerCallback('esx_service:isInService', 0, v, function(inService)
      if inService then
        TriggerClientEvent('esx:showNotification', v, message)
        countCops = countCops + 1
      end
    end, 'theater')
  end

  if countCops <= 0 then
    return false
  end
  return true
end

ESX.RegisterServerCallback('sandy_abys:getCallsList', function(source, cb)
  local _source = source
  cb(CallsList)
end)

RegisterNetEvent('sandy_abys:newCall')
AddEventHandler('sandy_abys:newCall', function(type, pos, message)
  local _source = source

  if type ~= "custom" then
    if type == "reservation" then
      print(reservation)
      message = "Une personne souhaite faire une réservation."
    elseif type == "commande" then
      print(commande)
      message = "Une personne souhaite passer une commande."
    end
  end

  CallsList[_source] = {
    type = type,
    position = pos,
    text = message,
    nbr = 0
  }

  local notif = "Un nouvel appel viens d'arriver : "

  if not SendNotificationToPlayersInService(notif) then
    TriggerClientEvent('sandy_abys:noAbys', _source)
  else
    SendNotificationToPlayersInService("~y~".. message)
  end
end)

RegisterNetEvent('sandy_abys:takeCall')
AddEventHandler('sandy_abys:takeCall', function(id)
  CallsList[id].nbr = CallsList[id].nbr + 1
  TriggerClientEvent('sandy_abys:callTaked', id)
end)

RegisterNetEvent('sandy_abys:endCall')
AddEventHandler('sandy_abys:endCall', function(id)
  TriggerClientEvent('sandy_abys:endCall', id)
  CallsList[id] = nil
end)

RegisterNetEvent('sandy_abys:refuseCall')
AddEventHandler('sandy_abys:refuseCall', function(id)
  TriggerClientEvent('sandy_abys:refuseCall', id)
  CallsList[id] = nil
end)

ESX.RegisterServerCallback("sandy_abys:cancelCall", function(source, cb)
  local _source = source
  if CallsList[_source] ~= nil then
    CallsList[_source] = nil
    SendNotificationToPlayersInService("Appel Annulé")
    cb(true)
  else
    cb(false)
  end
end)

