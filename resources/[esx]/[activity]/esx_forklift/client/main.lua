local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX 			       = nil
local playerData = {}

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(1)
  end
end)

local isInMarker       = false
local menuIsOpen       = false
local taskPoints       = {}
local forkBlips        = {}
local currentZone      = 'none'
local Blips            = {}
local packetsDelivered = 0
local currentJob        = 'none'
local currentBox        = nil

local lastDelivery = nil
local lastPickup = nil
local zOffset = -0.65
local hintToDisplay = "no hint to display"
local hintIsShowed = false
local currentVehicle = nil
local currentPlate = ''

local isInForklift = false

function sizeOfTable(tab)
  local count = 0
  for k, v in pairs(tab) do
    count = count + 1
  end
  return count
end

function elementAt(tab, indx)

  local count = 0
  local ret = nil
  for k, v in pairs(tab) do
    count = count + 1
  	if count == indx then
    	ret = v
    	break
  	end
  end
  return ret
end

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  playerData = xPlayer
  refreshBlips()
  onDuty = false
  deleteBlips()
end)

function drawBlip(coords, icon, text)

  local blip = AddBlipForCoord(coords.x, coords.y, coords.z)
  SetBlipSprite (blip, icon)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 0.9)
  SetBlipColour (blip, 3)
  SetBlipAsShortRange(blip, true)

  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString(text)
  EndTextCommandSetBlipName(blip)
  table.insert(forkBlips, blip)
end

function refreshBlips()
	if isInForklift == true then
		drawBlip(Config.locker, 280, "Valise de chariot élévateur")
		drawBlip(Config.carSpawner, 479, "Acheter un camion")
		drawBlip(Config.carDelete, 490, "Retourner le camion")
	end
end

function deleteBlips()
  if forkBlips[1] ~= nil then
    for i = 1, #forkBlips, 1 do
      RemoveBlip(forkBlips[i])
      forkBlips[i] = nil
    end
  end
end

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)
    if hintIsShowed then
      SetTextComponentFormat("STRING")
      AddTextComponentString(hintToDisplay)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)
    end
  end
end)

function displayMarker(coords)
	DrawMarker(0, coords.x, coords.y, coords.z + 0.75, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.0, 1.0, 1.0, 15, 15, 255, 100, false, true, 2, false, false, false, false)
end

function isMyCar()
	if currentPlate == GetVehicleNumberPlateText(GetVehiclePedIsIn(GetPlayerPed(-1), false)) then
		return true
	else
		return false
	end
end

function spawnFork()

	local vehicleModel = GetHashKey('forklift')
	RequestModel(vehicleModel)

	while not HasModelLoaded(vehicleModel) do
		Citizen.Wait(0)
	end

	currentCar = CreateVehicle(vehicleModel, Config.carSpawnPoint.x, Config.carSpawnPoint.y, Config.carSpawnPoint.z, Config.carSpawnPoint.h, true, false)
	SetVehicleHasBeenOwnedByPlayer(currentCar,  true)
	SetEntityAsMissionEntity(currentCar,  true,  true)
	SetVehicleNumberPlateText(currentCar, "HOMO" .. math.random(1000, 9999))
	local id = NetworkGetNetworkIdFromEntity(currentCar)
	SetNetworkIdCanMigrate(id, true)
	TaskWarpPedIntoVehicle(GetPlayerPed(-1), currentCar, -1)
	local props = {
		modEngine       = 1,
		modTransmission = 1,
		modSuspension   = 1,
		modTurbo        = false,
	}
	ESX.Game.SetVehicleProperties(currentCar, props)
	Wait(1000)
	currentPlate = GetVehicleNumberPlateText(GetVehiclePedIsIn(GetPlayerPed(-1), false))
end

function trackBox()
	Citizen.CreateThread(function()
		while currentJob == 'pickup' do
			Citizen.Wait(5)
			if currentBox ~= nil and DoesEntityExist(currentBox) then
				local coords = GetEntityCoords(currentBox)
				setGPS(coords)

				if playerIsInside(coords, 4) then
					goDeliver()
				end
				if playerIsInside(coords, Config.pickupDistance) then
					--[[ Do not enable, totally useless here since inmarker check thread overrides this anyways
					isInMarker = true
					hintIsShowed = true
					hintToDisplay = "Soulevez l'élévateur à fourche pour les piliers et le prendre à l'endroit marqué."
					currentZone = 'none' ]]--
				end
				if playerIsInside(coords, 100) then
					local temp = {x = coords.x, y = coords.y, z = coords.z + Config.boxZ}
					displayMarker(temp)
				end
			end
		end
	end)
end

function spawnBox(coords)
	Citizen.CreateThread(function()
		repeat
			Citizen.Wait(500)
		until boxCanSpawn(taskPoints['deliver'])

		ESX.Game.SpawnObject('prop_boxpile_07d', {
			x = coords.x,
			y = coords.y,
			z = coords.z
		}, function(obj)
			SetEntityHeading(obj, coords.h)
			PlaceObjectOnGroundProperly(obj)
			currentBox = obj
		end)

		--trackBox()
	end)
end

function deleteBox()
	--local object = GetClosestObjectOfType(coords.x,  coords.y,  coords.z,  3.0,  GetHashKey('prop_boxpile_07d'), false, false, false)
	if currentBox ~= nil and DoesEntityExist(currentBox) then
		DeleteEntity(currentBox)
		return true
	end
	return false
end

function deleteCurrentBox()
	if currentBox ~= nil and DoesEntityExist(currentBox) then
		DeleteEntity(currentBox)
	end
end

function giveWork()

	if lastDelivery == nil then
		lastDelivery = 0
	end
	if lastPickup == nil then
		lastPickup = 0
	end

	local indA = 0
	local indB = 0

	repeat
	indA = math.random(1, #Config.objPoints)
	until indA ~= lastPickup
	local temp = Config.objPoints[indA]
	taskPoints['pickup'] = { x = temp.x, y = temp.y, z = temp.z, h = temp.h}

	repeat
		indB = math.random(1, #Config.objPoints)
	until indB ~= indA and indB ~= lastDelivery and isFar(taskPoints['pickup'], Config.objPoints[indB], Config.minDistance)
	local temp2 = Config.objPoints[indB]

	taskPoints['deliver'] = { x = temp2.x, y = temp2.y, z = temp2.z, h = temp2.h}

	lastPickup = indA
	lastDelivery = indB
end

function boxIsInside(coords)
	--local object = GetClosestObjectOfType(coords.x,  coords.y,  coords.z,  3.0,  GetHashKey('prop_boxpile_07d'), false, false, false)
	if currentBox ~= nil and DoesEntityExist(currentBox) then
        local objCoords = GetEntityCoords(currentBox)
        local distance  = GetDistanceBetweenCoords(coords.x,  coords.y,  coords.z,  objCoords.x,  objCoords.y,  objCoords.z,  true)
		return distance < 1.25
	else
		return false
	end
end

function boxCanSpawn(coords)
	local object = GetClosestObjectOfType(coords.x,  coords.y,  coords.z,  3.0,  GetHashKey('prop_boxpile_07d'), false, false, false)
	if DoesEntityExist(object) then
        local objCoords = GetEntityCoords(object)
        local distance  = GetDistanceBetweenCoords(coords.x,  coords.y,  coords.z,  objCoords.x,  objCoords.y,  objCoords.z,  true)
		return distance > 5.0
	else
		return true
	end
end

function goDeliver()
	Citizen.CreateThread(function()
		ESX.ShowNotification('Transporter la palette au lieu de stockage.')
		setGPS(taskPoints['deliver'])
		currentJob = 'deliver'
	end)
end

function goPickup()
	Citizen.CreateThread(function()
		ESX.ShowNotification('Récupérer une palette.')
		setGPS(taskPoints['pickup'])
		currentJob = 'pickup'
		trackBox()
	end)
	spawnBox(taskPoints['pickup'])
end

function nextJob()
	--lastDelivery = taskPoints['delivery']
	--lastPickup = taskPoints['pickup']
	packetsDelivered = packetsDelivered + 1
	giveWork()
	goPickup()
end

function startWork()
	packetsDelivered = 0
	spawnFork()
	giveWork()
	goPickup()
end

function deleteCar()
	local entity = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	ESX.Game.DeleteVehicle(entity)
end

function getPaid()
	setGPS(0)
	local playerPed = GetPlayerPed(-1)
	if IsPedInAnyVehicle(playerPed) and isMyCar() then
		deleteCar()
		TriggerServerEvent('esx_fork:getPaid', packetsDelivered * Config.pay)
	else
		ESX.ShowNotification('~r~Où est le camion?')
		local amount = 400
		if packetsDelivered < 2 then
			amount = 1200
		end
		ESX.ShowNotification('~w~Gains: ~r~ -' .. amount .. ' ~w~$.')
	end
	currentJob = 'none'
	currentPlate = ''
	currentVehicle = nil
	packetsDelivered = 0
	taskPoints = {}
	deleteCurrentBox()
end

function isFar(coords1, coords2, distance)
	local vecDiffrence = GetDistanceBetweenCoords(coords1x, coords1.y, coords1.z, coords2.x, coords2.y, coords2.z, false)
	return vecDiffrence > distance
end

function setGPS(coords)
	if Blips['fork'] ~= nil then
		RemoveBlip(Blips['fork'])
		Blips['fork'] = nil
	end
	if coords ~= 0 then
		Blips['fork'] = AddBlipForCoord(coords.x, coords.y, coords.z)
		SetBlipRoute(Blips['buzz'], true)
	end
end

function playerIsInside(coords, distance)
	local playerCoords = GetEntityCoords(GetPlayerPed(-1))
	local vecDiffrence = GetDistanceBetweenCoords(playerCoords, coords.x, coords.y, coords.z, false)
	return vecDiffrence < distance
end

function taskTrigger(zone)
	if zone == 'locker' then
		openMenu()
	elseif zone == 'start' then
		startWork()
	elseif zone == 'pay' then
		getPaid()
	end
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(50)
		if isInForklift == true then
			if taskPoints['deliver'] ~= nil and playerIsInside(taskPoints['deliver'], 5.5) and boxIsInside(taskPoints['deliver']) then
				if deleteBox() then
					nextJob()
				end
			end
		end
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(2)
		if menuIsOpen == false then
			if playerIsInside(Config.locker, 2.5) then
				isInMarker = true
				hintIsShowed = true
				hintToDisplay = "Appuyez sur E pour changer de vêtements"
				currentZone = 'locker'
      end
      --if isInForklift == true then
        if taskPoints['deliver'] == nil and playerIsInside(Config.carSpawner, 2.5) then
          isInMarker = true
          hintIsShowed = true
          hintToDisplay = "Appuyez sur E pour commencer le travail"
          currentZone = 'start'
        elseif currentJob == 'deliver' and taskPoints['deliver'] ~= nil and playerIsInside(taskPoints['deliver'], Config.pickupDistance) then
          isInMarker = true
          hintIsShowed = true
          hintToDisplay = "Déposer la palette à l'endroit indiqué."
          currentZone = 'none'
        elseif currentPlate ~= '' and playerIsInside(Config.carDelete, 1.5) then
          isInMarker = true
          hintIsShowed = true
          hintToDisplay = "Appuyez sur E pour ranger le camion"
          currentZone = 'pay'
        else
          isInMarker = false
          hintIsShowed = false
          hintToDisplay = "No hint to display"
          currentZone = 'none'
        end
      --end

			if IsControlJustReleased(0, Keys["E"]) and isInMarker then
				taskTrigger(currentZone)
			end
		end
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)

		if playerIsInside(Config.locker, 100) then
			displayMarker(Config.locker)
		end
		if playerIsInside(Config.carSpawner, 100) then
			displayMarker(Config.carSpawner)
		end
		if currentJob == 'deliver' and playerIsInside(taskPoints['deliver'], 100) then
			displayMarker(taskPoints['deliver'])
		end
		if currentPlate ~= '' and playerIsInside(Config.carDelete, 100) then
			displayMarker(Config.carDelete)
		end
	end
end)

function openMenu()
  menuIsOpen = true
  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'locker',
    {
      title    = "Vestiaire",
      elements = {
        {label = "Tenue de travail", value = 'fork_wear'},
        {label = "Tenue personelle", value = 'everyday_wear'}
      }
    },
    function(data, menu)
      if data.current.value == 'everyday_wear' then
        isInForklift = false
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
            TriggerEvent('skinchanger:loadSkin', skin)
        end)
      end
      if data.current.value == 'fork_wear' then
        isInForklift = true
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
          if skin.sex == 0 then
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
          else
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
          end
        end)
      end
      menu.close()
	    menuIsOpen = false
    end,
    function(data, menu)
      menu.close()
	  menuIsOpen = false
    end
  )
end
