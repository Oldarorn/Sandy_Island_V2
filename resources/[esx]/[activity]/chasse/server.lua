ESX                = nil
PlayersHarvesting  = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

local function Harvest(source)
	SetTimeout(4000, function()

		if PlayersHarvesting[source] == true then

			local xPlayer        = ESX.GetPlayerFromId(source)
			local ViandeQuantity = xPlayer.getInventoryItem('viande').count

			if ViandeQuantity >= 100 then
				TriggerClientEvent('esx:showNotification', source, '~r~Vous n avez plus de place')
			else
        xPlayer.addInventoryItem('viande', 1)
			end
		end
	end)
end

RegisterServerEvent('chasse:startRecup')
AddEventHandler('chasse:startRecup', function()
	local _source = source
	PlayersHarvesting[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Récupération de ~b~viande~s~...')
	Harvest(source)
end)

RegisterServerEvent('chasse:stopRecup')
AddEventHandler('chasse:stopRecup', function()
	local _source = source
	PlayersHarvesting[_source] = false
end)
