ESX = nil
local GeneralParkedVehicles = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function GetGeneralParkedVehicles()
	MySQL.Async.fetchAll("SELECT * FROM user_parkings", {},
		function(results)
			for k,v in pairs(results) do
				GeneralParkedVehicles[v.id] = {
					owner = v.identifier,
					plate = json.decode(v.vehicle).plate
				}
			end
		end
	)
end
GetGeneralParkedVehicles()

function VehicleIsParked(plate)
	for k,v in pairs(GeneralParkedVehicles) do
		if v.plate == plate then
			return true
		end
	end
	return false
end

RegisterNetEvent('esx_garage:setParking')
AddEventHandler('esx_garage:setParking', function(garage, zone, vehicleProps)
	local _source = source
	if vehicleProps == false then
		for k,v in pairs(GeneralParkedVehicles) do
			if v.plate == vehicleProps.plate then
				table.remove(GeneralParkedVehicles, k)
			end
		end
	else
		table.insert(GeneralParkedVehicles, {owner = ESX.GetPlayerFromId(_source), plate = vehicleProps.plate})
	end
end)

function GetOutsideVehicles(player, cb)
	local xPlayer = ESX.GetPlayerFromId(player)
	local OutVehicles = {}
	local ParkedVehicles = {}
	MySQL.Async.fetchAll(
    'SELECT * FROM owned_vehicles WHERE owner = @owner',
    { ['@owner'] = xPlayer.identifier },
    function (result)
      for i=1, #result, 1 do
        local vehicleData = json.decode(result[i].vehicle)
        table.insert(OutVehicles, vehicleData)
      end
  --     MySQL.Async.fetchAll(
		-- 'SELECT * FROM `user_parkings` WHERE `identifier` = @identifier',
		-- {
		-- 	['@identifier'] = xPlayer.identifier,
		-- },
		-- function(result2)
		-- 	for i=1, #result2, 1 do
		-- 		local vehicleDatas = json.decode(result2[i].vehicle)
		-- 		table.insert(ParkedVehicles, vehicleDatas)
		-- 	end
			for k,v in pairs(OutVehicles) do
				for k2, v2 in pairs(GeneralParkedVehicles) do
					if v.plate == v2.plate then
						table.remove(OutVehicles, k)
					end
				end
				-- for k2, v2 in pairs(ParkedVehicles) do
				-- 	if v.plate == v2.plate then
				-- 		table.remove(OutVehicles, k)
				-- 	end
				-- end
			end
			cb(OutVehicles)
		--end)
    end)
end

ESX.RegisterServerCallback("esx_policepound:getOutVehicles", function(source, cb, player)
	GetOutsideVehicles(player, cb)
end)

ESX.RegisterServerCallback("esx_policepound:getSocietyVehicles", function(source, cb, player)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local job = 'society:'
	local Vehicles = {}

	if xPlayer.job ~= nil and xPlayer.job.name ~= nil then
		job = job .. xPlayer.job.name
	end
	MySQL.Sync.fetchAll("SELECT * FROM owned_vehicles WHERE owner = @identifier", {["@identifier"] = job}, function(vehicles)
		for _,v in pairs(vehicles) do
			local props = json.decode(v.vehicle)
			table.insert(Vehicles, props)
		end
		cb(Vehicles)
	end)
end)

RegisterNetEvent("esx_policepound:payPound")
AddEventHandler("esx_policepound:payPound", function(player, vehicle)
	local xPlayer = ESX.GetPlayerFromId(player)

	xPlayer.removeMoney(Config.PoundPrice)

	TriggerClientEvent('esx:showNotification', player, 'Vous avez payé ' .. Config.PoundPrice)
	TriggerClientEvent("esx_policepound:outOfPound", player, vehicle)
end)