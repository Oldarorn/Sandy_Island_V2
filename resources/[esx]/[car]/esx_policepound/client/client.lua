ESX = nil
local PlayerData = {}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end
	
	Citizen.Wait(5000)
	PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent("esx:setJob")
AddEventHandler("esx:setJob", function(job)
	PlayerData.job = job
end)

function OpenPoundMenu()
	local player, distance = ESX.Game.GetClosestPlayer()
	if distance ~= -1 and distance <= 5.0 then
		local OutVehicles
		ESX.TriggerServerCallback("esx_policepound:getOutVehicles", function(result)
			ESX.UI.Menu.CloseAll()

			local elements = {}
			for k,v in pairs(result) do
				table.insert(elements, {label = GetDisplayNameFromVehicleModel(v.model).." : ("..v.plate..")", value = v})
			end
			ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'vehicle_pounded_list',
			{
				title = "Fourrière",
				align = "top-left",
				elements = elements
			},
			function(data, menu)
				TriggerServerEvent("esx_policepound:payPound", GetPlayerServerId(player), data.current.value)
				OpenPoundMenu()
			end,
			function(data, menu)
				menu.close()
			end)
		end, GetPlayerServerId(player))
	else
		ESX.ShowNotification("Aucun Joueur Proche de vous")
	end
end

RegisterNetEvent("esx_policepound:outOfPound")
AddEventHandler("esx_policepound:outOfPound", function(vehicle)
	Citizen.Trace(vehicle.model)
	ESX.Game.SpawnVehicle(vehicle.model, Config.OutPos, 0.0, function(vehicle2)
		ESX.Game.SetVehicleProperties(vehicle2, vehicle)
	end)
end)

Citizen.CreateThread(function()
	local x,y,z = table.unpack(Config.PoundPos)
	local blip = AddBlipForCoord(x, y, z)

	SetBlipSprite (blip, 147)
	SetBlipDisplay(blip, 4)
	SetBlipScale  (blip, 1.2)
	SetBlipColour (blip, 77)
	SetBlipAsShortRange(blip, true)

	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString("Fourrière")
	EndTextCommandSetBlipName(blip)
	while true do
		Wait(1)
		if PlayerData ~= {} and PlayerData.job ~= nil and PlayerData.job.name == "police" then
			local coords = GetEntityCoords(GetPlayerPed(-1), true)
			local distance = GetDistanceBetweenCoords(x, y, z, coords.x, coords.y, coords.z, true)
			if distance < 25 then
				DrawMarker(1, x, y, z, 0, 0, 0, 0, 0, 0, 1.0, 1.0, 1.0, 255, 0, 0, 100, 0, 0, 0, 0, 0, 0, 0)
			end
			if distance < 1.5 then
				if IsControlJustPressed(0, 38) then
					OpenPoundMenu()
				end
			end
		end
	end
end)