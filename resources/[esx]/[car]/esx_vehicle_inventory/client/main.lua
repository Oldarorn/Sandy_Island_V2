local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


ESX                           = nil

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(1)
  end
end)

function OpenVehicleMenu()
  if not IsPedInAnyVehicle(GetPlayerPed(-1), false) then
    local vehicle = ESX.Game.GetClosestVehicle()

    if DoesEntityExist(vehicle) then
      local playerCoords = GetEntityCoords(GetPlayerPed(-1), true)
      local vehicleCoords = GetEntityCoords(vehicle)
      if GetDistanceBetweenCoords(playerCoords, vehicleCoords.x, vehicleCoords.y, vehicleCoords.z, true) < 3.2 then
        ESX.UI.Menu.CloseAll()

        local vehicleClass = GetVehicleClass(vehicle)

        SetVehicleDoorOpen(vehicle, 5, false, false)

        ESX.UI.Menu.Open(
          'default', GetCurrentResourceName(), "vehicle_inventory_menu",
          {
            title = "Coffre du véhicule",
            align = "right",
            elements = {
              {label = "Déposer", value = "put"},
              {label = "Retirer", value = "get"}
            }
          },
          function(data, menu)
            local value = data.current.value
            if value == "put" then
              OpenVehiclePutInventory(GetVehicleNumberPlateText(vehicle), 1, vehicleClass)
            elseif value == "get" then
              OpenVehicleGetInventory(GetVehicleNumberPlateText(vehicle), 1)
            end
          end,
          function(data, menu)
            menu.close()
            SetVehicleDoorShut(vehicle, 5, false)
          end)
      end
    end
  elseif IsPedInAnyVehicle(GetPlayerPed(-1), true) then
    local vehicle = GetVehiclePedIsIn(GetPlayerPed(-1), false)

    if DoesEntityExist(vehicle) then
      ESX.UI.Menu.CloseAll()

      local vehicleClass = GetVehicleClass(vehicle)

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), "vehicle_inventory_menu",
        {
          title = "Coffre du véhicule",
          align = "right",
          elements = {
            {label = "Déposer", value = "put"},
            {label = "Retirer", value = "get"}
          }
        },
        function(data, menu)
          local value = data.current.value
          if value == "put" then
            OpenVehiclePutInventory(GetVehicleNumberPlateText(vehicle), 2, vehicleClass)
          elseif value == "get" then
            OpenVehicleGetInventory(GetVehicleNumberPlateText(vehicle), 2)
          end
        end,
        function(data, menu)
          menu.close()
        end)
    end
  end
end

function OpenAmountAskWindow()
  DisplayOnscreenKeyboard(1, "FMMC_MPM_NA", "", "", "", "", "", 3)
  while (UpdateOnscreenKeyboard() == 0) do
    DisableAllControlActions(0);
    Citizen.Wait(0);
  end
  if (GetOnscreenKeyboardResult()) then
    local amount = tonumber(GetOnscreenKeyboardResult())
    return amount
  end
end

function OpenVehicleGetInventory(plate, type)
  ESX.UI.Menu.CloseAll()

  ESX.TriggerServerCallback("esx_vehicle_inventory:getObjectsInCompartment", function(inventory)
    local elements = {}

    for k,v in pairs(inventory) do
      if v.type == "item" then
        if v.count > 0 then
          table.insert(elements, {label = "x" .. v.count .. " " ..v.itemName, type = v.type, amount = v.count, name = v.itemName})
        end
        table.remove(inventory, k)
      end
    end
    table.insert(elements, {label = "--- Armes ---", value = "menu"})
    for k,v in pairs(inventory) do
      if v.type == "weapon" then
        table.insert(elements, {label = "x" .. v.count .. " " ..v.itemName, type = "weapon", amount = v.count, name = v.itemName})
        table.remove(inventory, k)
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), "vehicle_inventory",
      {
        title = Config.CompartmentName[type],
        align = "right",
        elements = elements
      },
      function(data, menu)
        if data.current.value == nil or data.current.value ~= "menu" then
          if data.current.type == "item" then
            local amount = OpenAmountAskWindow()
            if amount > data.current.amount then
              ESX.ShowNotification("~r~Quantité invalide")
              OpenVehicleGetInventory(plate, type)
            end
            ESX.TriggerServerCallback("esx_vehicle_inventory:removeItemInCompartment", function(success)
              OpenVehicleGetInventory(plate, type)
            end, plate, data.current.name, amount, type)
          else
            ESX.TriggerServerCallback("esx_vehicle_inventory:removeWeaponInCompartment", function(success)
              OpenVehiclePutInventory(plate, type)
            end, plate, data.current.name, 1, type)
          end
        end
      end,
      function(data, menu)
        menu.close()
        OpenVehicleMenu()
      end)
  end, plate, type)
end

function OpenVehiclePutInventory(plate, type, vehicleClass)
  ESX.UI.Menu.CloseAll()

  ESX.TriggerServerCallback("esx_vehicle_inventory:getPlayerInventory", function(inventory)
    local weaponList = ESX.GetWeaponList()
    local elements = {}

    for i = 1, #inventory.items do
      if inventory.items[i].count > 0 then
        table.insert(elements, {label = 'x' .. inventory.items[i].count .. ' ' .. inventory.items[i].label,  type = "item", amount = inventory.items[i].count, name = inventory.items[i].name})
      end
    end
    table.insert(elements, {label = "--- Armes ---", value = "menu"})
    for i=1, #weaponList, 1 do

      local weaponHash = GetHashKey(weaponList[i].name)

      if HasPedGotWeapon(GetPlayerPed(-1),  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
        table.insert(elements, {label = weaponList[i].label, type = "weapon", name = weaponList[i].name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), "vehicle_inventory",
      {
        title = Config.CompartmentName[type] .. " : " .. Config.CompartmentSize[type][vehicleClass + 1] .. " Objets",
        align = "right",
        elements = elements
      },
      function(data, menu)
        if data.current.value == nil or data.current.value ~= "menu" then
          if data.current.type == "item" then
            local amount = OpenAmountAskWindow()
            if amount > data.current.amount then
              ESX.ShowNotification("~r~Quantité invalide")
              OpenVehicleGetInventory(plate, type, vehicleClass)
            end
            ESX.TriggerServerCallback("esx_vehicle_inventory:addItemInCompartment", function(success)
              if success == "not_enough_place" then
                ESX.ShowNotification("~r~Il n'y a plus assez de place")
              end
              OpenVehiclePutInventory(plate, type, vehicleClass)
            end, plate, data.current.name, amount, type, vehicleClass)
          else
            ESX.TriggerServerCallback("esx_vehicle_inventory:addWeaponInCompartment", function(success)
              if success == "not_enough_place" then
                ESX.ShowNotification("~r~Il n'y a plus assez de place")
              end
              OpenVehiclePutInventory(plate, type, vehicleClass)
            end, plate, data.current.name, type, vehicleClass)
          end
        end
      end,
      function(data, menu)
        menu.close()
        OpenVehicleMenu()
      end)
  end, plate, type)
end

Citizen.CreateThread(function()
  while true do
    if IsControlJustPressed(0, Keys["L"]) then
      OpenVehicleMenu()
    end
    Citizen.Wait(5)
  end
end)
