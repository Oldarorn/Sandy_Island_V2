ESX = nil
local arrayWeight = Config.localWeight

TriggerEvent('esx:getSharedObject', function(obj)
  ESX = obj
end)

ESX.RegisterServerCallback("esx_vehicle_inventory:getObjectsInCompartment", function(source, cb, plate, compartment)
  MySQL.Async.fetchAll("SELECT * FROM vehicle_inventory WHERE plate = @plate AND compartment = @compartment", 
    {
      ["@plate"] = plate,
      ["@compartment"] = compartment
    }, function(results)
      cb(results)
    end)
end)

ESX.RegisterServerCallback("esx_vehicle_inventory:addItemInCompartment", function(source, cb, plate, itemName, amount, compartment, vehicleClass)
  MySQL.Async.fetchAll("SELECT * FROM vehicle_inventory WHERE plate = @plate AND compartment = @compartment",
  {
    ["@plate"] = plate,
    ["@compartment"] = compartment
  }, function(results)
    local totalCount = 0
    for k,v in pairs(results) do
      totalCount = totalCount + v.count
    end
    if totalCount + amount > Config.CompartmentSize[compartment][vehicleClass + 1] then
      cb("not_enough_place")
      return
    end
    local founded = false
    local xPlayer = ESX.GetPlayerFromId(source)
    for k,v in pairs(results) do
      if v.itemName == itemName then
        founded = true
        MySQL.Async.execute("UPDATE vehicle_inventory SET count = @count WHERE plate = @plate AND compartment = @compartment AND itemName = @itemName",
        {
          ["@plate"] = plate,
          ["@compartment"] = compartment,
          ["@itemName"] = itemName,
          ["@count"] = v.count + amount
        })
        xPlayer.removeInventoryItem(itemName, amount)
        cb(true)
      end
    end
    if not founded then
      MySQL.Async.execute("INSERT INTO vehicle_inventory (plate, compartment, itemName, count, type) VALUES (@plate, @compartment, @itemName, @count, 'item')",
      {
        ["@plate"] = plate,
        ["@compartment"] = compartment,
        ["@itemName"] = itemName,
        ["@count"] = amount
      })
      xPlayer.removeInventoryItem(itemName, amount)
      cb(true)
    end
  end)
end)

ESX.RegisterServerCallback("esx_vehicle_inventory:addWeaponInCompartment", function(source, cb, plate, weaponName, compartment, vehicleClass)
  MySQL.Async.fetchAll("SELECT * FROM vehicle_inventory WHERE plate = @plate AND compartment = @compartment",
  {
    ["@plate"] = plate,
    ["@compartment"] = compartment
  }, function(results)
    local totalCount = 0
    local xPlayer = ESX.GetPlayerFromId(source)
    for k,v in pairs(results) do
      totalCount = totalCount + v.count
    end
    if totalCount + 1 > Config.CompartmentSize[compartment][vehicleClass + 1] then
      cb("not_enough_place")
      return
    end
    local founded = false
    for k,v in pairs(results) do
      if v.itemName == weaponName then
        founded = true
        MySQL.Async.execute("UPDATE vehicle_inventory SET count = @count WHERE plate = @plate AND compartment = @compartment AND itemName = @itemName",
        {
          ["@plate"] = plate,
          ["@compartment"] = compartment,
          ["@itemName"] = weaponName,
          ["@count"] = v.count + 1
        })
        xPlayer.removeWeapon(weaponName)
        cb(true)
      end
    end
    if not founded then
      MySQL.Async.execute("INSERT INTO vehicle_inventory (plate, compartment, itemName, count, type) VALUES (@plate, @compartment, @itemName, @count, 'weapon')",
      {
        ["@plate"] = plate,
        ["@compartment"] = compartment,
        ["@itemName"] = weaponName,
        ["@count"] = 1
      })
      xPlayer.removeWeapon(weaponName)
      cb(true)
    end
  end)
end)

ESX.RegisterServerCallback("esx_vehicle_inventory:removeItemInCompartment", function(source, cb, plate, itemName, amount, compartment)
  MySQL.Async.fetchAll("SELECT * FROM vehicle_inventory WHERE plate = @plate AND compartment = @compartment",
  {
    ["@plate"] = plate,
    ["@compartment"] = compartment
  }, function(results)
    for k,v in pairs(results) do
      if v.itemName == itemName then
        if v.count >= amount then
          MySQL.Async.execute("UPDATE vehicle_inventory SET count = @count WHERE plate = @plate AND compartment = @compartment AND itemName = @itemName",
          {
            ["@plate"] = plate,
            ["@compartment"] = compartment,
            ["@itemName"] = itemName,
            ["@count"] = v.count - amount
          })
          local xPlayer = ESX.GetPlayerFromId(source)
          xPlayer.addInventoryItem(itemName, amount)
          cb(true)
        elseif v.count < amount then
          TriggerClientEvent("esx:showNotification", source, "~r~Vous ne pouvez pas déposer plus que ce que vous avez")
          cb(false)
        end
      end
    end
  end)
end)

ESX.RegisterServerCallback("esx_vehicle_inventory:removeWeaponInCompartment", function(source, cb, plate, itemName, amount, compartment)
  MySQL.Async.fetchAll("SELECT * FROM vehicle_inventory WHERE plate = @plate AND compartment = @compartment",
  {
    ["@plate"] = plate,
    ["@compartment"] = compartment
  }, function(results)
    for k,v in pairs(results) do
      if v.itemName == itemName then
        MySQL.Async.execute("UPDATE vehicle_inventory SET count = @count WHERE plate = @plate AND compartment = @compartment AND itemName = @itemName",
        {
          ["@plate"] = plate,
          ["@compartment"] = compartment,
          ["@itemName"] = itemName,
          ["@count"] = v.count - 1
        })
        local xPlayer = ESX.GetPlayerFromId(source)
        xPlayer.addWeapon(itemName, 0)
        cb(true)
      end
    end
  end)
end)

ESX.RegisterServerCallback('esx_vehicle_inventory:getPlayerInventory', function(source, cb)

  local xPlayer = ESX.GetPlayerFromId(source)
  local items   = xPlayer.inventory

  cb({
    items = items
  })

end)