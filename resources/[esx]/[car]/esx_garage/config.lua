Config                      = {}
Config.DrawDistance         = 100.0
Config.MarkerType           = 27
Config.MarkerSize           = {x = 2.0, y = 2.0, z = 1.0}
Config.MarkerColor          = {r = 204, g = 204, b = 0}
Config.ParkingMarkerSize    = {x = 3.0, y = 3.0, z = 2.0}
Config.ParkingMarkerColor   = {r = 102, g = 102, b = 204}
Config.ZDiff                = 0.5
Config.EnableOwnedVehicles  = true
Config.MinimumHealthPercent = 0.0

Config.Locale = 'fr'

Config.Zones = {}

Config.Garages = {

	MiltonDrive = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x= 132.7794, y = -1083.6387, z = 28.199},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = 143.5144, y = -1082.408, z = 28.192},
			Heading = 0.0
		},

		InteriorSpawnPoint = {
			Pos     = {x = 228.930, y = -1000.698, z = -99.99},
			Heading = 0.0
		},

		InteriorExitPoint = {
			Pos = {x = 224.613, y = -1004.769, z = -99.99},
		},

		Parkings = {
			{
				Pos     = {x = 224.500, y = -998.695, z = -99.99},
				Heading = 225.0
			},
		  	{
				Pos     = {x = 224.500, y = -994.630, z = -99.99},
				Heading = 225.0
			},
		  	{
				Pos     = {x = 224.500, y = -990.255, z = -99.99},
				Heading = 225.0
			},
		  	{
				Pos     = {x = 224.500, y = -986.628, z = -99.99},
				Heading = 225.0
			},
		  	{
				Pos     = {x = 224.500, y = -982.496, z = -99.99},
				Heading = 225.0
			},
		  	{
				Pos     = {x = 232.500, y = -982.496, z = -99.99},
				Heading = 135.0
			},
		  	{
				Pos     = {x = 232.500, y = -986.628, z = -99.99},
				Heading = 135.0
			},
		  	{
				Pos     = {x = 232.500, y = -990.255, z = -99.99},
				Heading = 135.0
			},
		  	{
				Pos     = {x = 232.500, y = -994.630, z = -99.99},
				Heading = 135.0
			},
		  	{
				Pos     = {x = 232.500, y = -998.695, z = -99.99},
				Heading = 135.0
			},
		}
	},
	Paleto = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x= -355.2535, y = 6067.4087, z = 30.499},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = -358.542, y = 6070.96972, z = 31.198},
			Heading = 0.0
		},

		InteriorSpawnPoint = {
			Pos     = {x = 198.3893, y = -1004.69189, z = -99.4},
			Heading = 0.0
		},

		InteriorExitPoint = {
			Pos = {x = 194.605331, y = -1007.0377, z = -99.8},
		},

		Parkings = {
			--{
			--	Pos     = {x = 193.3718, y = -1001.447, z = -99.9217},
			--	Heading = 180.0
			--},
		 	{
				Pos     = {x = 192.675, y = -996.1777, z = -99.9217},
				Heading = -135.0
			},
		    {
				Pos     = {x = 197.099, y = -996.173, z = -99.9217},
				Heading = 180.0
			},
		    {
				Pos     = {x = 200.258, y = -996.187, z = -99.9217},
				Heading = 180.0
			},
		    --{
			--	Pos     = {x = 224.500, y = -982.496, z = -99.9217},
			--	Heading = 180.0
			--},
		    {
				Pos     = {x = 204.3808, y = -996.8317, z = -99.9217},
				Heading = 135.0
			},
		    {
				Pos     = {x = 205.149, y = -1002.254, z = -99.9217},
				Heading = 180.0
			},
		}
	},
	Sandy = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x = 1695.2197, y = 3610.6238, z = 35.3227},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = 1692.0439, y = 3607.3105, z = 35.37},
			Heading = 0.0
		},

		InteriorSpawnPoint = {
			Pos     = {x = 172.900, y = -1007.434, z = -99.999},
			Heading = 0.0
		},

		InteriorExitPoint = {
			Pos = {x = 170.536, y = -1007.209, z = -99.999},
		},

		Parkings = {
			{
				Pos     = {x = 173.358, y = -1003.489, z = -99.999},
				Heading = 180.0
			}
		}
	},
	Aeroport = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x = 1695.2197, y = 3610.6238, z = 35.3227},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = 1692.0439, y = 3607.3105, z = 35.37},
			Heading = 0.0
		},

		InteriorSpawnPoint = {
			Pos     = {x = 172.900, y = -1007.434, z = -99.999},
			Heading = 0.0
		},

		InteriorExitPoint = {
			Pos = {x = 170.536, y = -1007.209, z = -99.999},
		},

		Parkings = {
			{
				Pos     = {x = 173.358, y = -1003.489, z = -99.999},
				Heading = 180.0
			},
			{
				Pos     = {x = 173.358, y = -1003.489, z = -99.999},
				Heading = 180.0
			},
		}
	}
}
