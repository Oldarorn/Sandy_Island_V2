USE `essentialmode`;

INSERT INTO `licenses` (`type`, `label`) VALUES
	('drive', 'Permis de conduire'),
	('drive_bike', 'Permis moto'),
	('drive_truck', 'Permis camion')
;